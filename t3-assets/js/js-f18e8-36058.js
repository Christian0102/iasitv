

/*===============================
/media/sourcecoast/js/jq-bootstrap-1.8.3.js
================================================================================*/;
/*!
 * jQuery JavaScript Library v1.8.3
 * http://jquery.com/
 *
 * Includes Sizzle.js
 * http://sizzlejs.com/
 *
 * Copyright 2012 jQuery Foundation and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: Tue Nov 13 2012 08:20:33 GMT-0500 (Eastern Standard Time)
 */
(function(window,undefined)
{var
rootjQuery,readyList,document=window.document,location=window.location,navigator=window.navigator,_jQuery=window.jQuery,_$=window.$,core_push=Array.prototype.push,core_slice=Array.prototype.slice,core_indexOf=Array.prototype.indexOf,core_toString=Object.prototype.toString,core_hasOwn=Object.prototype.hasOwnProperty,core_trim=String.prototype.trim,jQuery=function(selector,context)
{return new jQuery.fn.init(selector,context,rootjQuery);},core_pnum=/[\-+]?(?:\d*\.|)\d+(?:[eE][\-+]?\d+|)/.source,core_rnotwhite=/\S/,core_rspace=/\s+/,rtrim=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,rquickExpr=/^(?:[^#<]*(<[\w\W]+>)[^>]*$|#([\w\-]*)$)/,rsingleTag=/^<(\w+)\s*\/?>(?:<\/\1>|)$/,rvalidchars=/^[\],:{}\s]*$/,rvalidbraces=/(?:^|:|,)(?:\s*\[)+/g,rvalidescape=/\\(?:["\\\/bfnrt]|u[\da-fA-F]{4})/g,rvalidtokens=/"[^"\\\r\n]*"|true|false|null|-?(?:\d\d*\.|)\d+(?:[eE][\-+]?\d+|)/g,rmsPrefix=/^-ms-/,rdashAlpha=/-([\da-z])/gi,fcamelCase=function(all,letter)
{return(letter+"").toUpperCase();},DOMContentLoaded=function()
{if(document.addEventListener)
{document.removeEventListener("DOMContentLoaded",DOMContentLoaded,false);jQuery.ready();}else if(document.readyState==="complete")
{document.detachEvent("onreadystatechange",DOMContentLoaded);jQuery.ready();}},class2type={};jQuery.fn=jQuery.prototype={constructor:jQuery,init:function(selector,context,rootjQuery)
{var match,elem,ret,doc;if(!selector)
{return this;}
if(selector.nodeType)
{this.context=this[0]=selector;this.length=1;return this;}
if(typeof selector==="string")
{if(selector.charAt(0)==="<"&&selector.charAt(selector.length-1)===">"&&selector.length>=3)
{match=[null,selector,null];}else
{match=rquickExpr.exec(selector);}
if(match&&(match[1]||!context))
{if(match[1])
{context=context instanceof jQuery?context[0]:context;doc=(context&&context.nodeType?context.ownerDocument||context:document);selector=jQuery.parseHTML(match[1],doc,true);if(rsingleTag.test(match[1])&&jQuery.isPlainObject(context))
{this.attr.call(selector,context,true);}
return jQuery.merge(this,selector);}else
{elem=document.getElementById(match[2]);if(elem&&elem.parentNode)
{if(elem.id!==match[2])
{return rootjQuery.find(selector);}
this.length=1;this[0]=elem;}
this.context=document;this.selector=selector;return this;}}else if(!context||context.jquery)
{return(context||rootjQuery).find(selector);}else
{return this.constructor(context).find(selector);}}else if(jQuery.isFunction(selector))
{return rootjQuery.ready(selector);}
if(selector.selector!==undefined)
{this.selector=selector.selector;this.context=selector.context;}
return jQuery.makeArray(selector,this);},selector:"",jquery:"1.8.3",length:0,size:function()
{return this.length;},toArray:function()
{return core_slice.call(this);},get:function(num)
{return num==null?this.toArray():(num<0?this[this.length+num]:this[num]);},pushStack:function(elems,name,selector)
{var ret=jQuery.merge(this.constructor(),elems);ret.prevObject=this;ret.context=this.context;if(name==="find")
{ret.selector=this.selector+(this.selector?" ":"")+selector;}else if(name)
{ret.selector=this.selector+"."+name+"("+selector+")";}
return ret;},each:function(callback,args)
{return jQuery.each(this,callback,args);},ready:function(fn)
{jQuery.ready.promise().done(fn);return this;},eq:function(i)
{i=+i;return i===-1?this.slice(i):this.slice(i,i+1);},first:function()
{return this.eq(0);},last:function()
{return this.eq(-1);},slice:function()
{return this.pushStack(core_slice.apply(this,arguments),"slice",core_slice.call(arguments).join(","));},map:function(callback)
{return this.pushStack(jQuery.map(this,function(elem,i)
{return callback.call(elem,i,elem);}));},end:function()
{return this.prevObject||this.constructor(null);},push:core_push,sort:[].sort,splice:[].splice};jQuery.fn.init.prototype=jQuery.fn;jQuery.extend=jQuery.fn.extend=function()
{var options,name,src,copy,copyIsArray,clone,target=arguments[0]||{},i=1,length=arguments.length,deep=false;if(typeof target==="boolean")
{deep=target;target=arguments[1]||{};i=2;}
if(typeof target!=="object"&&!jQuery.isFunction(target))
{target={};}
if(length===i)
{target=this;--i;}
for(;i<length;i++)
{if((options=arguments[i])!=null)
{for(name in options)
{src=target[name];copy=options[name];if(target===copy)
{continue;}
if(deep&&copy&&(jQuery.isPlainObject(copy)||(copyIsArray=jQuery.isArray(copy))))
{if(copyIsArray)
{copyIsArray=false;clone=src&&jQuery.isArray(src)?src:[];}else
{clone=src&&jQuery.isPlainObject(src)?src:{};}
target[name]=jQuery.extend(deep,clone,copy);}else if(copy!==undefined)
{target[name]=copy;}}}}
return target;};jQuery.extend({noConflict:function(deep)
{if(window.$===jQuery)
{window.$=_$;}
if(deep&&window.jQuery===jQuery)
{window.jQuery=_jQuery;}
return jQuery;},isReady:false,readyWait:1,holdReady:function(hold)
{if(hold)
{jQuery.readyWait++;}else
{jQuery.ready(true);}},ready:function(wait)
{if(wait===true?--jQuery.readyWait:jQuery.isReady)
{return;}
if(!document.body)
{return setTimeout(jQuery.ready,1);}
jQuery.isReady=true;if(wait!==true&&--jQuery.readyWait>0)
{return;}
readyList.resolveWith(document,[jQuery]);if(jQuery.fn.trigger)
{jQuery(document).trigger("ready").off("ready");}},isFunction:function(obj)
{return jQuery.type(obj)==="function";},isArray:Array.isArray||function(obj)
{return jQuery.type(obj)==="array";},isWindow:function(obj)
{return obj!=null&&obj==obj.window;},isNumeric:function(obj)
{return!isNaN(parseFloat(obj))&&isFinite(obj);},type:function(obj)
{return obj==null?String(obj):class2type[core_toString.call(obj)]||"object";},isPlainObject:function(obj)
{if(!obj||jQuery.type(obj)!=="object"||obj.nodeType||jQuery.isWindow(obj))
{return false;}
try
{if(obj.constructor&&!core_hasOwn.call(obj,"constructor")&&!core_hasOwn.call(obj.constructor.prototype,"isPrototypeOf"))
{return false;}}catch(e)
{return false;}
var key;for(key in obj)
{}
return key===undefined||core_hasOwn.call(obj,key);},isEmptyObject:function(obj)
{var name;for(name in obj)
{return false;}
return true;},error:function(msg)
{throw new Error(msg);},parseHTML:function(data,context,scripts)
{var parsed;if(!data||typeof data!=="string")
{return null;}
if(typeof context==="boolean")
{scripts=context;context=0;}
context=context||document;if((parsed=rsingleTag.exec(data)))
{return[context.createElement(parsed[1])];}
parsed=jQuery.buildFragment([data],context,scripts?null:[]);return jQuery.merge([],(parsed.cacheable?jQuery.clone(parsed.fragment):parsed.fragment).childNodes);},parseJSON:function(data)
{if(!data||typeof data!=="string")
{return null;}
data=jQuery.trim(data);if(window.JSON&&window.JSON.parse)
{return window.JSON.parse(data);}
if(rvalidchars.test(data.replace(rvalidescape,"@").replace(rvalidtokens,"]").replace(rvalidbraces,"")))
{return(new Function("return "+data))();}
jQuery.error("Invalid JSON: "+data);},parseXML:function(data)
{var xml,tmp;if(!data||typeof data!=="string")
{return null;}
try
{if(window.DOMParser)
{tmp=new DOMParser();xml=tmp.parseFromString(data,"text/xml");}else
{xml=new ActiveXObject("Microsoft.XMLDOM");xml.async="false";xml.loadXML(data);}}catch(e)
{xml=undefined;}
if(!xml||!xml.documentElement||xml.getElementsByTagName("parsererror").length)
{jQuery.error("Invalid XML: "+data);}
return xml;},noop:function()
{},globalEval:function(data)
{if(data&&core_rnotwhite.test(data))
{(window.execScript||function(data)
{window["eval"].call(window,data);})(data);}},camelCase:function(string)
{return string.replace(rmsPrefix,"ms-").replace(rdashAlpha,fcamelCase);},nodeName:function(elem,name)
{return elem.nodeName&&elem.nodeName.toLowerCase()===name.toLowerCase();},each:function(obj,callback,args)
{var name,i=0,length=obj.length,isObj=length===undefined||jQuery.isFunction(obj);if(args)
{if(isObj)
{for(name in obj)
{if(callback.apply(obj[name],args)===false)
{break;}}}else
{for(;i<length;)
{if(callback.apply(obj[i++],args)===false)
{break;}}}}else
{if(isObj)
{for(name in obj)
{if(callback.call(obj[name],name,obj[name])===false)
{break;}}}else
{for(;i<length;)
{if(callback.call(obj[i],i,obj[i++])===false)
{break;}}}}
return obj;},trim:core_trim&&!core_trim.call("\uFEFF\xA0")?function(text)
{return text==null?"":core_trim.call(text);}:function(text)
{return text==null?"":(text+"").replace(rtrim,"");},makeArray:function(arr,results)
{var type,ret=results||[];if(arr!=null)
{type=jQuery.type(arr);if(arr.length==null||type==="string"||type==="function"||type==="regexp"||jQuery.isWindow(arr))
{core_push.call(ret,arr);}else
{jQuery.merge(ret,arr);}}
return ret;},inArray:function(elem,arr,i)
{var len;if(arr)
{if(core_indexOf)
{return core_indexOf.call(arr,elem,i);}
len=arr.length;i=i?i<0?Math.max(0,len+i):i:0;for(;i<len;i++)
{if(i in arr&&arr[i]===elem)
{return i;}}}
return-1;},merge:function(first,second)
{var l=second.length,i=first.length,j=0;if(typeof l==="number")
{for(;j<l;j++)
{first[i++]=second[j];}}else
{while(second[j]!==undefined)
{first[i++]=second[j++];}}
first.length=i;return first;},grep:function(elems,callback,inv)
{var retVal,ret=[],i=0,length=elems.length;inv=!!inv;for(;i<length;i++)
{retVal=!!callback(elems[i],i);if(inv!==retVal)
{ret.push(elems[i]);}}
return ret;},map:function(elems,callback,arg)
{var value,key,ret=[],i=0,length=elems.length,isArray=elems instanceof jQuery||length!==undefined&&typeof length==="number"&&((length>0&&elems[0]&&elems[length-1])||length===0||jQuery.isArray(elems));if(isArray)
{for(;i<length;i++)
{value=callback(elems[i],i,arg);if(value!=null)
{ret[ret.length]=value;}}}else
{for(key in elems)
{value=callback(elems[key],key,arg);if(value!=null)
{ret[ret.length]=value;}}}
return ret.concat.apply([],ret);},guid:1,proxy:function(fn,context)
{var tmp,args,proxy;if(typeof context==="string")
{tmp=fn[context];context=fn;fn=tmp;}
if(!jQuery.isFunction(fn))
{return undefined;}
args=core_slice.call(arguments,2);proxy=function()
{return fn.apply(context,args.concat(core_slice.call(arguments)));};proxy.guid=fn.guid=fn.guid||jQuery.guid++;return proxy;},access:function(elems,fn,key,value,chainable,emptyGet,pass)
{var exec,bulk=key==null,i=0,length=elems.length;if(key&&typeof key==="object")
{for(i in key)
{jQuery.access(elems,fn,i,key[i],1,emptyGet,value);}
chainable=1;}else if(value!==undefined)
{exec=pass===undefined&&jQuery.isFunction(value);if(bulk)
{if(exec)
{exec=fn;fn=function(elem,key,value)
{return exec.call(jQuery(elem),value);};}else
{fn.call(elems,value);fn=null;}}
if(fn)
{for(;i<length;i++)
{fn(elems[i],key,exec?value.call(elems[i],i,fn(elems[i],key)):value,pass);}}
chainable=1;}
return chainable?elems:bulk?fn.call(elems):length?fn(elems[0],key):emptyGet;},now:function()
{return(new Date()).getTime();}});jQuery.ready.promise=function(obj)
{if(!readyList)
{readyList=jQuery.Deferred();if(document.readyState==="complete")
{setTimeout(jQuery.ready,1);}else if(document.addEventListener)
{document.addEventListener("DOMContentLoaded",DOMContentLoaded,false);window.addEventListener("load",jQuery.ready,false);}else
{document.attachEvent("onreadystatechange",DOMContentLoaded);window.attachEvent("onload",jQuery.ready);var top=false;try
{top=window.frameElement==null&&document.documentElement;}catch(e)
{}
if(top&&top.doScroll)
{(function doScrollCheck()
{if(!jQuery.isReady)
{try
{top.doScroll("left");}catch(e)
{return setTimeout(doScrollCheck,50);}
jQuery.ready();}})();}}}
return readyList.promise(obj);};jQuery.each("Boolean Number String Function Array Date RegExp Object".split(" "),function(i,name)
{class2type["[object "+name+"]"]=name.toLowerCase();});rootjQuery=jQuery(document);var optionsCache={};function createOptions(options)
{var object=optionsCache[options]={};jQuery.each(options.split(core_rspace),function(_,flag)
{object[flag]=true;});return object;}
jQuery.Callbacks=function(options)
{options=typeof options==="string"?(optionsCache[options]||createOptions(options)):jQuery.extend({},options);var
memory,fired,firing,firingStart,firingLength,firingIndex,list=[],stack=!options.once&&[],fire=function(data)
{memory=options.memory&&data;fired=true;firingIndex=firingStart||0;firingStart=0;firingLength=list.length;firing=true;for(;list&&firingIndex<firingLength;firingIndex++)
{if(list[firingIndex].apply(data[0],data[1])===false&&options.stopOnFalse)
{memory=false;break;}}
firing=false;if(list)
{if(stack)
{if(stack.length)
{fire(stack.shift());}}else if(memory)
{list=[];}else
{self.disable();}}},self={add:function()
{if(list)
{var start=list.length;(function add(args)
{jQuery.each(args,function(_,arg)
{var type=jQuery.type(arg);if(type==="function")
{if(!options.unique||!self.has(arg))
{list.push(arg);}}else if(arg&&arg.length&&type!=="string")
{add(arg);}});})(arguments);if(firing)
{firingLength=list.length;}else if(memory)
{firingStart=start;fire(memory);}}
return this;},remove:function()
{if(list)
{jQuery.each(arguments,function(_,arg)
{var index;while((index=jQuery.inArray(arg,list,index))>-1)
{list.splice(index,1);if(firing)
{if(index<=firingLength)
{firingLength--;}
if(index<=firingIndex)
{firingIndex--;}}}});}
return this;},has:function(fn)
{return jQuery.inArray(fn,list)>-1;},empty:function()
{list=[];return this;},disable:function()
{list=stack=memory=undefined;return this;},disabled:function()
{return!list;},lock:function()
{stack=undefined;if(!memory)
{self.disable();}
return this;},locked:function()
{return!stack;},fireWith:function(context,args)
{args=args||[];args=[context,args.slice?args.slice():args];if(list&&(!fired||stack))
{if(firing)
{stack.push(args);}else
{fire(args);}}
return this;},fire:function()
{self.fireWith(this,arguments);return this;},fired:function()
{return!!fired;}};return self;};jQuery.extend({Deferred:function(func)
{var tuples=[["resolve","done",jQuery.Callbacks("once memory"),"resolved"],["reject","fail",jQuery.Callbacks("once memory"),"rejected"],["notify","progress",jQuery.Callbacks("memory")]],state="pending",promise={state:function()
{return state;},always:function()
{deferred.done(arguments).fail(arguments);return this;},then:function()
{var fns=arguments;return jQuery.Deferred(function(newDefer)
{jQuery.each(tuples,function(i,tuple)
{var action=tuple[0],fn=fns[i];deferred[tuple[1]](jQuery.isFunction(fn)?function()
{var returned=fn.apply(this,arguments);if(returned&&jQuery.isFunction(returned.promise))
{returned.promise().done(newDefer.resolve).fail(newDefer.reject).progress(newDefer.notify);}else
{newDefer[action+"With"](this===deferred?newDefer:this,[returned]);}}:newDefer[action]);});fns=null;}).promise();},promise:function(obj)
{return obj!=null?jQuery.extend(obj,promise):promise;}},deferred={};promise.pipe=promise.then;jQuery.each(tuples,function(i,tuple)
{var list=tuple[2],stateString=tuple[3];promise[tuple[1]]=list.add;if(stateString)
{list.add(function()
{state=stateString;},tuples[i^1][2].disable,tuples[2][2].lock);}
deferred[tuple[0]]=list.fire;deferred[tuple[0]+"With"]=list.fireWith;});promise.promise(deferred);if(func)
{func.call(deferred,deferred);}
return deferred;},when:function(subordinate)
{var i=0,resolveValues=core_slice.call(arguments),length=resolveValues.length,remaining=length!==1||(subordinate&&jQuery.isFunction(subordinate.promise))?length:0,deferred=remaining===1?subordinate:jQuery.Deferred(),updateFunc=function(i,contexts,values)
{return function(value)
{contexts[i]=this;values[i]=arguments.length>1?core_slice.call(arguments):value;if(values===progressValues)
{deferred.notifyWith(contexts,values);}else if(!(--remaining))
{deferred.resolveWith(contexts,values);}};},progressValues,progressContexts,resolveContexts;if(length>1)
{progressValues=new Array(length);progressContexts=new Array(length);resolveContexts=new Array(length);for(;i<length;i++)
{if(resolveValues[i]&&jQuery.isFunction(resolveValues[i].promise))
{resolveValues[i].promise().done(updateFunc(i,resolveContexts,resolveValues)).fail(deferred.reject).progress(updateFunc(i,progressContexts,progressValues));}else
{--remaining;}}}
if(!remaining)
{deferred.resolveWith(resolveContexts,resolveValues);}
return deferred.promise();}});jQuery.support=(function()
{var support,all,a,select,opt,input,fragment,eventName,i,isSupported,clickFn,div=document.createElement("div");div.setAttribute("className","t");div.innerHTML="  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>";all=div.getElementsByTagName("*");a=div.getElementsByTagName("a")[0];if(!all||!a||!all.length)
{return{};}
select=document.createElement("select");opt=select.appendChild(document.createElement("option"));input=div.getElementsByTagName("input")[0];a.style.cssText="top:1px;float:left;opacity:.5";support={leadingWhitespace:(div.firstChild.nodeType===3),tbody:!div.getElementsByTagName("tbody").length,htmlSerialize:!!div.getElementsByTagName("link").length,style:/top/.test(a.getAttribute("style")),hrefNormalized:(a.getAttribute("href")==="/a"),opacity:/^0.5/.test(a.style.opacity),cssFloat:!!a.style.cssFloat,checkOn:(input.value==="on"),optSelected:opt.selected,getSetAttribute:div.className!=="t",enctype:!!document.createElement("form").enctype,html5Clone:document.createElement("nav").cloneNode(true).outerHTML!=="<:nav></:nav>",boxModel:(document.compatMode==="CSS1Compat"),submitBubbles:true,changeBubbles:true,focusinBubbles:false,deleteExpando:true,noCloneEvent:true,inlineBlockNeedsLayout:false,shrinkWrapBlocks:false,reliableMarginRight:true,boxSizingReliable:true,pixelPosition:false};input.checked=true;support.noCloneChecked=input.cloneNode(true).checked;select.disabled=true;support.optDisabled=!opt.disabled;try
{delete div.test;}catch(e)
{support.deleteExpando=false;}
if(!div.addEventListener&&div.attachEvent&&div.fireEvent)
{div.attachEvent("onclick",clickFn=function()
{support.noCloneEvent=false;});div.cloneNode(true).fireEvent("onclick");div.detachEvent("onclick",clickFn);}
input=document.createElement("input");input.value="t";input.setAttribute("type","radio");support.radioValue=input.value==="t";input.setAttribute("checked","checked");input.setAttribute("name","t");div.appendChild(input);fragment=document.createDocumentFragment();fragment.appendChild(div.lastChild);support.checkClone=fragment.cloneNode(true).cloneNode(true).lastChild.checked;support.appendChecked=input.checked;fragment.removeChild(input);fragment.appendChild(div);if(div.attachEvent)
{for(i in{submit:true,change:true,focusin:true})
{eventName="on"+i;isSupported=(eventName in div);if(!isSupported)
{div.setAttribute(eventName,"return;");isSupported=(typeof div[eventName]==="function");}
support[i+"Bubbles"]=isSupported;}}
jQuery(function()
{var container,div,tds,marginDiv,divReset="padding:0;margin:0;border:0;display:block;overflow:hidden;",body=document.getElementsByTagName("body")[0];if(!body)
{return;}
container=document.createElement("div");container.style.cssText="visibility:hidden;border:0;width:0;height:0;position:static;top:0;margin-top:1px";body.insertBefore(container,body.firstChild);div=document.createElement("div");container.appendChild(div);div.innerHTML="<table><tr><td></td><td>t</td></tr></table>";tds=div.getElementsByTagName("td");tds[0].style.cssText="padding:0;margin:0;border:0;display:none";isSupported=(tds[0].offsetHeight===0);tds[0].style.display="";tds[1].style.display="none";support.reliableHiddenOffsets=isSupported&&(tds[0].offsetHeight===0);div.innerHTML="";div.style.cssText="box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding:1px;border:1px;display:block;width:4px;margin-top:1%;position:absolute;top:1%;";support.boxSizing=(div.offsetWidth===4);support.doesNotIncludeMarginInBodyOffset=(body.offsetTop!==1);if(window.getComputedStyle)
{support.pixelPosition=(window.getComputedStyle(div,null)||{}).top!=="1%";support.boxSizingReliable=(window.getComputedStyle(div,null)||{width:"4px"}).width==="4px";marginDiv=document.createElement("div");marginDiv.style.cssText=div.style.cssText=divReset;marginDiv.style.marginRight=marginDiv.style.width="0";div.style.width="1px";div.appendChild(marginDiv);support.reliableMarginRight=!parseFloat((window.getComputedStyle(marginDiv,null)||{}).marginRight);}
if(typeof div.style.zoom!=="undefined")
{div.innerHTML="";div.style.cssText=divReset+"width:1px;padding:1px;display:inline;zoom:1";support.inlineBlockNeedsLayout=(div.offsetWidth===3);div.style.display="block";div.style.overflow="visible";div.innerHTML="<div></div>";div.firstChild.style.width="5px";support.shrinkWrapBlocks=(div.offsetWidth!==3);container.style.zoom=1;}
body.removeChild(container);container=div=tds=marginDiv=null;});fragment.removeChild(div);all=a=select=opt=input=fragment=div=null;return support;})();var rbrace=/(?:\{[\s\S]*\}|\[[\s\S]*\])$/,rmultiDash=/([A-Z])/g;jQuery.extend({cache:{},deletedIds:[],uuid:0,expando:"jQuery"+(jQuery.fn.jquery+Math.random()).replace(/\D/g,""),noData:{"embed":true,"object":"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000","applet":true},hasData:function(elem)
{elem=elem.nodeType?jQuery.cache[elem[jQuery.expando]]:elem[jQuery.expando];return!!elem&&!isEmptyDataObject(elem);},data:function(elem,name,data,pvt)
{if(!jQuery.acceptData(elem))
{return;}
var thisCache,ret,internalKey=jQuery.expando,getByName=typeof name==="string",isNode=elem.nodeType,cache=isNode?jQuery.cache:elem,id=isNode?elem[internalKey]:elem[internalKey]&&internalKey;if((!id||!cache[id]||(!pvt&&!cache[id].data))&&getByName&&data===undefined)
{return;}
if(!id)
{if(isNode)
{elem[internalKey]=id=jQuery.deletedIds.pop()||jQuery.guid++;}else
{id=internalKey;}}
if(!cache[id])
{cache[id]={};if(!isNode)
{cache[id].toJSON=jQuery.noop;}}
if(typeof name==="object"||typeof name==="function")
{if(pvt)
{cache[id]=jQuery.extend(cache[id],name);}else
{cache[id].data=jQuery.extend(cache[id].data,name);}}
thisCache=cache[id];if(!pvt)
{if(!thisCache.data)
{thisCache.data={};}
thisCache=thisCache.data;}
if(data!==undefined)
{thisCache[jQuery.camelCase(name)]=data;}
if(getByName)
{ret=thisCache[name];if(ret==null)
{ret=thisCache[jQuery.camelCase(name)];}}else
{ret=thisCache;}
return ret;},removeData:function(elem,name,pvt)
{if(!jQuery.acceptData(elem))
{return;}
var thisCache,i,l,isNode=elem.nodeType,cache=isNode?jQuery.cache:elem,id=isNode?elem[jQuery.expando]:jQuery.expando;if(!cache[id])
{return;}
if(name)
{thisCache=pvt?cache[id]:cache[id].data;if(thisCache)
{if(!jQuery.isArray(name))
{if(name in thisCache)
{name=[name];}else
{name=jQuery.camelCase(name);if(name in thisCache)
{name=[name];}else
{name=name.split(" ");}}}
for(i=0,l=name.length;i<l;i++)
{delete thisCache[name[i]];}
if(!(pvt?isEmptyDataObject:jQuery.isEmptyObject)(thisCache))
{return;}}}
if(!pvt)
{delete cache[id].data;if(!isEmptyDataObject(cache[id]))
{return;}}
if(isNode)
{jQuery.cleanData([elem],true);}else if(jQuery.support.deleteExpando||cache!=cache.window)
{delete cache[id];}else
{cache[id]=null;}},_data:function(elem,name,data)
{return jQuery.data(elem,name,data,true);},acceptData:function(elem)
{var noData=elem.nodeName&&jQuery.noData[elem.nodeName.toLowerCase()];return!noData||noData!==true&&elem.getAttribute("classid")===noData;}});jQuery.fn.extend({data:function(key,value)
{var parts,part,attr,name,l,elem=this[0],i=0,data=null;if(key===undefined)
{if(this.length)
{data=jQuery.data(elem);if(elem.nodeType===1&&!jQuery._data(elem,"parsedAttrs"))
{attr=elem.attributes;for(l=attr.length;i<l;i++)
{name=attr[i].name;if(!name.indexOf("data-"))
{name=jQuery.camelCase(name.substring(5));dataAttr(elem,name,data[name]);}}
jQuery._data(elem,"parsedAttrs",true);}}
return data;}
if(typeof key==="object")
{return this.each(function()
{jQuery.data(this,key);});}
parts=key.split(".",2);parts[1]=parts[1]?"."+parts[1]:"";part=parts[1]+"!";return jQuery.access(this,function(value)
{if(value===undefined)
{data=this.triggerHandler("getData"+part,[parts[0]]);if(data===undefined&&elem)
{data=jQuery.data(elem,key);data=dataAttr(elem,key,data);}
return data===undefined&&parts[1]?this.data(parts[0]):data;}
parts[1]=value;this.each(function()
{var self=jQuery(this);self.triggerHandler("setData"+part,parts);jQuery.data(this,key,value);self.triggerHandler("changeData"+part,parts);});},null,value,arguments.length>1,null,false);},removeData:function(key)
{return this.each(function()
{jQuery.removeData(this,key);});}});function dataAttr(elem,key,data)
{if(data===undefined&&elem.nodeType===1)
{var name="data-"+key.replace(rmultiDash,"-$1").toLowerCase();data=elem.getAttribute(name);if(typeof data==="string")
{try
{data=data==="true"?true:data==="false"?false:data==="null"?null:+data+""===data?+data:rbrace.test(data)?jQuery.parseJSON(data):data;}catch(e)
{}
jQuery.data(elem,key,data);}else
{data=undefined;}}
return data;}
function isEmptyDataObject(obj)
{var name;for(name in obj)
{if(name==="data"&&jQuery.isEmptyObject(obj[name]))
{continue;}
if(name!=="toJSON")
{return false;}}
return true;}
jQuery.extend({queue:function(elem,type,data)
{var queue;if(elem)
{type=(type||"fx")+"queue";queue=jQuery._data(elem,type);if(data)
{if(!queue||jQuery.isArray(data))
{queue=jQuery._data(elem,type,jQuery.makeArray(data));}else
{queue.push(data);}}
return queue||[];}},dequeue:function(elem,type)
{type=type||"fx";var queue=jQuery.queue(elem,type),startLength=queue.length,fn=queue.shift(),hooks=jQuery._queueHooks(elem,type),next=function()
{jQuery.dequeue(elem,type);};if(fn==="inprogress")
{fn=queue.shift();startLength--;}
if(fn)
{if(type==="fx")
{queue.unshift("inprogress");}
delete hooks.stop;fn.call(elem,next,hooks);}
if(!startLength&&hooks)
{hooks.empty.fire();}},_queueHooks:function(elem,type)
{var key=type+"queueHooks";return jQuery._data(elem,key)||jQuery._data(elem,key,{empty:jQuery.Callbacks("once memory").add(function()
{jQuery.removeData(elem,type+"queue",true);jQuery.removeData(elem,key,true);})});}});jQuery.fn.extend({queue:function(type,data)
{var setter=2;if(typeof type!=="string")
{data=type;type="fx";setter--;}
if(arguments.length<setter)
{return jQuery.queue(this[0],type);}
return data===undefined?this:this.each(function()
{var queue=jQuery.queue(this,type,data);jQuery._queueHooks(this,type);if(type==="fx"&&queue[0]!=="inprogress")
{jQuery.dequeue(this,type);}});},dequeue:function(type)
{return this.each(function()
{jQuery.dequeue(this,type);});},delay:function(time,type)
{time=jQuery.fx?jQuery.fx.speeds[time]||time:time;type=type||"fx";return this.queue(type,function(next,hooks)
{var timeout=setTimeout(next,time);hooks.stop=function()
{clearTimeout(timeout);};});},clearQueue:function(type)
{return this.queue(type||"fx",[]);},promise:function(type,obj)
{var tmp,count=1,defer=jQuery.Deferred(),elements=this,i=this.length,resolve=function()
{if(!(--count))
{defer.resolveWith(elements,[elements]);}};if(typeof type!=="string")
{obj=type;type=undefined;}
type=type||"fx";while(i--)
{tmp=jQuery._data(elements[i],type+"queueHooks");if(tmp&&tmp.empty)
{count++;tmp.empty.add(resolve);}}
resolve();return defer.promise(obj);}});var nodeHook,boolHook,fixSpecified,rclass=/[\t\r\n]/g,rreturn=/\r/g,rtype=/^(?:button|input)$/i,rfocusable=/^(?:button|input|object|select|textarea)$/i,rclickable=/^a(?:rea|)$/i,rboolean=/^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i,getSetAttribute=jQuery.support.getSetAttribute;jQuery.fn.extend({attr:function(name,value)
{return jQuery.access(this,jQuery.attr,name,value,arguments.length>1);},removeAttr:function(name)
{return this.each(function()
{jQuery.removeAttr(this,name);});},prop:function(name,value)
{return jQuery.access(this,jQuery.prop,name,value,arguments.length>1);},removeProp:function(name)
{name=jQuery.propFix[name]||name;return this.each(function()
{try
{this[name]=undefined;delete this[name];}catch(e)
{}});},addClass:function(value)
{var classNames,i,l,elem,setClass,c,cl;if(jQuery.isFunction(value))
{return this.each(function(j)
{jQuery(this).addClass(value.call(this,j,this.className));});}
if(value&&typeof value==="string")
{classNames=value.split(core_rspace);for(i=0,l=this.length;i<l;i++)
{elem=this[i];if(elem.nodeType===1)
{if(!elem.className&&classNames.length===1)
{elem.className=value;}else
{setClass=" "+elem.className+" ";for(c=0,cl=classNames.length;c<cl;c++)
{if(setClass.indexOf(" "+classNames[c]+" ")<0)
{setClass+=classNames[c]+" ";}}
elem.className=jQuery.trim(setClass);}}}}
return this;},removeClass:function(value)
{var removes,className,elem,c,cl,i,l;if(jQuery.isFunction(value))
{return this.each(function(j)
{jQuery(this).removeClass(value.call(this,j,this.className));});}
if((value&&typeof value==="string")||value===undefined)
{removes=(value||"").split(core_rspace);for(i=0,l=this.length;i<l;i++)
{elem=this[i];if(elem.nodeType===1&&elem.className)
{className=(" "+elem.className+" ").replace(rclass," ");for(c=0,cl=removes.length;c<cl;c++)
{while(className.indexOf(" "+removes[c]+" ")>=0)
{className=className.replace(" "+removes[c]+" "," ");}}
elem.className=value?jQuery.trim(className):"";}}}
return this;},toggleClass:function(value,stateVal)
{var type=typeof value,isBool=typeof stateVal==="boolean";if(jQuery.isFunction(value))
{return this.each(function(i)
{jQuery(this).toggleClass(value.call(this,i,this.className,stateVal),stateVal);});}
return this.each(function()
{if(type==="string")
{var className,i=0,self=jQuery(this),state=stateVal,classNames=value.split(core_rspace);while((className=classNames[i++]))
{state=isBool?state:!self.hasClass(className);self[state?"addClass":"removeClass"](className);}}else if(type==="undefined"||type==="boolean")
{if(this.className)
{jQuery._data(this,"__className__",this.className);}
this.className=this.className||value===false?"":jQuery._data(this,"__className__")||"";}});},hasClass:function(selector)
{var className=" "+selector+" ",i=0,l=this.length;for(;i<l;i++)
{if(this[i].nodeType===1&&(" "+this[i].className+" ").replace(rclass," ").indexOf(className)>=0)
{return true;}}
return false;},val:function(value)
{var hooks,ret,isFunction,elem=this[0];if(!arguments.length)
{if(elem)
{hooks=jQuery.valHooks[elem.type]||jQuery.valHooks[elem.nodeName.toLowerCase()];if(hooks&&"get"in hooks&&(ret=hooks.get(elem,"value"))!==undefined)
{return ret;}
ret=elem.value;return typeof ret==="string"?ret.replace(rreturn,""):ret==null?"":ret;}
return;}
isFunction=jQuery.isFunction(value);return this.each(function(i)
{var val,self=jQuery(this);if(this.nodeType!==1)
{return;}
if(isFunction)
{val=value.call(this,i,self.val());}else
{val=value;}
if(val==null)
{val="";}else if(typeof val==="number")
{val+="";}else if(jQuery.isArray(val))
{val=jQuery.map(val,function(value)
{return value==null?"":value+"";});}
hooks=jQuery.valHooks[this.type]||jQuery.valHooks[this.nodeName.toLowerCase()];if(!hooks||!("set"in hooks)||hooks.set(this,val,"value")===undefined)
{this.value=val;}});}});jQuery.extend({valHooks:{option:{get:function(elem)
{var val=elem.attributes.value;return!val||val.specified?elem.value:elem.text;}},select:{get:function(elem)
{var value,option,options=elem.options,index=elem.selectedIndex,one=elem.type==="select-one"||index<0,values=one?null:[],max=one?index+1:options.length,i=index<0?max:one?index:0;for(;i<max;i++)
{option=options[i];if((option.selected||i===index)&&(jQuery.support.optDisabled?!option.disabled:option.getAttribute("disabled")===null)&&(!option.parentNode.disabled||!jQuery.nodeName(option.parentNode,"optgroup")))
{value=jQuery(option).val();if(one)
{return value;}
values.push(value);}}
return values;},set:function(elem,value)
{var values=jQuery.makeArray(value);jQuery(elem).find("option").each(function()
{this.selected=jQuery.inArray(jQuery(this).val(),values)>=0;});if(!values.length)
{elem.selectedIndex=-1;}
return values;}}},attrFn:{},attr:function(elem,name,value,pass)
{var ret,hooks,notxml,nType=elem.nodeType;if(!elem||nType===3||nType===8||nType===2)
{return;}
if(pass&&jQuery.isFunction(jQuery.fn[name]))
{return jQuery(elem)[name](value);}
if(typeof elem.getAttribute==="undefined")
{return jQuery.prop(elem,name,value);}
notxml=nType!==1||!jQuery.isXMLDoc(elem);if(notxml)
{name=name.toLowerCase();hooks=jQuery.attrHooks[name]||(rboolean.test(name)?boolHook:nodeHook);}
if(value!==undefined)
{if(value===null)
{jQuery.removeAttr(elem,name);return;}else if(hooks&&"set"in hooks&&notxml&&(ret=hooks.set(elem,value,name))!==undefined)
{return ret;}else
{elem.setAttribute(name,value+"");return value;}}else if(hooks&&"get"in hooks&&notxml&&(ret=hooks.get(elem,name))!==null)
{return ret;}else
{ret=elem.getAttribute(name);return ret===null?undefined:ret;}},removeAttr:function(elem,value)
{var propName,attrNames,name,isBool,i=0;if(value&&elem.nodeType===1)
{attrNames=value.split(core_rspace);for(;i<attrNames.length;i++)
{name=attrNames[i];if(name)
{propName=jQuery.propFix[name]||name;isBool=rboolean.test(name);if(!isBool)
{jQuery.attr(elem,name,"");}
elem.removeAttribute(getSetAttribute?name:propName);if(isBool&&propName in elem)
{elem[propName]=false;}}}}},attrHooks:{type:{set:function(elem,value)
{if(rtype.test(elem.nodeName)&&elem.parentNode)
{jQuery.error("type property can't be changed");}else if(!jQuery.support.radioValue&&value==="radio"&&jQuery.nodeName(elem,"input"))
{var val=elem.value;elem.setAttribute("type",value);if(val)
{elem.value=val;}
return value;}}},value:{get:function(elem,name)
{if(nodeHook&&jQuery.nodeName(elem,"button"))
{return nodeHook.get(elem,name);}
return name in elem?elem.value:null;},set:function(elem,value,name)
{if(nodeHook&&jQuery.nodeName(elem,"button"))
{return nodeHook.set(elem,value,name);}
elem.value=value;}}},propFix:{tabindex:"tabIndex",readonly:"readOnly","for":"htmlFor","class":"className",maxlength:"maxLength",cellspacing:"cellSpacing",cellpadding:"cellPadding",rowspan:"rowSpan",colspan:"colSpan",usemap:"useMap",frameborder:"frameBorder",contenteditable:"contentEditable"},prop:function(elem,name,value)
{var ret,hooks,notxml,nType=elem.nodeType;if(!elem||nType===3||nType===8||nType===2)
{return;}
notxml=nType!==1||!jQuery.isXMLDoc(elem);if(notxml)
{name=jQuery.propFix[name]||name;hooks=jQuery.propHooks[name];}
if(value!==undefined)
{if(hooks&&"set"in hooks&&(ret=hooks.set(elem,value,name))!==undefined)
{return ret;}else
{return(elem[name]=value);}}else
{if(hooks&&"get"in hooks&&(ret=hooks.get(elem,name))!==null)
{return ret;}else
{return elem[name];}}},propHooks:{tabIndex:{get:function(elem)
{var attributeNode=elem.getAttributeNode("tabindex");return attributeNode&&attributeNode.specified?parseInt(attributeNode.value,10):rfocusable.test(elem.nodeName)||rclickable.test(elem.nodeName)&&elem.href?0:undefined;}}}});boolHook={get:function(elem,name)
{var attrNode,property=jQuery.prop(elem,name);return property===true||typeof property!=="boolean"&&(attrNode=elem.getAttributeNode(name))&&attrNode.nodeValue!==false?name.toLowerCase():undefined;},set:function(elem,value,name)
{var propName;if(value===false)
{jQuery.removeAttr(elem,name);}else
{propName=jQuery.propFix[name]||name;if(propName in elem)
{elem[propName]=true;}
elem.setAttribute(name,name.toLowerCase());}
return name;}};if(!getSetAttribute)
{fixSpecified={name:true,id:true,coords:true};nodeHook=jQuery.valHooks.button={get:function(elem,name)
{var ret;ret=elem.getAttributeNode(name);return ret&&(fixSpecified[name]?ret.value!=="":ret.specified)?ret.value:undefined;},set:function(elem,value,name)
{var ret=elem.getAttributeNode(name);if(!ret)
{ret=document.createAttribute(name);elem.setAttributeNode(ret);}
return(ret.value=value+"");}};jQuery.each(["width","height"],function(i,name)
{jQuery.attrHooks[name]=jQuery.extend(jQuery.attrHooks[name],{set:function(elem,value)
{if(value==="")
{elem.setAttribute(name,"auto");return value;}}});});jQuery.attrHooks.contenteditable={get:nodeHook.get,set:function(elem,value,name)
{if(value==="")
{value="false";}
nodeHook.set(elem,value,name);}};}
if(!jQuery.support.hrefNormalized)
{jQuery.each(["href","src","width","height"],function(i,name)
{jQuery.attrHooks[name]=jQuery.extend(jQuery.attrHooks[name],{get:function(elem)
{var ret=elem.getAttribute(name,2);return ret===null?undefined:ret;}});});}
if(!jQuery.support.style)
{jQuery.attrHooks.style={get:function(elem)
{return elem.style.cssText.toLowerCase()||undefined;},set:function(elem,value)
{return(elem.style.cssText=value+"");}};}
if(!jQuery.support.optSelected)
{jQuery.propHooks.selected=jQuery.extend(jQuery.propHooks.selected,{get:function(elem)
{var parent=elem.parentNode;if(parent)
{parent.selectedIndex;if(parent.parentNode)
{parent.parentNode.selectedIndex;}}
return null;}});}
if(!jQuery.support.enctype)
{jQuery.propFix.enctype="encoding";}
if(!jQuery.support.checkOn)
{jQuery.each(["radio","checkbox"],function()
{jQuery.valHooks[this]={get:function(elem)
{return elem.getAttribute("value")===null?"on":elem.value;}};});}
jQuery.each(["radio","checkbox"],function()
{jQuery.valHooks[this]=jQuery.extend(jQuery.valHooks[this],{set:function(elem,value)
{if(jQuery.isArray(value))
{return(elem.checked=jQuery.inArray(jQuery(elem).val(),value)>=0);}}});});var rformElems=/^(?:textarea|input|select)$/i,rtypenamespace=/^([^\.]*|)(?:\.(.+)|)$/,rhoverHack=/(?:^|\s)hover(\.\S+|)\b/,rkeyEvent=/^key/,rmouseEvent=/^(?:mouse|contextmenu)|click/,rfocusMorph=/^(?:focusinfocus|focusoutblur)$/,hoverHack=function(events)
{return jQuery.event.special.hover?events:events.replace(rhoverHack,"mouseenter$1 mouseleave$1");};jQuery.event={add:function(elem,types,handler,data,selector)
{var elemData,eventHandle,events,t,tns,type,namespaces,handleObj,handleObjIn,handlers,special;if(elem.nodeType===3||elem.nodeType===8||!types||!handler||!(elemData=jQuery._data(elem)))
{return;}
if(handler.handler)
{handleObjIn=handler;handler=handleObjIn.handler;selector=handleObjIn.selector;}
if(!handler.guid)
{handler.guid=jQuery.guid++;}
events=elemData.events;if(!events)
{elemData.events=events={};}
eventHandle=elemData.handle;if(!eventHandle)
{elemData.handle=eventHandle=function(e)
{return typeof jQuery!=="undefined"&&(!e||jQuery.event.triggered!==e.type)?jQuery.event.dispatch.apply(eventHandle.elem,arguments):undefined;};eventHandle.elem=elem;}
types=jQuery.trim(hoverHack(types)).split(" ");for(t=0;t<types.length;t++)
{tns=rtypenamespace.exec(types[t])||[];type=tns[1];namespaces=(tns[2]||"").split(".").sort();special=jQuery.event.special[type]||{};type=(selector?special.delegateType:special.bindType)||type;special=jQuery.event.special[type]||{};handleObj=jQuery.extend({type:type,origType:tns[1],data:data,handler:handler,guid:handler.guid,selector:selector,needsContext:selector&&jQuery.expr.match.needsContext.test(selector),namespace:namespaces.join(".")},handleObjIn);handlers=events[type];if(!handlers)
{handlers=events[type]=[];handlers.delegateCount=0;if(!special.setup||special.setup.call(elem,data,namespaces,eventHandle)===false)
{if(elem.addEventListener)
{elem.addEventListener(type,eventHandle,false);}else if(elem.attachEvent)
{elem.attachEvent("on"+type,eventHandle);}}}
if(special.add)
{special.add.call(elem,handleObj);if(!handleObj.handler.guid)
{handleObj.handler.guid=handler.guid;}}
if(selector)
{handlers.splice(handlers.delegateCount++,0,handleObj);}else
{handlers.push(handleObj);}
jQuery.event.global[type]=true;}
elem=null;},global:{},remove:function(elem,types,handler,selector,mappedTypes)
{var t,tns,type,origType,namespaces,origCount,j,events,special,eventType,handleObj,elemData=jQuery.hasData(elem)&&jQuery._data(elem);if(!elemData||!(events=elemData.events))
{return;}
types=jQuery.trim(hoverHack(types||"")).split(" ");for(t=0;t<types.length;t++)
{tns=rtypenamespace.exec(types[t])||[];type=origType=tns[1];namespaces=tns[2];if(!type)
{for(type in events)
{jQuery.event.remove(elem,type+types[t],handler,selector,true);}
continue;}
special=jQuery.event.special[type]||{};type=(selector?special.delegateType:special.bindType)||type;eventType=events[type]||[];origCount=eventType.length;namespaces=namespaces?new RegExp("(^|\\.)"+namespaces.split(".").sort().join("\\.(?:.*\\.|)")+"(\\.|$)"):null;for(j=0;j<eventType.length;j++)
{handleObj=eventType[j];if((mappedTypes||origType===handleObj.origType)&&(!handler||handler.guid===handleObj.guid)&&(!namespaces||namespaces.test(handleObj.namespace))&&(!selector||selector===handleObj.selector||selector==="**"&&handleObj.selector))
{eventType.splice(j--,1);if(handleObj.selector)
{eventType.delegateCount--;}
if(special.remove)
{special.remove.call(elem,handleObj);}}}
if(eventType.length===0&&origCount!==eventType.length)
{if(!special.teardown||special.teardown.call(elem,namespaces,elemData.handle)===false)
{jQuery.removeEvent(elem,type,elemData.handle);}
delete events[type];}}
if(jQuery.isEmptyObject(events))
{delete elemData.handle;jQuery.removeData(elem,"events",true);}},customEvent:{"getData":true,"setData":true,"changeData":true},trigger:function(event,data,elem,onlyHandlers)
{if(elem&&(elem.nodeType===3||elem.nodeType===8))
{return;}
var cache,exclusive,i,cur,old,ontype,special,handle,eventPath,bubbleType,type=event.type||event,namespaces=[];if(rfocusMorph.test(type+jQuery.event.triggered))
{return;}
if(type.indexOf("!")>=0)
{type=type.slice(0,-1);exclusive=true;}
if(type.indexOf(".")>=0)
{namespaces=type.split(".");type=namespaces.shift();namespaces.sort();}
if((!elem||jQuery.event.customEvent[type])&&!jQuery.event.global[type])
{return;}
event=typeof event==="object"?event[jQuery.expando]?event:new jQuery.Event(type,event):new jQuery.Event(type);event.type=type;event.isTrigger=true;event.exclusive=exclusive;event.namespace=namespaces.join(".");event.namespace_re=event.namespace?new RegExp("(^|\\.)"+namespaces.join("\\.(?:.*\\.|)")+"(\\.|$)"):null;ontype=type.indexOf(":")<0?"on"+type:"";if(!elem)
{cache=jQuery.cache;for(i in cache)
{if(cache[i].events&&cache[i].events[type])
{jQuery.event.trigger(event,data,cache[i].handle.elem,true);}}
return;}
event.result=undefined;if(!event.target)
{event.target=elem;}
data=data!=null?jQuery.makeArray(data):[];data.unshift(event);special=jQuery.event.special[type]||{};if(special.trigger&&special.trigger.apply(elem,data)===false)
{return;}
eventPath=[[elem,special.bindType||type]];if(!onlyHandlers&&!special.noBubble&&!jQuery.isWindow(elem))
{bubbleType=special.delegateType||type;cur=rfocusMorph.test(bubbleType+type)?elem:elem.parentNode;for(old=elem;cur;cur=cur.parentNode)
{eventPath.push([cur,bubbleType]);old=cur;}
if(old===(elem.ownerDocument||document))
{eventPath.push([old.defaultView||old.parentWindow||window,bubbleType]);}}
for(i=0;i<eventPath.length&&!event.isPropagationStopped();i++)
{cur=eventPath[i][0];event.type=eventPath[i][1];handle=(jQuery._data(cur,"events")||{})[event.type]&&jQuery._data(cur,"handle");if(handle)
{handle.apply(cur,data);}
handle=ontype&&cur[ontype];if(handle&&jQuery.acceptData(cur)&&handle.apply&&handle.apply(cur,data)===false)
{event.preventDefault();}}
event.type=type;if(!onlyHandlers&&!event.isDefaultPrevented())
{if((!special._default||special._default.apply(elem.ownerDocument,data)===false)&&!(type==="click"&&jQuery.nodeName(elem,"a"))&&jQuery.acceptData(elem))
{if(ontype&&elem[type]&&((type!=="focus"&&type!=="blur")||event.target.offsetWidth!==0)&&!jQuery.isWindow(elem))
{old=elem[ontype];if(old)
{elem[ontype]=null;}
jQuery.event.triggered=type;elem[type]();jQuery.event.triggered=undefined;if(old)
{elem[ontype]=old;}}}}
return event.result;},dispatch:function(event)
{event=jQuery.event.fix(event||window.event);var i,j,cur,ret,selMatch,matched,matches,handleObj,sel,related,handlers=((jQuery._data(this,"events")||{})[event.type]||[]),delegateCount=handlers.delegateCount,args=core_slice.call(arguments),run_all=!event.exclusive&&!event.namespace,special=jQuery.event.special[event.type]||{},handlerQueue=[];args[0]=event;event.delegateTarget=this;if(special.preDispatch&&special.preDispatch.call(this,event)===false)
{return;}
if(delegateCount&&!(event.button&&event.type==="click"))
{for(cur=event.target;cur!=this;cur=cur.parentNode||this)
{if(cur.disabled!==true||event.type!=="click")
{selMatch={};matches=[];for(i=0;i<delegateCount;i++)
{handleObj=handlers[i];sel=handleObj.selector;if(selMatch[sel]===undefined)
{selMatch[sel]=handleObj.needsContext?jQuery(sel,this).index(cur)>=0:jQuery.find(sel,this,null,[cur]).length;}
if(selMatch[sel])
{matches.push(handleObj);}}
if(matches.length)
{handlerQueue.push({elem:cur,matches:matches});}}}}
if(handlers.length>delegateCount)
{handlerQueue.push({elem:this,matches:handlers.slice(delegateCount)});}
for(i=0;i<handlerQueue.length&&!event.isPropagationStopped();i++)
{matched=handlerQueue[i];event.currentTarget=matched.elem;for(j=0;j<matched.matches.length&&!event.isImmediatePropagationStopped();j++)
{handleObj=matched.matches[j];if(run_all||(!event.namespace&&!handleObj.namespace)||event.namespace_re&&event.namespace_re.test(handleObj.namespace))
{event.data=handleObj.data;event.handleObj=handleObj;ret=((jQuery.event.special[handleObj.origType]||{}).handle||handleObj.handler).apply(matched.elem,args);if(ret!==undefined)
{event.result=ret;if(ret===false)
{event.preventDefault();event.stopPropagation();}}}}}
if(special.postDispatch)
{special.postDispatch.call(this,event);}
return event.result;},props:"attrChange attrName relatedNode srcElement altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),fixHooks:{},keyHooks:{props:"char charCode key keyCode".split(" "),filter:function(event,original)
{if(event.which==null)
{event.which=original.charCode!=null?original.charCode:original.keyCode;}
return event;}},mouseHooks:{props:"button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),filter:function(event,original)
{var eventDoc,doc,body,button=original.button,fromElement=original.fromElement;if(event.pageX==null&&original.clientX!=null)
{eventDoc=event.target.ownerDocument||document;doc=eventDoc.documentElement;body=eventDoc.body;event.pageX=original.clientX+(doc&&doc.scrollLeft||body&&body.scrollLeft||0)-(doc&&doc.clientLeft||body&&body.clientLeft||0);event.pageY=original.clientY+(doc&&doc.scrollTop||body&&body.scrollTop||0)-(doc&&doc.clientTop||body&&body.clientTop||0);}
if(!event.relatedTarget&&fromElement)
{event.relatedTarget=fromElement===event.target?original.toElement:fromElement;}
if(!event.which&&button!==undefined)
{event.which=(button&1?1:(button&2?3:(button&4?2:0)));}
return event;}},fix:function(event)
{if(event[jQuery.expando])
{return event;}
var i,prop,originalEvent=event,fixHook=jQuery.event.fixHooks[event.type]||{},copy=fixHook.props?this.props.concat(fixHook.props):this.props;event=jQuery.Event(originalEvent);for(i=copy.length;i;)
{prop=copy[--i];event[prop]=originalEvent[prop];}
if(!event.target)
{event.target=originalEvent.srcElement||document;}
if(event.target.nodeType===3)
{event.target=event.target.parentNode;}
event.metaKey=!!event.metaKey;return fixHook.filter?fixHook.filter(event,originalEvent):event;},special:{load:{noBubble:true},focus:{delegateType:"focusin"},blur:{delegateType:"focusout"},beforeunload:{setup:function(data,namespaces,eventHandle)
{if(jQuery.isWindow(this))
{this.onbeforeunload=eventHandle;}},teardown:function(namespaces,eventHandle)
{if(this.onbeforeunload===eventHandle)
{this.onbeforeunload=null;}}}},simulate:function(type,elem,event,bubble)
{var e=jQuery.extend(new jQuery.Event(),event,{type:type,isSimulated:true,originalEvent:{}});if(bubble)
{jQuery.event.trigger(e,null,elem);}else
{jQuery.event.dispatch.call(elem,e);}
if(e.isDefaultPrevented())
{event.preventDefault();}}};jQuery.event.handle=jQuery.event.dispatch;jQuery.removeEvent=document.removeEventListener?function(elem,type,handle)
{if(elem.removeEventListener)
{elem.removeEventListener(type,handle,false);}}:function(elem,type,handle)
{var name="on"+type;if(elem.detachEvent)
{if(typeof elem[name]==="undefined")
{elem[name]=null;}
elem.detachEvent(name,handle);}};jQuery.Event=function(src,props)
{if(!(this instanceof jQuery.Event))
{return new jQuery.Event(src,props);}
if(src&&src.type)
{this.originalEvent=src;this.type=src.type;this.isDefaultPrevented=(src.defaultPrevented||src.returnValue===false||src.getPreventDefault&&src.getPreventDefault())?returnTrue:returnFalse;}else
{this.type=src;}
if(props)
{jQuery.extend(this,props);}
this.timeStamp=src&&src.timeStamp||jQuery.now();this[jQuery.expando]=true;};function returnFalse()
{return false;}
function returnTrue()
{return true;}
jQuery.Event.prototype={preventDefault:function()
{this.isDefaultPrevented=returnTrue;var e=this.originalEvent;if(!e)
{return;}
if(e.preventDefault)
{e.preventDefault();}else
{e.returnValue=false;}},stopPropagation:function()
{this.isPropagationStopped=returnTrue;var e=this.originalEvent;if(!e)
{return;}
if(e.stopPropagation)
{e.stopPropagation();}
e.cancelBubble=true;},stopImmediatePropagation:function()
{this.isImmediatePropagationStopped=returnTrue;this.stopPropagation();},isDefaultPrevented:returnFalse,isPropagationStopped:returnFalse,isImmediatePropagationStopped:returnFalse};jQuery.each({mouseenter:"mouseover",mouseleave:"mouseout"},function(orig,fix)
{jQuery.event.special[orig]={delegateType:fix,bindType:fix,handle:function(event)
{var ret,target=this,related=event.relatedTarget,handleObj=event.handleObj,selector=handleObj.selector;if(!related||(related!==target&&!jQuery.contains(target,related)))
{event.type=handleObj.origType;ret=handleObj.handler.apply(this,arguments);event.type=fix;}
return ret;}};});if(!jQuery.support.submitBubbles)
{jQuery.event.special.submit={setup:function()
{if(jQuery.nodeName(this,"form"))
{return false;}
jQuery.event.add(this,"click._submit keypress._submit",function(e)
{var elem=e.target,form=jQuery.nodeName(elem,"input")||jQuery.nodeName(elem,"button")?elem.form:undefined;if(form&&!jQuery._data(form,"_submit_attached"))
{jQuery.event.add(form,"submit._submit",function(event)
{event._submit_bubble=true;});jQuery._data(form,"_submit_attached",true);}});},postDispatch:function(event)
{if(event._submit_bubble)
{delete event._submit_bubble;if(this.parentNode&&!event.isTrigger)
{jQuery.event.simulate("submit",this.parentNode,event,true);}}},teardown:function()
{if(jQuery.nodeName(this,"form"))
{return false;}
jQuery.event.remove(this,"._submit");}};}
if(!jQuery.support.changeBubbles)
{jQuery.event.special.change={setup:function()
{if(rformElems.test(this.nodeName))
{if(this.type==="checkbox"||this.type==="radio")
{jQuery.event.add(this,"propertychange._change",function(event)
{if(event.originalEvent.propertyName==="checked")
{this._just_changed=true;}});jQuery.event.add(this,"click._change",function(event)
{if(this._just_changed&&!event.isTrigger)
{this._just_changed=false;}
jQuery.event.simulate("change",this,event,true);});}
return false;}
jQuery.event.add(this,"beforeactivate._change",function(e)
{var elem=e.target;if(rformElems.test(elem.nodeName)&&!jQuery._data(elem,"_change_attached"))
{jQuery.event.add(elem,"change._change",function(event)
{if(this.parentNode&&!event.isSimulated&&!event.isTrigger)
{jQuery.event.simulate("change",this.parentNode,event,true);}});jQuery._data(elem,"_change_attached",true);}});},handle:function(event)
{var elem=event.target;if(this!==elem||event.isSimulated||event.isTrigger||(elem.type!=="radio"&&elem.type!=="checkbox"))
{return event.handleObj.handler.apply(this,arguments);}},teardown:function()
{jQuery.event.remove(this,"._change");return!rformElems.test(this.nodeName);}};}
if(!jQuery.support.focusinBubbles)
{jQuery.each({focus:"focusin",blur:"focusout"},function(orig,fix)
{var attaches=0,handler=function(event)
{jQuery.event.simulate(fix,event.target,jQuery.event.fix(event),true);};jQuery.event.special[fix]={setup:function()
{if(attaches++===0)
{document.addEventListener(orig,handler,true);}},teardown:function()
{if(--attaches===0)
{document.removeEventListener(orig,handler,true);}}};});}
jQuery.fn.extend({on:function(types,selector,data,fn,one)
{var origFn,type;if(typeof types==="object")
{if(typeof selector!=="string")
{data=data||selector;selector=undefined;}
for(type in types)
{this.on(type,selector,data,types[type],one);}
return this;}
if(data==null&&fn==null)
{fn=selector;data=selector=undefined;}else if(fn==null)
{if(typeof selector==="string")
{fn=data;data=undefined;}else
{fn=data;data=selector;selector=undefined;}}
if(fn===false)
{fn=returnFalse;}else if(!fn)
{return this;}
if(one===1)
{origFn=fn;fn=function(event)
{jQuery().off(event);return origFn.apply(this,arguments);};fn.guid=origFn.guid||(origFn.guid=jQuery.guid++);}
return this.each(function()
{jQuery.event.add(this,types,fn,data,selector);});},one:function(types,selector,data,fn)
{return this.on(types,selector,data,fn,1);},off:function(types,selector,fn)
{var handleObj,type;if(types&&types.preventDefault&&types.handleObj)
{handleObj=types.handleObj;jQuery(types.delegateTarget).off(handleObj.namespace?handleObj.origType+"."+handleObj.namespace:handleObj.origType,handleObj.selector,handleObj.handler);return this;}
if(typeof types==="object")
{for(type in types)
{this.off(type,selector,types[type]);}
return this;}
if(selector===false||typeof selector==="function")
{fn=selector;selector=undefined;}
if(fn===false)
{fn=returnFalse;}
return this.each(function()
{jQuery.event.remove(this,types,fn,selector);});},bind:function(types,data,fn)
{return this.on(types,null,data,fn);},unbind:function(types,fn)
{return this.off(types,null,fn);},live:function(types,data,fn)
{jQuery(this.context).on(types,this.selector,data,fn);return this;},die:function(types,fn)
{jQuery(this.context).off(types,this.selector||"**",fn);return this;},delegate:function(selector,types,data,fn)
{return this.on(types,selector,data,fn);},undelegate:function(selector,types,fn)
{return arguments.length===1?this.off(selector,"**"):this.off(types,selector||"**",fn);},trigger:function(type,data)
{return this.each(function()
{jQuery.event.trigger(type,data,this);});},triggerHandler:function(type,data)
{if(this[0])
{return jQuery.event.trigger(type,data,this[0],true);}},toggle:function(fn)
{var args=arguments,guid=fn.guid||jQuery.guid++,i=0,toggler=function(event)
{var lastToggle=(jQuery._data(this,"lastToggle"+fn.guid)||0)%i;jQuery._data(this,"lastToggle"+fn.guid,lastToggle+1);event.preventDefault();return args[lastToggle].apply(this,arguments)||false;};toggler.guid=guid;while(i<args.length)
{args[i++].guid=guid;}
return this.click(toggler);},hover:function(fnOver,fnOut)
{return this.mouseenter(fnOver).mouseleave(fnOut||fnOver);}});jQuery.each(("blur focus focusin focusout load resize scroll unload click dblclick "+"mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave "+"change select submit keydown keypress keyup error contextmenu").split(" "),function(i,name)
{jQuery.fn[name]=function(data,fn)
{if(fn==null)
{fn=data;data=null;}
return arguments.length>0?this.on(name,null,data,fn):this.trigger(name);};if(rkeyEvent.test(name))
{jQuery.event.fixHooks[name]=jQuery.event.keyHooks;}
if(rmouseEvent.test(name))
{jQuery.event.fixHooks[name]=jQuery.event.mouseHooks;}});
/*!
     * Sizzle CSS Selector Engine
     * Copyright 2012 jQuery Foundation and other contributors
     * Released under the MIT license
     * http://sizzlejs.com/
     */
(function(window,undefined)
{var cachedruns,assertGetIdNotName,Expr,getText,isXML,contains,compile,sortOrder,hasDuplicate,outermostContext,baseHasDuplicate=true,strundefined="undefined",expando=("sizcache"+Math.random()).replace(".",""),Token=String,document=window.document,docElem=document.documentElement,dirruns=0,done=0,pop=[].pop,push=[].push,slice=[].slice,indexOf=[].indexOf||function(elem)
{var i=0,len=this.length;for(;i<len;i++)
{if(this[i]===elem)
{return i;}}
return-1;},markFunction=function(fn,value)
{fn[expando]=value==null||value;return fn;},createCache=function()
{var cache={},keys=[];return markFunction(function(key,value)
{if(keys.push(key)>Expr.cacheLength)
{delete cache[keys.shift()];}
return(cache[key+" "]=value);},cache);},classCache=createCache(),tokenCache=createCache(),compilerCache=createCache(),whitespace="[\\x20\\t\\r\\n\\f]",characterEncoding="(?:\\\\.|[-\\w]|[^\\x00-\\xa0])+",identifier=characterEncoding.replace("w","w#"),operators="([*^$|!~]?=)",attributes="\\["+whitespace+"*("+characterEncoding+")"+whitespace+"*(?:"+operators+whitespace+"*(?:(['\"])((?:\\\\.|[^\\\\])*?)\\3|("+identifier+")|)|)"+whitespace+"*\\]",pseudos=":("+characterEncoding+")(?:\\((?:(['\"])((?:\\\\.|[^\\\\])*?)\\2|([^()[\\]]*|(?:(?:"+attributes+")|[^:]|\\\\.)*|.*))\\)|)",pos=":(even|odd|eq|gt|lt|nth|first|last)(?:\\("+whitespace+"*((?:-\\d)?\\d*)"+whitespace+"*\\)|)(?=[^-]|$)",rtrim=new RegExp("^"+whitespace+"+|((?:^|[^\\\\])(?:\\\\.)*)"+whitespace+"+$","g"),rcomma=new RegExp("^"+whitespace+"*,"+whitespace+"*"),rcombinators=new RegExp("^"+whitespace+"*([\\x20\\t\\r\\n\\f>+~])"+whitespace+"*"),rpseudo=new RegExp(pseudos),rquickExpr=/^(?:#([\w\-]+)|(\w+)|\.([\w\-]+))$/,rnot=/^:not/,rsibling=/[\x20\t\r\n\f]*[+~]/,rendsWithNot=/:not\($/,rheader=/h\d/i,rinputs=/input|select|textarea|button/i,rbackslash=/\\(?!\\)/g,matchExpr={"ID":new RegExp("^#("+characterEncoding+")"),"CLASS":new RegExp("^\\.("+characterEncoding+")"),"NAME":new RegExp("^\\[name=['\"]?("+characterEncoding+")['\"]?\\]"),"TAG":new RegExp("^("+characterEncoding.replace("w","w*")+")"),"ATTR":new RegExp("^"+attributes),"PSEUDO":new RegExp("^"+pseudos),"POS":new RegExp(pos,"i"),"CHILD":new RegExp("^:(only|nth|first|last)-child(?:\\("+whitespace+"*(even|odd|(([+-]|)(\\d*)n|)"+whitespace+"*(?:([+-]|)"+whitespace+"*(\\d+)|))"+whitespace+"*\\)|)","i"),"needsContext":new RegExp("^"+whitespace+"*[>+~]|"+pos,"i")},assert=function(fn)
{var div=document.createElement("div");try
{return fn(div);}catch(e)
{return false;}finally
{div=null;}},assertTagNameNoComments=assert(function(div)
{div.appendChild(document.createComment(""));return!div.getElementsByTagName("*").length;}),assertHrefNotNormalized=assert(function(div)
{div.innerHTML="<a href='#'></a>";return div.firstChild&&typeof div.firstChild.getAttribute!==strundefined&&div.firstChild.getAttribute("href")==="#";}),assertAttributes=assert(function(div)
{div.innerHTML="<select></select>";var type=typeof div.lastChild.getAttribute("multiple");return type!=="boolean"&&type!=="string";}),assertUsableClassName=assert(function(div)
{div.innerHTML="<div class='hidden e'></div><div class='hidden'></div>";if(!div.getElementsByClassName||!div.getElementsByClassName("e").length)
{return false;}
div.lastChild.className="e";return div.getElementsByClassName("e").length===2;}),assertUsableName=assert(function(div)
{div.id=expando+0;div.innerHTML="<a name='"+expando+"'></a><div name='"+expando+"'></div>";docElem.insertBefore(div,docElem.firstChild);var pass=document.getElementsByName&&document.getElementsByName(expando).length===2+
document.getElementsByName(expando+0).length;assertGetIdNotName=!document.getElementById(expando);docElem.removeChild(div);return pass;});try
{slice.call(docElem.childNodes,0)[0].nodeType;}catch(e)
{slice=function(i)
{var elem,results=[];for(;(elem=this[i]);i++)
{results.push(elem);}
return results;};}
function Sizzle(selector,context,results,seed)
{results=results||[];context=context||document;var match,elem,xml,m,nodeType=context.nodeType;if(!selector||typeof selector!=="string")
{return results;}
if(nodeType!==1&&nodeType!==9)
{return[];}
xml=isXML(context);if(!xml&&!seed)
{if((match=rquickExpr.exec(selector)))
{if((m=match[1]))
{if(nodeType===9)
{elem=context.getElementById(m);if(elem&&elem.parentNode)
{if(elem.id===m)
{results.push(elem);return results;}}else
{return results;}}else
{if(context.ownerDocument&&(elem=context.ownerDocument.getElementById(m))&&contains(context,elem)&&elem.id===m)
{results.push(elem);return results;}}}else if(match[2])
{push.apply(results,slice.call(context.getElementsByTagName(selector),0));return results;}else if((m=match[3])&&assertUsableClassName&&context.getElementsByClassName)
{push.apply(results,slice.call(context.getElementsByClassName(m),0));return results;}}}
return select(selector.replace(rtrim,"$1"),context,results,seed,xml);}
Sizzle.matches=function(expr,elements)
{return Sizzle(expr,null,null,elements);};Sizzle.matchesSelector=function(elem,expr)
{return Sizzle(expr,null,null,[elem]).length>0;};function createInputPseudo(type)
{return function(elem)
{var name=elem.nodeName.toLowerCase();return name==="input"&&elem.type===type;};}
function createButtonPseudo(type)
{return function(elem)
{var name=elem.nodeName.toLowerCase();return(name==="input"||name==="button")&&elem.type===type;};}
function createPositionalPseudo(fn)
{return markFunction(function(argument)
{argument=+argument;return markFunction(function(seed,matches)
{var j,matchIndexes=fn([],seed.length,argument),i=matchIndexes.length;while(i--)
{if(seed[(j=matchIndexes[i])])
{seed[j]=!(matches[j]=seed[j]);}}});});}
getText=Sizzle.getText=function(elem)
{var node,ret="",i=0,nodeType=elem.nodeType;if(nodeType)
{if(nodeType===1||nodeType===9||nodeType===11)
{if(typeof elem.textContent==="string")
{return elem.textContent;}else
{for(elem=elem.firstChild;elem;elem=elem.nextSibling)
{ret+=getText(elem);}}}else if(nodeType===3||nodeType===4)
{return elem.nodeValue;}}else
{for(;(node=elem[i]);i++)
{ret+=getText(node);}}
return ret;};isXML=Sizzle.isXML=function(elem)
{var documentElement=elem&&(elem.ownerDocument||elem).documentElement;return documentElement?documentElement.nodeName!=="HTML":false;};contains=Sizzle.contains=docElem.contains?function(a,b)
{var adown=a.nodeType===9?a.documentElement:a,bup=b&&b.parentNode;return a===bup||!!(bup&&bup.nodeType===1&&adown.contains&&adown.contains(bup));}:docElem.compareDocumentPosition?function(a,b)
{return b&&!!(a.compareDocumentPosition(b)&16);}:function(a,b)
{while((b=b.parentNode))
{if(b===a)
{return true;}}
return false;};Sizzle.attr=function(elem,name)
{var val,xml=isXML(elem);if(!xml)
{name=name.toLowerCase();}
if((val=Expr.attrHandle[name]))
{return val(elem);}
if(xml||assertAttributes)
{return elem.getAttribute(name);}
val=elem.getAttributeNode(name);return val?typeof elem[name]==="boolean"?elem[name]?name:null:val.specified?val.value:null:null;};Expr=Sizzle.selectors={cacheLength:50,createPseudo:markFunction,match:matchExpr,attrHandle:assertHrefNotNormalized?{}:{"href":function(elem)
{return elem.getAttribute("href",2);},"type":function(elem)
{return elem.getAttribute("type");}},find:{"ID":assertGetIdNotName?function(id,context,xml)
{if(typeof context.getElementById!==strundefined&&!xml)
{var m=context.getElementById(id);return m&&m.parentNode?[m]:[];}}:function(id,context,xml)
{if(typeof context.getElementById!==strundefined&&!xml)
{var m=context.getElementById(id);return m?m.id===id||typeof m.getAttributeNode!==strundefined&&m.getAttributeNode("id").value===id?[m]:undefined:[];}},"TAG":assertTagNameNoComments?function(tag,context)
{if(typeof context.getElementsByTagName!==strundefined)
{return context.getElementsByTagName(tag);}}:function(tag,context)
{var results=context.getElementsByTagName(tag);if(tag==="*")
{var elem,tmp=[],i=0;for(;(elem=results[i]);i++)
{if(elem.nodeType===1)
{tmp.push(elem);}}
return tmp;}
return results;},"NAME":assertUsableName&&function(tag,context)
{if(typeof context.getElementsByName!==strundefined)
{return context.getElementsByName(name);}},"CLASS":assertUsableClassName&&function(className,context,xml)
{if(typeof context.getElementsByClassName!==strundefined&&!xml)
{return context.getElementsByClassName(className);}}},relative:{">":{dir:"parentNode",first:true}," ":{dir:"parentNode"},"+":{dir:"previousSibling",first:true},"~":{dir:"previousSibling"}},preFilter:{"ATTR":function(match)
{match[1]=match[1].replace(rbackslash,"");match[3]=(match[4]||match[5]||"").replace(rbackslash,"");if(match[2]==="~=")
{match[3]=" "+match[3]+" ";}
return match.slice(0,4);},"CHILD":function(match)
{match[1]=match[1].toLowerCase();if(match[1]==="nth")
{if(!match[2])
{Sizzle.error(match[0]);}
match[3]=+(match[3]?match[4]+(match[5]||1):2*(match[2]==="even"||match[2]==="odd"));match[4]=+((match[6]+match[7])||match[2]==="odd");}else if(match[2])
{Sizzle.error(match[0]);}
return match;},"PSEUDO":function(match)
{var unquoted,excess;if(matchExpr["CHILD"].test(match[0]))
{return null;}
if(match[3])
{match[2]=match[3];}else if((unquoted=match[4]))
{if(rpseudo.test(unquoted)&&(excess=tokenize(unquoted,true))&&(excess=unquoted.indexOf(")",unquoted.length-excess)-unquoted.length))
{unquoted=unquoted.slice(0,excess);match[0]=match[0].slice(0,excess);}
match[2]=unquoted;}
return match.slice(0,3);}},filter:{"ID":assertGetIdNotName?function(id)
{id=id.replace(rbackslash,"");return function(elem)
{return elem.getAttribute("id")===id;};}:function(id)
{id=id.replace(rbackslash,"");return function(elem)
{var node=typeof elem.getAttributeNode!==strundefined&&elem.getAttributeNode("id");return node&&node.value===id;};},"TAG":function(nodeName)
{if(nodeName==="*")
{return function()
{return true;};}
nodeName=nodeName.replace(rbackslash,"").toLowerCase();return function(elem)
{return elem.nodeName&&elem.nodeName.toLowerCase()===nodeName;};},"CLASS":function(className)
{var pattern=classCache[expando][className+" "];return pattern||(pattern=new RegExp("(^|"+whitespace+")"+className+"("+whitespace+"|$)"))&&classCache(className,function(elem)
{return pattern.test(elem.className||(typeof elem.getAttribute!==strundefined&&elem.getAttribute("class"))||"");});},"ATTR":function(name,operator,check)
{return function(elem,context)
{var result=Sizzle.attr(elem,name);if(result==null)
{return operator==="!=";}
if(!operator)
{return true;}
result+="";return operator==="="?result===check:operator==="!="?result!==check:operator==="^="?check&&result.indexOf(check)===0:operator==="*="?check&&result.indexOf(check)>-1:operator==="$="?check&&result.substr(result.length-check.length)===check:operator==="~="?(" "+result+" ").indexOf(check)>-1:operator==="|="?result===check||result.substr(0,check.length+1)===check+"-":false;};},"CHILD":function(type,argument,first,last)
{if(type==="nth")
{return function(elem)
{var node,diff,parent=elem.parentNode;if(first===1&&last===0)
{return true;}
if(parent)
{diff=0;for(node=parent.firstChild;node;node=node.nextSibling)
{if(node.nodeType===1)
{diff++;if(elem===node)
{break;}}}}
diff-=last;return diff===first||(diff%first===0&&diff/first>=0);};}
return function(elem)
{var node=elem;switch(type)
{case"only":case"first":while((node=node.previousSibling))
{if(node.nodeType===1)
{return false;}}
if(type==="first")
{return true;}
node=elem;case"last":while((node=node.nextSibling))
{if(node.nodeType===1)
{return false;}}
return true;}};},"PSEUDO":function(pseudo,argument)
{var args,fn=Expr.pseudos[pseudo]||Expr.setFilters[pseudo.toLowerCase()]||Sizzle.error("unsupported pseudo: "+pseudo);if(fn[expando])
{return fn(argument);}
if(fn.length>1)
{args=[pseudo,pseudo,"",argument];return Expr.setFilters.hasOwnProperty(pseudo.toLowerCase())?markFunction(function(seed,matches)
{var idx,matched=fn(seed,argument),i=matched.length;while(i--)
{idx=indexOf.call(seed,matched[i]);seed[idx]=!(matches[idx]=matched[i]);}}):function(elem)
{return fn(elem,0,args);};}
return fn;}},pseudos:{"not":markFunction(function(selector)
{var input=[],results=[],matcher=compile(selector.replace(rtrim,"$1"));return matcher[expando]?markFunction(function(seed,matches,context,xml)
{var elem,unmatched=matcher(seed,null,xml,[]),i=seed.length;while(i--)
{if((elem=unmatched[i]))
{seed[i]=!(matches[i]=elem);}}}):function(elem,context,xml)
{input[0]=elem;matcher(input,null,xml,results);return!results.pop();};}),"has":markFunction(function(selector)
{return function(elem)
{return Sizzle(selector,elem).length>0;};}),"contains":markFunction(function(text)
{return function(elem)
{return(elem.textContent||elem.innerText||getText(elem)).indexOf(text)>-1;};}),"enabled":function(elem)
{return elem.disabled===false;},"disabled":function(elem)
{return elem.disabled===true;},"checked":function(elem)
{var nodeName=elem.nodeName.toLowerCase();return(nodeName==="input"&&!!elem.checked)||(nodeName==="option"&&!!elem.selected);},"selected":function(elem)
{if(elem.parentNode)
{elem.parentNode.selectedIndex;}
return elem.selected===true;},"parent":function(elem)
{return!Expr.pseudos["empty"](elem);},"empty":function(elem)
{var nodeType;elem=elem.firstChild;while(elem)
{if(elem.nodeName>"@"||(nodeType=elem.nodeType)===3||nodeType===4)
{return false;}
elem=elem.nextSibling;}
return true;},"header":function(elem)
{return rheader.test(elem.nodeName);},"text":function(elem)
{var type,attr;return elem.nodeName.toLowerCase()==="input"&&(type=elem.type)==="text"&&((attr=elem.getAttribute("type"))==null||attr.toLowerCase()===type);},"radio":createInputPseudo("radio"),"checkbox":createInputPseudo("checkbox"),"file":createInputPseudo("file"),"password":createInputPseudo("password"),"image":createInputPseudo("image"),"submit":createButtonPseudo("submit"),"reset":createButtonPseudo("reset"),"button":function(elem)
{var name=elem.nodeName.toLowerCase();return name==="input"&&elem.type==="button"||name==="button";},"input":function(elem)
{return rinputs.test(elem.nodeName);},"focus":function(elem)
{var doc=elem.ownerDocument;return elem===doc.activeElement&&(!doc.hasFocus||doc.hasFocus())&&!!(elem.type||elem.href||~elem.tabIndex);},"active":function(elem)
{return elem===elem.ownerDocument.activeElement;},"first":createPositionalPseudo(function()
{return[0];}),"last":createPositionalPseudo(function(matchIndexes,length)
{return[length-1];}),"eq":createPositionalPseudo(function(matchIndexes,length,argument)
{return[argument<0?argument+length:argument];}),"even":createPositionalPseudo(function(matchIndexes,length)
{for(var i=0;i<length;i+=2)
{matchIndexes.push(i);}
return matchIndexes;}),"odd":createPositionalPseudo(function(matchIndexes,length)
{for(var i=1;i<length;i+=2)
{matchIndexes.push(i);}
return matchIndexes;}),"lt":createPositionalPseudo(function(matchIndexes,length,argument)
{for(var i=argument<0?argument+length:argument;--i>=0;)
{matchIndexes.push(i);}
return matchIndexes;}),"gt":createPositionalPseudo(function(matchIndexes,length,argument)
{for(var i=argument<0?argument+length:argument;++i<length;)
{matchIndexes.push(i);}
return matchIndexes;})}};function siblingCheck(a,b,ret)
{if(a===b)
{return ret;}
var cur=a.nextSibling;while(cur)
{if(cur===b)
{return-1;}
cur=cur.nextSibling;}
return 1;}
sortOrder=docElem.compareDocumentPosition?function(a,b)
{if(a===b)
{hasDuplicate=true;return 0;}
return(!a.compareDocumentPosition||!b.compareDocumentPosition?a.compareDocumentPosition:a.compareDocumentPosition(b)&4)?-1:1;}:function(a,b)
{if(a===b)
{hasDuplicate=true;return 0;}else if(a.sourceIndex&&b.sourceIndex)
{return a.sourceIndex-b.sourceIndex;}
var al,bl,ap=[],bp=[],aup=a.parentNode,bup=b.parentNode,cur=aup;if(aup===bup)
{return siblingCheck(a,b);}else if(!aup)
{return-1;}else if(!bup)
{return 1;}
while(cur)
{ap.unshift(cur);cur=cur.parentNode;}
cur=bup;while(cur)
{bp.unshift(cur);cur=cur.parentNode;}
al=ap.length;bl=bp.length;for(var i=0;i<al&&i<bl;i++)
{if(ap[i]!==bp[i])
{return siblingCheck(ap[i],bp[i]);}}
return i===al?siblingCheck(a,bp[i],-1):siblingCheck(ap[i],b,1);};[0,0].sort(sortOrder);baseHasDuplicate=!hasDuplicate;Sizzle.uniqueSort=function(results)
{var elem,duplicates=[],i=1,j=0;hasDuplicate=baseHasDuplicate;results.sort(sortOrder);if(hasDuplicate)
{for(;(elem=results[i]);i++)
{if(elem===results[i-1])
{j=duplicates.push(i);}}
while(j--)
{results.splice(duplicates[j],1);}}
return results;};Sizzle.error=function(msg)
{throw new Error("Syntax error, unrecognized expression: "+msg);};function tokenize(selector,parseOnly)
{var matched,match,tokens,type,soFar,groups,preFilters,cached=tokenCache[expando][selector+" "];if(cached)
{return parseOnly?0:cached.slice(0);}
soFar=selector;groups=[];preFilters=Expr.preFilter;while(soFar)
{if(!matched||(match=rcomma.exec(soFar)))
{if(match)
{soFar=soFar.slice(match[0].length)||soFar;}
groups.push(tokens=[]);}
matched=false;if((match=rcombinators.exec(soFar)))
{tokens.push(matched=new Token(match.shift()));soFar=soFar.slice(matched.length);matched.type=match[0].replace(rtrim," ");}
for(type in Expr.filter)
{if((match=matchExpr[type].exec(soFar))&&(!preFilters[type]||(match=preFilters[type](match))))
{tokens.push(matched=new Token(match.shift()));soFar=soFar.slice(matched.length);matched.type=type;matched.matches=match;}}
if(!matched)
{break;}}
return parseOnly?soFar.length:soFar?Sizzle.error(selector):tokenCache(selector,groups).slice(0);}
function addCombinator(matcher,combinator,base)
{var dir=combinator.dir,checkNonElements=base&&combinator.dir==="parentNode",doneName=done++;return combinator.first?function(elem,context,xml)
{while((elem=elem[dir]))
{if(checkNonElements||elem.nodeType===1)
{return matcher(elem,context,xml);}}}:function(elem,context,xml)
{if(!xml)
{var cache,dirkey=dirruns+" "+doneName+" ",cachedkey=dirkey+cachedruns;while((elem=elem[dir]))
{if(checkNonElements||elem.nodeType===1)
{if((cache=elem[expando])===cachedkey)
{return elem.sizset;}else if(typeof cache==="string"&&cache.indexOf(dirkey)===0)
{if(elem.sizset)
{return elem;}}else
{elem[expando]=cachedkey;if(matcher(elem,context,xml))
{elem.sizset=true;return elem;}
elem.sizset=false;}}}}else
{while((elem=elem[dir]))
{if(checkNonElements||elem.nodeType===1)
{if(matcher(elem,context,xml))
{return elem;}}}}};}
function elementMatcher(matchers)
{return matchers.length>1?function(elem,context,xml)
{var i=matchers.length;while(i--)
{if(!matchers[i](elem,context,xml))
{return false;}}
return true;}:matchers[0];}
function condense(unmatched,map,filter,context,xml)
{var elem,newUnmatched=[],i=0,len=unmatched.length,mapped=map!=null;for(;i<len;i++)
{if((elem=unmatched[i]))
{if(!filter||filter(elem,context,xml))
{newUnmatched.push(elem);if(mapped)
{map.push(i);}}}}
return newUnmatched;}
function setMatcher(preFilter,selector,matcher,postFilter,postFinder,postSelector)
{if(postFilter&&!postFilter[expando])
{postFilter=setMatcher(postFilter);}
if(postFinder&&!postFinder[expando])
{postFinder=setMatcher(postFinder,postSelector);}
return markFunction(function(seed,results,context,xml)
{var temp,i,elem,preMap=[],postMap=[],preexisting=results.length,elems=seed||multipleContexts(selector||"*",context.nodeType?[context]:context,[]),matcherIn=preFilter&&(seed||!selector)?condense(elems,preMap,preFilter,context,xml):elems,matcherOut=matcher?postFinder||(seed?preFilter:preexisting||postFilter)?[]:results:matcherIn;if(matcher)
{matcher(matcherIn,matcherOut,context,xml);}
if(postFilter)
{temp=condense(matcherOut,postMap);postFilter(temp,[],context,xml);i=temp.length;while(i--)
{if((elem=temp[i]))
{matcherOut[postMap[i]]=!(matcherIn[postMap[i]]=elem);}}}
if(seed)
{if(postFinder||preFilter)
{if(postFinder)
{temp=[];i=matcherOut.length;while(i--)
{if((elem=matcherOut[i]))
{temp.push((matcherIn[i]=elem));}}
postFinder(null,(matcherOut=[]),temp,xml);}
i=matcherOut.length;while(i--)
{if((elem=matcherOut[i])&&(temp=postFinder?indexOf.call(seed,elem):preMap[i])>-1)
{seed[temp]=!(results[temp]=elem);}}}}else
{matcherOut=condense(matcherOut===results?matcherOut.splice(preexisting,matcherOut.length):matcherOut);if(postFinder)
{postFinder(null,results,matcherOut,xml);}else
{push.apply(results,matcherOut);}}});}
function matcherFromTokens(tokens)
{var checkContext,matcher,j,len=tokens.length,leadingRelative=Expr.relative[tokens[0].type],implicitRelative=leadingRelative||Expr.relative[" "],i=leadingRelative?1:0,matchContext=addCombinator(function(elem)
{return elem===checkContext;},implicitRelative,true),matchAnyContext=addCombinator(function(elem)
{return indexOf.call(checkContext,elem)>-1;},implicitRelative,true),matchers=[function(elem,context,xml)
{return(!leadingRelative&&(xml||context!==outermostContext))||((checkContext=context).nodeType?matchContext(elem,context,xml):matchAnyContext(elem,context,xml));}];for(;i<len;i++)
{if((matcher=Expr.relative[tokens[i].type]))
{matchers=[addCombinator(elementMatcher(matchers),matcher)];}else
{matcher=Expr.filter[tokens[i].type].apply(null,tokens[i].matches);if(matcher[expando])
{j=++i;for(;j<len;j++)
{if(Expr.relative[tokens[j].type])
{break;}}
return setMatcher(i>1&&elementMatcher(matchers),i>1&&tokens.slice(0,i-1).join("").replace(rtrim,"$1"),matcher,i<j&&matcherFromTokens(tokens.slice(i,j)),j<len&&matcherFromTokens((tokens=tokens.slice(j))),j<len&&tokens.join(""));}
matchers.push(matcher);}}
return elementMatcher(matchers);}
function matcherFromGroupMatchers(elementMatchers,setMatchers)
{var bySet=setMatchers.length>0,byElement=elementMatchers.length>0,superMatcher=function(seed,context,xml,results,expandContext)
{var elem,j,matcher,setMatched=[],matchedCount=0,i="0",unmatched=seed&&[],outermost=expandContext!=null,contextBackup=outermostContext,elems=seed||byElement&&Expr.find["TAG"]("*",expandContext&&context.parentNode||context),dirrunsUnique=(dirruns+=contextBackup==null?1:Math.E);if(outermost)
{outermostContext=context!==document&&context;cachedruns=superMatcher.el;}
for(;(elem=elems[i])!=null;i++)
{if(byElement&&elem)
{for(j=0;(matcher=elementMatchers[j]);j++)
{if(matcher(elem,context,xml))
{results.push(elem);break;}}
if(outermost)
{dirruns=dirrunsUnique;cachedruns=++superMatcher.el;}}
if(bySet)
{if((elem=!matcher&&elem))
{matchedCount--;}
if(seed)
{unmatched.push(elem);}}}
matchedCount+=i;if(bySet&&i!==matchedCount)
{for(j=0;(matcher=setMatchers[j]);j++)
{matcher(unmatched,setMatched,context,xml);}
if(seed)
{if(matchedCount>0)
{while(i--)
{if(!(unmatched[i]||setMatched[i]))
{setMatched[i]=pop.call(results);}}}
setMatched=condense(setMatched);}
push.apply(results,setMatched);if(outermost&&!seed&&setMatched.length>0&&(matchedCount+setMatchers.length)>1)
{Sizzle.uniqueSort(results);}}
if(outermost)
{dirruns=dirrunsUnique;outermostContext=contextBackup;}
return unmatched;};superMatcher.el=0;return bySet?markFunction(superMatcher):superMatcher;}
compile=Sizzle.compile=function(selector,group)
{var i,setMatchers=[],elementMatchers=[],cached=compilerCache[expando][selector+" "];if(!cached)
{if(!group)
{group=tokenize(selector);}
i=group.length;while(i--)
{cached=matcherFromTokens(group[i]);if(cached[expando])
{setMatchers.push(cached);}else
{elementMatchers.push(cached);}}
cached=compilerCache(selector,matcherFromGroupMatchers(elementMatchers,setMatchers));}
return cached;};function multipleContexts(selector,contexts,results)
{var i=0,len=contexts.length;for(;i<len;i++)
{Sizzle(selector,contexts[i],results);}
return results;}
function select(selector,context,results,seed,xml)
{var i,tokens,token,type,find,match=tokenize(selector),j=match.length;if(!seed)
{if(match.length===1)
{tokens=match[0]=match[0].slice(0);if(tokens.length>2&&(token=tokens[0]).type==="ID"&&context.nodeType===9&&!xml&&Expr.relative[tokens[1].type])
{context=Expr.find["ID"](token.matches[0].replace(rbackslash,""),context,xml)[0];if(!context)
{return results;}
selector=selector.slice(tokens.shift().length);}
for(i=matchExpr["POS"].test(selector)?-1:tokens.length-1;i>=0;i--)
{token=tokens[i];if(Expr.relative[(type=token.type)])
{break;}
if((find=Expr.find[type]))
{if((seed=find(token.matches[0].replace(rbackslash,""),rsibling.test(tokens[0].type)&&context.parentNode||context,xml)))
{tokens.splice(i,1);selector=seed.length&&tokens.join("");if(!selector)
{push.apply(results,slice.call(seed,0));return results;}
break;}}}}}
compile(selector,match)(seed,context,xml,results,rsibling.test(selector));return results;}
if(document.querySelectorAll)
{(function()
{var disconnectedMatch,oldSelect=select,rescape=/'|\\/g,rattributeQuotes=/\=[\x20\t\r\n\f]*([^'"\]]*)[\x20\t\r\n\f]*\]/g,rbuggyQSA=[":focus"],rbuggyMatches=[":active"],matches=docElem.matchesSelector||docElem.mozMatchesSelector||docElem.webkitMatchesSelector||docElem.oMatchesSelector||docElem.msMatchesSelector;assert(function(div)
{div.innerHTML="<select><option selected=''></option></select>";if(!div.querySelectorAll("[selected]").length)
{rbuggyQSA.push("\\["+whitespace+"*(?:checked|disabled|ismap|multiple|readonly|selected|value)");}
if(!div.querySelectorAll(":checked").length)
{rbuggyQSA.push(":checked");}});assert(function(div)
{div.innerHTML="<p test=''></p>";if(div.querySelectorAll("[test^='']").length)
{rbuggyQSA.push("[*^$]="+whitespace+"*(?:\"\"|'')");}
div.innerHTML="<input type='hidden'/>";if(!div.querySelectorAll(":enabled").length)
{rbuggyQSA.push(":enabled",":disabled");}});rbuggyQSA=new RegExp(rbuggyQSA.join("|"));select=function(selector,context,results,seed,xml)
{if(!seed&&!xml&&!rbuggyQSA.test(selector))
{var groups,i,old=true,nid=expando,newContext=context,newSelector=context.nodeType===9&&selector;if(context.nodeType===1&&context.nodeName.toLowerCase()!=="object")
{groups=tokenize(selector);if((old=context.getAttribute("id")))
{nid=old.replace(rescape,"\\$&");}else
{context.setAttribute("id",nid);}
nid="[id='"+nid+"'] ";i=groups.length;while(i--)
{groups[i]=nid+groups[i].join("");}
newContext=rsibling.test(selector)&&context.parentNode||context;newSelector=groups.join(",");}
if(newSelector)
{try
{push.apply(results,slice.call(newContext.querySelectorAll(newSelector),0));return results;}catch(qsaError)
{}finally
{if(!old)
{context.removeAttribute("id");}}}}
return oldSelect(selector,context,results,seed,xml);};if(matches)
{assert(function(div)
{disconnectedMatch=matches.call(div,"div");try
{matches.call(div,"[test!='']:sizzle");rbuggyMatches.push("!=",pseudos);}catch(e)
{}});rbuggyMatches=new RegExp(rbuggyMatches.join("|"));Sizzle.matchesSelector=function(elem,expr)
{expr=expr.replace(rattributeQuotes,"='$1']");if(!isXML(elem)&&!rbuggyMatches.test(expr)&&!rbuggyQSA.test(expr))
{try
{var ret=matches.call(elem,expr);if(ret||disconnectedMatch||elem.document&&elem.document.nodeType!==11)
{return ret;}}catch(e)
{}}
return Sizzle(expr,null,null,[elem]).length>0;};}})();}
Expr.pseudos["nth"]=Expr.pseudos["eq"];function setFilters()
{}
Expr.filters=setFilters.prototype=Expr.pseudos;Expr.setFilters=new setFilters();Sizzle.attr=jQuery.attr;jQuery.find=Sizzle;jQuery.expr=Sizzle.selectors;jQuery.expr[":"]=jQuery.expr.pseudos;jQuery.unique=Sizzle.uniqueSort;jQuery.text=Sizzle.getText;jQuery.isXMLDoc=Sizzle.isXML;jQuery.contains=Sizzle.contains;})(window);var runtil=/Until$/,rparentsprev=/^(?:parents|prev(?:Until|All))/,isSimple=/^.[^:#\[\.,]*$/,rneedsContext=jQuery.expr.match.needsContext,guaranteedUnique={children:true,contents:true,next:true,prev:true};jQuery.fn.extend({find:function(selector)
{var i,l,length,n,r,ret,self=this;if(typeof selector!=="string")
{return jQuery(selector).filter(function()
{for(i=0,l=self.length;i<l;i++)
{if(jQuery.contains(self[i],this))
{return true;}}});}
ret=this.pushStack("","find",selector);for(i=0,l=this.length;i<l;i++)
{length=ret.length;jQuery.find(selector,this[i],ret);if(i>0)
{for(n=length;n<ret.length;n++)
{for(r=0;r<length;r++)
{if(ret[r]===ret[n])
{ret.splice(n--,1);break;}}}}}
return ret;},has:function(target)
{var i,targets=jQuery(target,this),len=targets.length;return this.filter(function()
{for(i=0;i<len;i++)
{if(jQuery.contains(this,targets[i]))
{return true;}}});},not:function(selector)
{return this.pushStack(winnow(this,selector,false),"not",selector);},filter:function(selector)
{return this.pushStack(winnow(this,selector,true),"filter",selector);},is:function(selector)
{return!!selector&&(typeof selector==="string"?rneedsContext.test(selector)?jQuery(selector,this.context).index(this[0])>=0:jQuery.filter(selector,this).length>0:this.filter(selector).length>0);},closest:function(selectors,context)
{var cur,i=0,l=this.length,ret=[],pos=rneedsContext.test(selectors)||typeof selectors!=="string"?jQuery(selectors,context||this.context):0;for(;i<l;i++)
{cur=this[i];while(cur&&cur.ownerDocument&&cur!==context&&cur.nodeType!==11)
{if(pos?pos.index(cur)>-1:jQuery.find.matchesSelector(cur,selectors))
{ret.push(cur);break;}
cur=cur.parentNode;}}
ret=ret.length>1?jQuery.unique(ret):ret;return this.pushStack(ret,"closest",selectors);},index:function(elem)
{if(!elem)
{return(this[0]&&this[0].parentNode)?this.prevAll().length:-1;}
if(typeof elem==="string")
{return jQuery.inArray(this[0],jQuery(elem));}
return jQuery.inArray(elem.jquery?elem[0]:elem,this);},add:function(selector,context)
{var set=typeof selector==="string"?jQuery(selector,context):jQuery.makeArray(selector&&selector.nodeType?[selector]:selector),all=jQuery.merge(this.get(),set);return this.pushStack(isDisconnected(set[0])||isDisconnected(all[0])?all:jQuery.unique(all));},addBack:function(selector)
{return this.add(selector==null?this.prevObject:this.prevObject.filter(selector));}});jQuery.fn.andSelf=jQuery.fn.addBack;function isDisconnected(node)
{return!node||!node.parentNode||node.parentNode.nodeType===11;}
function sibling(cur,dir)
{do{cur=cur[dir];}while(cur&&cur.nodeType!==1);return cur;}
jQuery.each({parent:function(elem)
{var parent=elem.parentNode;return parent&&parent.nodeType!==11?parent:null;},parents:function(elem)
{return jQuery.dir(elem,"parentNode");},parentsUntil:function(elem,i,until)
{return jQuery.dir(elem,"parentNode",until);},next:function(elem)
{return sibling(elem,"nextSibling");},prev:function(elem)
{return sibling(elem,"previousSibling");},nextAll:function(elem)
{return jQuery.dir(elem,"nextSibling");},prevAll:function(elem)
{return jQuery.dir(elem,"previousSibling");},nextUntil:function(elem,i,until)
{return jQuery.dir(elem,"nextSibling",until);},prevUntil:function(elem,i,until)
{return jQuery.dir(elem,"previousSibling",until);},siblings:function(elem)
{return jQuery.sibling((elem.parentNode||{}).firstChild,elem);},children:function(elem)
{return jQuery.sibling(elem.firstChild);},contents:function(elem)
{return jQuery.nodeName(elem,"iframe")?elem.contentDocument||elem.contentWindow.document:jQuery.merge([],elem.childNodes);}},function(name,fn)
{jQuery.fn[name]=function(until,selector)
{var ret=jQuery.map(this,fn,until);if(!runtil.test(name))
{selector=until;}
if(selector&&typeof selector==="string")
{ret=jQuery.filter(selector,ret);}
ret=this.length>1&&!guaranteedUnique[name]?jQuery.unique(ret):ret;if(this.length>1&&rparentsprev.test(name))
{ret=ret.reverse();}
return this.pushStack(ret,name,core_slice.call(arguments).join(","));};});jQuery.extend({filter:function(expr,elems,not)
{if(not)
{expr=":not("+expr+")";}
return elems.length===1?jQuery.find.matchesSelector(elems[0],expr)?[elems[0]]:[]:jQuery.find.matches(expr,elems);},dir:function(elem,dir,until)
{var matched=[],cur=elem[dir];while(cur&&cur.nodeType!==9&&(until===undefined||cur.nodeType!==1||!jQuery(cur).is(until)))
{if(cur.nodeType===1)
{matched.push(cur);}
cur=cur[dir];}
return matched;},sibling:function(n,elem)
{var r=[];for(;n;n=n.nextSibling)
{if(n.nodeType===1&&n!==elem)
{r.push(n);}}
return r;}});function winnow(elements,qualifier,keep)
{qualifier=qualifier||0;if(jQuery.isFunction(qualifier))
{return jQuery.grep(elements,function(elem,i)
{var retVal=!!qualifier.call(elem,i,elem);return retVal===keep;});}else if(qualifier.nodeType)
{return jQuery.grep(elements,function(elem,i)
{return(elem===qualifier)===keep;});}else if(typeof qualifier==="string")
{var filtered=jQuery.grep(elements,function(elem)
{return elem.nodeType===1;});if(isSimple.test(qualifier))
{return jQuery.filter(qualifier,filtered,!keep);}else
{qualifier=jQuery.filter(qualifier,filtered);}}
return jQuery.grep(elements,function(elem,i)
{return(jQuery.inArray(elem,qualifier)>=0)===keep;});}
function createSafeFragment(document)
{var list=nodeNames.split("|"),safeFrag=document.createDocumentFragment();if(safeFrag.createElement)
{while(list.length)
{safeFrag.createElement(list.pop());}}
return safeFrag;}
var nodeNames="abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|"+"header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",rinlinejQuery=/ jQuery\d+="(?:null|\d+)"/g,rleadingWhitespace=/^\s+/,rxhtmlTag=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,rtagName=/<([\w:]+)/,rtbody=/<tbody/i,rhtml=/<|&#?\w+;/,rnoInnerhtml=/<(?:script|style|link)/i,rnocache=/<(?:script|object|embed|option|style)/i,rnoshimcache=new RegExp("<(?:"+nodeNames+")[\\s/>]","i"),rcheckableType=/^(?:checkbox|radio)$/,rchecked=/checked\s*(?:[^=]|=\s*.checked.)/i,rscriptType=/\/(java|ecma)script/i,rcleanScript=/^\s*<!(?:\[CDATA\[|\-\-)|[\]\-]{2}>\s*$/g,wrapMap={option:[1,"<select multiple='multiple'>","</select>"],legend:[1,"<fieldset>","</fieldset>"],thead:[1,"<table>","</table>"],tr:[2,"<table><tbody>","</tbody></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],col:[2,"<table><tbody></tbody><colgroup>","</colgroup></table>"],area:[1,"<map>","</map>"],_default:[0,"",""]},safeFragment=createSafeFragment(document),fragmentDiv=safeFragment.appendChild(document.createElement("div"));wrapMap.optgroup=wrapMap.option;wrapMap.tbody=wrapMap.tfoot=wrapMap.colgroup=wrapMap.caption=wrapMap.thead;wrapMap.th=wrapMap.td;if(!jQuery.support.htmlSerialize)
{wrapMap._default=[1,"X<div>","</div>"];}
jQuery.fn.extend({text:function(value)
{return jQuery.access(this,function(value)
{return value===undefined?jQuery.text(this):this.empty().append((this[0]&&this[0].ownerDocument||document).createTextNode(value));},null,value,arguments.length);},wrapAll:function(html)
{if(jQuery.isFunction(html))
{return this.each(function(i)
{jQuery(this).wrapAll(html.call(this,i));});}
if(this[0])
{var wrap=jQuery(html,this[0].ownerDocument).eq(0).clone(true);if(this[0].parentNode)
{wrap.insertBefore(this[0]);}
wrap.map(function()
{var elem=this;while(elem.firstChild&&elem.firstChild.nodeType===1)
{elem=elem.firstChild;}
return elem;}).append(this);}
return this;},wrapInner:function(html)
{if(jQuery.isFunction(html))
{return this.each(function(i)
{jQuery(this).wrapInner(html.call(this,i));});}
return this.each(function()
{var self=jQuery(this),contents=self.contents();if(contents.length)
{contents.wrapAll(html);}else
{self.append(html);}});},wrap:function(html)
{var isFunction=jQuery.isFunction(html);return this.each(function(i)
{jQuery(this).wrapAll(isFunction?html.call(this,i):html);});},unwrap:function()
{return this.parent().each(function()
{if(!jQuery.nodeName(this,"body"))
{jQuery(this).replaceWith(this.childNodes);}}).end();},append:function()
{return this.domManip(arguments,true,function(elem)
{if(this.nodeType===1||this.nodeType===11)
{this.appendChild(elem);}});},prepend:function()
{return this.domManip(arguments,true,function(elem)
{if(this.nodeType===1||this.nodeType===11)
{this.insertBefore(elem,this.firstChild);}});},before:function()
{if(!isDisconnected(this[0]))
{return this.domManip(arguments,false,function(elem)
{this.parentNode.insertBefore(elem,this);});}
if(arguments.length)
{var set=jQuery.clean(arguments);return this.pushStack(jQuery.merge(set,this),"before",this.selector);}},after:function()
{if(!isDisconnected(this[0]))
{return this.domManip(arguments,false,function(elem)
{this.parentNode.insertBefore(elem,this.nextSibling);});}
if(arguments.length)
{var set=jQuery.clean(arguments);return this.pushStack(jQuery.merge(this,set),"after",this.selector);}},remove:function(selector,keepData)
{var elem,i=0;for(;(elem=this[i])!=null;i++)
{if(!selector||jQuery.filter(selector,[elem]).length)
{if(!keepData&&elem.nodeType===1)
{jQuery.cleanData(elem.getElementsByTagName("*"));jQuery.cleanData([elem]);}
if(elem.parentNode)
{elem.parentNode.removeChild(elem);}}}
return this;},empty:function()
{var elem,i=0;for(;(elem=this[i])!=null;i++)
{if(elem.nodeType===1)
{jQuery.cleanData(elem.getElementsByTagName("*"));}
while(elem.firstChild)
{elem.removeChild(elem.firstChild);}}
return this;},clone:function(dataAndEvents,deepDataAndEvents)
{dataAndEvents=dataAndEvents==null?false:dataAndEvents;deepDataAndEvents=deepDataAndEvents==null?dataAndEvents:deepDataAndEvents;return this.map(function()
{return jQuery.clone(this,dataAndEvents,deepDataAndEvents);});},html:function(value)
{return jQuery.access(this,function(value)
{var elem=this[0]||{},i=0,l=this.length;if(value===undefined)
{return elem.nodeType===1?elem.innerHTML.replace(rinlinejQuery,""):undefined;}
if(typeof value==="string"&&!rnoInnerhtml.test(value)&&(jQuery.support.htmlSerialize||!rnoshimcache.test(value))&&(jQuery.support.leadingWhitespace||!rleadingWhitespace.test(value))&&!wrapMap[(rtagName.exec(value)||["",""])[1].toLowerCase()])
{value=value.replace(rxhtmlTag,"<$1></$2>");try
{for(;i<l;i++)
{elem=this[i]||{};if(elem.nodeType===1)
{jQuery.cleanData(elem.getElementsByTagName("*"));elem.innerHTML=value;}}
elem=0;}catch(e)
{}}
if(elem)
{this.empty().append(value);}},null,value,arguments.length);},replaceWith:function(value)
{if(!isDisconnected(this[0]))
{if(jQuery.isFunction(value))
{return this.each(function(i)
{var self=jQuery(this),old=self.html();self.replaceWith(value.call(this,i,old));});}
if(typeof value!=="string")
{value=jQuery(value).detach();}
return this.each(function()
{var next=this.nextSibling,parent=this.parentNode;jQuery(this).remove();if(next)
{jQuery(next).before(value);}else
{jQuery(parent).append(value);}});}
return this.length?this.pushStack(jQuery(jQuery.isFunction(value)?value():value),"replaceWith",value):this;},detach:function(selector)
{return this.remove(selector,true);},domManip:function(args,table,callback)
{args=[].concat.apply([],args);var results,first,fragment,iNoClone,i=0,value=args[0],scripts=[],l=this.length;if(!jQuery.support.checkClone&&l>1&&typeof value==="string"&&rchecked.test(value))
{return this.each(function()
{jQuery(this).domManip(args,table,callback);});}
if(jQuery.isFunction(value))
{return this.each(function(i)
{var self=jQuery(this);args[0]=value.call(this,i,table?self.html():undefined);self.domManip(args,table,callback);});}
if(this[0])
{results=jQuery.buildFragment(args,this,scripts);fragment=results.fragment;first=fragment.firstChild;if(fragment.childNodes.length===1)
{fragment=first;}
if(first)
{table=table&&jQuery.nodeName(first,"tr");for(iNoClone=results.cacheable||l-1;i<l;i++)
{callback.call(table&&jQuery.nodeName(this[i],"table")?findOrAppend(this[i],"tbody"):this[i],i===iNoClone?fragment:jQuery.clone(fragment,true,true));}}
fragment=first=null;if(scripts.length)
{jQuery.each(scripts,function(i,elem)
{if(elem.src)
{if(jQuery.ajax)
{jQuery.ajax({url:elem.src,type:"GET",dataType:"script",async:false,global:false,"throws":true});}else
{jQuery.error("no ajax");}}else
{jQuery.globalEval((elem.text||elem.textContent||elem.innerHTML||"").replace(rcleanScript,""));}
if(elem.parentNode)
{elem.parentNode.removeChild(elem);}});}}
return this;}});function findOrAppend(elem,tag)
{return elem.getElementsByTagName(tag)[0]||elem.appendChild(elem.ownerDocument.createElement(tag));}
function cloneCopyEvent(src,dest)
{if(dest.nodeType!==1||!jQuery.hasData(src))
{return;}
var type,i,l,oldData=jQuery._data(src),curData=jQuery._data(dest,oldData),events=oldData.events;if(events)
{delete curData.handle;curData.events={};for(type in events)
{for(i=0,l=events[type].length;i<l;i++)
{jQuery.event.add(dest,type,events[type][i]);}}}
if(curData.data)
{curData.data=jQuery.extend({},curData.data);}}
function cloneFixAttributes(src,dest)
{var nodeName;if(dest.nodeType!==1)
{return;}
if(dest.clearAttributes)
{dest.clearAttributes();}
if(dest.mergeAttributes)
{dest.mergeAttributes(src);}
nodeName=dest.nodeName.toLowerCase();if(nodeName==="object")
{if(dest.parentNode)
{dest.outerHTML=src.outerHTML;}
if(jQuery.support.html5Clone&&(src.innerHTML&&!jQuery.trim(dest.innerHTML)))
{dest.innerHTML=src.innerHTML;}}else if(nodeName==="input"&&rcheckableType.test(src.type))
{dest.defaultChecked=dest.checked=src.checked;if(dest.value!==src.value)
{dest.value=src.value;}}else if(nodeName==="option")
{dest.selected=src.defaultSelected;}else if(nodeName==="input"||nodeName==="textarea")
{dest.defaultValue=src.defaultValue;}else if(nodeName==="script"&&dest.text!==src.text)
{dest.text=src.text;}
dest.removeAttribute(jQuery.expando);}
jQuery.buildFragment=function(args,context,scripts)
{var fragment,cacheable,cachehit,first=args[0];context=context||document;context=!context.nodeType&&context[0]||context;context=context.ownerDocument||context;if(args.length===1&&typeof first==="string"&&first.length<512&&context===document&&first.charAt(0)==="<"&&!rnocache.test(first)&&(jQuery.support.checkClone||!rchecked.test(first))&&(jQuery.support.html5Clone||!rnoshimcache.test(first)))
{cacheable=true;fragment=jQuery.fragments[first];cachehit=fragment!==undefined;}
if(!fragment)
{fragment=context.createDocumentFragment();jQuery.clean(args,context,fragment,scripts);if(cacheable)
{jQuery.fragments[first]=cachehit&&fragment;}}
return{fragment:fragment,cacheable:cacheable};};jQuery.fragments={};jQuery.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(name,original)
{jQuery.fn[name]=function(selector)
{var elems,i=0,ret=[],insert=jQuery(selector),l=insert.length,parent=this.length===1&&this[0].parentNode;if((parent==null||parent&&parent.nodeType===11&&parent.childNodes.length===1)&&l===1)
{insert[original](this[0]);return this;}else
{for(;i<l;i++)
{elems=(i>0?this.clone(true):this).get();jQuery(insert[i])[original](elems);ret=ret.concat(elems);}
return this.pushStack(ret,name,insert.selector);}};});function getAll(elem)
{if(typeof elem.getElementsByTagName!=="undefined")
{return elem.getElementsByTagName("*");}else if(typeof elem.querySelectorAll!=="undefined")
{return elem.querySelectorAll("*");}else
{return[];}}
function fixDefaultChecked(elem)
{if(rcheckableType.test(elem.type))
{elem.defaultChecked=elem.checked;}}
jQuery.extend({clone:function(elem,dataAndEvents,deepDataAndEvents)
{var srcElements,destElements,i,clone;if(jQuery.support.html5Clone||jQuery.isXMLDoc(elem)||!rnoshimcache.test("<"+elem.nodeName+">"))
{clone=elem.cloneNode(true);}else
{fragmentDiv.innerHTML=elem.outerHTML;fragmentDiv.removeChild(clone=fragmentDiv.firstChild);}
if((!jQuery.support.noCloneEvent||!jQuery.support.noCloneChecked)&&(elem.nodeType===1||elem.nodeType===11)&&!jQuery.isXMLDoc(elem))
{cloneFixAttributes(elem,clone);srcElements=getAll(elem);destElements=getAll(clone);for(i=0;srcElements[i];++i)
{if(destElements[i])
{cloneFixAttributes(srcElements[i],destElements[i]);}}}
if(dataAndEvents)
{cloneCopyEvent(elem,clone);if(deepDataAndEvents)
{srcElements=getAll(elem);destElements=getAll(clone);for(i=0;srcElements[i];++i)
{cloneCopyEvent(srcElements[i],destElements[i]);}}}
srcElements=destElements=null;return clone;},clean:function(elems,context,fragment,scripts)
{var i,j,elem,tag,wrap,depth,div,hasBody,tbody,len,handleScript,jsTags,safe=context===document&&safeFragment,ret=[];if(!context||typeof context.createDocumentFragment==="undefined")
{context=document;}
for(i=0;(elem=elems[i])!=null;i++)
{if(typeof elem==="number")
{elem+="";}
if(!elem)
{continue;}
if(typeof elem==="string")
{if(!rhtml.test(elem))
{elem=context.createTextNode(elem);}else
{safe=safe||createSafeFragment(context);div=context.createElement("div");safe.appendChild(div);elem=elem.replace(rxhtmlTag,"<$1></$2>");tag=(rtagName.exec(elem)||["",""])[1].toLowerCase();wrap=wrapMap[tag]||wrapMap._default;depth=wrap[0];div.innerHTML=wrap[1]+elem+wrap[2];while(depth--)
{div=div.lastChild;}
if(!jQuery.support.tbody)
{hasBody=rtbody.test(elem);tbody=tag==="table"&&!hasBody?div.firstChild&&div.firstChild.childNodes:wrap[1]==="<table>"&&!hasBody?div.childNodes:[];for(j=tbody.length-1;j>=0;--j)
{if(jQuery.nodeName(tbody[j],"tbody")&&!tbody[j].childNodes.length)
{tbody[j].parentNode.removeChild(tbody[j]);}}}
if(!jQuery.support.leadingWhitespace&&rleadingWhitespace.test(elem))
{div.insertBefore(context.createTextNode(rleadingWhitespace.exec(elem)[0]),div.firstChild);}
elem=div.childNodes;div.parentNode.removeChild(div);}}
if(elem.nodeType)
{ret.push(elem);}else
{jQuery.merge(ret,elem);}}
if(div)
{elem=div=safe=null;}
if(!jQuery.support.appendChecked)
{for(i=0;(elem=ret[i])!=null;i++)
{if(jQuery.nodeName(elem,"input"))
{fixDefaultChecked(elem);}else if(typeof elem.getElementsByTagName!=="undefined")
{jQuery.grep(elem.getElementsByTagName("input"),fixDefaultChecked);}}}
if(fragment)
{handleScript=function(elem)
{if(!elem.type||rscriptType.test(elem.type))
{return scripts?scripts.push(elem.parentNode?elem.parentNode.removeChild(elem):elem):fragment.appendChild(elem);}};for(i=0;(elem=ret[i])!=null;i++)
{if(!(jQuery.nodeName(elem,"script")&&handleScript(elem)))
{fragment.appendChild(elem);if(typeof elem.getElementsByTagName!=="undefined")
{jsTags=jQuery.grep(jQuery.merge([],elem.getElementsByTagName("script")),handleScript);ret.splice.apply(ret,[i+1,0].concat(jsTags));i+=jsTags.length;}}}}
return ret;},cleanData:function(elems,acceptData)
{var data,id,elem,type,i=0,internalKey=jQuery.expando,cache=jQuery.cache,deleteExpando=jQuery.support.deleteExpando,special=jQuery.event.special;for(;(elem=elems[i])!=null;i++)
{if(acceptData||jQuery.acceptData(elem))
{id=elem[internalKey];data=id&&cache[id];if(data)
{if(data.events)
{for(type in data.events)
{if(special[type])
{jQuery.event.remove(elem,type);}else
{jQuery.removeEvent(elem,type,data.handle);}}}
if(cache[id])
{delete cache[id];if(deleteExpando)
{delete elem[internalKey];}else if(elem.removeAttribute)
{elem.removeAttribute(internalKey);}else
{elem[internalKey]=null;}
jQuery.deletedIds.push(id);}}}}}});(function()
{var matched,browser;jQuery.uaMatch=function(ua)
{ua=ua.toLowerCase();var match=/(chrome)[ \/]([\w.]+)/.exec(ua)||/(webkit)[ \/]([\w.]+)/.exec(ua)||/(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua)||/(msie) ([\w.]+)/.exec(ua)||ua.indexOf("compatible")<0&&/(mozilla)(?:.*? rv:([\w.]+)|)/.exec(ua)||[];return{browser:match[1]||"",version:match[2]||"0"};};matched=jQuery.uaMatch(navigator.userAgent);browser={};if(matched.browser)
{browser[matched.browser]=true;browser.version=matched.version;}
if(browser.chrome)
{browser.webkit=true;}else if(browser.webkit)
{browser.safari=true;}
jQuery.browser=browser;jQuery.sub=function()
{function jQuerySub(selector,context)
{return new jQuerySub.fn.init(selector,context);}
jQuery.extend(true,jQuerySub,this);jQuerySub.superclass=this;jQuerySub.fn=jQuerySub.prototype=this();jQuerySub.fn.constructor=jQuerySub;jQuerySub.sub=this.sub;jQuerySub.fn.init=function init(selector,context)
{if(context&&context instanceof jQuery&&!(context instanceof jQuerySub))
{context=jQuerySub(context);}
return jQuery.fn.init.call(this,selector,context,rootjQuerySub);};jQuerySub.fn.init.prototype=jQuerySub.fn;var rootjQuerySub=jQuerySub(document);return jQuerySub;};})();var curCSS,iframe,iframeDoc,ralpha=/alpha\([^)]*\)/i,ropacity=/opacity=([^)]*)/,rposition=/^(top|right|bottom|left)$/,rdisplayswap=/^(none|table(?!-c[ea]).+)/,rmargin=/^margin/,rnumsplit=new RegExp("^("+core_pnum+")(.*)$","i"),rnumnonpx=new RegExp("^("+core_pnum+")(?!px)[a-z%]+$","i"),rrelNum=new RegExp("^([-+])=("+core_pnum+")","i"),elemdisplay={BODY:"block"},cssShow={position:"absolute",visibility:"hidden",display:"block"},cssNormalTransform={letterSpacing:0,fontWeight:400},cssExpand=["Top","Right","Bottom","Left"],cssPrefixes=["Webkit","O","Moz","ms"],eventsToggle=jQuery.fn.toggle;function vendorPropName(style,name)
{if(name in style)
{return name;}
var capName=name.charAt(0).toUpperCase()+name.slice(1),origName=name,i=cssPrefixes.length;while(i--)
{name=cssPrefixes[i]+capName;if(name in style)
{return name;}}
return origName;}
function isHidden(elem,el)
{elem=el||elem;return jQuery.css(elem,"display")==="none"||!jQuery.contains(elem.ownerDocument,elem);}
function showHide(elements,show)
{var elem,display,values=[],index=0,length=elements.length;for(;index<length;index++)
{elem=elements[index];if(!elem.style)
{continue;}
values[index]=jQuery._data(elem,"olddisplay");if(show)
{if(!values[index]&&elem.style.display==="none")
{elem.style.display="";}
if(elem.style.display===""&&isHidden(elem))
{values[index]=jQuery._data(elem,"olddisplay",css_defaultDisplay(elem.nodeName));}}else
{display=curCSS(elem,"display");if(!values[index]&&display!=="none")
{jQuery._data(elem,"olddisplay",display);}}}
for(index=0;index<length;index++)
{elem=elements[index];if(!elem.style)
{continue;}
if(!show||elem.style.display==="none"||elem.style.display==="")
{elem.style.display=show?values[index]||"":"none";}}
return elements;}
jQuery.fn.extend({css:function(name,value)
{return jQuery.access(this,function(elem,name,value)
{return value!==undefined?jQuery.style(elem,name,value):jQuery.css(elem,name);},name,value,arguments.length>1);},show:function()
{return showHide(this,true);},hide:function()
{return showHide(this);},toggle:function(state,fn2)
{var bool=typeof state==="boolean";if(jQuery.isFunction(state)&&jQuery.isFunction(fn2))
{return eventsToggle.apply(this,arguments);}
return this.each(function()
{if(bool?state:isHidden(this))
{jQuery(this).show();}else
{jQuery(this).hide();}});}});jQuery.extend({cssHooks:{opacity:{get:function(elem,computed)
{if(computed)
{var ret=curCSS(elem,"opacity");return ret===""?"1":ret;}}}},cssNumber:{"fillOpacity":true,"fontWeight":true,"lineHeight":true,"opacity":true,"orphans":true,"widows":true,"zIndex":true,"zoom":true},cssProps:{"float":jQuery.support.cssFloat?"cssFloat":"styleFloat"},style:function(elem,name,value,extra)
{if(!elem||elem.nodeType===3||elem.nodeType===8||!elem.style)
{return;}
var ret,type,hooks,origName=jQuery.camelCase(name),style=elem.style;name=jQuery.cssProps[origName]||(jQuery.cssProps[origName]=vendorPropName(style,origName));hooks=jQuery.cssHooks[name]||jQuery.cssHooks[origName];if(value!==undefined)
{type=typeof value;if(type==="string"&&(ret=rrelNum.exec(value)))
{value=(ret[1]+1)*ret[2]+parseFloat(jQuery.css(elem,name));type="number";}
if(value==null||type==="number"&&isNaN(value))
{return;}
if(type==="number"&&!jQuery.cssNumber[origName])
{value+="px";}
if(!hooks||!("set"in hooks)||(value=hooks.set(elem,value,extra))!==undefined)
{try
{style[name]=value;}catch(e)
{}}}else
{if(hooks&&"get"in hooks&&(ret=hooks.get(elem,false,extra))!==undefined)
{return ret;}
return style[name];}},css:function(elem,name,numeric,extra)
{var val,num,hooks,origName=jQuery.camelCase(name);name=jQuery.cssProps[origName]||(jQuery.cssProps[origName]=vendorPropName(elem.style,origName));hooks=jQuery.cssHooks[name]||jQuery.cssHooks[origName];if(hooks&&"get"in hooks)
{val=hooks.get(elem,true,extra);}
if(val===undefined)
{val=curCSS(elem,name);}
if(val==="normal"&&name in cssNormalTransform)
{val=cssNormalTransform[name];}
if(numeric||extra!==undefined)
{num=parseFloat(val);return numeric||jQuery.isNumeric(num)?num||0:val;}
return val;},swap:function(elem,options,callback)
{var ret,name,old={};for(name in options)
{old[name]=elem.style[name];elem.style[name]=options[name];}
ret=callback.call(elem);for(name in options)
{elem.style[name]=old[name];}
return ret;}});if(window.getComputedStyle)
{curCSS=function(elem,name)
{var ret,width,minWidth,maxWidth,computed=window.getComputedStyle(elem,null),style=elem.style;if(computed)
{ret=computed.getPropertyValue(name)||computed[name];if(ret===""&&!jQuery.contains(elem.ownerDocument,elem))
{ret=jQuery.style(elem,name);}
if(rnumnonpx.test(ret)&&rmargin.test(name))
{width=style.width;minWidth=style.minWidth;maxWidth=style.maxWidth;style.minWidth=style.maxWidth=style.width=ret;ret=computed.width;style.width=width;style.minWidth=minWidth;style.maxWidth=maxWidth;}}
return ret;};}else if(document.documentElement.currentStyle)
{curCSS=function(elem,name)
{var left,rsLeft,ret=elem.currentStyle&&elem.currentStyle[name],style=elem.style;if(ret==null&&style&&style[name])
{ret=style[name];}
if(rnumnonpx.test(ret)&&!rposition.test(name))
{left=style.left;rsLeft=elem.runtimeStyle&&elem.runtimeStyle.left;if(rsLeft)
{elem.runtimeStyle.left=elem.currentStyle.left;}
style.left=name==="fontSize"?"1em":ret;ret=style.pixelLeft+"px";style.left=left;if(rsLeft)
{elem.runtimeStyle.left=rsLeft;}}
return ret===""?"auto":ret;};}
function setPositiveNumber(elem,value,subtract)
{var matches=rnumsplit.exec(value);return matches?Math.max(0,matches[1]-(subtract||0))+(matches[2]||"px"):value;}
function augmentWidthOrHeight(elem,name,extra,isBorderBox)
{var i=extra===(isBorderBox?"border":"content")?4:name==="width"?1:0,val=0;for(;i<4;i+=2)
{if(extra==="margin")
{val+=jQuery.css(elem,extra+cssExpand[i],true);}
if(isBorderBox)
{if(extra==="content")
{val-=parseFloat(curCSS(elem,"padding"+cssExpand[i]))||0;}
if(extra!=="margin")
{val-=parseFloat(curCSS(elem,"border"+cssExpand[i]+"Width"))||0;}}else
{val+=parseFloat(curCSS(elem,"padding"+cssExpand[i]))||0;if(extra!=="padding")
{val+=parseFloat(curCSS(elem,"border"+cssExpand[i]+"Width"))||0;}}}
return val;}
function getWidthOrHeight(elem,name,extra)
{var val=name==="width"?elem.offsetWidth:elem.offsetHeight,valueIsBorderBox=true,isBorderBox=jQuery.support.boxSizing&&jQuery.css(elem,"boxSizing")==="border-box";if(val<=0||val==null)
{val=curCSS(elem,name);if(val<0||val==null)
{val=elem.style[name];}
if(rnumnonpx.test(val))
{return val;}
valueIsBorderBox=isBorderBox&&(jQuery.support.boxSizingReliable||val===elem.style[name]);val=parseFloat(val)||0;}
return(val+
augmentWidthOrHeight(elem,name,extra||(isBorderBox?"border":"content"),valueIsBorderBox))+"px";}
function css_defaultDisplay(nodeName)
{if(elemdisplay[nodeName])
{return elemdisplay[nodeName];}
var elem=jQuery("<"+nodeName+">").appendTo(document.body),display=elem.css("display");elem.remove();if(display==="none"||display==="")
{iframe=document.body.appendChild(iframe||jQuery.extend(document.createElement("iframe"),{frameBorder:0,width:0,height:0}));if(!iframeDoc||!iframe.createElement)
{iframeDoc=(iframe.contentWindow||iframe.contentDocument).document;iframeDoc.write("<!doctype html><html><body>");iframeDoc.close();}
elem=iframeDoc.body.appendChild(iframeDoc.createElement(nodeName));display=curCSS(elem,"display");document.body.removeChild(iframe);}
elemdisplay[nodeName]=display;return display;}
jQuery.each(["height","width"],function(i,name)
{jQuery.cssHooks[name]={get:function(elem,computed,extra)
{if(computed)
{if(elem.offsetWidth===0&&rdisplayswap.test(curCSS(elem,"display")))
{return jQuery.swap(elem,cssShow,function()
{return getWidthOrHeight(elem,name,extra);});}else
{return getWidthOrHeight(elem,name,extra);}}},set:function(elem,value,extra)
{return setPositiveNumber(elem,value,extra?augmentWidthOrHeight(elem,name,extra,jQuery.support.boxSizing&&jQuery.css(elem,"boxSizing")==="border-box"):0);}};});if(!jQuery.support.opacity)
{jQuery.cssHooks.opacity={get:function(elem,computed)
{return ropacity.test((computed&&elem.currentStyle?elem.currentStyle.filter:elem.style.filter)||"")?(0.01*parseFloat(RegExp.$1))+"":computed?"1":"";},set:function(elem,value)
{var style=elem.style,currentStyle=elem.currentStyle,opacity=jQuery.isNumeric(value)?"alpha(opacity="+value*100+")":"",filter=currentStyle&&currentStyle.filter||style.filter||"";style.zoom=1;if(value>=1&&jQuery.trim(filter.replace(ralpha,""))===""&&style.removeAttribute)
{style.removeAttribute("filter");if(currentStyle&&!currentStyle.filter)
{return;}}
style.filter=ralpha.test(filter)?filter.replace(ralpha,opacity):filter+" "+opacity;}};}
jQuery(function()
{if(!jQuery.support.reliableMarginRight)
{jQuery.cssHooks.marginRight={get:function(elem,computed)
{return jQuery.swap(elem,{"display":"inline-block"},function()
{if(computed)
{return curCSS(elem,"marginRight");}});}};}
if(!jQuery.support.pixelPosition&&jQuery.fn.position)
{jQuery.each(["top","left"],function(i,prop)
{jQuery.cssHooks[prop]={get:function(elem,computed)
{if(computed)
{var ret=curCSS(elem,prop);return rnumnonpx.test(ret)?jQuery(elem).position()[prop]+"px":ret;}}};});}});if(jQuery.expr&&jQuery.expr.filters)
{jQuery.expr.filters.hidden=function(elem)
{return(elem.offsetWidth===0&&elem.offsetHeight===0)||(!jQuery.support.reliableHiddenOffsets&&((elem.style&&elem.style.display)||curCSS(elem,"display"))==="none");};jQuery.expr.filters.visible=function(elem)
{return!jQuery.expr.filters.hidden(elem);};}
jQuery.each({margin:"",padding:"",border:"Width"},function(prefix,suffix)
{jQuery.cssHooks[prefix+suffix]={expand:function(value)
{var i,parts=typeof value==="string"?value.split(" "):[value],expanded={};for(i=0;i<4;i++)
{expanded[prefix+cssExpand[i]+suffix]=parts[i]||parts[i-2]||parts[0];}
return expanded;}};if(!rmargin.test(prefix))
{jQuery.cssHooks[prefix+suffix].set=setPositiveNumber;}});var r20=/%20/g,rbracket=/\[\]$/,rCRLF=/\r?\n/g,rinput=/^(?:color|date|datetime|datetime-local|email|hidden|month|number|password|range|search|tel|text|time|url|week)$/i,rselectTextarea=/^(?:select|textarea)/i;jQuery.fn.extend({serialize:function()
{return jQuery.param(this.serializeArray());},serializeArray:function()
{return this.map(function()
{return this.elements?jQuery.makeArray(this.elements):this;}).filter(function()
{return this.name&&!this.disabled&&(this.checked||rselectTextarea.test(this.nodeName)||rinput.test(this.type));}).map(function(i,elem)
{var val=jQuery(this).val();return val==null?null:jQuery.isArray(val)?jQuery.map(val,function(val,i)
{return{name:elem.name,value:val.replace(rCRLF,"\r\n")};}):{name:elem.name,value:val.replace(rCRLF,"\r\n")};}).get();}});jQuery.param=function(a,traditional)
{var prefix,s=[],add=function(key,value)
{value=jQuery.isFunction(value)?value():(value==null?"":value);s[s.length]=encodeURIComponent(key)+"="+encodeURIComponent(value);};if(traditional===undefined)
{traditional=jQuery.ajaxSettings&&jQuery.ajaxSettings.traditional;}
if(jQuery.isArray(a)||(a.jquery&&!jQuery.isPlainObject(a)))
{jQuery.each(a,function()
{add(this.name,this.value);});}else
{for(prefix in a)
{buildParams(prefix,a[prefix],traditional,add);}}
return s.join("&").replace(r20,"+");};function buildParams(prefix,obj,traditional,add)
{var name;if(jQuery.isArray(obj))
{jQuery.each(obj,function(i,v)
{if(traditional||rbracket.test(prefix))
{add(prefix,v);}else
{buildParams(prefix+"["+(typeof v==="object"?i:"")+"]",v,traditional,add);}});}else if(!traditional&&jQuery.type(obj)==="object")
{for(name in obj)
{buildParams(prefix+"["+name+"]",obj[name],traditional,add);}}else
{add(prefix,obj);}}
var
ajaxLocParts,ajaxLocation,rhash=/#.*$/,rheaders=/^(.*?):[ \t]*([^\r\n]*)\r?$/mg,rlocalProtocol=/^(?:about|app|app\-storage|.+\-extension|file|res|widget):$/,rnoContent=/^(?:GET|HEAD)$/,rprotocol=/^\/\//,rquery=/\?/,rscript=/<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi,rts=/([?&])_=[^&]*/,rurl=/^([\w\+\.\-]+:)(?:\/\/([^\/?#:]*)(?::(\d+)|)|)/,_load=jQuery.fn.load,prefilters={},transports={},allTypes=["*/"]+["*"];try
{ajaxLocation=location.href;}catch(e)
{ajaxLocation=document.createElement("a");ajaxLocation.href="";ajaxLocation=ajaxLocation.href;}
ajaxLocParts=rurl.exec(ajaxLocation.toLowerCase())||[];function addToPrefiltersOrTransports(structure)
{return function(dataTypeExpression,func)
{if(typeof dataTypeExpression!=="string")
{func=dataTypeExpression;dataTypeExpression="*";}
var dataType,list,placeBefore,dataTypes=dataTypeExpression.toLowerCase().split(core_rspace),i=0,length=dataTypes.length;if(jQuery.isFunction(func))
{for(;i<length;i++)
{dataType=dataTypes[i];placeBefore=/^\+/.test(dataType);if(placeBefore)
{dataType=dataType.substr(1)||"*";}
list=structure[dataType]=structure[dataType]||[];list[placeBefore?"unshift":"push"](func);}}};}
function inspectPrefiltersOrTransports(structure,options,originalOptions,jqXHR,dataType,inspected)
{dataType=dataType||options.dataTypes[0];inspected=inspected||{};inspected[dataType]=true;var selection,list=structure[dataType],i=0,length=list?list.length:0,executeOnly=(structure===prefilters);for(;i<length&&(executeOnly||!selection);i++)
{selection=list[i](options,originalOptions,jqXHR);if(typeof selection==="string")
{if(!executeOnly||inspected[selection])
{selection=undefined;}else
{options.dataTypes.unshift(selection);selection=inspectPrefiltersOrTransports(structure,options,originalOptions,jqXHR,selection,inspected);}}}
if((executeOnly||!selection)&&!inspected["*"])
{selection=inspectPrefiltersOrTransports(structure,options,originalOptions,jqXHR,"*",inspected);}
return selection;}
function ajaxExtend(target,src)
{var key,deep,flatOptions=jQuery.ajaxSettings.flatOptions||{};for(key in src)
{if(src[key]!==undefined)
{(flatOptions[key]?target:(deep||(deep={})))[key]=src[key];}}
if(deep)
{jQuery.extend(true,target,deep);}}
jQuery.fn.load=function(url,params,callback)
{if(typeof url!=="string"&&_load)
{return _load.apply(this,arguments);}
if(!this.length)
{return this;}
var selector,type,response,self=this,off=url.indexOf(" ");if(off>=0)
{selector=url.slice(off,url.length);url=url.slice(0,off);}
if(jQuery.isFunction(params))
{callback=params;params=undefined;}else if(params&&typeof params==="object")
{type="POST";}
jQuery.ajax({url:url,type:type,dataType:"html",data:params,complete:function(jqXHR,status)
{if(callback)
{self.each(callback,response||[jqXHR.responseText,status,jqXHR]);}}}).done(function(responseText)
{response=arguments;self.html(selector?jQuery("<div>").append(responseText.replace(rscript,"")).find(selector):responseText);});return this;};jQuery.each("ajaxStart ajaxStop ajaxComplete ajaxError ajaxSuccess ajaxSend".split(" "),function(i,o)
{jQuery.fn[o]=function(f)
{return this.on(o,f);};});jQuery.each(["get","post"],function(i,method)
{jQuery[method]=function(url,data,callback,type)
{if(jQuery.isFunction(data))
{type=type||callback;callback=data;data=undefined;}
return jQuery.ajax({type:method,url:url,data:data,success:callback,dataType:type});};});jQuery.extend({getScript:function(url,callback)
{return jQuery.get(url,undefined,callback,"script");},getJSON:function(url,data,callback)
{return jQuery.get(url,data,callback,"json");},ajaxSetup:function(target,settings)
{if(settings)
{ajaxExtend(target,jQuery.ajaxSettings);}else
{settings=target;target=jQuery.ajaxSettings;}
ajaxExtend(target,settings);return target;},ajaxSettings:{url:ajaxLocation,isLocal:rlocalProtocol.test(ajaxLocParts[1]),global:true,type:"GET",contentType:"application/x-www-form-urlencoded; charset=UTF-8",processData:true,async:true,accepts:{xml:"application/xml, text/xml",html:"text/html",text:"text/plain",json:"application/json, text/javascript","*":allTypes},contents:{xml:/xml/,html:/html/,json:/json/},responseFields:{xml:"responseXML",text:"responseText"},converters:{"* text":window.String,"text html":true,"text json":jQuery.parseJSON,"text xml":jQuery.parseXML},flatOptions:{context:true,url:true}},ajaxPrefilter:addToPrefiltersOrTransports(prefilters),ajaxTransport:addToPrefiltersOrTransports(transports),ajax:function(url,options)
{if(typeof url==="object")
{options=url;url=undefined;}
options=options||{};var
ifModifiedKey,responseHeadersString,responseHeaders,transport,timeoutTimer,parts,fireGlobals,i,s=jQuery.ajaxSetup({},options),callbackContext=s.context||s,globalEventContext=callbackContext!==s&&(callbackContext.nodeType||callbackContext instanceof jQuery)?jQuery(callbackContext):jQuery.event,deferred=jQuery.Deferred(),completeDeferred=jQuery.Callbacks("once memory"),statusCode=s.statusCode||{},requestHeaders={},requestHeadersNames={},state=0,strAbort="canceled",jqXHR={readyState:0,setRequestHeader:function(name,value)
{if(!state)
{var lname=name.toLowerCase();name=requestHeadersNames[lname]=requestHeadersNames[lname]||name;requestHeaders[name]=value;}
return this;},getAllResponseHeaders:function()
{return state===2?responseHeadersString:null;},getResponseHeader:function(key)
{var match;if(state===2)
{if(!responseHeaders)
{responseHeaders={};while((match=rheaders.exec(responseHeadersString)))
{responseHeaders[match[1].toLowerCase()]=match[2];}}
match=responseHeaders[key.toLowerCase()];}
return match===undefined?null:match;},overrideMimeType:function(type)
{if(!state)
{s.mimeType=type;}
return this;},abort:function(statusText)
{statusText=statusText||strAbort;if(transport)
{transport.abort(statusText);}
done(0,statusText);return this;}};function done(status,nativeStatusText,responses,headers)
{var isSuccess,success,error,response,modified,statusText=nativeStatusText;if(state===2)
{return;}
state=2;if(timeoutTimer)
{clearTimeout(timeoutTimer);}
transport=undefined;responseHeadersString=headers||"";jqXHR.readyState=status>0?4:0;if(responses)
{response=ajaxHandleResponses(s,jqXHR,responses);}
if(status>=200&&status<300||status===304)
{if(s.ifModified)
{modified=jqXHR.getResponseHeader("Last-Modified");if(modified)
{jQuery.lastModified[ifModifiedKey]=modified;}
modified=jqXHR.getResponseHeader("Etag");if(modified)
{jQuery.etag[ifModifiedKey]=modified;}}
if(status===304)
{statusText="notmodified";isSuccess=true;}else
{isSuccess=ajaxConvert(s,response);statusText=isSuccess.state;success=isSuccess.data;error=isSuccess.error;isSuccess=!error;}}else
{error=statusText;if(!statusText||status)
{statusText="error";if(status<0)
{status=0;}}}
jqXHR.status=status;jqXHR.statusText=(nativeStatusText||statusText)+"";if(isSuccess)
{deferred.resolveWith(callbackContext,[success,statusText,jqXHR]);}else
{deferred.rejectWith(callbackContext,[jqXHR,statusText,error]);}
jqXHR.statusCode(statusCode);statusCode=undefined;if(fireGlobals)
{globalEventContext.trigger("ajax"+(isSuccess?"Success":"Error"),[jqXHR,s,isSuccess?success:error]);}
completeDeferred.fireWith(callbackContext,[jqXHR,statusText]);if(fireGlobals)
{globalEventContext.trigger("ajaxComplete",[jqXHR,s]);if(!(--jQuery.active))
{jQuery.event.trigger("ajaxStop");}}}
deferred.promise(jqXHR);jqXHR.success=jqXHR.done;jqXHR.error=jqXHR.fail;jqXHR.complete=completeDeferred.add;jqXHR.statusCode=function(map)
{if(map)
{var tmp;if(state<2)
{for(tmp in map)
{statusCode[tmp]=[statusCode[tmp],map[tmp]];}}else
{tmp=map[jqXHR.status];jqXHR.always(tmp);}}
return this;};s.url=((url||s.url)+"").replace(rhash,"").replace(rprotocol,ajaxLocParts[1]+"//");s.dataTypes=jQuery.trim(s.dataType||"*").toLowerCase().split(core_rspace);if(s.crossDomain==null)
{parts=rurl.exec(s.url.toLowerCase());s.crossDomain=!!(parts&&(parts[1]!==ajaxLocParts[1]||parts[2]!==ajaxLocParts[2]||(parts[3]||(parts[1]==="http:"?80:443))!=(ajaxLocParts[3]||(ajaxLocParts[1]==="http:"?80:443))));}
if(s.data&&s.processData&&typeof s.data!=="string")
{s.data=jQuery.param(s.data,s.traditional);}
inspectPrefiltersOrTransports(prefilters,s,options,jqXHR);if(state===2)
{return jqXHR;}
fireGlobals=s.global;s.type=s.type.toUpperCase();s.hasContent=!rnoContent.test(s.type);if(fireGlobals&&jQuery.active++===0)
{jQuery.event.trigger("ajaxStart");}
if(!s.hasContent)
{if(s.data)
{s.url+=(rquery.test(s.url)?"&":"?")+s.data;delete s.data;}
ifModifiedKey=s.url;if(s.cache===false)
{var ts=jQuery.now(),ret=s.url.replace(rts,"$1_="+ts);s.url=ret+((ret===s.url)?(rquery.test(s.url)?"&":"?")+"_="+ts:"");}}
if(s.data&&s.hasContent&&s.contentType!==false||options.contentType)
{jqXHR.setRequestHeader("Content-Type",s.contentType);}
if(s.ifModified)
{ifModifiedKey=ifModifiedKey||s.url;if(jQuery.lastModified[ifModifiedKey])
{jqXHR.setRequestHeader("If-Modified-Since",jQuery.lastModified[ifModifiedKey]);}
if(jQuery.etag[ifModifiedKey])
{jqXHR.setRequestHeader("If-None-Match",jQuery.etag[ifModifiedKey]);}}
jqXHR.setRequestHeader("Accept",s.dataTypes[0]&&s.accepts[s.dataTypes[0]]?s.accepts[s.dataTypes[0]]+(s.dataTypes[0]!=="*"?", "+allTypes+"; q=0.01":""):s.accepts["*"]);for(i in s.headers)
{jqXHR.setRequestHeader(i,s.headers[i]);}
if(s.beforeSend&&(s.beforeSend.call(callbackContext,jqXHR,s)===false||state===2))
{return jqXHR.abort();}
strAbort="abort";for(i in{success:1,error:1,complete:1})
{jqXHR[i](s[i]);}
transport=inspectPrefiltersOrTransports(transports,s,options,jqXHR);if(!transport)
{done(-1,"No Transport");}else
{jqXHR.readyState=1;if(fireGlobals)
{globalEventContext.trigger("ajaxSend",[jqXHR,s]);}
if(s.async&&s.timeout>0)
{timeoutTimer=setTimeout(function()
{jqXHR.abort("timeout");},s.timeout);}
try
{state=1;transport.send(requestHeaders,done);}catch(e)
{if(state<2)
{done(-1,e);}else
{throw e;}}}
return jqXHR;},active:0,lastModified:{},etag:{}});function ajaxHandleResponses(s,jqXHR,responses)
{var ct,type,finalDataType,firstDataType,contents=s.contents,dataTypes=s.dataTypes,responseFields=s.responseFields;for(type in responseFields)
{if(type in responses)
{jqXHR[responseFields[type]]=responses[type];}}
while(dataTypes[0]==="*")
{dataTypes.shift();if(ct===undefined)
{ct=s.mimeType||jqXHR.getResponseHeader("content-type");}}
if(ct)
{for(type in contents)
{if(contents[type]&&contents[type].test(ct))
{dataTypes.unshift(type);break;}}}
if(dataTypes[0]in responses)
{finalDataType=dataTypes[0];}else
{for(type in responses)
{if(!dataTypes[0]||s.converters[type+" "+dataTypes[0]])
{finalDataType=type;break;}
if(!firstDataType)
{firstDataType=type;}}
finalDataType=finalDataType||firstDataType;}
if(finalDataType)
{if(finalDataType!==dataTypes[0])
{dataTypes.unshift(finalDataType);}
return responses[finalDataType];}}
function ajaxConvert(s,response)
{var conv,conv2,current,tmp,dataTypes=s.dataTypes.slice(),prev=dataTypes[0],converters={},i=0;if(s.dataFilter)
{response=s.dataFilter(response,s.dataType);}
if(dataTypes[1])
{for(conv in s.converters)
{converters[conv.toLowerCase()]=s.converters[conv];}}
for(;(current=dataTypes[++i]);)
{if(current!=="*")
{if(prev!=="*"&&prev!==current)
{conv=converters[prev+" "+current]||converters["* "+current];if(!conv)
{for(conv2 in converters)
{tmp=conv2.split(" ");if(tmp[1]===current)
{conv=converters[prev+" "+tmp[0]]||converters["* "+tmp[0]];if(conv)
{if(conv===true)
{conv=converters[conv2];}else if(converters[conv2]!==true)
{current=tmp[0];dataTypes.splice(i--,0,current);}
break;}}}}
if(conv!==true)
{if(conv&&s["throws"])
{response=conv(response);}else
{try
{response=conv(response);}catch(e)
{return{state:"parsererror",error:conv?e:"No conversion from "+prev+" to "+current};}}}}
prev=current;}}
return{state:"success",data:response};}
var oldCallbacks=[],rquestion=/\?/,rjsonp=/(=)\?(?=&|$)|\?\?/,nonce=jQuery.now();jQuery.ajaxSetup({jsonp:"callback",jsonpCallback:function()
{var callback=oldCallbacks.pop()||(jQuery.expando+"_"+(nonce++));this[callback]=true;return callback;}});jQuery.ajaxPrefilter("json jsonp",function(s,originalSettings,jqXHR)
{var callbackName,overwritten,responseContainer,data=s.data,url=s.url,hasCallback=s.jsonp!==false,replaceInUrl=hasCallback&&rjsonp.test(url),replaceInData=hasCallback&&!replaceInUrl&&typeof data==="string"&&!(s.contentType||"").indexOf("application/x-www-form-urlencoded")&&rjsonp.test(data);if(s.dataTypes[0]==="jsonp"||replaceInUrl||replaceInData)
{callbackName=s.jsonpCallback=jQuery.isFunction(s.jsonpCallback)?s.jsonpCallback():s.jsonpCallback;overwritten=window[callbackName];if(replaceInUrl)
{s.url=url.replace(rjsonp,"$1"+callbackName);}else if(replaceInData)
{s.data=data.replace(rjsonp,"$1"+callbackName);}else if(hasCallback)
{s.url+=(rquestion.test(url)?"&":"?")+s.jsonp+"="+callbackName;}
s.converters["script json"]=function()
{if(!responseContainer)
{jQuery.error(callbackName+" was not called");}
return responseContainer[0];};s.dataTypes[0]="json";window[callbackName]=function()
{responseContainer=arguments;};jqXHR.always(function()
{window[callbackName]=overwritten;if(s[callbackName])
{s.jsonpCallback=originalSettings.jsonpCallback;oldCallbacks.push(callbackName);}
if(responseContainer&&jQuery.isFunction(overwritten))
{overwritten(responseContainer[0]);}
responseContainer=overwritten=undefined;});return"script";}});jQuery.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/javascript|ecmascript/},converters:{"text script":function(text)
{jQuery.globalEval(text);return text;}}});jQuery.ajaxPrefilter("script",function(s)
{if(s.cache===undefined)
{s.cache=false;}
if(s.crossDomain)
{s.type="GET";s.global=false;}});jQuery.ajaxTransport("script",function(s)
{if(s.crossDomain)
{var script,head=document.head||document.getElementsByTagName("head")[0]||document.documentElement;return{send:function(_,callback)
{script=document.createElement("script");script.async="async";if(s.scriptCharset)
{script.charset=s.scriptCharset;}
script.src=s.url;script.onload=script.onreadystatechange=function(_,isAbort)
{if(isAbort||!script.readyState||/loaded|complete/.test(script.readyState))
{script.onload=script.onreadystatechange=null;if(head&&script.parentNode)
{head.removeChild(script);}
script=undefined;if(!isAbort)
{callback(200,"success");}}};head.insertBefore(script,head.firstChild);},abort:function()
{if(script)
{script.onload(0,1);}}};}});var xhrCallbacks,xhrOnUnloadAbort=window.ActiveXObject?function()
{for(var key in xhrCallbacks)
{xhrCallbacks[key](0,1);}}:false,xhrId=0;function createStandardXHR()
{try
{return new window.XMLHttpRequest();}catch(e)
{}}
function createActiveXHR()
{try
{return new window.ActiveXObject("Microsoft.XMLHTTP");}catch(e)
{}}
jQuery.ajaxSettings.xhr=window.ActiveXObject?function()
{return!this.isLocal&&createStandardXHR()||createActiveXHR();}:createStandardXHR;(function(xhr)
{jQuery.extend(jQuery.support,{ajax:!!xhr,cors:!!xhr&&("withCredentials"in xhr)});})(jQuery.ajaxSettings.xhr());if(jQuery.support.ajax)
{jQuery.ajaxTransport(function(s)
{if(!s.crossDomain||jQuery.support.cors)
{var callback;return{send:function(headers,complete)
{var handle,i,xhr=s.xhr();if(s.username)
{xhr.open(s.type,s.url,s.async,s.username,s.password);}else
{xhr.open(s.type,s.url,s.async);}
if(s.xhrFields)
{for(i in s.xhrFields)
{xhr[i]=s.xhrFields[i];}}
if(s.mimeType&&xhr.overrideMimeType)
{xhr.overrideMimeType(s.mimeType);}
if(!s.crossDomain&&!headers["X-Requested-With"])
{headers["X-Requested-With"]="XMLHttpRequest";}
try
{for(i in headers)
{xhr.setRequestHeader(i,headers[i]);}}catch(_)
{}
xhr.send((s.hasContent&&s.data)||null);callback=function(_,isAbort)
{var status,statusText,responseHeaders,responses,xml;try
{if(callback&&(isAbort||xhr.readyState===4))
{callback=undefined;if(handle)
{xhr.onreadystatechange=jQuery.noop;if(xhrOnUnloadAbort)
{delete xhrCallbacks[handle];}}
if(isAbort)
{if(xhr.readyState!==4)
{xhr.abort();}}else
{status=xhr.status;responseHeaders=xhr.getAllResponseHeaders();responses={};xml=xhr.responseXML;if(xml&&xml.documentElement)
{responses.xml=xml;}
try
{responses.text=xhr.responseText;}catch(e)
{}
try
{statusText=xhr.statusText;}catch(e)
{statusText="";}
if(!status&&s.isLocal&&!s.crossDomain)
{status=responses.text?200:404;}else if(status===1223)
{status=204;}}}}catch(firefoxAccessException)
{if(!isAbort)
{complete(-1,firefoxAccessException);}}
if(responses)
{complete(status,statusText,responses,responseHeaders);}};if(!s.async)
{callback();}else if(xhr.readyState===4)
{setTimeout(callback,0);}else
{handle=++xhrId;if(xhrOnUnloadAbort)
{if(!xhrCallbacks)
{xhrCallbacks={};jQuery(window).unload(xhrOnUnloadAbort);}
xhrCallbacks[handle]=callback;}
xhr.onreadystatechange=callback;}},abort:function()
{if(callback)
{callback(0,1);}}};}});}
var fxNow,timerId,rfxtypes=/^(?:toggle|show|hide)$/,rfxnum=new RegExp("^(?:([-+])=|)("+core_pnum+")([a-z%]*)$","i"),rrun=/queueHooks$/,animationPrefilters=[defaultPrefilter],tweeners={"*":[function(prop,value)
{var end,unit,tween=this.createTween(prop,value),parts=rfxnum.exec(value),target=tween.cur(),start=+target||0,scale=1,maxIterations=20;if(parts)
{end=+parts[2];unit=parts[3]||(jQuery.cssNumber[prop]?"":"px");if(unit!=="px"&&start)
{start=jQuery.css(tween.elem,prop,true)||end||1;do{scale=scale||".5";start=start/scale;jQuery.style(tween.elem,prop,start+unit);}while(scale!==(scale=tween.cur()/target)&&scale!==1&&--maxIterations);}
tween.unit=unit;tween.start=start;tween.end=parts[1]?start+(parts[1]+1)*end:end;}
return tween;}]};function createFxNow()
{setTimeout(function()
{fxNow=undefined;},0);return(fxNow=jQuery.now());}
function createTweens(animation,props)
{jQuery.each(props,function(prop,value)
{var collection=(tweeners[prop]||[]).concat(tweeners["*"]),index=0,length=collection.length;for(;index<length;index++)
{if(collection[index].call(animation,prop,value))
{return;}}});}
function Animation(elem,properties,options)
{var result,index=0,tweenerIndex=0,length=animationPrefilters.length,deferred=jQuery.Deferred().always(function()
{delete tick.elem;}),tick=function()
{var currentTime=fxNow||createFxNow(),remaining=Math.max(0,animation.startTime+animation.duration-currentTime),temp=remaining/animation.duration||0,percent=1-temp,index=0,length=animation.tweens.length;for(;index<length;index++)
{animation.tweens[index].run(percent);}
deferred.notifyWith(elem,[animation,percent,remaining]);if(percent<1&&length)
{return remaining;}else
{deferred.resolveWith(elem,[animation]);return false;}},animation=deferred.promise({elem:elem,props:jQuery.extend({},properties),opts:jQuery.extend(true,{specialEasing:{}},options),originalProperties:properties,originalOptions:options,startTime:fxNow||createFxNow(),duration:options.duration,tweens:[],createTween:function(prop,end,easing)
{var tween=jQuery.Tween(elem,animation.opts,prop,end,animation.opts.specialEasing[prop]||animation.opts.easing);animation.tweens.push(tween);return tween;},stop:function(gotoEnd)
{var index=0,length=gotoEnd?animation.tweens.length:0;for(;index<length;index++)
{animation.tweens[index].run(1);}
if(gotoEnd)
{deferred.resolveWith(elem,[animation,gotoEnd]);}else
{deferred.rejectWith(elem,[animation,gotoEnd]);}
return this;}}),props=animation.props;propFilter(props,animation.opts.specialEasing);for(;index<length;index++)
{result=animationPrefilters[index].call(animation,elem,props,animation.opts);if(result)
{return result;}}
createTweens(animation,props);if(jQuery.isFunction(animation.opts.start))
{animation.opts.start.call(elem,animation);}
jQuery.fx.timer(jQuery.extend(tick,{anim:animation,queue:animation.opts.queue,elem:elem}));return animation.progress(animation.opts.progress).done(animation.opts.done,animation.opts.complete).fail(animation.opts.fail).always(animation.opts.always);}
function propFilter(props,specialEasing)
{var index,name,easing,value,hooks;for(index in props)
{name=jQuery.camelCase(index);easing=specialEasing[name];value=props[index];if(jQuery.isArray(value))
{easing=value[1];value=props[index]=value[0];}
if(index!==name)
{props[name]=value;delete props[index];}
hooks=jQuery.cssHooks[name];if(hooks&&"expand"in hooks)
{value=hooks.expand(value);delete props[name];for(index in value)
{if(!(index in props))
{props[index]=value[index];specialEasing[index]=easing;}}}else
{specialEasing[name]=easing;}}}
jQuery.Animation=jQuery.extend(Animation,{tweener:function(props,callback)
{if(jQuery.isFunction(props))
{callback=props;props=["*"];}else
{props=props.split(" ");}
var prop,index=0,length=props.length;for(;index<length;index++)
{prop=props[index];tweeners[prop]=tweeners[prop]||[];tweeners[prop].unshift(callback);}},prefilter:function(callback,prepend)
{if(prepend)
{animationPrefilters.unshift(callback);}else
{animationPrefilters.push(callback);}}});function defaultPrefilter(elem,props,opts)
{var index,prop,value,length,dataShow,toggle,tween,hooks,oldfire,anim=this,style=elem.style,orig={},handled=[],hidden=elem.nodeType&&isHidden(elem);if(!opts.queue)
{hooks=jQuery._queueHooks(elem,"fx");if(hooks.unqueued==null)
{hooks.unqueued=0;oldfire=hooks.empty.fire;hooks.empty.fire=function()
{if(!hooks.unqueued)
{oldfire();}};}
hooks.unqueued++;anim.always(function()
{anim.always(function()
{hooks.unqueued--;if(!jQuery.queue(elem,"fx").length)
{hooks.empty.fire();}});});}
if(elem.nodeType===1&&("height"in props||"width"in props))
{opts.overflow=[style.overflow,style.overflowX,style.overflowY];if(jQuery.css(elem,"display")==="inline"&&jQuery.css(elem,"float")==="none")
{if(!jQuery.support.inlineBlockNeedsLayout||css_defaultDisplay(elem.nodeName)==="inline")
{style.display="inline-block";}else
{style.zoom=1;}}}
if(opts.overflow)
{style.overflow="hidden";if(!jQuery.support.shrinkWrapBlocks)
{anim.done(function()
{style.overflow=opts.overflow[0];style.overflowX=opts.overflow[1];style.overflowY=opts.overflow[2];});}}
for(index in props)
{value=props[index];if(rfxtypes.exec(value))
{delete props[index];toggle=toggle||value==="toggle";if(value===(hidden?"hide":"show"))
{continue;}
handled.push(index);}}
length=handled.length;if(length)
{dataShow=jQuery._data(elem,"fxshow")||jQuery._data(elem,"fxshow",{});if("hidden"in dataShow)
{hidden=dataShow.hidden;}
if(toggle)
{dataShow.hidden=!hidden;}
if(hidden)
{jQuery(elem).show();}else
{anim.done(function()
{jQuery(elem).hide();});}
anim.done(function()
{var prop;jQuery.removeData(elem,"fxshow",true);for(prop in orig)
{jQuery.style(elem,prop,orig[prop]);}});for(index=0;index<length;index++)
{prop=handled[index];tween=anim.createTween(prop,hidden?dataShow[prop]:0);orig[prop]=dataShow[prop]||jQuery.style(elem,prop);if(!(prop in dataShow))
{dataShow[prop]=tween.start;if(hidden)
{tween.end=tween.start;tween.start=prop==="width"||prop==="height"?1:0;}}}}}
function Tween(elem,options,prop,end,easing)
{return new Tween.prototype.init(elem,options,prop,end,easing);}
jQuery.Tween=Tween;Tween.prototype={constructor:Tween,init:function(elem,options,prop,end,easing,unit)
{this.elem=elem;this.prop=prop;this.easing=easing||"swing";this.options=options;this.start=this.now=this.cur();this.end=end;this.unit=unit||(jQuery.cssNumber[prop]?"":"px");},cur:function()
{var hooks=Tween.propHooks[this.prop];return hooks&&hooks.get?hooks.get(this):Tween.propHooks._default.get(this);},run:function(percent)
{var eased,hooks=Tween.propHooks[this.prop];if(this.options.duration)
{this.pos=eased=jQuery.easing[this.easing](percent,this.options.duration*percent,0,1,this.options.duration);}else
{this.pos=eased=percent;}
this.now=(this.end-this.start)*eased+this.start;if(this.options.step)
{this.options.step.call(this.elem,this.now,this);}
if(hooks&&hooks.set)
{hooks.set(this);}else
{Tween.propHooks._default.set(this);}
return this;}};Tween.prototype.init.prototype=Tween.prototype;Tween.propHooks={_default:{get:function(tween)
{var result;if(tween.elem[tween.prop]!=null&&(!tween.elem.style||tween.elem.style[tween.prop]==null))
{return tween.elem[tween.prop];}
result=jQuery.css(tween.elem,tween.prop,false,"");return!result||result==="auto"?0:result;},set:function(tween)
{if(jQuery.fx.step[tween.prop])
{jQuery.fx.step[tween.prop](tween);}else if(tween.elem.style&&(tween.elem.style[jQuery.cssProps[tween.prop]]!=null||jQuery.cssHooks[tween.prop]))
{jQuery.style(tween.elem,tween.prop,tween.now+tween.unit);}else
{tween.elem[tween.prop]=tween.now;}}}};Tween.propHooks.scrollTop=Tween.propHooks.scrollLeft={set:function(tween)
{if(tween.elem.nodeType&&tween.elem.parentNode)
{tween.elem[tween.prop]=tween.now;}}};jQuery.each(["toggle","show","hide"],function(i,name)
{var cssFn=jQuery.fn[name];jQuery.fn[name]=function(speed,easing,callback)
{return speed==null||typeof speed==="boolean"||(!i&&jQuery.isFunction(speed)&&jQuery.isFunction(easing))?cssFn.apply(this,arguments):this.animate(genFx(name,true),speed,easing,callback);};});jQuery.fn.extend({fadeTo:function(speed,to,easing,callback)
{return this.filter(isHidden).css("opacity",0).show().end().animate({opacity:to},speed,easing,callback);},animate:function(prop,speed,easing,callback)
{var empty=jQuery.isEmptyObject(prop),optall=jQuery.speed(speed,easing,callback),doAnimation=function()
{var anim=Animation(this,jQuery.extend({},prop),optall);if(empty)
{anim.stop(true);}};return empty||optall.queue===false?this.each(doAnimation):this.queue(optall.queue,doAnimation);},stop:function(type,clearQueue,gotoEnd)
{var stopQueue=function(hooks)
{var stop=hooks.stop;delete hooks.stop;stop(gotoEnd);};if(typeof type!=="string")
{gotoEnd=clearQueue;clearQueue=type;type=undefined;}
if(clearQueue&&type!==false)
{this.queue(type||"fx",[]);}
return this.each(function()
{var dequeue=true,index=type!=null&&type+"queueHooks",timers=jQuery.timers,data=jQuery._data(this);if(index)
{if(data[index]&&data[index].stop)
{stopQueue(data[index]);}}else
{for(index in data)
{if(data[index]&&data[index].stop&&rrun.test(index))
{stopQueue(data[index]);}}}
for(index=timers.length;index--;)
{if(timers[index].elem===this&&(type==null||timers[index].queue===type))
{timers[index].anim.stop(gotoEnd);dequeue=false;timers.splice(index,1);}}
if(dequeue||!gotoEnd)
{jQuery.dequeue(this,type);}});}});function genFx(type,includeWidth)
{var which,attrs={height:type},i=0;includeWidth=includeWidth?1:0;for(;i<4;i+=2-includeWidth)
{which=cssExpand[i];attrs["margin"+which]=attrs["padding"+which]=type;}
if(includeWidth)
{attrs.opacity=attrs.width=type;}
return attrs;}
jQuery.each({slideDown:genFx("show"),slideUp:genFx("hide"),slideToggle:genFx("toggle"),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(name,props)
{jQuery.fn[name]=function(speed,easing,callback)
{return this.animate(props,speed,easing,callback);};});jQuery.speed=function(speed,easing,fn)
{var opt=speed&&typeof speed==="object"?jQuery.extend({},speed):{complete:fn||!fn&&easing||jQuery.isFunction(speed)&&speed,duration:speed,easing:fn&&easing||easing&&!jQuery.isFunction(easing)&&easing};opt.duration=jQuery.fx.off?0:typeof opt.duration==="number"?opt.duration:opt.duration in jQuery.fx.speeds?jQuery.fx.speeds[opt.duration]:jQuery.fx.speeds._default;if(opt.queue==null||opt.queue===true)
{opt.queue="fx";}
opt.old=opt.complete;opt.complete=function()
{if(jQuery.isFunction(opt.old))
{opt.old.call(this);}
if(opt.queue)
{jQuery.dequeue(this,opt.queue);}};return opt;};jQuery.easing={linear:function(p)
{return p;},swing:function(p)
{return 0.5-Math.cos(p*Math.PI)/2;}};jQuery.timers=[];jQuery.fx=Tween.prototype.init;jQuery.fx.tick=function()
{var timer,timers=jQuery.timers,i=0;fxNow=jQuery.now();for(;i<timers.length;i++)
{timer=timers[i];if(!timer()&&timers[i]===timer)
{timers.splice(i--,1);}}
if(!timers.length)
{jQuery.fx.stop();}
fxNow=undefined;};jQuery.fx.timer=function(timer)
{if(timer()&&jQuery.timers.push(timer)&&!timerId)
{timerId=setInterval(jQuery.fx.tick,jQuery.fx.interval);}};jQuery.fx.interval=13;jQuery.fx.stop=function()
{clearInterval(timerId);timerId=null;};jQuery.fx.speeds={slow:600,fast:200,_default:400};jQuery.fx.step={};if(jQuery.expr&&jQuery.expr.filters)
{jQuery.expr.filters.animated=function(elem)
{return jQuery.grep(jQuery.timers,function(fn)
{return elem===fn.elem;}).length;};}
var rroot=/^(?:body|html)$/i;jQuery.fn.offset=function(options)
{if(arguments.length)
{return options===undefined?this:this.each(function(i)
{jQuery.offset.setOffset(this,options,i);});}
var docElem,body,win,clientTop,clientLeft,scrollTop,scrollLeft,box={top:0,left:0},elem=this[0],doc=elem&&elem.ownerDocument;if(!doc)
{return;}
if((body=doc.body)===elem)
{return jQuery.offset.bodyOffset(elem);}
docElem=doc.documentElement;if(!jQuery.contains(docElem,elem))
{return box;}
if(typeof elem.getBoundingClientRect!=="undefined")
{box=elem.getBoundingClientRect();}
win=getWindow(doc);clientTop=docElem.clientTop||body.clientTop||0;clientLeft=docElem.clientLeft||body.clientLeft||0;scrollTop=win.pageYOffset||docElem.scrollTop;scrollLeft=win.pageXOffset||docElem.scrollLeft;return{top:box.top+scrollTop-clientTop,left:box.left+scrollLeft-clientLeft};};jQuery.offset={bodyOffset:function(body)
{var top=body.offsetTop,left=body.offsetLeft;if(jQuery.support.doesNotIncludeMarginInBodyOffset)
{top+=parseFloat(jQuery.css(body,"marginTop"))||0;left+=parseFloat(jQuery.css(body,"marginLeft"))||0;}
return{top:top,left:left};},setOffset:function(elem,options,i)
{var position=jQuery.css(elem,"position");if(position==="static")
{elem.style.position="relative";}
var curElem=jQuery(elem),curOffset=curElem.offset(),curCSSTop=jQuery.css(elem,"top"),curCSSLeft=jQuery.css(elem,"left"),calculatePosition=(position==="absolute"||position==="fixed")&&jQuery.inArray("auto",[curCSSTop,curCSSLeft])>-1,props={},curPosition={},curTop,curLeft;if(calculatePosition)
{curPosition=curElem.position();curTop=curPosition.top;curLeft=curPosition.left;}else
{curTop=parseFloat(curCSSTop)||0;curLeft=parseFloat(curCSSLeft)||0;}
if(jQuery.isFunction(options))
{options=options.call(elem,i,curOffset);}
if(options.top!=null)
{props.top=(options.top-curOffset.top)+curTop;}
if(options.left!=null)
{props.left=(options.left-curOffset.left)+curLeft;}
if("using"in options)
{options.using.call(elem,props);}else
{curElem.css(props);}}};jQuery.fn.extend({position:function()
{if(!this[0])
{return;}
var elem=this[0],offsetParent=this.offsetParent(),offset=this.offset(),parentOffset=rroot.test(offsetParent[0].nodeName)?{top:0,left:0}:offsetParent.offset();offset.top-=parseFloat(jQuery.css(elem,"marginTop"))||0;offset.left-=parseFloat(jQuery.css(elem,"marginLeft"))||0;parentOffset.top+=parseFloat(jQuery.css(offsetParent[0],"borderTopWidth"))||0;parentOffset.left+=parseFloat(jQuery.css(offsetParent[0],"borderLeftWidth"))||0;return{top:offset.top-parentOffset.top,left:offset.left-parentOffset.left};},offsetParent:function()
{return this.map(function()
{var offsetParent=this.offsetParent||document.body;while(offsetParent&&(!rroot.test(offsetParent.nodeName)&&jQuery.css(offsetParent,"position")==="static"))
{offsetParent=offsetParent.offsetParent;}
return offsetParent||document.body;});}});jQuery.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(method,prop)
{var top=/Y/.test(prop);jQuery.fn[method]=function(val)
{return jQuery.access(this,function(elem,method,val)
{var win=getWindow(elem);if(val===undefined)
{return win?(prop in win)?win[prop]:win.document.documentElement[method]:elem[method];}
if(win)
{win.scrollTo(!top?val:jQuery(win).scrollLeft(),top?val:jQuery(win).scrollTop());}else
{elem[method]=val;}},method,val,arguments.length,null);};});function getWindow(elem)
{return jQuery.isWindow(elem)?elem:elem.nodeType===9?elem.defaultView||elem.parentWindow:false;}
jQuery.each({Height:"height",Width:"width"},function(name,type)
{jQuery.each({padding:"inner"+name,content:type,"":"outer"+name},function(defaultExtra,funcName)
{jQuery.fn[funcName]=function(margin,value)
{var chainable=arguments.length&&(defaultExtra||typeof margin!=="boolean"),extra=defaultExtra||(margin===true||value===true?"margin":"border");return jQuery.access(this,function(elem,type,value)
{var doc;if(jQuery.isWindow(elem))
{return elem.document.documentElement["client"+name];}
if(elem.nodeType===9)
{doc=elem.documentElement;return Math.max(elem.body["scroll"+name],doc["scroll"+name],elem.body["offset"+name],doc["offset"+name],doc["client"+name]);}
return value===undefined?jQuery.css(elem,type,value,extra):jQuery.style(elem,type,value,extra);},type,chainable?margin:undefined,chainable,null);};});});if(typeof define==="function"&&define.amd&&define.amd.jQuery)
{define("jquery",[],function()
{return jQuery;});}
window.jfbcJQuery=jQuery.noConflict();window.jfbcJQ={extend:function(obj)
{window.jfbcJQuery.extend(true,window.jfbcJQ,obj)},jQuery:window.jfbcJQuery};})(window);!function(a)
{a(function()
{a.support.transition=function()
{var a=function()
{var a=document.createElement("sc-bootstrap"),b={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd otransitionend",transition:"transitionend"},c;for(c in b)if(a.style[c]!==undefined)return b[c]}();return a&&{end:a}}()})}(window.jfbcJQuery),!function(a)
{var b=function(b,c)
{this.options=c,this.$element=a(b).delegate('[data-dismiss="sc-modal"]',"click.dismiss.sc-modal",a.proxy(this.hide,this)),this.options.remote&&this.$element.find(".sc-modal-body").load(this.options.remote)};b.prototype={constructor:b,toggle:function()
{return this[this.isShown?"hide":"show"]()},show:function()
{var b=this,c=a.Event("show");this.$element.trigger(c);if(this.isShown||c.isDefaultPrevented())return;this.isShown=!0,this.escape(),this.backdrop(function()
{var c=a.support.transition&&b.$element.hasClass("fade");b.$element.parent().length||b.$element.appendTo(document.body),b.$element.show(),c&&b.$element[0].offsetWidth,b.$element.addClass("in").attr("aria-hidden",!1),b.enforceFocus(),c?b.$element.one(a.support.transition.end,function()
{b.$element.focus().trigger("shown")}):b.$element.focus().trigger("shown")})},hide:function(b)
{b&&b.preventDefault();var c=this;b=a.Event("hide"),this.$element.trigger(b);if(!this.isShown||b.isDefaultPrevented())return;this.isShown=!1,this.escape(),a(document).off("focusin.sc-modal"),this.$element.removeClass("in").attr("aria-hidden",!0),a.support.transition&&this.$element.hasClass("fade")?this.hideWithTransition():this.hideModal()},enforceFocus:function()
{var b=this;a(document).on("focusin.sc-modal",function(a)
{b.$element[0]!==a.target&&!b.$element.has(a.target).length&&b.$element.focus()})},escape:function()
{var a=this;this.isShown&&this.options.keyboard?this.$element.on("keyup.dismiss.sc-modal",function(b)
{b.which==27&&a.hide()}):this.isShown||this.$element.off("keyup.dismiss.sc-modal")},hideWithTransition:function()
{var b=this,c=setTimeout(function()
{b.$element.off(a.support.transition.end),b.hideModal()},500);this.$element.one(a.support.transition.end,function()
{clearTimeout(c),b.hideModal()})},hideModal:function()
{var a=this;this.$element.hide(),this.backdrop(function()
{a.removeBackdrop(),a.$element.trigger("hidden")})},removeBackdrop:function()
{this.$backdrop&&this.$backdrop.remove(),this.$backdrop=null},backdrop:function(b)
{var c=this,d=this.$element.hasClass("fade")?"fade":"";if(this.isShown&&this.options.backdrop)
{var e=a.support.transition&&d;this.$backdrop=a('<div class="sourcecoast modal-backdrop '+d+'" />').appendTo(document.body),this.$backdrop.click(this.options.backdrop=="static"?a.proxy(this.$element[0].focus,this.$element[0]):a.proxy(this.hide,this)),e&&this.$backdrop[0].offsetWidth,this.$backdrop.addClass("in");if(!b)return;e?this.$backdrop.one(a.support.transition.end,b):b()}else!this.isShown&&this.$backdrop?(this.$backdrop.removeClass("in"),a.support.transition&&this.$element.hasClass("fade")?this.$backdrop.one(a.support.transition.end,b):b()):b&&b()}};var c=a.fn.modal;a.fn.modal=function(c)
{return this.each(function()
{var d=a(this),e=d.data("sc-modal"),f=a.extend({},a.fn.modal.defaults,d.data(),typeof c=="object"&&c);e||d.data("sc-modal",e=new b(this,f)),typeof c=="string"?e[c]():f.show&&e.show()})},a.fn.modal.defaults={backdrop:!0,keyboard:!0,show:!0},a.fn.modal.Constructor=b,a.fn.modal.noConflict=function()
{return a.fn.modal=c,this},a(document).on("click.sc-modal.data-api",'[data-toggle="sc-modal"]',function(b)
{var c=a(this),d=c.attr("href"),e=a(c.attr("data-target")||d&&d.replace(/.*(?=#[^\s]+$)/,"")),f=e.data("sc-modal")?"toggle":a.extend({remote:!/#/.test(d)&&d},e.data(),c.data());b.preventDefault(),e.modal(f).one("hide",function()
{c.focus()})})}(window.jfbcJQuery);!function(e)
{"use strict";function r()
{e(t).parent().parent().removeClass("nav-hover");e(".dropdown-backdrop").remove();e(t).each(function()
{i(e(this)).removeClass("open")})}
function i(t)
{var n=t.attr("data-target"),r;if(!n)
{n=t.attr("href");n=n&&/#/.test(n)&&n.replace(/.*(?=#[^\s]*$)/,"")}
r=n&&e(n);if(!r||!r.length)r=t.parent();return r}
var t="[data-toggle=sc-dropdown]",n=function(t)
{var n=e(t).on("click.dropdown.data-api",this.toggle).on('mouseover.dropdown.data-api',this.toggle);e("html").on("click.dropdown.data-api",function()
{n.parent().parent().removeClass('nav-hover');n.parent().removeClass("open")})};n.prototype={constructor:n,toggle:function(t)
{var n=e(this),s,o,u,h;if(n.is(".disabled, :disabled"))return;s=i(n);o=s.hasClass("open");h=s.parent().hasClass("nav-hover");if(!h&&t.type=="mouseover")return;u=n.attr("href");if(u&&u!=="#")
{window.location=u;return}
r();if((!o&&t.type!="mouseover")||(h&&t.type=="mouseover"))
{if("ontouchstart"in document.documentElement)
{e('<div class="dropdown-backdrop"/>').insertBefore(e(this)).on("click",r)}
s.parent().toggleClass("nav-hover");s.toggleClass("open")}
n.focus();return false},keydown:function(n)
{var r,s,o,u,a,f;if(!/(38|40|27)/.test(n.keyCode))return;r=e(this);n.preventDefault();n.stopPropagation();if(r.is(".disabled, :disabled"))return;u=i(r);a=u.hasClass("open");if(!a||a&&n.keyCode==27)
{if(n.which==27)u.find(t).focus();return r.click()}
s=e("[role=menu] li:not(.divider):visible a",u);if(!s.length)return;f=s.index(s.filter(":focus"));if(n.keyCode==38&&f>0)f--;if(n.keyCode==40&&f<s.length-1)f++;if(!~f)f=0;s.eq(f).focus()}};var s=e.fn.dropdown;e.fn.dropdown=function(t)
{return this.each(function()
{var r=e(this),i=r.data("dropdown");if(!i)r.data("dropdown",i=new n(this));if(typeof t=="string")i[t].call(r)})};e.fn.dropdown.Constructor=n;e.fn.dropdown.noConflict=function()
{e.fn.dropdown=s;return this};e(document).on("click.dropdown.data-api",r).on("click.dropdown.data-api",".dropdown form",function(e)
{e.stopPropagation()}).on("click.dropdown.data-api",t,n.prototype.toggle).on("keydown.dropdown.data-api",t+", [role=menu]",n.prototype.keydown).on("mouseover.dropdown.data-api",t,n.prototype.toggle)}(window.jfbcJQuery);!function(a)
{"use strict";var Tooltip=function(element,options)
{this.init('tooltip',element,options)}
Tooltip.prototype={constructor:Tooltip,init:function(type,element,options)
{var eventIn,eventOut,triggers,trigger,i
this.type=type
this.$element=a(element)
this.options=this.getOptions(options)
this.enabled=true
triggers=this.options.trigger.split(' ')
for(i=triggers.length;i--;)
{trigger=triggers[i]
if(trigger=='click')
{this.$element.on('click.'+this.type,this.options.selector,a.proxy(this.toggle,this))}else if(trigger!='manual')
{eventIn=trigger=='hover'?'mouseenter':'focus'
eventOut=trigger=='hover'?'mouseleave':'blur'
this.$element.on(eventIn+'.'+this.type,this.options.selector,a.proxy(this.enter,this))
this.$element.on(eventOut+'.'+this.type,this.options.selector,a.proxy(this.leave,this))}}
this.options.selector?(this._options=a.extend({},this.options,{trigger:'manual',selector:''})):this.fixTitle()},getOptions:function(options)
{options=a.extend({},a.fn[this.type].defaults,this.aelement.data(),options)
if(options.delay&&typeof options.delay=='number')
{options.delay={show:options.delay,hide:options.delay}}
return options},enter:function(e)
{var defaults=a.fn[this.type].defaults,options={},self
this._options&&a.each(this._options,function(key,value)
{if(defaults[key]!=value)options[key]=value},this)
self=a(e.currentTarget)[this.type](options).data(this.type)
if(!self.options.delay||!self.options.delay.show)return self.show()
clearTimeout(this.timeout)
self.hoverState='in'
this.timeout=setTimeout(function()
{if(self.hoverState=='in')self.show()},self.options.delay.show)},leave:function(e)
{var self=a(e.currentTarget)[this.type](this._options).data(this.type)
if(this.timeout)clearTimeout(this.timeout)
if(!self.options.delay||!self.options.delay.hide)return self.hide()
self.hoverState='out'
this.timeout=setTimeout(function()
{if(self.hoverState=='out')self.hide()},self.options.delay.hide)},show:function()
{var $tip,pos,actualWidth,actualHeight,placement,tp,e=a.Event('show')
if(this.hasContent()&&this.enabled)
{this.$element.trigger(e)
if(e.isDefaultPrevented())return
$tip=this.tip()
this.setContent()
if(this.options.animation)
{$tip.addClass('fade')}
placement=typeof this.options.placement=='function'?this.options.placement.call(this,$tip[0],this.$element[0]):this.options.placement
$tip.detach().css({top:0,left:0,display:'block'})
this.options.container?$tip.appendTo(this.options.container):$tip.insertAfter(this.$element)
pos=this.getPosition()
actualWidth=$tip[0].offsetWidth
actualHeight=$tip[0].offsetHeight
switch(placement)
{case'bottom':tp={top:pos.top+pos.height,left:pos.left+pos.width/2-actualWidth/2}
break
case'top':tp={top:pos.top-actualHeight,left:pos.left+pos.width/2-actualWidth/2}
break
case'left':tp={top:pos.top+pos.height/2-actualHeight/2,left:pos.left-actualWidth}
break
case'right':tp={top:pos.top+pos.height/2-actualHeight/2,left:pos.left+pos.width}
break}
this.applyPlacement(tp,placement)
this.$element.trigger('shown')}},applyPlacement:function(offset,placement)
{var $tip=this.tip(),width=$tip[0].offsetWidth,height=$tip[0].offsetHeight,actualWidth,actualHeight,delta,replace
$tip.offset(offset).addClass(placement).addClass('in')
actualWidth=$tip[0].offsetWidth
actualHeight=$tip[0].offsetHeight
if(placement=='top'&&actualHeight!=height)
{offset.top=offset.top+height-actualHeight
replace=true}
if(placement=='bottom'||placement=='top')
{delta=0
if(offset.left<0)
{delta=offset.left*-2
offset.left=0
$tip.offset(offset)
actualWidth=$tip[0].offsetWidth
actualHeight=$tip[0].offsetHeight}
this.replaceArrow(delta-width+actualWidth,actualWidth,'left')}else
{this.replaceArrow(actualHeight-height,actualHeight,'top')}
if(replace)$tip.offset(offset)},replaceArrow:function(delta,dimension,position)
{this.arrow().css(position,delta?(50*(1-delta/dimension)+"%"):'')},setContent:function()
{var $tip=this.tip(),title=this.getTitle()
$tip.find('.tooltip-inner')[this.options.html?'html':'text'](title)
$tip.removeClass('fade in top bottom left right')},hide:function()
{var that=this,$tip=this.tip(),e=a.Event('hide')
this.$element.trigger(e)
if(e.isDefaultPrevented())return
$tip.removeClass('in')
function removeWithAnimation()
{var timeout=setTimeout(function()
{$tip.off(a.support.transition.end).detach()},500)
$tip.one(a.support.transition.end,function()
{clearTimeout(timeout)
$tip.detach()})}
a.support.transition&&this.$tip.hasClass('fade')?removeWithAnimation():$tip.detach()
this.$element.trigger('hidden')
return this},fixTitle:function()
{var $e=this.$element
if($e.attr('title')||typeof($e.attr('data-original-title'))!='string')
{$e.attr('data-original-title',$e.attr('title')||'').attr('title','')}},hasContent:function()
{return this.getTitle()},getPosition:function()
{var el=this.$element[0]
return a.extend({},(typeof el.getBoundingClientRect=='function')?el.getBoundingClientRect():{width:el.offsetWidth,height:el.offsetHeight},this.$element.offset())},getTitle:function()
{var title,$e=this.$element,o=this.options
title=$e.attr('data-original-title')||(typeof o.title=='function'?o.title.call($e[0]):o.title)
return title},tip:function()
{return this.$tip=this.$tip||a(this.options.template)},arrow:function()
{return this.$arrow=this.$arrow||this.tip().find(".tooltip-arrow")},validate:function()
{if(!this.$element[0].parentNode)
{this.hide()
this.$element=null
this.options=null}},enable:function()
{this.enabled=true},disable:function()
{this.enabled=false},toggleEnabled:function()
{this.enabled=!this.enabled},toggle:function(e)
{var self=e?a(e.currentTarget)[this.type](this._options).data(this.type):this
self.tip().hasClass('in')?self.hide():self.show()},destroy:function()
{this.hide().$element.off('.'+this.type).removeData(this.type)}}
var old=a.fn.tooltip
a.fn.tooltip=function(option)
{return this.each(function()
{var $this=a(this),data=$this.data('tooltip'),options=typeof option=='object'&&option
if(!data)$this.data('tooltip',(data=new Tooltip(this,options)))
if(typeof option=='string')data[option]()})}
a.fn.tooltip.Constructor=Tooltip
a.fn.tooltip.defaults={animation:true,placement:'top',selector:false,template:'<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',trigger:'hover focus',title:'',delay:0,html:false,container:false}
a.fn.tooltip.noConflict=function()
{a.fn.tooltip=old
return this}}(window.jfbcJQuery);!function(a)
{"use strict";var dismiss='[data-dismiss="alert"]',Alert=function(el)
{a(el).on('click',dismiss,this.close)}
Alert.prototype.close=function(e)
{var $this=a(this),selector=$this.attr('data-target'),$parent
if(!selector)
{selector=$this.attr('href')
selector=selector&&selector.replace(/.*(?=#[^\s]*$)/,'')}
$parent=a(selector)
e&&e.preventDefault()
$parent.length||($parent=$this.hasClass('alert')?$this:$this.parent())
$parent.trigger(e=a.Event('close'))
if(e.isDefaultPrevented())return
$parent.removeClass('in')
function removeElement()
{$parent.trigger('closed').remove()}
a.support.transition&&$parent.hasClass('fade')?$parent.on(a.support.transition.end,removeElement):removeElement()}
var old=a.fn.alert
a.fn.alert=function(option)
{return this.each(function()
{var $this=a(this),data=$this.data('alert')
if(!data)$this.data('alert',(data=new Alert(this)))
if(typeof option=='string')data[option].call($this)})}
a.fn.alert.Constructor=Alert
a.fn.alert.noConflict=function()
{a.fn.alert=old
return this}
a(document).on('click.alert.data-api',dismiss,Alert.prototype.close)}(window.jfbcJQuery),!function(a)
{"use strict";var Collapse=function(element,options)
{this.$element=a(element)
this.options=a.extend({},a.fn.collapse.defaults,options)
if(this.options.parent)
{this.$parent=a(this.options.parent)}
this.options.toggle&&this.toggle()}
Collapse.prototype={constructor:Collapse,dimension:function()
{var hasWidth=this.$element.hasClass('width')
return hasWidth?'width':'height'},show:function()
{var dimension,scroll,actives,hasData
if(this.transitioning||this.$element.hasClass('in'))return
dimension=this.dimension()
scroll=a.camelCase(['scroll',dimension].join('-'))
actives=this.$parent&&this.$parent.find('> .accordion-group > .in')
if(actives&&actives.length)
{hasData=actives.data('collapse')
if(hasData&&hasData.transitioning)return
actives.collapse('hide')
hasData||actives.data('collapse',null)}
this.$element[dimension](0)
this.transition('addClass',a.Event('show'),'shown')
a.support.transition&&this.$element[dimension](this.$element[0][scroll])},hide:function()
{var dimension
if(this.transitioning||!this.$element.hasClass('in'))return
dimension=this.dimension()
this.reset(this.$element[dimension]())
this.transition('removeClass',a.Event('hide'),'hidden')
this.$element[dimension](0)},reset:function(size)
{var dimension=this.dimension()
this.$element.removeClass('collapse')
[dimension](size||'auto')
[0].offsetWidth
this.$element[size!==null?'addClass':'removeClass']('collapse')
return this},transition:function(method,startEvent,completeEvent)
{var that=this,complete=function()
{if(startEvent.type=='show')that.reset()
that.transitioning=0
that.$element.trigger(completeEvent)}
this.$element.trigger(startEvent)
if(startEvent.isDefaultPrevented())return
this.$element[method]('in')
a.support.transition&&this.$element.hasClass('collapse')?this.$element.one(a.support.transition.end,complete):complete()},toggle:function()
{this[this.$element.hasClass('in')?'hide':'show']()}}
var old=a.fn.collapse
a.fn.collapse=function(option)
{return this.each(function()
{var $this=a(this),data=$this.data('collapse'),options=a.extend({},a.fn.collapse.defaults,$this.data(),typeof option=='object'&&option)
if(!data)$this.data('collapse',(data=new Collapse(this,options)))
if(typeof option=='string')data[option]()})}
a.fn.collapse.defaults={toggle:true}
a.fn.collapse.Constructor=Collapse
a.fn.collapse.noConflict=function()
{a.fn.collapse=old
return this}
a(document).on('click.collapse.data-api','[data-toggle=jfbc-collapse]',function(e)
{var $this=a(this),href,target=$this.attr('data-target')||e.preventDefault()||(href=$this.attr('href'))&&href.replace(/.*(?=#[^\s]+$)/,''),option=a(target).data('collapse')?'toggle':$this.data()
$this[a(target).hasClass('in')?'addClass':'removeClass']('collapsed')
a(target).collapse(option)})}(window.jfbcJQuery);


/*===============================
/plugins/system/t3/base-bs3/bootstrap/js/bootstrap.js
================================================================================*/;
/*!
 * Bootstrap v3.3.6 (http://getbootstrap.com)
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under the MIT license
 */
if(typeof jQuery==='undefined'){throw new Error('Bootstrap\'s JavaScript requires jQuery')}
+function($){'use strict';var version=$.fn.jquery.split(' ')[0].split('.')
if((version[0]<2&&version[1]<9)||(version[0]==1&&version[1]==9&&version[2]<1)||(version[0]>2)){throw new Error('Bootstrap\'s JavaScript requires jQuery version 1.9.1 or higher, but lower than version 3')}}(jQuery);+function($){'use strict';function transitionEnd(){var el=document.createElement('bootstrap')
var transEndEventNames={WebkitTransition:'webkitTransitionEnd',MozTransition:'transitionend',OTransition:'oTransitionEnd otransitionend',transition:'transitionend'}
for(var name in transEndEventNames){if(el.style[name]!==undefined){return{end:transEndEventNames[name]}}}
return false}
$.fn.emulateTransitionEnd=function(duration){var called=false
var $el=this
$(this).one('bsTransitionEnd',function(){called=true})
var callback=function(){if(!called)$($el).trigger($.support.transition.end)}
setTimeout(callback,duration)
return this}
$(function(){$.support.transition=transitionEnd()
if(!$.support.transition)return
$.event.special.bsTransitionEnd={bindType:$.support.transition.end,delegateType:$.support.transition.end,handle:function(e){if($(e.target).is(this))return e.handleObj.handler.apply(this,arguments)}}})}(jQuery);+function($){'use strict';var dismiss='[data-dismiss="alert"]'
var Alert=function(el){$(el).on('click',dismiss,this.close)}
Alert.VERSION='3.3.6'
Alert.TRANSITION_DURATION=150
Alert.prototype.close=function(e){var $this=$(this)
var selector=$this.attr('data-target')
if(!selector){selector=$this.attr('href')
selector=selector&&selector.replace(/.*(?=#[^\s]*$)/,'')}
var $parent=$(selector)
if(e)e.preventDefault()
if(!$parent.length){$parent=$this.closest('.alert')}
$parent.trigger(e=$.Event('close.bs.alert'))
if(e.isDefaultPrevented())return
$parent.removeClass('in')
function removeElement(){$parent.detach().trigger('closed.bs.alert').remove()}
$.support.transition&&$parent.hasClass('fade')?$parent.one('bsTransitionEnd',removeElement).emulateTransitionEnd(Alert.TRANSITION_DURATION):removeElement()}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('bs.alert')
if(!data)$this.data('bs.alert',(data=new Alert(this)))
if(typeof option=='string')data[option].call($this)})}
var old=$.fn.alert
$.fn.alert=Plugin
$.fn.alert.Constructor=Alert
$.fn.alert.noConflict=function(){$.fn.alert=old
return this}
$(document).on('click.bs.alert.data-api',dismiss,Alert.prototype.close)}(jQuery);+function($){'use strict';var Button=function(element,options){this.$element=$(element)
this.options=$.extend({},Button.DEFAULTS,options)
this.isLoading=false}
Button.VERSION='3.3.6'
Button.DEFAULTS={loadingText:'loading...'}
Button.prototype.setState=function(state){var d='disabled'
var $el=this.$element
var val=$el.is('input')?'val':'html'
var data=$el.data()
state+='Text'
if(data.resetText==null)$el.data('resetText',$el[val]())
setTimeout($.proxy(function(){$el[val](data[state]==null?this.options[state]:data[state])
if(state=='loadingText'){this.isLoading=true
$el.addClass(d).attr(d,d)}else if(this.isLoading){this.isLoading=false
$el.removeClass(d).removeAttr(d)}},this),0)}
Button.prototype.toggle=function(){var changed=true
var $parent=this.$element.closest('[data-toggle="buttons"]')
if($parent.length){var $input=this.$element.find('input')
if($input.prop('type')=='radio'){if($input.prop('checked'))changed=false
$parent.find('.active').removeClass('active')
this.$element.addClass('active')}else if($input.prop('type')=='checkbox'){if(($input.prop('checked'))!==this.$element.hasClass('active'))changed=false
this.$element.toggleClass('active')}
$input.prop('checked',this.$element.hasClass('active'))
if(changed)$input.trigger('change')}else{this.$element.attr('aria-pressed',!this.$element.hasClass('active'))
this.$element.toggleClass('active')}}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('bs.button')
var options=typeof option=='object'&&option
if(!data)$this.data('bs.button',(data=new Button(this,options)))
if(option=='toggle')data.toggle()
else if(option)data.setState(option)})}
var old=$.fn.button
$.fn.button=Plugin
$.fn.button.Constructor=Button
$.fn.button.noConflict=function(){$.fn.button=old
return this}
$(document).on('click.bs.button.data-api','[data-toggle^="button"]',function(e){var $btn=$(e.target)
if(!$btn.hasClass('btn'))$btn=$btn.closest('.btn')
Plugin.call($btn,'toggle')
if(!($(e.target).is('input[type="radio"]')||$(e.target).is('input[type="checkbox"]')))e.preventDefault()}).on('focus.bs.button.data-api blur.bs.button.data-api','[data-toggle^="button"]',function(e){$(e.target).closest('.btn').toggleClass('focus',/^focus(in)?$/.test(e.type))})}(jQuery);+function($){'use strict';var Carousel=function(element,options){this.$element=$(element)
this.$indicators=this.$element.find('.carousel-indicators')
this.options=options
this.paused=null
this.sliding=null
this.interval=null
this.$active=null
this.$items=null
this.options.keyboard&&this.$element.on('keydown.bs.carousel',$.proxy(this.keydown,this))
this.options.pause=='hover'&&!('ontouchstart'in document.documentElement)&&this.$element.on('mouseenter.bs.carousel',$.proxy(this.pause,this)).on('mouseleave.bs.carousel',$.proxy(this.cycle,this))}
Carousel.VERSION='3.3.6'
Carousel.TRANSITION_DURATION=600
Carousel.DEFAULTS={interval:5000,pause:'hover',wrap:true,keyboard:true}
Carousel.prototype.keydown=function(e){if(/input|textarea/i.test(e.target.tagName))return
switch(e.which){case 37:this.prev();break
case 39:this.next();break
default:return}
e.preventDefault()}
Carousel.prototype.cycle=function(e){e||(this.paused=false)
this.interval&&clearInterval(this.interval)
this.options.interval&&!this.paused&&(this.interval=setInterval($.proxy(this.next,this),this.options.interval))
return this}
Carousel.prototype.getItemIndex=function(item){this.$items=item.parent().children('.item')
return this.$items.index(item||this.$active)}
Carousel.prototype.getItemForDirection=function(direction,active){var activeIndex=this.getItemIndex(active)
var willWrap=(direction=='prev'&&activeIndex===0)||(direction=='next'&&activeIndex==(this.$items.length-1))
if(willWrap&&!this.options.wrap)return active
var delta=direction=='prev'?-1:1
var itemIndex=(activeIndex+delta)%this.$items.length
return this.$items.eq(itemIndex)}
Carousel.prototype.to=function(pos){var that=this
var activeIndex=this.getItemIndex(this.$active=this.$element.find('.item.active'))
if(pos>(this.$items.length-1)||pos<0)return
if(this.sliding)return this.$element.one('slid.bs.carousel',function(){that.to(pos)})
if(activeIndex==pos)return this.pause().cycle()
return this.slide(pos>activeIndex?'next':'prev',this.$items.eq(pos))}
Carousel.prototype.pause=function(e){e||(this.paused=true)
if(this.$element.find('.next, .prev').length&&$.support.transition){this.$element.trigger($.support.transition.end)
this.cycle(true)}
this.interval=clearInterval(this.interval)
return this}
Carousel.prototype.next=function(){if(this.sliding)return
return this.slide('next')}
Carousel.prototype.prev=function(){if(this.sliding)return
return this.slide('prev')}
Carousel.prototype.slide=function(type,next){var $active=this.$element.find('.item.active')
var $next=next||this.getItemForDirection(type,$active)
var isCycling=this.interval
var direction=type=='next'?'left':'right'
var that=this
if($next.hasClass('active'))return(this.sliding=false)
var relatedTarget=$next[0]
var slideEvent=$.Event('slide.bs.carousel',{relatedTarget:relatedTarget,direction:direction})
this.$element.trigger(slideEvent)
if(slideEvent.isDefaultPrevented())return
this.sliding=true
isCycling&&this.pause()
if(this.$indicators.length){this.$indicators.find('.active').removeClass('active')
var $nextIndicator=$(this.$indicators.children()[this.getItemIndex($next)])
$nextIndicator&&$nextIndicator.addClass('active')}
var slidEvent=$.Event('slid.bs.carousel',{relatedTarget:relatedTarget,direction:direction})
if($.support.transition&&this.$element.hasClass('slide')){$next.addClass(type)
$next[0].offsetWidth
$active.addClass(direction)
$next.addClass(direction)
$active.one('bsTransitionEnd',function(){$next.removeClass([type,direction].join(' ')).addClass('active')
$active.removeClass(['active',direction].join(' '))
that.sliding=false
setTimeout(function(){that.$element.trigger(slidEvent)},0)}).emulateTransitionEnd(Carousel.TRANSITION_DURATION)}else{$active.removeClass('active')
$next.addClass('active')
this.sliding=false
this.$element.trigger(slidEvent)}
isCycling&&this.cycle()
return this}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('bs.carousel')
var options=$.extend({},Carousel.DEFAULTS,$this.data(),typeof option=='object'&&option)
var action=typeof option=='string'?option:options.slide
if(!data)$this.data('bs.carousel',(data=new Carousel(this,options)))
if(typeof option=='number')data.to(option)
else if(action)data[action]()
else if(options.interval)data.pause().cycle()})}
var old=$.fn.carousel
$.fn.carousel=Plugin
$.fn.carousel.Constructor=Carousel
$.fn.carousel.noConflict=function(){$.fn.carousel=old
return this}
var clickHandler=function(e){var href
var $this=$(this)
var $target=$($this.attr('data-target')||(href=$this.attr('href'))&&href.replace(/.*(?=#[^\s]+$)/,''))
if(!$target.hasClass('carousel'))return
var options=$.extend({},$target.data(),$this.data())
var slideIndex=$this.attr('data-slide-to')
if(slideIndex)options.interval=false
Plugin.call($target,options)
if(slideIndex){$target.data('bs.carousel').to(slideIndex)}
e.preventDefault()}
$(document).on('click.bs.carousel.data-api','[data-slide]',clickHandler).on('click.bs.carousel.data-api','[data-slide-to]',clickHandler)
$(window).on('load',function(){$('[data-ride="carousel"]').each(function(){var $carousel=$(this)
Plugin.call($carousel,$carousel.data())})})}(jQuery);+function($){'use strict';var Collapse=function(element,options){this.$element=$(element)
this.options=$.extend({},Collapse.DEFAULTS,options)
this.$trigger=$('[data-toggle="collapse"][href="#'+element.id+'"],'+'[data-toggle="collapse"][data-target="#'+element.id+'"]')
this.transitioning=null
if(this.options.parent){this.$parent=this.getParent()}else{this.addAriaAndCollapsedClass(this.$element,this.$trigger)}
if(this.options.toggle)this.toggle()}
Collapse.VERSION='3.3.6'
Collapse.TRANSITION_DURATION=350
Collapse.DEFAULTS={toggle:true}
Collapse.prototype.dimension=function(){var hasWidth=this.$element.hasClass('width')
return hasWidth?'width':'height'}
Collapse.prototype.show=function(){if(this.transitioning||this.$element.hasClass('in'))return
var activesData
var actives=this.$parent&&this.$parent.children('.panel').children('.in, .collapsing')
if(actives&&actives.length){activesData=actives.data('bs.collapse')
if(activesData&&activesData.transitioning)return}
var startEvent=$.Event('show.bs.collapse')
this.$element.trigger(startEvent)
if(startEvent.isDefaultPrevented())return
if(actives&&actives.length){Plugin.call(actives,'hide')
activesData||actives.data('bs.collapse',null)}
var dimension=this.dimension()
this.$element.removeClass('collapse').addClass('collapsing')[dimension](0).attr('aria-expanded',true)
this.$trigger.removeClass('collapsed').attr('aria-expanded',true)
this.transitioning=1
var complete=function(){this.$element.removeClass('collapsing').addClass('collapse in')[dimension]('')
this.transitioning=0
this.$element.trigger('shown.bs.collapse')}
if(!$.support.transition)return complete.call(this)
var scrollSize=$.camelCase(['scroll',dimension].join('-'))
this.$element.one('bsTransitionEnd',$.proxy(complete,this)).emulateTransitionEnd(Collapse.TRANSITION_DURATION)[dimension](this.$element[0][scrollSize])}
Collapse.prototype.hide=function(){if(this.transitioning||!this.$element.hasClass('in'))return
var startEvent=$.Event('hide.bs.collapse')
this.$element.trigger(startEvent)
if(startEvent.isDefaultPrevented())return
var dimension=this.dimension()
this.$element[dimension](this.$element[dimension]())[0].offsetHeight
this.$element.addClass('collapsing').removeClass('collapse in').attr('aria-expanded',false)
this.$trigger.addClass('collapsed').attr('aria-expanded',false)
this.transitioning=1
var complete=function(){this.transitioning=0
this.$element.removeClass('collapsing').addClass('collapse').trigger('hidden.bs.collapse')}
if(!$.support.transition)return complete.call(this)
this.$element
[dimension](0).one('bsTransitionEnd',$.proxy(complete,this)).emulateTransitionEnd(Collapse.TRANSITION_DURATION)}
Collapse.prototype.toggle=function(){this[this.$element.hasClass('in')?'hide':'show']()}
Collapse.prototype.getParent=function(){return $(this.options.parent).find('[data-toggle="collapse"][data-parent="'+this.options.parent+'"]').each($.proxy(function(i,element){var $element=$(element)
this.addAriaAndCollapsedClass(getTargetFromTrigger($element),$element)},this)).end()}
Collapse.prototype.addAriaAndCollapsedClass=function($element,$trigger){var isOpen=$element.hasClass('in')
$element.attr('aria-expanded',isOpen)
$trigger.toggleClass('collapsed',!isOpen).attr('aria-expanded',isOpen)}
function getTargetFromTrigger($trigger){var href
var target=$trigger.attr('data-target')||(href=$trigger.attr('href'))&&href.replace(/.*(?=#[^\s]+$)/,'')
return $(target)}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('bs.collapse')
var options=$.extend({},Collapse.DEFAULTS,$this.data(),typeof option=='object'&&option)
if(!data&&options.toggle&&/show|hide/.test(option))options.toggle=false
if(!data)$this.data('bs.collapse',(data=new Collapse(this,options)))
if(typeof option=='string')data[option]()})}
var old=$.fn.collapse
$.fn.collapse=Plugin
$.fn.collapse.Constructor=Collapse
$.fn.collapse.noConflict=function(){$.fn.collapse=old
return this}
$(document).on('click.bs.collapse.data-api','[data-toggle="collapse"]',function(e){var $this=$(this)
if(!$this.attr('data-target'))e.preventDefault()
var $target=getTargetFromTrigger($this)
var data=$target.data('bs.collapse')
var option=data?'toggle':$this.data()
Plugin.call($target,option)})}(jQuery);+function($){'use strict';var backdrop='.dropdown-backdrop'
var toggle='[data-toggle="dropdown"]'
var Dropdown=function(element){$(element).on('click.bs.dropdown',this.toggle)}
Dropdown.VERSION='3.3.6'
function getParent($this){var selector=$this.attr('data-target')
if(!selector){selector=$this.attr('href')
selector=selector&&/#[A-Za-z]/.test(selector)&&selector.replace(/.*(?=#[^\s]*$)/,'')}
var $parent=selector&&$(selector)
return $parent&&$parent.length?$parent:$this.parent()}
function clearMenus(e){if(e&&e.which===3)return
$(backdrop).remove()
$(toggle).each(function(){var $this=$(this)
var $parent=getParent($this)
var relatedTarget={relatedTarget:this}
if(!$parent.hasClass('open'))return
if(e&&e.type=='click'&&/input|textarea/i.test(e.target.tagName)&&$.contains($parent[0],e.target))return
$parent.trigger(e=$.Event('hide.bs.dropdown',relatedTarget))
if(e.isDefaultPrevented())return
$this.attr('aria-expanded','false')
$parent.removeClass('open').trigger($.Event('hidden.bs.dropdown',relatedTarget))})}
Dropdown.prototype.toggle=function(e){var $this=$(this)
if($this.is('.disabled, :disabled'))return
var $parent=getParent($this)
var isActive=$parent.hasClass('open')
clearMenus()
if(!isActive){if('ontouchstart'in document.documentElement&&!$parent.closest('.navbar-nav').length){$(document.createElement('div')).addClass('dropdown-backdrop').insertAfter($(this)).on('click',clearMenus)}
var relatedTarget={relatedTarget:this}
$parent.trigger(e=$.Event('show.bs.dropdown',relatedTarget))
if(e.isDefaultPrevented())return
$this.trigger('focus').attr('aria-expanded','true')
$parent.toggleClass('open').trigger($.Event('shown.bs.dropdown',relatedTarget))}
return false}
Dropdown.prototype.keydown=function(e){if(!/(38|40|27|32)/.test(e.which)||/input|textarea/i.test(e.target.tagName))return
var $this=$(this)
e.preventDefault()
e.stopPropagation()
if($this.is('.disabled, :disabled'))return
var $parent=getParent($this)
var isActive=$parent.hasClass('open')
if(!isActive&&e.which!=27||isActive&&e.which==27){if(e.which==27)$parent.find(toggle).trigger('focus')
return $this.trigger('click')}
var desc=' li:not(.disabled):visible a'
var $items=$parent.find('.dropdown-menu'+desc)
if(!$items.length)return
var index=$items.index(e.target)
if(e.which==38&&index>0)index--
if(e.which==40&&index<$items.length-1)index++
if(!~index)index=0
$items.eq(index).trigger('focus')}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('bs.dropdown')
if(!data)$this.data('bs.dropdown',(data=new Dropdown(this)))
if(typeof option=='string')data[option].call($this)})}
var old=$.fn.dropdown
$.fn.dropdown=Plugin
$.fn.dropdown.Constructor=Dropdown
$.fn.dropdown.noConflict=function(){$.fn.dropdown=old
return this}
$(document).on('click.bs.dropdown.data-api',clearMenus).on('click.bs.dropdown.data-api','.dropdown form',function(e){e.stopPropagation()}).on('click.bs.dropdown.data-api',toggle,Dropdown.prototype.toggle).on('keydown.bs.dropdown.data-api',toggle,Dropdown.prototype.keydown).on('keydown.bs.dropdown.data-api','.dropdown-menu',Dropdown.prototype.keydown)}(jQuery);+function($){'use strict';var Modal=function(element,options){this.options=options
this.$body=$(document.body)
this.$element=$(element)
this.$dialog=this.$element.find('.modal-dialog')
this.$backdrop=null
this.isShown=null
this.originalBodyPad=null
this.scrollbarWidth=0
this.ignoreBackdropClick=false
if(this.options.remote){this.$element.find('.modal-content').load(this.options.remote,$.proxy(function(){this.$element.trigger('loaded.bs.modal')},this))}}
Modal.VERSION='3.3.6'
Modal.TRANSITION_DURATION=300
Modal.BACKDROP_TRANSITION_DURATION=150
Modal.DEFAULTS={backdrop:true,keyboard:true,show:true}
Modal.prototype.toggle=function(_relatedTarget){return this.isShown?this.hide():this.show(_relatedTarget)}
Modal.prototype.show=function(_relatedTarget){var that=this
var e=$.Event('show.bs.modal',{relatedTarget:_relatedTarget})
this.$element.trigger(e)
if(this.isShown||e.isDefaultPrevented())return
this.isShown=true
this.checkScrollbar()
this.setScrollbar()
this.$body.addClass('modal-open')
this.escape()
this.resize()
this.$element.on('click.dismiss.bs.modal','[data-dismiss="modal"]',$.proxy(this.hide,this))
this.$dialog.on('mousedown.dismiss.bs.modal',function(){that.$element.one('mouseup.dismiss.bs.modal',function(e){if($(e.target).is(that.$element))that.ignoreBackdropClick=true})})
this.backdrop(function(){var transition=$.support.transition&&that.$element.hasClass('fade')
if(!that.$element.parent().length){that.$element.appendTo(that.$body)}
that.$element.show().scrollTop(0)
that.adjustDialog()
if(transition){that.$element[0].offsetWidth}
that.$element.addClass('in')
that.enforceFocus()
var e=$.Event('shown.bs.modal',{relatedTarget:_relatedTarget})
transition?that.$dialog.one('bsTransitionEnd',function(){that.$element.trigger('focus').trigger(e)}).emulateTransitionEnd(Modal.TRANSITION_DURATION):that.$element.trigger('focus').trigger(e)})}
Modal.prototype.hide=function(e){if(e)e.preventDefault()
e=$.Event('hide.bs.modal')
this.$element.trigger(e)
if(!this.isShown||e.isDefaultPrevented())return
this.isShown=false
this.escape()
this.resize()
$(document).off('focusin.bs.modal')
this.$element.removeClass('in').off('click.dismiss.bs.modal').off('mouseup.dismiss.bs.modal')
this.$dialog.off('mousedown.dismiss.bs.modal')
$.support.transition&&this.$element.hasClass('fade')?this.$element.one('bsTransitionEnd',$.proxy(this.hideModal,this)).emulateTransitionEnd(Modal.TRANSITION_DURATION):this.hideModal()}
Modal.prototype.enforceFocus=function(){$(document).off('focusin.bs.modal').on('focusin.bs.modal',$.proxy(function(e){if(this.$element[0]!==e.target&&!this.$element.has(e.target).length){this.$element.trigger('focus')}},this))}
Modal.prototype.escape=function(){if(this.isShown&&this.options.keyboard){this.$element.on('keydown.dismiss.bs.modal',$.proxy(function(e){e.which==27&&this.hide()},this))}else if(!this.isShown){this.$element.off('keydown.dismiss.bs.modal')}}
Modal.prototype.resize=function(){if(this.isShown){$(window).on('resize.bs.modal',$.proxy(this.handleUpdate,this))}else{$(window).off('resize.bs.modal')}}
Modal.prototype.hideModal=function(){var that=this
this.$element.hide()
this.backdrop(function(){that.$body.removeClass('modal-open')
that.resetAdjustments()
that.resetScrollbar()
that.$element.trigger('hidden.bs.modal')})}
Modal.prototype.removeBackdrop=function(){this.$backdrop&&this.$backdrop.remove()
this.$backdrop=null}
Modal.prototype.backdrop=function(callback){var that=this
var animate=this.$element.hasClass('fade')?'fade':''
if(this.isShown&&this.options.backdrop){var doAnimate=$.support.transition&&animate
this.$backdrop=$(document.createElement('div')).addClass('modal-backdrop '+animate).appendTo(this.$body)
this.$element.on('click.dismiss.bs.modal',$.proxy(function(e){if(this.ignoreBackdropClick){this.ignoreBackdropClick=false
return}
if(e.target!==e.currentTarget)return
this.options.backdrop=='static'?this.$element[0].focus():this.hide()},this))
if(doAnimate)this.$backdrop[0].offsetWidth
this.$backdrop.addClass('in')
if(!callback)return
doAnimate?this.$backdrop.one('bsTransitionEnd',callback).emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION):callback()}else if(!this.isShown&&this.$backdrop){this.$backdrop.removeClass('in')
var callbackRemove=function(){that.removeBackdrop()
callback&&callback()}
$.support.transition&&this.$element.hasClass('fade')?this.$backdrop.one('bsTransitionEnd',callbackRemove).emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION):callbackRemove()}else if(callback){callback()}}
Modal.prototype.handleUpdate=function(){this.adjustDialog()}
Modal.prototype.adjustDialog=function(){var modalIsOverflowing=this.$element[0].scrollHeight>document.documentElement.clientHeight
this.$element.css({paddingLeft:!this.bodyIsOverflowing&&modalIsOverflowing?this.scrollbarWidth:'',paddingRight:this.bodyIsOverflowing&&!modalIsOverflowing?this.scrollbarWidth:''})}
Modal.prototype.resetAdjustments=function(){this.$element.css({paddingLeft:'',paddingRight:''})}
Modal.prototype.checkScrollbar=function(){var fullWindowWidth=window.innerWidth
if(!fullWindowWidth){var documentElementRect=document.documentElement.getBoundingClientRect()
fullWindowWidth=documentElementRect.right-Math.abs(documentElementRect.left)}
this.bodyIsOverflowing=document.body.clientWidth<fullWindowWidth
this.scrollbarWidth=this.measureScrollbar()}
Modal.prototype.setScrollbar=function(){var bodyPad=parseInt((this.$body.css('padding-right')||0),10)
this.originalBodyPad=document.body.style.paddingRight||''
if(this.bodyIsOverflowing)this.$body.css('padding-right',bodyPad+this.scrollbarWidth)}
Modal.prototype.resetScrollbar=function(){this.$body.css('padding-right',this.originalBodyPad)}
Modal.prototype.measureScrollbar=function(){var scrollDiv=document.createElement('div')
scrollDiv.className='modal-scrollbar-measure'
this.$body.append(scrollDiv)
var scrollbarWidth=scrollDiv.offsetWidth-scrollDiv.clientWidth
this.$body[0].removeChild(scrollDiv)
return scrollbarWidth}
function Plugin(option,_relatedTarget){return this.each(function(){var $this=$(this)
var data=$this.data('bs.modal')
var options=$.extend({},Modal.DEFAULTS,$this.data(),typeof option=='object'&&option)
if(!data)$this.data('bs.modal',(data=new Modal(this,options)))
if(typeof option=='string')data[option](_relatedTarget)
else if(options.show)data.show(_relatedTarget)})}
var old=$.fn.modal
$.fn.modal=Plugin
$.fn.modal.Constructor=Modal
$.fn.modal.noConflict=function(){$.fn.modal=old
return this}
$(document).on('click.bs.modal.data-api','[data-toggle="modal"]',function(e){var $this=$(this)
var href=$this.attr('href')
var $target=$($this.attr('data-target')||(href&&href.replace(/.*(?=#[^\s]+$)/,'')))
var option=$target.data('bs.modal')?'toggle':$.extend({remote:!/#/.test(href)&&href},$target.data(),$this.data())
if($this.is('a'))e.preventDefault()
$target.one('show.bs.modal',function(showEvent){if(showEvent.isDefaultPrevented())return
$target.one('hidden.bs.modal',function(){$this.is(':visible')&&$this.trigger('focus')})})
Plugin.call($target,option,this)})}(jQuery);+function($){'use strict';var Tooltip=function(element,options){this.type=null
this.options=null
this.enabled=null
this.timeout=null
this.hoverState=null
this.$element=null
this.inState=null
this.init('tooltip',element,options)}
Tooltip.VERSION='3.3.6'
Tooltip.TRANSITION_DURATION=150
Tooltip.DEFAULTS={animation:true,placement:'top',selector:false,template:'<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',trigger:'hover focus',title:'',delay:0,html:false,container:false,viewport:{selector:'body',padding:0}}
Tooltip.prototype.init=function(type,element,options){this.enabled=true
this.type=type
this.$element=$(element)
this.options=this.getOptions(options)
this.$viewport=this.options.viewport&&$($.isFunction(this.options.viewport)?this.options.viewport.call(this,this.$element):(this.options.viewport.selector||this.options.viewport))
this.inState={click:false,hover:false,focus:false}
if(this.$element[0]instanceof document.constructor&&!this.options.selector){throw new Error('`selector` option must be specified when initializing '+this.type+' on the window.document object!')}
var triggers=this.options.trigger.split(' ')
for(var i=triggers.length;i--;){var trigger=triggers[i]
if(trigger=='click'){this.$element.on('click.'+this.type,this.options.selector,$.proxy(this.toggle,this))}else if(trigger!='manual'){var eventIn=trigger=='hover'?'mouseenter':'focusin'
var eventOut=trigger=='hover'?'mouseleave':'focusout'
this.$element.on(eventIn+'.'+this.type,this.options.selector,$.proxy(this.enter,this))
this.$element.on(eventOut+'.'+this.type,this.options.selector,$.proxy(this.leave,this))}}
this.options.selector?(this._options=$.extend({},this.options,{trigger:'manual',selector:''})):this.fixTitle()}
Tooltip.prototype.getDefaults=function(){return Tooltip.DEFAULTS}
Tooltip.prototype.getOptions=function(options){options=$.extend({},this.getDefaults(),this.$element.data(),options)
if(options.delay&&typeof options.delay=='number'){options.delay={show:options.delay,hide:options.delay}}
return options}
Tooltip.prototype.getDelegateOptions=function(){var options={}
var defaults=this.getDefaults()
this._options&&$.each(this._options,function(key,value){if(defaults[key]!=value)options[key]=value})
return options}
Tooltip.prototype.enter=function(obj){var self=obj instanceof this.constructor?obj:$(obj.currentTarget).data('bs.'+this.type)
if(!self){self=new this.constructor(obj.currentTarget,this.getDelegateOptions())
$(obj.currentTarget).data('bs.'+this.type,self)}
if(obj instanceof $.Event){self.inState[obj.type=='focusin'?'focus':'hover']=true}
if(self.tip().hasClass('in')||self.hoverState=='in'){self.hoverState='in'
return}
clearTimeout(self.timeout)
self.hoverState='in'
if(!self.options.delay||!self.options.delay.show)return self.show()
self.timeout=setTimeout(function(){if(self.hoverState=='in')self.show()},self.options.delay.show)}
Tooltip.prototype.isInStateTrue=function(){for(var key in this.inState){if(this.inState[key])return true}
return false}
Tooltip.prototype.leave=function(obj){var self=obj instanceof this.constructor?obj:$(obj.currentTarget).data('bs.'+this.type)
if(!self){self=new this.constructor(obj.currentTarget,this.getDelegateOptions())
$(obj.currentTarget).data('bs.'+this.type,self)}
if(obj instanceof $.Event){self.inState[obj.type=='focusout'?'focus':'hover']=false}
if(self.isInStateTrue())return
clearTimeout(self.timeout)
self.hoverState='out'
if(!self.options.delay||!self.options.delay.hide)return self.hide()
self.timeout=setTimeout(function(){if(self.hoverState=='out')self.hide()},self.options.delay.hide)}
Tooltip.prototype.show=function(){var e=$.Event('show.bs.'+this.type)
if(this.hasContent()&&this.enabled){this.$element.trigger(e)
var inDom=$.contains(this.$element[0].ownerDocument.documentElement,this.$element[0])
if(e.isDefaultPrevented()||!inDom)return
var that=this
var $tip=this.tip()
var tipId=this.getUID(this.type)
this.setContent()
$tip.attr('id',tipId)
this.$element.attr('aria-describedby',tipId)
if(this.options.animation)$tip.addClass('fade')
var placement=typeof this.options.placement=='function'?this.options.placement.call(this,$tip[0],this.$element[0]):this.options.placement
var autoToken=/\s?auto?\s?/i
var autoPlace=autoToken.test(placement)
if(autoPlace)placement=placement.replace(autoToken,'')||'top'
$tip.detach().css({top:0,left:0,display:'block'}).addClass(placement).data('bs.'+this.type,this)
this.options.container?$tip.appendTo(this.options.container):$tip.insertAfter(this.$element)
this.$element.trigger('inserted.bs.'+this.type)
var pos=this.getPosition()
var actualWidth=$tip[0].offsetWidth
var actualHeight=$tip[0].offsetHeight
if(autoPlace){var orgPlacement=placement
var viewportDim=this.getPosition(this.$viewport)
placement=placement=='bottom'&&pos.bottom+actualHeight>viewportDim.bottom?'top':placement=='top'&&pos.top-actualHeight<viewportDim.top?'bottom':placement=='right'&&pos.right+actualWidth>viewportDim.width?'left':placement=='left'&&pos.left-actualWidth<viewportDim.left?'right':placement
$tip.removeClass(orgPlacement).addClass(placement)}
var calculatedOffset=this.getCalculatedOffset(placement,pos,actualWidth,actualHeight)
this.applyPlacement(calculatedOffset,placement)
var complete=function(){var prevHoverState=that.hoverState
that.$element.trigger('shown.bs.'+that.type)
that.hoverState=null
if(prevHoverState=='out')that.leave(that)}
$.support.transition&&this.$tip.hasClass('fade')?$tip.one('bsTransitionEnd',complete).emulateTransitionEnd(Tooltip.TRANSITION_DURATION):complete()}}
Tooltip.prototype.applyPlacement=function(offset,placement){var $tip=this.tip()
var width=$tip[0].offsetWidth
var height=$tip[0].offsetHeight
var marginTop=parseInt($tip.css('margin-top'),10)
var marginLeft=parseInt($tip.css('margin-left'),10)
if(isNaN(marginTop))marginTop=0
if(isNaN(marginLeft))marginLeft=0
offset.top+=marginTop
offset.left+=marginLeft
$.offset.setOffset($tip[0],$.extend({using:function(props){$tip.css({top:Math.round(props.top),left:Math.round(props.left)})}},offset),0)
$tip.addClass('in')
var actualWidth=$tip[0].offsetWidth
var actualHeight=$tip[0].offsetHeight
if(placement=='top'&&actualHeight!=height){offset.top=offset.top+height-actualHeight}
var delta=this.getViewportAdjustedDelta(placement,offset,actualWidth,actualHeight)
if(delta.left)offset.left+=delta.left
else offset.top+=delta.top
var isVertical=/top|bottom/.test(placement)
var arrowDelta=isVertical?delta.left*2-width+actualWidth:delta.top*2-height+actualHeight
var arrowOffsetPosition=isVertical?'offsetWidth':'offsetHeight'
$tip.offset(offset)
this.replaceArrow(arrowDelta,$tip[0][arrowOffsetPosition],isVertical)}
Tooltip.prototype.replaceArrow=function(delta,dimension,isVertical){this.arrow().css(isVertical?'left':'top',50*(1-delta/dimension)+'%').css(isVertical?'top':'left','')}
Tooltip.prototype.setContent=function(){var $tip=this.tip()
var title=this.getTitle()
$tip.find('.tooltip-inner')[this.options.html?'html':'text'](title)
$tip.removeClass('fade in top bottom left right')}
Tooltip.prototype.hide=function(callback){var that=this
var $tip=$(this.$tip)
var e=$.Event('hide.bs.'+this.type)
function complete(){if(that.hoverState!='in')$tip.detach()
that.$element.removeAttr('aria-describedby').trigger('hidden.bs.'+that.type)
callback&&callback()}
this.$element.trigger(e)
if(e.isDefaultPrevented())return
$tip.removeClass('in')
$.support.transition&&$tip.hasClass('fade')?$tip.one('bsTransitionEnd',complete).emulateTransitionEnd(Tooltip.TRANSITION_DURATION):complete()
this.hoverState=null
return this}
Tooltip.prototype.fixTitle=function(){var $e=this.$element
if($e.attr('title')||typeof $e.attr('data-original-title')!='string'){$e.attr('data-original-title',$e.attr('title')||'').attr('title','')}}
Tooltip.prototype.hasContent=function(){return this.getTitle()}
Tooltip.prototype.getPosition=function($element){$element=$element||this.$element
var el=$element[0]
var isBody=el.tagName=='BODY'
var elRect=el.getBoundingClientRect()
if(elRect.width==null){elRect=$.extend({},elRect,{width:elRect.right-elRect.left,height:elRect.bottom-elRect.top})}
var elOffset=isBody?{top:0,left:0}:$element.offset()
var scroll={scroll:isBody?document.documentElement.scrollTop||document.body.scrollTop:$element.scrollTop()}
var outerDims=isBody?{width:$(window).width(),height:$(window).height()}:null
return $.extend({},elRect,scroll,outerDims,elOffset)}
Tooltip.prototype.getCalculatedOffset=function(placement,pos,actualWidth,actualHeight){return placement=='bottom'?{top:pos.top+pos.height,left:pos.left+pos.width/2-actualWidth/2}:placement=='top'?{top:pos.top-actualHeight,left:pos.left+pos.width/2-actualWidth/2}:placement=='left'?{top:pos.top+pos.height/2-actualHeight/2,left:pos.left-actualWidth}:{top:pos.top+pos.height/2-actualHeight/2,left:pos.left+pos.width}}
Tooltip.prototype.getViewportAdjustedDelta=function(placement,pos,actualWidth,actualHeight){var delta={top:0,left:0}
if(!this.$viewport)return delta
var viewportPadding=this.options.viewport&&this.options.viewport.padding||0
var viewportDimensions=this.getPosition(this.$viewport)
if(/right|left/.test(placement)){var topEdgeOffset=pos.top-viewportPadding-viewportDimensions.scroll
var bottomEdgeOffset=pos.top+viewportPadding-viewportDimensions.scroll+actualHeight
if(topEdgeOffset<viewportDimensions.top){delta.top=viewportDimensions.top-topEdgeOffset}else if(bottomEdgeOffset>viewportDimensions.top+viewportDimensions.height){delta.top=viewportDimensions.top+viewportDimensions.height-bottomEdgeOffset}}else{var leftEdgeOffset=pos.left-viewportPadding
var rightEdgeOffset=pos.left+viewportPadding+actualWidth
if(leftEdgeOffset<viewportDimensions.left){delta.left=viewportDimensions.left-leftEdgeOffset}else if(rightEdgeOffset>viewportDimensions.right){delta.left=viewportDimensions.left+viewportDimensions.width-rightEdgeOffset}}
return delta}
Tooltip.prototype.getTitle=function(){var title
var $e=this.$element
var o=this.options
title=$e.attr('data-original-title')||(typeof o.title=='function'?o.title.call($e[0]):o.title)
return title}
Tooltip.prototype.getUID=function(prefix){do prefix+=~~(Math.random()*1000000)
while(document.getElementById(prefix))
return prefix}
Tooltip.prototype.tip=function(){if(!this.$tip){this.$tip=$(this.options.template)
if(this.$tip.length!=1){throw new Error(this.type+' `template` option must consist of exactly 1 top-level element!')}}
return this.$tip}
Tooltip.prototype.arrow=function(){return(this.$arrow=this.$arrow||this.tip().find('.tooltip-arrow'))}
Tooltip.prototype.enable=function(){this.enabled=true}
Tooltip.prototype.disable=function(){this.enabled=false}
Tooltip.prototype.toggleEnabled=function(){this.enabled=!this.enabled}
Tooltip.prototype.toggle=function(e){var self=this
if(e){self=$(e.currentTarget).data('bs.'+this.type)
if(!self){self=new this.constructor(e.currentTarget,this.getDelegateOptions())
$(e.currentTarget).data('bs.'+this.type,self)}}
if(e){self.inState.click=!self.inState.click
if(self.isInStateTrue())self.enter(self)
else self.leave(self)}else{self.tip().hasClass('in')?self.leave(self):self.enter(self)}}
Tooltip.prototype.destroy=function(){var that=this
clearTimeout(this.timeout)
this.hide(function(){that.$element.off('.'+that.type).removeData('bs.'+that.type)
if(that.$tip){that.$tip.detach()}
that.$tip=null
that.$arrow=null
that.$viewport=null})}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('bs.tooltip')
var options=typeof option=='object'&&option
if(!data&&/destroy|hide/.test(option))return
if(!data)$this.data('bs.tooltip',(data=new Tooltip(this,options)))
if(typeof option=='string')data[option]()})}
var old=$.fn.tooltip
$.fn.tooltip=Plugin
$.fn.tooltip.Constructor=Tooltip
$.fn.tooltip.noConflict=function(){$.fn.tooltip=old
return this}}(jQuery);+function($){'use strict';var Popover=function(element,options){this.init('popover',element,options)}
if(!$.fn.tooltip)throw new Error('Popover requires tooltip.js')
Popover.VERSION='3.3.6'
Popover.DEFAULTS=$.extend({},$.fn.tooltip.Constructor.DEFAULTS,{placement:'right',trigger:'click',content:'',template:'<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'})
Popover.prototype=$.extend({},$.fn.tooltip.Constructor.prototype)
Popover.prototype.constructor=Popover
Popover.prototype.getDefaults=function(){return Popover.DEFAULTS}
Popover.prototype.setContent=function(){var $tip=this.tip()
var title=this.getTitle()
var content=this.getContent()
$tip.find('.popover-title')[this.options.html?'html':'text'](title)
$tip.find('.popover-content').children().detach().end()[this.options.html?(typeof content=='string'?'html':'append'):'text'](content)
$tip.removeClass('fade top bottom left right in')
if(!$tip.find('.popover-title').html())$tip.find('.popover-title').hide()}
Popover.prototype.hasContent=function(){return this.getTitle()||this.getContent()}
Popover.prototype.getContent=function(){var $e=this.$element
var o=this.options
return $e.attr('data-content')||(typeof o.content=='function'?o.content.call($e[0]):o.content)}
Popover.prototype.arrow=function(){return(this.$arrow=this.$arrow||this.tip().find('.arrow'))}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('bs.popover')
var options=typeof option=='object'&&option
if(!data&&/destroy|hide/.test(option))return
if(!data)$this.data('bs.popover',(data=new Popover(this,options)))
if(typeof option=='string')data[option]()})}
var old=$.fn.popover
$.fn.popover=Plugin
$.fn.popover.Constructor=Popover
$.fn.popover.noConflict=function(){$.fn.popover=old
return this}}(jQuery);+function($){'use strict';function ScrollSpy(element,options){this.$body=$(document.body)
this.$scrollElement=$(element).is(document.body)?$(window):$(element)
this.options=$.extend({},ScrollSpy.DEFAULTS,options)
this.selector=(this.options.target||'')+' .nav li > a'
this.offsets=[]
this.targets=[]
this.activeTarget=null
this.scrollHeight=0
this.$scrollElement.on('scroll.bs.scrollspy',$.proxy(this.process,this))
this.refresh()
this.process()}
ScrollSpy.VERSION='3.3.6'
ScrollSpy.DEFAULTS={offset:10}
ScrollSpy.prototype.getScrollHeight=function(){return this.$scrollElement[0].scrollHeight||Math.max(this.$body[0].scrollHeight,document.documentElement.scrollHeight)}
ScrollSpy.prototype.refresh=function(){var that=this
var offsetMethod='offset'
var offsetBase=0
this.offsets=[]
this.targets=[]
this.scrollHeight=this.getScrollHeight()
if(!$.isWindow(this.$scrollElement[0])){offsetMethod='position'
offsetBase=this.$scrollElement.scrollTop()}
this.$body.find(this.selector).map(function(){var $el=$(this)
var href=$el.data('target')||$el.attr('href')
var $href=/^#./.test(href)&&$(href)
return($href&&$href.length&&$href.is(':visible')&&[[$href[offsetMethod]().top+offsetBase,href]])||null}).sort(function(a,b){return a[0]-b[0]}).each(function(){that.offsets.push(this[0])
that.targets.push(this[1])})}
ScrollSpy.prototype.process=function(){var scrollTop=this.$scrollElement.scrollTop()+this.options.offset
var scrollHeight=this.getScrollHeight()
var maxScroll=this.options.offset+scrollHeight-this.$scrollElement.height()
var offsets=this.offsets
var targets=this.targets
var activeTarget=this.activeTarget
var i
if(this.scrollHeight!=scrollHeight){this.refresh()}
if(scrollTop>=maxScroll){return activeTarget!=(i=targets[targets.length-1])&&this.activate(i)}
if(activeTarget&&scrollTop<offsets[0]){this.activeTarget=null
return this.clear()}
for(i=offsets.length;i--;){activeTarget!=targets[i]&&scrollTop>=offsets[i]&&(offsets[i+1]===undefined||scrollTop<offsets[i+1])&&this.activate(targets[i])}}
ScrollSpy.prototype.activate=function(target){this.activeTarget=target
this.clear()
var selector=this.selector+'[data-target="'+target+'"],'+
this.selector+'[href="'+target+'"]'
var active=$(selector).parents('li').addClass('active')
if(active.parent('.dropdown-menu').length){active=active.closest('li.dropdown').addClass('active')}
active.trigger('activate.bs.scrollspy')}
ScrollSpy.prototype.clear=function(){$(this.selector).parentsUntil(this.options.target,'.active').removeClass('active')}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('bs.scrollspy')
var options=typeof option=='object'&&option
if(!data)$this.data('bs.scrollspy',(data=new ScrollSpy(this,options)))
if(typeof option=='string')data[option]()})}
var old=$.fn.scrollspy
$.fn.scrollspy=Plugin
$.fn.scrollspy.Constructor=ScrollSpy
$.fn.scrollspy.noConflict=function(){$.fn.scrollspy=old
return this}
$(window).on('load.bs.scrollspy.data-api',function(){$('[data-spy="scroll"]').each(function(){var $spy=$(this)
Plugin.call($spy,$spy.data())})})}(jQuery);+function($){'use strict';var Tab=function(element){this.element=$(element)}
Tab.VERSION='3.3.6'
Tab.TRANSITION_DURATION=150
Tab.prototype.show=function(){var $this=this.element
var $ul=$this.closest('ul:not(.dropdown-menu)')
var selector=$this.data('target')
if(!selector){selector=$this.attr('href')
selector=selector&&selector.replace(/.*(?=#[^\s]*$)/,'')}
if($this.parent('li').hasClass('active'))return
var $previous=$ul.find('.active:last a')
var hideEvent=$.Event('hide.bs.tab',{relatedTarget:$this[0]})
var showEvent=$.Event('show.bs.tab',{relatedTarget:$previous[0]})
$previous.trigger(hideEvent)
$this.trigger(showEvent)
if(showEvent.isDefaultPrevented()||hideEvent.isDefaultPrevented())return
var $target=$(selector)
this.activate($this.closest('li'),$ul)
this.activate($target,$target.parent(),function(){$previous.trigger({type:'hidden.bs.tab',relatedTarget:$this[0]})
$this.trigger({type:'shown.bs.tab',relatedTarget:$previous[0]})})}
Tab.prototype.activate=function(element,container,callback){var $active=container.find('> .active')
var transition=callback&&$.support.transition&&($active.length&&$active.hasClass('fade')||!!container.find('> .fade').length)
function next(){$active.removeClass('active').find('> .dropdown-menu > .active').removeClass('active').end().find('[data-toggle="tab"]').attr('aria-expanded',false)
element.addClass('active').find('[data-toggle="tab"]').attr('aria-expanded',true)
if(transition){element[0].offsetWidth
element.addClass('in')}else{element.removeClass('fade')}
if(element.parent('.dropdown-menu').length){element.closest('li.dropdown').addClass('active').end().find('[data-toggle="tab"]').attr('aria-expanded',true)}
callback&&callback()}
$active.length&&transition?$active.one('bsTransitionEnd',next).emulateTransitionEnd(Tab.TRANSITION_DURATION):next()
$active.removeClass('in')}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('bs.tab')
if(!data)$this.data('bs.tab',(data=new Tab(this)))
if(typeof option=='string')data[option]()})}
var old=$.fn.tab
$.fn.tab=Plugin
$.fn.tab.Constructor=Tab
$.fn.tab.noConflict=function(){$.fn.tab=old
return this}
var clickHandler=function(e){e.preventDefault()
Plugin.call($(this),'show')}
$(document).on('click.bs.tab.data-api','[data-toggle="tab"]',clickHandler).on('click.bs.tab.data-api','[data-toggle="pill"]',clickHandler)}(jQuery);+function($){'use strict';var Affix=function(element,options){this.options=$.extend({},Affix.DEFAULTS,options)
this.$target=$(this.options.target).on('scroll.bs.affix.data-api',$.proxy(this.checkPosition,this)).on('click.bs.affix.data-api',$.proxy(this.checkPositionWithEventLoop,this))
this.$element=$(element)
this.affixed=null
this.unpin=null
this.pinnedOffset=null
this.checkPosition()}
Affix.VERSION='3.3.6'
Affix.RESET='affix affix-top affix-bottom'
Affix.DEFAULTS={offset:0,target:window}
Affix.prototype.getState=function(scrollHeight,height,offsetTop,offsetBottom){var scrollTop=this.$target.scrollTop()
var position=this.$element.offset()
var targetHeight=this.$target.height()
if(offsetTop!=null&&this.affixed=='top')return scrollTop<offsetTop?'top':false
if(this.affixed=='bottom'){if(offsetTop!=null)return(scrollTop+this.unpin<=position.top)?false:'bottom'
return(scrollTop+targetHeight<=scrollHeight-offsetBottom)?false:'bottom'}
var initializing=this.affixed==null
var colliderTop=initializing?scrollTop:position.top
var colliderHeight=initializing?targetHeight:height
if(offsetTop!=null&&scrollTop<=offsetTop)return'top'
if(offsetBottom!=null&&(colliderTop+colliderHeight>=scrollHeight-offsetBottom))return'bottom'
return false}
Affix.prototype.getPinnedOffset=function(){if(this.pinnedOffset)return this.pinnedOffset
this.$element.removeClass(Affix.RESET).addClass('affix')
var scrollTop=this.$target.scrollTop()
var position=this.$element.offset()
return(this.pinnedOffset=position.top-scrollTop)}
Affix.prototype.checkPositionWithEventLoop=function(){setTimeout($.proxy(this.checkPosition,this),1)}
Affix.prototype.checkPosition=function(){if(!this.$element.is(':visible'))return
var height=this.$element.height()
var offset=this.options.offset
var offsetTop=offset.top
var offsetBottom=offset.bottom
var scrollHeight=Math.max($(document).height(),$(document.body).height())
if(typeof offset!='object')offsetBottom=offsetTop=offset
if(typeof offsetTop=='function')offsetTop=offset.top(this.$element)
if(typeof offsetBottom=='function')offsetBottom=offset.bottom(this.$element)
var affix=this.getState(scrollHeight,height,offsetTop,offsetBottom)
if(this.affixed!=affix){if(this.unpin!=null)this.$element.css('top','')
var affixType='affix'+(affix?'-'+affix:'')
var e=$.Event(affixType+'.bs.affix')
this.$element.trigger(e)
if(e.isDefaultPrevented())return
this.affixed=affix
this.unpin=affix=='bottom'?this.getPinnedOffset():null
this.$element.removeClass(Affix.RESET).addClass(affixType).trigger(affixType.replace('affix','affixed')+'.bs.affix')}
if(affix=='bottom'){this.$element.offset({top:scrollHeight-height-offsetBottom})}}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('bs.affix')
var options=typeof option=='object'&&option
if(!data)$this.data('bs.affix',(data=new Affix(this,options)))
if(typeof option=='string')data[option]()})}
var old=$.fn.affix
$.fn.affix=Plugin
$.fn.affix.Constructor=Affix
$.fn.affix.noConflict=function(){$.fn.affix=old
return this}
$(window).on('load',function(){$('[data-spy="affix"]').each(function(){var $spy=$(this)
var data=$spy.data()
data.offset=data.offset||{}
if(data.offsetBottom!=null)data.offset.bottom=data.offsetBottom
if(data.offsetTop!=null)data.offset.top=data.offsetTop
Plugin.call($spy,data)})})}(jQuery);


/*===============================
/plugins/system/t3/base-bs3/js/jquery.tap.min.js
================================================================================*/;
!function(a,b){"use strict";var c,d,e,f="._tap",g="._tapActive",h="tap",i="clientX clientY screenX screenY pageX pageY".split(" "),j={count:0,event:0},k=function(a,c){var d=c.originalEvent,e=b.Event(d);e.type=a;for(var f=0,g=i.length;g>f;f++)e[i[f]]=c[i[f]];return e},l=function(a){if(a.isTrigger)return!1;var c=j.event,d=Math.abs(a.pageX-c.pageX),e=Math.abs(a.pageY-c.pageY),f=Math.max(d,e);return a.timeStamp-c.timeStamp<b.tap.TIME_DELTA&&f<b.tap.POSITION_DELTA&&(!c.touches||1===j.count)&&o.isTracking},m=function(a){if(!e)return!1;var c=Math.abs(a.pageX-e.pageX),d=Math.abs(a.pageY-e.pageY),f=Math.max(c,d);return Math.abs(a.timeStamp-e.timeStamp)<750&&f<b.tap.POSITION_DELTA},n=function(a){if(0===a.type.indexOf("touch")){a.touches=a.originalEvent.changedTouches;for(var b=a.touches[0],c=0,d=i.length;d>c;c++)a[i[c]]=b[i[c]]}a.timeStamp=Date.now?Date.now():+new Date},o={isEnabled:!1,isTracking:!1,enable:function(){o.isEnabled||(o.isEnabled=!0,c=b(a.body).on("touchstart"+f,o.onStart).on("mousedown"+f,o.onStart).on("click"+f,o.onClick))},disable:function(){o.isEnabled&&(o.isEnabled=!1,c.off(f))},onStart:function(a){a.isTrigger||(n(a),(!b.tap.LEFT_BUTTON_ONLY||a.touches||1===a.which)&&(a.touches&&(j.count=a.touches.length),o.isTracking||(a.touches||!m(a))&&(o.isTracking=!0,j.event=a,a.touches?(e=a,c.on("touchend"+f+g,o.onEnd).on("touchcancel"+f+g,o.onCancel)):c.on("mouseup"+f+g,o.onEnd))))},onEnd:function(a){var c;a.isTrigger||(n(a),l(a)&&(c=k(h,a),d=c,b(j.event.target).trigger(c)),o.onCancel(a))},onCancel:function(a){a&&"touchcancel"===a.type&&a.preventDefault(),o.isTracking=!1,c.off(g)},onClick:function(a){return!a.isTrigger&&d&&d.isDefaultPrevented()&&d.target===a.target&&d.pageX===a.pageX&&d.pageY===a.pageY&&a.timeStamp-d.timeStamp<750?(d=null,!1):void 0}};b(a).ready(o.enable),b.tap={POSITION_DELTA:10,TIME_DELTA:400,LEFT_BUTTON_ONLY:!0}}(document,jQuery);


/*===============================
/plugins/system/t3/base-bs3/js/off-canvas.js
================================================================================*/;
jQuery(document).ready(function($){function getAndroidVersion(ua){var ua=ua||navigator.userAgent;var match=ua.match(/Android\s([0-9\.]*)/);return match?match[1]:false;};if(parseInt(getAndroidVersion())==4){$('#t3-mainnav').addClass('t3-mainnav-android');}
var JA_isLoading=false;if(/MSIE\s([\d.]+)/.test(navigator.userAgent)?new Number(RegExp.$1)<10:false){$('html').addClass('old-ie');}else if(/constructor/i.test(window.HTMLElement)){$('html').addClass('safari');}
var $wrapper=$('body'),$inner=$('.t3-wrapper'),$toggles=$('.off-canvas-toggle'),$offcanvas=$('.t3-off-canvas'),$close=$('.t3-off-canvas .close'),$btn=null,$nav=null,direction='left',$fixed=null;if(!$wrapper.length)return;$toggles.each(function(){var $this=$(this),$nav=$($this.data('nav')),effect=$this.data('effect'),direction=($('html').attr('dir')=='rtl'&&$this.data('pos')!='right')||($('html').attr('dir')!='rtl'&&$this.data('pos')=='right')?'right':'left';$nav.addClass(effect).addClass('off-canvas-'+direction);var inside_effect=['off-canvas-effect-3','off-canvas-effect-16','off-canvas-effect-7','off-canvas-effect-8','off-canvas-effect-14'];if($.inArray(effect,inside_effect)==-1){$inner.before($nav);}else{$inner.prepend($nav);}});$toggles.on('tap',function(e){stopBubble(e);if($wrapper.hasClass('off-canvas-open')){oc_hide(e);return false;}
$btn=$(this);$nav=$($btn.data('nav'));if(!$fixed)$fixed=$inner.find('*').filter(function(){return $(this).css("position")==='fixed';});else $fixed=$fixed.filter(function(){return $(this).css("position")==='fixed';}).add($inner.find('.affix'));$nav.addClass('off-canvas-current');direction=($('html').attr('dir')=='rtl'&&$btn.data('pos')!='right')||($('html').attr('dir')!='rtl'&&$btn.data('pos')=='right')?'right':'left';$offcanvas.height($(window).height());var events=$(window).data('events');if(events&&events.scroll&&events.scroll.length){var handlers=[];for(var i=0;i<events.scroll.length;i++){handlers[i]=events.scroll[i].handler;}
$(window).data('scroll-events',handlers);$(window).off('scroll');}
var scrollTop=($('html').scrollTop())?$('html').scrollTop():$('body').scrollTop();$('html').addClass('noscroll').css('top',-scrollTop).data('top',scrollTop);$('.t3-off-canvas').css('top',scrollTop);$fixed.each(function(){var $this=$(this),$parent=$this.parent(),mtop=0;while(!$parent.is($inner)&&$parent.css("position")==='static')$parent=$parent.parent();mtop=-$parent.offset().top;$this.css({'position':'absolute','margin-top':mtop});});$wrapper.scrollTop(scrollTop);$wrapper[0].className=$.trim($wrapper[0].className.replace(/\s*off\-canvas\-effect\-\d+\s*/g,' '))+' '+$btn.data('effect')+' '+'off-canvas-'+direction;setTimeout(oc_show,50);return false;});var oc_show=function(){if(JA_isLoading==true){return;}
JA_isLoading=true;$wrapper.addClass('off-canvas-open');$inner.on('click',oc_hide);$close.on('click',oc_hide);$offcanvas.on('click',handleClick);if($.browser.msie&&$.browser.version<10){var p1={},p2={};p1['padding-'+direction]=$('.t3-off-canvas').width();p2[direction]=0;$inner.animate(p1);$nav.animate(p2);}
setTimeout(function(){JA_isLoading=false;},200);};var oc_hide=function(){if(JA_isLoading==true){return;}
JA_isLoading=true;$inner.off('click',oc_hide);$close.off('click',oc_hide);$offcanvas.off('click',handleClick);setTimeout(function(){$wrapper.removeClass('off-canvas-open');},100);setTimeout(function(){$wrapper.removeClass($btn.data('effect')).removeClass('off-canvas-'+direction);$wrapper.scrollTop(0);$('html').removeClass('noscroll').css('top','');$('html,body').scrollTop($('html').data('top'));$nav.removeClass('off-canvas-current');$fixed.css({'position':'','margin-top':''});if($(window).data('scroll-events')){var handlers=$(window).data('scroll-events');for(var i=0;i<handlers.length;i++){$(window).on('scroll',handlers[i]);}
$(window).data('scroll-events',null);}
JA_isLoading=false;},700);if($('html').hasClass('old-ie')){var p1={},p2={};p1['padding-'+direction]=0;p2[direction]=-$('.t3-off-canvas').width();$inner.animate(p1);$nav.animate(p2);}};var handleClick=function(e){if(e.target.tagName=='A'){var arr1=e.target.href.split('#'),arr2=location.href.split('#');if(arr1[0]==arr2[0]&&arr1.length>1&&arr1[1].length){oc_hide();setTimeout(function(){var anchor=$("a[name='"+arr1[1]+"']");if(!anchor.length)anchor=$('#'+arr1[1]);$('html,body').animate({scrollTop:anchor.offset().top},'slow');},500);}}
stopBubble(e);return true;}
var stopBubble=function(e){e.stopPropagation();}
$(window).load(function(){setTimeout(function(){$fixed=$inner.find('*').filter(function(){return $(this).css("position")==='fixed';});},100);});})


/*===============================
/plugins/system/t3/base-bs3/js/script.js
================================================================================*/;
!function($){if($.browser==undefined||$.browser.msie==undefined){$.browser={msie:false,version:0};if(match=navigator.userAgent.match(/MSIE ([0-9]{1,}[\.0-9]{0,})/)||navigator.userAgent.match(/Trident.*rv:([0-9]{1,}[\.0-9]{0,})/)){$.browser.msie=true;$.browser.version=match[1];}}
if($.browser.msie){$('html').addClass('ie'+Math.floor($.browser.version));}
$(document).ready(function(){if(!window.getComputedStyle){window.getComputedStyle=function(el,pseudo){this.el=el;this.getPropertyValue=function(prop){var re=/(\-([a-z]){1})/g;if(prop=='float')prop='styleFloat';if(re.test(prop)){prop=prop.replace(re,function(){return arguments[2].toUpperCase();});}
return el.currentStyle[prop]?el.currentStyle[prop]:null;}
return this;}}
var fromClass='body-data-holder',prop='content',$inspector=$('<div>').css('display','none').addClass(fromClass).appendTo($('body'));try{var computedStyle=window.getComputedStyle($inspector[0],':before');if(computedStyle){var attrs=computedStyle.getPropertyValue(prop);if(attrs){var matches=attrs.match(/([\da-z\-]+)/gi),data={};if(matches&&matches.length){for(var i=0;i<matches.length;i++){data[matches[i++]]=i<matches.length?matches[i]:null;}}
$('body').data(data);}}}finally{$inspector.remove();}});(function(){$.support.t3transform=(function(){var style=document.createElement('div').style,vendors=['t','webkitT','MozT','msT','OT'],transform,i=0,l=vendors.length;for(;i<l;i++){transform=vendors[i]+'ransform';if(transform in style){return transform;}}
return false;})();})();(function(){$('html').addClass('ontouchstart'in window?'touch':'no-touch');})();$(document).ready(function(){(function(){if(window.MooTools&&window.MooTools.More&&Element&&Element.implement){var mthide=Element.prototype.hide,mtshow=Element.prototype.show,mtslide=Element.prototype.slide;Element.implement({show:function(args){if(arguments.callee&&arguments.callee.caller&&arguments.callee.caller.toString().indexOf('isPropagationStopped')!==-1){return this;}
return $.isFunction(mtshow)&&mtshow.apply(this,args);},hide:function(){if(arguments.callee&&arguments.callee.caller&&arguments.callee.caller.toString().indexOf('isPropagationStopped')!==-1){return this;}
return $.isFunction(mthide)&&mthide.apply(this,arguments);},slide:function(args){if(arguments.callee&&arguments.callee.caller&&arguments.callee.caller.toString().indexOf('isPropagationStopped')!==-1){return this;}
return $.isFunction(mtslide)&&mtslide.apply(this,args);}})}})();$.fn.tooltip.Constructor&&$.fn.tooltip.Constructor.DEFAULTS&&($.fn.tooltip.Constructor.DEFAULTS.html=true);$.fn.popover.Constructor&&$.fn.popover.Constructor.DEFAULTS&&($.fn.popover.Constructor.DEFAULTS.html=true);$.fn.tooltip.defaults&&($.fn.tooltip.defaults.html=true);$.fn.popover.defaults&&($.fn.popover.defaults.html=true);(function(){if(window.jomsQuery&&jomsQuery.fn.collapse){$('[data-toggle="collapse"]').on('click',function(e){$($(this).attr('data-target')).eq(0).collapse('toggle');e.stopPropagation();return false;});jomsQuery('html, body').off('touchstart.dropdown.data-api');}})();(function(){if($.fn.chosen&&$(document.documentElement).attr('dir')=='rtl'){$('select').addClass('chzn-rtl');}})();});$(window).load(function(){if(!$(document.documentElement).hasClass('off-canvas-ready')&&($('.navbar-collapse-fixed-top').length||$('.navbar-collapse-fixed-bottom').length)){var btn=$('.btn-navbar[data-toggle="collapse"]');if(!btn.length){return;}
if(btn.data('target')){var nav=$(btn.data('target'));if(!nav.length){return;}
var fixedtop=nav.closest('.navbar-collapse-fixed-top').length;btn.on('click',function(){var wheight=(window.innerHeight||$(window).height());if(!$.support.transition){nav.parent().css('height',!btn.hasClass('collapsed')&&btn.data('t3-clicked')?'':wheight);btn.data('t3-clicked',1);}
nav.addClass('animate').css('max-height',wheight-
(fixedtop?(parseFloat(nav.css('top'))||0):(parseFloat(nav.css('bottom'))||0)));});nav.on('shown hidden',function(){nav.removeClass('animate');});}}});}(jQuery);


/*===============================
/plugins/system/t3/base-bs3/js/menu.js
================================================================================*/;
;(function($){var T3Menu=function(elm,options){this.$menu=$(elm);if(!this.$menu.length){return;}
this.options=$.extend({},$.fn.t3menu.defaults,options);this.child_open=[];this.loaded=false;this.start();};T3Menu.prototype={constructor:T3Menu,start:function(){if(this.loaded){return;}
this.loaded=true;var self=this,options=this.options,$menu=this.$menu;this.$items=$menu.find('li');this.$items.each(function(idx,li){var $item=$(this),$child=$item.children('.dropdown-menu'),$link=$item.children('a'),item={$item:$item,child:$child.length,link:$link.length,clickable:!($link.length&&$child.length),mega:$item.hasClass('mega'),status:'close',timer:null,atimer:null};$item.data('t3menu.item',item);if($child.length&&!options.hover){$item.on('click',function(e){e.stopPropagation();if($item.hasClass('group')){return;}
if(item.status=='close'){e.preventDefault();self.show(item);}});}else{$item.on('click',function(e){if($(e.target).data('toggle'))return;e.stopPropagation()});}
$item.find('a > .caret').on('click tap',function(e){item.clickable=false;});if(options.hover){$item.on('mouseover',function(e){if($item.hasClass('group'))
return;var $target=$(e.target);if($target.data('show-processed'))
return;$target.data('show-processed',true);setTimeout(function(){$target.data('show-processed',false);},10);self.show(item);}).on('mouseleave',function(e){if($item.hasClass('group'))
return;var $target=$(e.target);if($target.data('hide-processed'))
return;$target.data('hide-processed',true);setTimeout(function(){$target.data('hide-processed',false);},10);self.hide(item,$target);});if($link.length&&$child.length){$link.on('click',function(e){if(item.clickable){e.stopPropagation();}
return item.clickable;});}}});$(document.body).on('tap hideall.t3menu',function(e){clearTimeout(self.timer);self.timer=setTimeout($.proxy(self.hide_alls,self),e.type=='tap'?500:self.options.hidedelay);});$menu.find('.mega-dropdown-menu').on('hideall.t3menu',function(e){e.stopPropagation();e.preventDefault();return false;});$menu.find('input, select, textarea, label').on('click tap',function(e){e.stopPropagation();});var $megatab=$menu.find('.mega-tab');if($megatab.length){$megatab.each(function(){var $tabul=$(this).find('>div>ul'),$tabItems=$tabul.children('.dropdown-submenu'),$tabs=$tabul.find('>li>.dropdown-menu'),tabheight=0,$parentItem=$(this).closest('li');$tabItems.data('mega-tab-item',1);var megatabs=$parentItem.data('mega-tabs')?$parentItem.data('mega-tabs'):[];megatabs.push($tabul);$parentItem.data('mega-tabs',megatabs);$tabItems.first().data('mega-tab-active',true).addClass('open');var $p=$tabul.parents('.dropdown-menu');$p.each(function(){var $this=$(this);$this.data('prev-style',$this.attr('style')).css({visibility:"visible",display:"block"});})
$tabs.each(function(){var $this=$(this),thisstyle=$this.attr('style');$this.css({visibility:"hidden",display:"block"});tabheight=Math.max(tabheight,$this.children().innerHeight());if(thisstyle){$this.attr('style',thisstyle);}else{$this.removeAttr('style');}});$tabul.css('min-height',tabheight);$p.each(function(){var $this=$(this);if($this.data('prev-style'))
$this.attr('style',$this.data('prev-style'));else
$this.removeAttr('style');$this.removeData('prev-style');})})}
$menu.find('.modal').appendTo('body');},show:function(item){if(item.$item.data('mega-tab-item')){item.$item.parent().children().removeClass('open').data('mega-tab-active',false);item.$item.addClass('open').data('mega-tab-active',true);}
if($.inArray(item,this.child_open)<this.child_open.length-1){this.hide_others(item);}
$(document.body).trigger('hideall.t3menu',[this]);clearTimeout(this.timer);clearTimeout(item.timer);clearTimeout(item.ftimer);clearTimeout(item.ctimer);if(item.status!='open'||!item.$item.hasClass('open')||!this.child_open.length){if(item.mega){clearTimeout(item.astimer);clearTimeout(item.atimer);this.position(item.$item);item.astimer=setTimeout(function(){item.$item.addClass('animating')},10);item.atimer=setTimeout(function(){item.$item.removeClass('animating')},this.options.duration+50);item.timer=setTimeout(function(){item.$item.addClass('open');},100);}else{item.$item.addClass('open');}
item.status='open';if(item.child&&$.inArray(item,this.child_open)==-1){this.child_open.push(item);}}
item.ctimer=setTimeout($.proxy(this.clickable,this,item),300);},hide:function(item,$target){clearTimeout(this.timer);clearTimeout(item.timer);clearTimeout(item.astimer);clearTimeout(item.atimer);clearTimeout(item.ftimer);if($target&&$target.is('input',item.$item)){return;}
if(item.mega){item.$item.addClass('animating');item.atimer=setTimeout(function(){item.$item.removeClass('animating')},this.options.duration);item.timer=setTimeout(function(){if(!item.$item.data('mega-tab-active'))
item.$item.removeClass('open')},100);}else{item.timer=setTimeout(function(){if(!item.$item.data('mega-tab-active'))
item.$item.removeClass('open');},100);}
item.status='close';for(var i=this.child_open.length;i--;){if(this.child_open[i]===item){this.child_open.splice(i,1);}}
item.ftimer=setTimeout($.proxy(this.hidden,this,item),this.options.duration);this.timer=setTimeout($.proxy(this.hide_alls,this),this.options.hidedelay);},hidden:function(item){if(item.status=='close'){item.clickable=false;}},hide_others:function(item){var self=this;$.each(this.child_open.slice(),function(idx,open){if(!item||(open!=item&&!open.$item.has(item.$item).length)){self.hide(open);}});},hide_alls:function(e,inst){if(!e||e.type=='tap'||(e.type=='hideall'&&this!=inst)){var self=this;$.each(this.child_open.slice(),function(idx,item){item&&self.hide(item);});}},clickable:function(item){item.clickable=true;},position:function($item){var sub=$item.children('.mega-dropdown-menu'),is_show=sub.is(':visible');if(!is_show){sub.show();}
var offset=$item.offset(),width=$item.outerWidth(),screen_width=$(window).width()
-this.options.sb_width,sub_width=sub.outerWidth(),level=$item.data('level');if(!is_show){sub.css('display','');}
sub.css({left:'',right:''});if(level==1){var align=$item.data('alignsub'),align_offset=0,align_delta=0,align_trans=0;if(align=='justify'){return;}
if(!align){align='left';}
if(align=='center'){align_offset=offset.left+(width/2);if(!$.support.t3transform){align_trans=-sub_width/2;sub.css(this.options.rtl?'right':'left',align_trans+width/2);}}else{align_offset=offset.left
+((align=='left'&&this.options.rtl||align=='right'&&!this.options.rtl)?width:0);}
if(this.options.rtl){if(align=='right'){if(align_offset+sub_width>screen_width){align_delta=screen_width-align_offset
-sub_width;sub.css('left',align_delta);if(screen_width<sub_width){sub.css('left',align_delta+sub_width
-screen_width);}}}else{if(align_offset<(align=='center'?sub_width/2:sub_width)){align_delta=align_offset
-(align=='center'?sub_width/2:sub_width);sub.css('right',align_delta+align_trans);}
if(align_offset
+(align=='center'?sub_width/2:0)
-align_delta>screen_width){sub.css('right',align_offset
+(align=='center'?(sub_width+width)/2:0)+align_trans
-screen_width);}}}else{if(align=='right'){if(align_offset<sub_width){align_delta=align_offset-sub_width;sub.css('right',align_delta);if(sub_width>screen_width){sub.css('right',sub_width-screen_width
+align_delta);}}}else{if(align_offset
+(align=='center'?sub_width/2:sub_width)>screen_width){align_delta=screen_width
-align_offset
-(align=='center'?sub_width/2:sub_width);sub.css('left',align_delta+align_trans);}
if(align_offset
-(align=='center'?sub_width/2:0)
+align_delta<0){sub.css('left',(align=='center'?(sub_width+width)/2:0)
+align_trans
-align_offset);}}}}else{if(this.options.rtl){if($item.closest('.mega-dropdown-menu').parent().hasClass('mega-align-right')){if(offset.left+width+sub_width>screen_width){$item.removeClass('mega-align-right');if(offset.left-sub_width<0){sub.css('right',offset.left+width
-sub_width);}}}else{if(offset.left-sub_width<0){$item.removeClass('mega-align-left').addClass('mega-align-right');if(offset.left+width+sub_width>screen_width){sub.css('left',screen_width-offset.left
-sub_width);}}}}else{if($item.closest('.mega-dropdown-menu').parent().hasClass('mega-align-right')){if(offset.left-sub_width<0){$item.removeClass('mega-align-right');if(offset.left+width+sub_width>screen_width){sub.css('left',screen_width-offset.left
-sub_width);}}}else{if(offset.left+width+sub_width>screen_width){$item.removeClass('mega-align-left').addClass('mega-align-right');if(offset.left-sub_width<0){sub.css('right',offset.left+width
-sub_width);}}}}}}};$.fn.t3menu=function(option){return this.each(function(){var $this=$(this),data=$this.data('megamenu'),options=typeof option=='object'&&option;if($this.parents('#off-canvas-nav').length)
return;if($this.parents('#t3-off-canvas').length)
return;if(!data){$this.data('megamenu',(data=new T3Menu(this,options)));}else{if(typeof option=='string'&&data[option]){data[option]()}}})};$.fn.t3menu.defaults={duration:400,timeout:100,hidedelay:200,hover:true,sb_width:20};$(document).ready(function(){var mm_duration=$('.t3-megamenu').data('duration')||0;if(mm_duration){$('<style type="text/css">'
+'.t3-megamenu.animate .animating > .mega-dropdown-menu,'
+'.t3-megamenu.animate.slide .animating > .mega-dropdown-menu > div {'
+'transition-duration: '
+mm_duration+'ms !important;'
+'-webkit-transition-duration: '
+mm_duration+'ms !important;'
+'}'+'</style>').appendTo('head');}
var mm_timeout=mm_duration?100+mm_duration:500,mm_rtl=$(document.documentElement).attr('dir')=='rtl',mm_trigger=$(document.documentElement).hasClass('mm-hover'),sb_width=(function(){var parent=$('<div style="width:50px;height:50px;overflow:auto"><div/></div>').appendTo('body'),child=parent.children(),width=child.innerWidth()
-child.height(100).innerWidth();parent.remove();return width;})();if(!$.support.transition){$('.t3-megamenu').removeClass('animate');mm_timeout=100;}
$('ul.nav').has('.dropdown-menu').t3menu({duration:mm_duration,timeout:mm_timeout,rtl:mm_rtl,sb_width:sb_width,hover:mm_trigger});$(window).load(function(){$('ul.nav').has('.dropdown-menu').t3menu({duration:mm_duration,timeout:mm_timeout,rtl:mm_rtl,sb_width:sb_width,hover:mm_trigger});});});})(jQuery);


/*===============================
/plugins/system/t3/base-bs3/js/frontend-edit.js
================================================================================*/;
!function($){$(document).ready(function(){$('fieldset.radio').filter(function(){return $(this).find('input').length==2&&$(this).find('input').filter(function(){return $.inArray(this.value+'',['0','1'])!==-1;}).length==2;}).addClass('t3onoff').removeClass('btn-group');$('fieldset.t3onoff').find('label').addClass(function(){var $this=$(this),$input=$this.prev('input'),cls=$this.hasClass('off')||$input.val()=='0'?'off':'on';cls+=$input.prop('checked')?' active':'';return cls;});$('fieldset.radio').find('label').unbind('click').click(function(){var label=$(this),input=$('#'+label.attr('for'));if(!input.prop('checked')){label.addClass('active').siblings().removeClass('active');input.prop('checked',true).trigger('change');}});$(".btn-group input[checked=checked]").each(function()
{if($(this).val()==''){$("label[for="+$(this).attr('id')+"]").addClass('active btn-primary');}else if($(this).val()==0){$("label[for="+$(this).attr('id')+"]").addClass('active btn-danger');}else{$("label[for="+$(this).attr('id')+"]").addClass('active btn-success');}});});}(jQuery);


/*===============================
/templates/ts_dailytimes/js/script.js
================================================================================*/;
jQuery(function($){"use strict";$(window).on('scroll',function(){if($(window).scrollTop()>100){$('.t3-mainnav').addClass('navbar-fixed');}else{$('.t3-mainnav').removeClass('navbar-fixed');}});});


/*===============================
/plugins/system/t3/base-bs3/js/nav-collapse.js
================================================================================*/;
jQuery(document).ready(function($){$('.t3-navbar').each(function(){var $navwrapper=$(this),$menu=null,$placeholder=null;if($navwrapper.find('.t3-megamenu').length){$menu=$navwrapper.find('ul.level0').clone(),$placeholder=$navwrapper.prev('.navbar-collapse');if(!$placeholder.length){$placeholder=$navwrapper.closest('.container, .t3-mainnav').find('.navbar-collapse:empty');}
var lis=$menu.find('li[data-id]'),liactive=lis.filter('.current');lis.removeClass('mega dropdown mega-align-left mega-align-right mega-align-center mega-align-adjust');lis.each(function(){var $li=$(this),$child=$li.find('>:first-child');if($child[0].nodeName=='DIV'){$child.find('>:first-child').prependTo($li);$child.remove();}
if($li.data('hidewcol')){$child.find('.caret').remove();$child.nextAll().remove();return;}
var subul=$li.find('ul.level'+$li.data('level'));if(subul.length){$ul=$('<ul class="level'+$li.data('level')+' dropdown-menu">');subul.each(function(){if($(this).parents('.mega-col-nav').data('hidewcol'))return;$(this).find('>li').appendTo($ul);});if($ul.children().length){$ul.appendTo($li);}}
$li.find('>div').remove();if(!$li.children('ul').length){$child.find('.caret').remove();}
var divider=$li.hasClass('divider');for(var x in $li.data()){$li.removeAttr('data-'+x)}
$child.removeAttr('class');for(var x in $child.data()){$child.removeAttr('data-'+x)}
if(divider){$li.addClass('divider');}});liactive.addClass('current active');}else{$menu=$navwrapper.find('ul.nav').clone();$placeholder=$('.t3-navbar-collapse:empty, .navbar-collapse:empty').eq(0);}
$menu.find('a[data-toggle="dropdown"]').removeAttr('data-toggle').removeAttr('data-target');$menu.find('> li > ul.dropdown-menu').prev('a').attr('data-toggle','dropdown').attr('data-target','#').parent('li').addClass(function(){return'dropdown'+($(this).data('level')>1?' dropdown-submenu':'');});$menu.appendTo($placeholder);});});


/*===============================
/modules/mod_jq-dailypop/js/js.js
================================================================================*/;
var temp=0;function dpanimfull(num,tp){if(num==1){if(tp==0){jQuery('#dailyposition').show();jQuery("#dailyfullscreen").animate({opacity:"toggle"});}
if(tp==1){jQuery("#dailyfullscreen").animate({opacity:"toggle"},function(){jQuery("#dailyposition").animate({opacity:"toggle"});});}
if(tp==2){largzone=jQuery("#dailypopupwidth").val();jQuery('#dailyposition').css({'width':'0px'});jQuery("#dailyfullscreen").animate({opacity:"toggle"},function(){jQuery('#dailyposition').show();jQuery("#dailyposition").animate({width:largzone},1500);});}}else{jQuery("#dailyposition").toggle();jQuery("#dailyfullscreen").toggle();}}
function rmanimfull(num,tp){if(num==1){jQuery("#dailyfullscreen").animate({opacity:"toggle"},function(){jQuery("#dailyfullscreen").remove();});}else{jQuery("#dailyfullscreen").remove();}}
window.onload=function(){letexte2=jQuery("#dailycomplete").html();largzone=jQuery("#dailypopupwidth").val();zetimer=jQuery("#jqtimer").val();actianim=jQuery("#actianim").val();jqanime=jQuery("#jqanime").val();jQuery("#dailycomplete").remove();jQuery('body').prepend(letexte2);fermcl=jQuery('#closedailyp').val();jQuery('#dailyposition').css({'max-width':largzone+'px','display':'none'});dpanimfull(actianim,jqanime);timetmp=zetimer*1000;if(actianim==1){endrem='jQuery( "#dailyfullscreen" ).animate({ opacity: "toggle" }, function(){jQuery("#dailyfullscreen").remove();});';}else{endrem='jQuery("#dailyfullscreen").remove();';}
for(i=zetimer;i>-1;i--){setTimeout("jQuery('#closedailyp').val('"+fermcl+" "+i+"');",temp);temp+=1000;if(temp==timetmp){setTimeout(endrem,temp);}}
jQuery('#closedailyp').click(function(){rmanimfull(actianim,jqanime);});}


/*===============================
/media/system/js/frontediting.js
================================================================================*/;
!function(t){t.fn.extend({jEditMakeAbsolute:function(e){return this.each(function(){var o,i=t(this);o=e?i.offset():i.position(),i.css({position:"absolute",marginLeft:0,marginTop:0,top:o.top,left:o.left,bottom:"auto",right:"auto"}),e&&i.detach().appendTo("body")})}}),t(document).ready(function(){var e=200,o=100,i=function(i,n){var d,a,l,r,s,p,u,m,c,h,f,v,j,b,g;return v=function(t){return u<t.top&&s<t.left&&p>t.left+e&&r>t.top+o},d=t(n),b=t.extend({},d.offset(),{width:n.offsetWidth,height:n.offsetHeight}),u=t(document).scrollTop(),s=t(document).scrollLeft(),p=s+t(window).width(),r=u+t(window).height(),m={top:b.top-o,left:b.left+b.width/2-e/2},c={top:b.top+b.height,left:b.left+b.width/2-e/2},h={top:b.top+b.height/2-o/2,left:b.left-e},f={top:b.top+b.height/2-o/2,left:b.left+b.width},a=v(m),l=v(c),j=v(h),g=v(f),a?"top":l?"bottom":j?"left":"right"};t(".jmoddiv").on({mouseenter:function(){var e=t(this).data("jmodediturl"),o=t(this).data("jmodtip"),n=t(this).data("target");t("body>.btn.jmodedit").clearQueue().tooltip("destroy").remove(),t(this).addClass("jmodinside").prepend('<a class="btn jmodedit" href="#" target="'+n+'"><span class="icon-edit"></span></a>').children(":first").attr("href",e).attr("title",o).tooltip({container:!1,html:!0,placement:i}).jEditMakeAbsolute(!0),t(".btn.jmodedit").on({mouseenter:function(){t(this).clearQueue()},mouseleave:function(){t(this).delay(500).queue(function(e){t(this).tooltip("destroy").remove(),e()})}})},mouseleave:function(){t("body>.btn.jmodedit").delay(500).queue(function(e){t(this).tooltip("destroy").remove(),e()})}});var n=null;t(".jmoddiv[data-jmenuedittip] .nav li,.jmoddiv[data-jmenuedittip].nav li,.jmoddiv[data-jmenuedittip] .nav .nav-child li,.jmoddiv[data-jmenuedittip].nav .nav-child li").on({mouseenter:function(){var e=/\bitem-(\d+)\b/.exec(t(this).attr("class"));if("string"==typeof e[1])var o=t(this).closest(".jmoddiv"),i=o.data("jmodediturl"),d=i.replace(/\/index.php\?option=com_config&controller=config.display.modules([^\d]+).+$/,"/administrator/index.php?option=com_menus&view=item&layout=edit$1"+e[1]);var a=o.data("jmenuedittip").replace("%s",e[1]),l=t('<div><a class="btn jfedit-menu" href="#" target="_blank"><span class="icon-edit"></span></a></div>');l.children("a.jfedit-menu").prop("href",d).prop("title",a),n&&t(n).popover("hide"),t(this).popover({html:!0,content:l.html(),container:"body",trigger:"manual",animation:!1,placement:"bottom"}).popover("show"),n=this,t("body>div.popover").on({mouseenter:function(){n&&t(n).clearQueue()},mouseleave:function(){n&&t(n).popover("hide")}}).find("a.jfedit-menu").tooltip({container:!1,html:!0,placement:"bottom"})},mouseleave:function(){t(this).delay(1500).queue(function(e){t(this).popover("hide"),e()})}})})}(jQuery);


/*===============================
/media/system/js/mootools-core.js
================================================================================*/;
(function(){this.MooTools={version:"1.4.5",build:"74e34796f5f76640cdb98853004650aea1499d69"};var b=this.typeOf=function(b){if(null==b)return"null";if(null!=b.$family)return b.$family();if(b.nodeName){if(1==b.nodeType)return"element";if(3==b.nodeType)return/\S/.test(b.nodeValue)?"textnode":"whitespace"}else if("number"==typeof b.length){if(b.callee)return"arguments";if("item"in b)return"collection"}return typeof b};this.instanceOf=function(b,a){if(null==b)return!1;for(var c=b.$constructor||b.constructor;c;){if(c===a)return!0;c=c.parent}return!b.hasOwnProperty?!1:b instanceof a};var a=this.Function,c=!0,d;for(d in{toString:1})c=null;c&&(c="hasOwnProperty,valueOf,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,constructor".split(","));a.prototype.overloadSetter=function(b){var a=this;return function(h,k){if(null==h)return this;if(b||"string"!=typeof h){for(var e in h)a.call(this,e,h[e]);if(c)for(var d=c.length;d--;)e=c[d],h.hasOwnProperty(e)&&a.call(this,e,h[e])}else a.call(this,h,k);return this}};a.prototype.overloadGetter=function(b){var a=this;return function(c){var h,k;"string"!=typeof c?h=c:1<arguments.length?h=arguments:b&&(h=[c]);if(h){k={};for(var e=0;e<h.length;e++)k[h[e]]=a.call(this,h[e])}else k=a.call(this,c);return k}};a.prototype.extend=function(b,a){this[b]=a}.overloadSetter();a.prototype.implement=function(b,a){this.prototype[b]=a}.overloadSetter();var e=Array.prototype.slice;a.from=function(a){return"function"==b(a)?a:function(){return a}};Array.from=function(a){return null==a?[]:f.isEnumerable(a)&&"string"!=typeof a?"array"==b(a)?a:e.call(a):[a]};Number.from=function(b){b=parseFloat(b);return isFinite(b)?b:null};String.from=function(b){return b+""};a.implement({hide:function(){this.$hidden=!0;return this},protect:function(){this.$protected=!0;return this}});var f=this.Type=function(a,c){if(a){var h=a.toLowerCase();f["is"+a]=function(a){return b(a)==h};null!=c&&(c.prototype.$family=function(){return h}.hide())}if(null==c)return null;c.extend(this);c.$constructor=f;return c.prototype.$constructor=c},g=Object.prototype.toString;f.isEnumerable=function(b){return null!=b&&"number"==typeof b.length&&"[object Function]"!=g.call(b)};var i={},j=function(a){a=b(a.prototype);return i[a]||(i[a]=[])},m=function(a,c){if(!c||!c.$hidden){for(var k=j(this),d=0;d<k.length;d++){var o=k[d];"type"==b(o)?m.call(o,a,c):o.call(this,a,c)}k=this.prototype[a];if(null==k||!k.$protected)this.prototype[a]=c;null==this[a]&&"function"==b(c)&&h.call(this,a,function(b){return c.apply(b,e.call(arguments,1))})}},h=function(b,a){if(!a||!a.$hidden){var c=this[b];if(null==c||!c.$protected)this[b]=a}};f.implement({implement:m.overloadSetter(),extend:h.overloadSetter(),alias:function(b,a){m.call(this,b,this.prototype[a])}.overloadSetter(),mirror:function(b){j(this).push(b);return this}});new f("Type",f);var k=function(b,a,c){var h=a!=Object,e=a.prototype;h&&(a=new f(b,a));for(var b=0,d=c.length;b<d;b++){var o=c[b],q=a[o],g=e[o];q&&q.protect();h&&g&&a.implement(o,g.protect())}if(h){var j=e.propertyIsEnumerable(c[0]);a.forEachMethod=function(b){if(!j)for(var a=0,h=c.length;a<h;a++)b.call(e,e[c[a]],c[a]);for(var k in e)b.call(e,e[k],k)}}return k};k("String",String,"charAt,charCodeAt,concat,indexOf,lastIndexOf,match,quote,replace,search,slice,split,substr,substring,trim,toLowerCase,toUpperCase".split(","))("Array",Array,"pop,push,reverse,shift,sort,splice,unshift,concat,join,slice,indexOf,lastIndexOf,filter,forEach,every,map,some,reduce,reduceRight".split(","))("Number",Number,["toExponential","toFixed","toLocaleString","toPrecision"])("Function",a,["apply","call","bind"])("RegExp",RegExp,["exec","test"])("Object",Object,"create,defineProperty,defineProperties,keys,getPrototypeOf,getOwnPropertyDescriptor,getOwnPropertyNames,preventExtensions,isExtensible,seal,isSealed,freeze,isFrozen".split(","))("Date",Date,["now"]);Object.extend=h.overloadSetter();Date.extend("now",function(){return+new Date});new f("Boolean",Boolean);Number.prototype.$family=function(){return isFinite(this)?"number":"null"}.hide();Number.extend("random",function(b,a){return Math.floor(Math.random()*(a-b+1)+b)});var o=Object.prototype.hasOwnProperty;Object.extend("forEach",function(b,a,c){for(var h in b)o.call(b,h)&&a.call(c,b[h],h,b)});Object.each=Object.forEach;Array.implement({forEach:function(b,a){for(var c=0,h=this.length;c<h;c++)c in this&&b.call(a,this[c],c,this)},each:function(b,a){Array.forEach(this,b,a);return this}});var q=function(a){switch(b(a)){case"array":return a.clone();case"object":return Object.clone(a);default:return a}};Array.implement("clone",function(){for(var b=this.length,a=Array(b);b--;)a[b]=q(this[b]);return a});var u=function(a,c,h){switch(b(h)){case"object":"object"==b(a[c])?Object.merge(a[c],h):a[c]=Object.clone(h);break;case"array":a[c]=h.clone();break;default:a[c]=h}return a};Object.extend({merge:function(a,c,h){if("string"==b(c))return u(a,c,h);for(var k=1,e=arguments.length;k<e;k++){var d=arguments[k],o;for(o in d)u(a,o,d[o])}return a},clone:function(b){var a={},c;for(c in b)a[c]=q(b[c]);return a},append:function(b){for(var a=1,c=arguments.length;a<c;a++){var h=arguments[a]||{},k;for(k in h)b[k]=h[k]}return b}});["Object","WhiteSpace","TextNode","Collection","Arguments"].each(function(b){new f(b)});var r=Date.now();String.extend("uniqueID",function(){return(r++).toString(36)})})();Array.implement({every:function(b,a){for(var c=0,d=this.length>>>0;c<d;c++)if(c in this&&!b.call(a,this[c],c,this))return!1;return!0},filter:function(b,a){for(var c=[],d,e=0,f=this.length>>>0;e<f;e++)e in this&&(d=this[e],b.call(a,d,e,this)&&c.push(d));return c},indexOf:function(b,a){for(var c=this.length>>>0,d=0>a?Math.max(0,c+a):a||0;d<c;d++)if(this[d]===b)return d;return-1},map:function(b,a){for(var c=this.length>>>0,d=Array(c),e=0;e<c;e++)e in this&&(d[e]=b.call(a,this[e],e,this));return d},some:function(b,a){for(var c=0,d=this.length>>>0;c<d;c++)if(c in this&&b.call(a,this[c],c,this))return!0;return!1},clean:function(){return this.filter(function(b){return null!=b})},invoke:function(b){var a=Array.slice(arguments,1);return this.map(function(c){return c[b].apply(c,a)})},associate:function(b){for(var a={},c=Math.min(this.length,b.length),d=0;d<c;d++)a[b[d]]=this[d];return a},link:function(b){for(var a={},c=0,d=this.length;c<d;c++)for(var e in b)if(b[e](this[c])){a[e]=this[c];delete b[e];break}return a},contains:function(b,a){return-1!=this.indexOf(b,a)},append:function(b){this.push.apply(this,b);return this},getLast:function(){return this.length?this[this.length-1]:null},getRandom:function(){return this.length?this[Number.random(0,this.length-1)]:null},include:function(b){this.contains(b)||this.push(b);return this},combine:function(b){for(var a=0,c=b.length;a<c;a++)this.include(b[a]);return this},erase:function(b){for(var a=this.length;a--;)this[a]===b&&this.splice(a,1);return this},empty:function(){this.length=0;return this},flatten:function(){for(var b=[],a=0,c=this.length;a<c;a++){var d=typeOf(this[a]);"null"!=d&&(b=b.concat("array"==d||"collection"==d||"arguments"==d||instanceOf(this[a],Array)?Array.flatten(this[a]):this[a]))}return b},pick:function(){for(var b=0,a=this.length;b<a;b++)if(null!=this[b])return this[b];return null},hexToRgb:function(b){if(3!=this.length)return null;var a=this.map(function(b){1==b.length&&(b+=b);return b.toInt(16)});return b?a:"rgb("+a+")"},rgbToHex:function(b){if(3>this.length)return null;if(4==this.length&&0==this[3]&&!b)return"transparent";for(var a=[],c=0;3>c;c++){var d=(this[c]-0).toString(16);a.push(1==d.length?"0"+d:d)}return b?a:"#"+a.join("")}});String.implement({test:function(b,a){return("regexp"==typeOf(b)?b:RegExp(""+b,a)).test(this)},contains:function(b,a){return a?-1<(a+this+a).indexOf(a+b+a):-1<(""+this).indexOf(b)},trim:function(){return(""+this).replace(/^\s+|\s+$/g,"")},clean:function(){return(""+this).replace(/\s+/g," ").trim()},camelCase:function(){return(""+this).replace(/-\D/g,function(b){return b.charAt(1).toUpperCase()})},hyphenate:function(){return(""+this).replace(/[A-Z]/g,function(b){return"-"+b.charAt(0).toLowerCase()})},capitalize:function(){return(""+this).replace(/\b[a-z]/g,function(b){return b.toUpperCase()})},escapeRegExp:function(){return(""+this).replace(/([-.*+?^${}()|[\]\/\\])/g,"\\$1")},toInt:function(b){return parseInt(this,b||10)},toFloat:function(){return parseFloat(this)},hexToRgb:function(b){var a=(""+this).match(/^#?(\w{1,2})(\w{1,2})(\w{1,2})$/);return a?a.slice(1).hexToRgb(b):null},rgbToHex:function(b){var a=(""+this).match(/\d{1,3}/g);return a?a.rgbToHex(b):null},substitute:function(b,a){return(""+
this).replace(a||/\\?\{([^{}]+)\}/g,function(a,d){return"\\"==a.charAt(0)?a.slice(1):null!=b[d]?b[d]:""})}});Number.implement({limit:function(b,a){return Math.min(a,Math.max(b,this))},round:function(b){b=Math.pow(10,b||0).toFixed(0>b?-b:0);return Math.round(this*b)/b},times:function(b,a){for(var c=0;c<this;c++)b.call(a,c,this)},toFloat:function(){return parseFloat(this)},toInt:function(b){return parseInt(this,b||10)}});Number.alias("each","times");(function(b){var a={};b.each(function(b){Number[b]||(a[b]=function(){return Math[b].apply(null,[this].concat(Array.from(arguments)))})});Number.implement(a)})("abs,acos,asin,atan,atan2,ceil,cos,exp,floor,log,max,min,pow,sin,sqrt,tan".split(","));Function.extend({attempt:function(){for(var b=0,a=arguments.length;b<a;b++)try{return arguments[b]()}catch(c){}return null}});Function.implement({attempt:function(b,a){try{return this.apply(a,Array.from(b))}catch(c){}return null},bind:function(b){var a=this,c=1<arguments.length?Array.slice(arguments,1):null,d=function(){},e=function(){var f=b,g=arguments.length;this instanceof e&&(d.prototype=a.prototype,f=new d);g=!c&&!g?a.call(f):a.apply(f,c&&g?c.concat(Array.slice(arguments)):c||arguments);return f==b?g:f};return e},pass:function(b,a){var c=this;null!=b&&(b=Array.from(b));return function(){return c.apply(a,b||arguments)}},delay:function(b,a,c){return setTimeout(this.pass(null==c?[]:c,a),b)},periodical:function(b,a,c){return setInterval(this.pass(null==c?[]:c,a),b)}});(function(){var b=Object.prototype.hasOwnProperty;Object.extend({subset:function(b,c){for(var d={},e=0,f=c.length;e<f;e++){var g=c[e];g in b&&(d[g]=b[g])}return d},map:function(a,c,d){var e={},f;for(f in a)b.call(a,f)&&(e[f]=c.call(d,a[f],f,a));return e},filter:function(a,c,d){var e={},f;for(f in a){var g=a[f];b.call(a,f)&&c.call(d,g,f,a)&&(e[f]=g)}return e},every:function(a,c,d){for(var e in a)if(b.call(a,e)&&!c.call(d,a[e],e))return!1;return!0},some:function(a,c,d){for(var e in a)if(b.call(a,e)&&c.call(d,a[e],e))return!0;return!1},keys:function(a){var c=[],d;for(d in a)b.call(a,d)&&c.push(d);return c},values:function(a){var c=[],d;for(d in a)b.call(a,d)&&c.push(a[d]);return c},getLength:function(b){return Object.keys(b).length},keyOf:function(a,c){for(var d in a)if(b.call(a,d)&&a[d]===c)return d;return null},contains:function(b,c){return null!=Object.keyOf(b,c)},toQueryString:function(b,c){var d=[];Object.each(b,function(b,a){c&&(a=c+"["+a+"]");var g;switch(typeOf(b)){case"object":g=Object.toQueryString(b,a);break;case"array":var i={};b.each(function(b,a){i[a]=b});g=Object.toQueryString(i,a);break;default:g=a+"="+encodeURIComponent(b)}null!=b&&d.push(g)});return d.join("&")}})})();(function(){var b=this.document,a=b.window=this,c=navigator.userAgent.toLowerCase(),d=navigator.platform.toLowerCase(),e=c.match(/(opera|ie|firefox|chrome|version)[\s\/:]([\w\d\.]+)?.*?(safari|version[\s\/:]([\w\d\.]+)|$)/)||[null,"unknown",0],f=this.Browser={extend:Function.prototype.extend,name:"version"==e[1]?e[3]:e[1],version:"ie"==e[1]&&b.documentMode||parseFloat("opera"==e[1]&&e[4]?e[4]:e[2]),Platform:{name:c.match(/ip(?:ad|od|hone)/)?"ios":(c.match(/(?:webos|android)/)||d.match(/mac|win|linux/)||["other"])[0]},Features:{xpath:!!b.evaluate,air:!!a.runtime,query:!!b.querySelector,json:!!a.JSON},Plugins:{}};f[f.name]=!0;f[f.name+parseInt(f.version,10)]=!0;f.Platform[f.Platform.name]=!0;f.Request=function(){var b=function(){return new XMLHttpRequest},a=function(){return new ActiveXObject("MSXML2.XMLHTTP")},c=function(){return new ActiveXObject("Microsoft.XMLHTTP")};return Function.attempt(function(){b();return b},function(){a();return a},function(){c();return c})}();f.Features.xhr=!!f.Request;c=(Function.attempt(function(){return navigator.plugins["Shockwave Flash"].description},function(){return(new ActiveXObject("ShockwaveFlash.ShockwaveFlash")).GetVariable("$version")})||"0 r0").match(/\d+/g);f.Plugins.Flash={version:Number(c[0]||"0."+c[1])||0,build:Number(c[2])||0};f.exec=function(c){if(!c)return c;if(a.execScript)a.execScript(c);else{var h=b.createElement("script");h.setAttribute("type","text/javascript");h.text=c;b.head.appendChild(h);b.head.removeChild(h)}return c};String.implement("stripScripts",function(b){var a="",c=this.replace(/<script[^>]*>([\s\S]*?)<\/script>/gi,function(b,c){a+=c+"\n";return""});!0===b?f.exec(a):"function"==typeOf(b)&&b(a,c);return c});f.extend({Document:this.Document,Window:this.Window,Element:this.Element,Event:this.Event});this.Window=this.$constructor=new Type("Window",function(){});this.$family=Function.from("window").hide();Window.mirror(function(b,c){a[b]=c});this.Document=b.$constructor=new Type("Document",function(){});b.$family=Function.from("document").hide();Document.mirror(function(a,c){b[a]=c});b.html=b.documentElement;b.head||(b.head=b.getElementsByTagName("head")[0]);if(b.execCommand)try{b.execCommand("BackgroundImageCache",!1,!0)}catch(g){}if(this.attachEvent&&!this.addEventListener){var i=function(){this.detachEvent("onunload",i);b.head=b.html=b.window=null};this.attachEvent("onunload",i)}var j=Array.from;try{j(b.html.childNodes)}catch(m){Array.from=function(b){if(typeof b!="string"&&Type.isEnumerable(b)&&typeOf(b)!="array"){for(var a=b.length,c=Array(a);a--;)c[a]=b[a];return c}return j(b)};var h=Array.prototype,k=h.slice;"pop,push,reverse,shift,sort,splice,unshift,concat,join,slice".split(",").each(function(b){var a=h[b];Array[b]=function(b){return a.apply(Array.from(b),k.call(arguments,1))}})}})();(function(){var b={},a=this.DOMEvent=new Type("DOMEvent",function(a,d){d||(d=window);a=a||d.event;if(a.$extended)return a;this.event=a;this.$extended=!0;this.shift=a.shiftKey;this.control=a.ctrlKey;this.alt=a.altKey;this.meta=a.metaKey;for(var e=this.type=a.type,f=a.target||a.srcElement;f&&3==f.nodeType;)f=f.parentNode;this.target=document.id(f);if(0==e.indexOf("key")){if(f=this.code=a.which||a.keyCode,this.key=b[f],"keydown"==e&&(111<f&&124>f?this.key="f"+(f-111):95<f&&106>f&&(this.key=f-96)),null==this.key)this.key=String.fromCharCode(f).toLowerCase()}else if("click"==e||"dblclick"==e||"contextmenu"==e||"DOMMouseScroll"==e||0==e.indexOf("mouse")){f=d.document;f=!f.compatMode||"CSS1Compat"==f.compatMode?f.html:f.body;this.page={x:null!=a.pageX?a.pageX:a.clientX+f.scrollLeft,y:null!=a.pageY?a.pageY:a.clientY+f.scrollTop};this.client={x:null!=a.pageX?a.pageX-d.pageXOffset:a.clientX,y:null!=a.pageY?a.pageY-d.pageYOffset:a.clientY};if("DOMMouseScroll"==e||"mousewheel"==e)this.wheel=a.wheelDelta?a.wheelDelta/120:-(a.detail||0)/3;this.rightClick=3==a.which||2==a.button;if("mouseover"==e||"mouseout"==e){for(e=a.relatedTarget||a[("mouseover"==e?"from":"to")+"Element"];e&&3==e.nodeType;)e=e.parentNode;this.relatedTarget=document.id(e)}}else if(0==e.indexOf("touch")||0==e.indexOf("gesture"))if(this.rotation=a.rotation,this.scale=a.scale,this.targetTouches=a.targetTouches,this.changedTouches=a.changedTouches,(e=this.touches=a.touches)&&e[0])e=e[0],this.page={x:e.pageX,y:e.pageY},this.client={x:e.clientX,y:e.clientY};this.client||(this.client={});this.page||(this.page={})});a.implement({stop:function(){return this.preventDefault().stopPropagation()},stopPropagation:function(){this.event.stopPropagation?this.event.stopPropagation():this.event.cancelBubble=!0;return this},preventDefault:function(){this.event.preventDefault?this.event.preventDefault():this.event.returnValue=!1;return this}});a.defineKey=function(a,d){b[a]=d;return this};a.defineKeys=a.defineKey.overloadSetter(!0);a.defineKeys({38:"up",40:"down",37:"left",39:"right",27:"esc",32:"space",8:"backspace",9:"tab",46:"delete",13:"enter"})})();(function(){var b=this.Class=new Type("Class",function(e){instanceOf(e,Function)&&(e={initialize:e});var d=function(){c(this);if(d.$prototyping)return this;this.$caller=null;var a=this.initialize?this.initialize.apply(this,arguments):this;this.$caller=this.caller=null;return a}.extend(this).implement(e);d.$constructor=b;d.prototype.$constructor=d;d.prototype.parent=a;return d}),a=function(){if(!this.$caller)throw Error('The method "parent" cannot be called.');var a=this.$caller.$name,b=this.$caller.$owner.parent,b=b?b.prototype[a]:null;if(!b)throw Error('The method "'+a+'" has no parent.');return b.apply(this,arguments)},c=function(a){for(var b in a){var e=a[b];switch(typeOf(e)){case"object":var d=function(){};d.prototype=e;a[b]=c(new d);break;case"array":a[b]=e.clone()}}return a},d=function(a,b,c){c.$origin&&(c=c.$origin);var e=function(){if(c.$protected&&this.$caller==null)throw Error('The method "'+b+'" cannot be called.');var a=this.caller,h=this.$caller;this.caller=h;this.$caller=e;var k=c.apply(this,arguments);this.$caller=h;this.caller=a;return k}.extend({$owner:a,$origin:c,$name:b});return e},e=function(a,c,e){if(b.Mutators.hasOwnProperty(a)&&(c=b.Mutators[a].call(this,c),null==c))return this;if("function"==typeOf(c)){if(c.$hidden)return this;this.prototype[a]=e?c:d(this,a,c)}else Object.merge(this.prototype,a,c);return this};b.implement("implement",e.overloadSetter());b.Mutators={Extends:function(a){this.parent=a;a.$prototyping=!0;var b=new a;delete a.$prototyping;this.prototype=b},Implements:function(a){Array.from(a).each(function(a){var a=new a,b;for(b in a)e.call(this,b,a[b],!0)},this)}}})();(function(){this.Chain=new Class({$chain:[],chain:function(){this.$chain.append(Array.flatten(arguments));return this},callChain:function(){return this.$chain.length?this.$chain.shift().apply(this,arguments):!1},clearChain:function(){this.$chain.empty();return this}});var b=function(a){return a.replace(/^on([A-Z])/,function(a,b){return b.toLowerCase()})};this.Events=new Class({$events:{},addEvent:function(a,c,d){a=b(a);this.$events[a]=(this.$events[a]||[]).include(c);d&&(c.internal=!0);return this},addEvents:function(a){for(var b in a)this.addEvent(b,a[b]);return this},fireEvent:function(a,c,d){a=b(a);a=this.$events[a];if(!a)return this;c=Array.from(c);a.each(function(a){d?a.delay(d,this,c):a.apply(this,c)},this);return this},removeEvent:function(a,c){var a=b(a),d=this.$events[a];if(d&&!c.internal){var e=d.indexOf(c);-1!=e&&delete d[e]}return this},removeEvents:function(a){var c;if("object"==typeOf(a)){for(c in a)this.removeEvent(c,a[c]);return this}a&&(a=b(a));for(c in this.$events)if(!(a&&a!=c))for(var d=this.$events[c],e=d.length;e--;)e in d&&this.removeEvent(c,d[e]);return this}});this.Options=new Class({setOptions:function(){var a=this.options=Object.merge.apply(null,[{},this.options].append(arguments));if(this.addEvent)for(var b in a)"function"==typeOf(a[b])&&/^on[A-Z]/.test(b)&&(this.addEvent(b,a[b]),delete a[b]);return this}})})();(function(){function b(b,h,o,l,f,q,j,g,x,F,t,B,A,D,v,z){if(h||-1===c)if(a.expressions[++c]=[],d=-1,h)return"";if(o||l||-1===d)o=o||" ",b=a.expressions[c],e&&b[d]&&(b[d].reverseCombinator=m(o)),b[++d]={combinator:o,tag:"*"};o=a.expressions[c][d];if(f)o.tag=f.replace(i,"");else if(q)o.id=q.replace(i,"");else if(j)j=j.replace(i,""),o.classList||(o.classList=[]),o.classes||(o.classes=[]),o.classList.push(j),o.classes.push({value:j,regexp:RegExp("(^|\\s)"+k(j)+"(\\s|$)")});else if(A)z=(z=z||v)?z.replace(i,""):null,o.pseudos||(o.pseudos=[]),o.pseudos.push({key:A.replace(i,""),value:z,type:1==B.length?"class":"element"});else if(g){var g=g.replace(i,""),t=(t||"").replace(i,""),y,E;switch(x){case"^=":E=RegExp("^"+k(t));break;case"$=":E=RegExp(k(t)+"$");break;case"~=":E=RegExp("(^|\\s)"+k(t)+"(\\s|$)");break;case"|=":E=RegExp("^"+k(t)+"(-|$)");break;case"=":y=function(a){return t==a};break;case"*=":y=function(a){return a&&-1<a.indexOf(t)};break;case"!=":y=function(a){return t!=a};break;default:y=function(a){return!!a}}""==t&&/^[*$^]=$/.test(x)&&(y=function(){return!1});y||(y=function(a){return a&&E.test(a)});o.attributes||(o.attributes=[]);o.attributes.push({key:g,operator:x,value:t,test:y})}return""}var a,c,d,e,f={},g={},i=/\\/g,j=function(k,d){if(null==k)return null;if(!0===k.Slick)return k;var k=(""+k).replace(/^\s+|\s+$/g,""),q=(e=!!d)?g:f;if(q[k])return q[k];a={Slick:!0,expressions:[],raw:k,reverse:function(){return j(this.raw,!0)}};for(c=-1;k!=(k=k.replace(o,b)););a.length=a.expressions.length;return q[a.raw]=e?h(a):a},m=function(a){return"!"===a?" ":" "===a?"!":/^!/.test(a)?a.replace(/^!/,""):"!"+a},h=function(a){for(var b=a.expressions,c=0;c<b.length;c++){for(var h=b[c],k={parts:[],tag:"*",combinator:m(h[0].combinator)},e=0;e<h.length;e++){var d=h[e];d.reverseCombinator||(d.reverseCombinator=" ");d.combinator=d.reverseCombinator;delete d.reverseCombinator}h.reverse().push(k)}return a},k=function(a){return a.replace(/[-[\]{}()*+?.\\^$|,#\s]/g,function(a){return"\\"+a})},o=RegExp("^(?:\\s*(,)\\s*|\\s*(<combinator>+)\\s*|(\\s+)|(<unicode>+|\\*)|\\#(<unicode>+)|\\.(<unicode>+)|\\[\\s*(<unicode1>+)(?:\\s*([*^$!~|]?=)(?:\\s*(?:([\"']?)(.*?)\\9)))?\\s*\\](?!\\])|(:+)(<unicode>+)(?:\\((?:(?:([\"'])([^\\13]*)\\13)|((?:\\([^)]+\\)|[^()]*)+))\\))?)".replace(/<combinator>/,"["+k(">+~`!@$%^&={}\\;</")+"]").replace(/<unicode>/g,"(?:[\\w\\u00a1-\\uFFFF-]|\\\\[^\\s0-9a-f])").replace(/<unicode1>/g,"(?:[:\\w\\u00a1-\\uFFFF-]|\\\\[^\\s0-9a-f])")),q=this.Slick||{};q.parse=function(a){return j(a)};q.escapeRegExp=k;this.Slick||(this.Slick=q)}).apply("undefined"!=typeof exports?exports:this);(function(){var b={},a={},c=Object.prototype.toString;b.isNativeCode=function(a){return/\{\s*\[native code\]\s*\}/.test(""+a)};b.isXML=function(a){return!!a.xmlVersion||!!a.xml||"[object XMLDocument]"==c.call(a)||9==a.nodeType&&"HTML"!=a.documentElement.nodeName};b.setDocument=function(b){var c=b.nodeType;if(9!=c)if(c)b=b.ownerDocument;else if(b.navigator)b=b.document;else return;if(this.document!==b){this.document=b;var c=b.documentElement,e=this.getUIDXML(c),d=a[e],f;if(!d){d=a[e]={};d.root=c;d.isXMLDocument=this.isXML(b);d.brokenStarGEBTN=d.starSelectsClosedQSA=d.idGetsName=d.brokenMixedCaseQSA=d.brokenGEBCN=d.brokenCheckedQSA=d.brokenEmptyAttributeQSA=d.isHTMLDocument=d.nativeMatchesSelector=!1;var j,m,l,s,g,n=b.createElement("div"),i=b.body||b.getElementsByTagName("body")[0]||c;i.appendChild(n);try{n.innerHTML='<a id="slick_uniqueid"></a>',d.isHTMLDocument=!!b.getElementById("slick_uniqueid")}catch(x){}if(d.isHTMLDocument){n.style.display="none";n.appendChild(b.createComment(""));e=1<n.getElementsByTagName("*").length;try{n.innerHTML="foo</foo>",j=(g=n.getElementsByTagName("*"))&&!!g.length&&"/"==g[0].nodeName.charAt(0)}catch(F){}d.brokenStarGEBTN=e||j;try{n.innerHTML='<a name="slick_uniqueid"></a><b id="slick_uniqueid"></b>',d.idGetsName=b.getElementById("slick_uniqueid")===n.firstChild}catch(t){}if(n.getElementsByClassName){try{n.innerHTML='<a class="f"></a><a class="b"></a>',n.getElementsByClassName("b").length,n.firstChild.className="b",l=2!=n.getElementsByClassName("b").length}catch(B){}try{n.innerHTML='<a class="a"></a><a class="f b a"></a>',m=2!=n.getElementsByClassName("a").length}catch(A){}d.brokenGEBCN=l||m}if(n.querySelectorAll){try{n.innerHTML="foo</foo>",g=n.querySelectorAll("*"),d.starSelectsClosedQSA=g&&!!g.length&&"/"==g[0].nodeName.charAt(0)}catch(D){}try{n.innerHTML='<a class="MiX"></a>',d.brokenMixedCaseQSA=!n.querySelectorAll(".MiX").length}catch(v){}try{n.innerHTML='<select><option selected="selected">a</option></select>',d.brokenCheckedQSA=0==n.querySelectorAll(":checked").length}catch(z){}try{n.innerHTML='<a class=""></a>',d.brokenEmptyAttributeQSA=0!=n.querySelectorAll('[class*=""]').length}catch(y){}}try{n.innerHTML='<form action="s"><input id="action"/></form>',s="s"!=n.firstChild.getAttribute("action")}catch(E){}d.nativeMatchesSelector=c.matchesSelector||c.mozMatchesSelector||c.webkitMatchesSelector;if(d.nativeMatchesSelector)try{d.nativeMatchesSelector.call(c,":slick"),d.nativeMatchesSelector=null}catch(G){}}try{c.slick_expando=1,delete c.slick_expando,d.getUID=this.getUIDHTML}catch(H){d.getUID=this.getUIDXML}i.removeChild(n);n=g=i=null;d.getAttribute=d.isHTMLDocument&&s?function(a,b){var c=this.attributeGetters[b];return c?c.call(a):(c=a.getAttributeNode(b))?c.nodeValue:null}:function(a,b){var c=this.attributeGetters[b];return c?c.call(a):a.getAttribute(b)};d.hasAttribute=c&&this.isNativeCode(c.hasAttribute)?function(a,b){return a.hasAttribute(b)}:function(a,b){a=a.getAttributeNode(b);return!(!a||!a.specified&&!a.nodeValue)};j=c&&this.isNativeCode(c.contains);m=b&&this.isNativeCode(b.contains);d.contains=j&&m?function(a,b){return a.contains(b)}:j&&!m?function(a,c){return a===c||(a===b?b.documentElement:a).contains(c)}:c&&c.compareDocumentPosition?function(a,b){return a===b||!!(a.compareDocumentPosition(b)&16)}:function(a,b){if(b){do if(b===a)return!0;while(b=b.parentNode)}return!1};d.documentSorter=c.compareDocumentPosition?function(a,b){return!a.compareDocumentPosition||!b.compareDocumentPosition?0:a.compareDocumentPosition(b)&4?-1:a===b?0:1}:"sourceIndex"in c?function(a,b){return!a.sourceIndex||!b.sourceIndex?0:a.sourceIndex-b.sourceIndex}:b.createRange?function(a,b){if(!a.ownerDocument||!b.ownerDocument)return 0;var c=a.ownerDocument.createRange(),h=b.ownerDocument.createRange();c.setStart(a,0);c.setEnd(a,0);h.setStart(b,0);h.setEnd(b,0);return c.compareBoundaryPoints(Range.START_TO_END,h)}:null;c=null}for(f in d)this[f]=d[f]}};var d=/^([#.]?)((?:[\w-]+|\*))$/,e=/\[.+[*$^]=(?:""|'')?\]/,f={};b.search=function(a,b,c,j){var g=this.found=j?null:c||[];if(a)if(a.navigator)a=a.document;else{if(!a.nodeType)return g}else return g;var r,i,l=this.uniques={},c=!(!c||!c.length),s=9==a.nodeType;this.document!==(s?a:a.ownerDocument)&&this.setDocument(a);if(c)for(i=g.length;i--;)l[this.getUID(g[i])]=!0;if("string"==typeof b){var p=b.match(d);a:if(p){i=p[1];var n=p[2];if(i)if("#"==i){if(!this.isHTMLDocument||!s)break a;p=a.getElementById(n);if(!p)return g;if(this.idGetsName&&p.getAttributeNode("id").nodeValue!=n)break a;if(j)return p||null;(!c||!l[this.getUID(p)])&&g.push(p)}else{if("."==i){if(!this.isHTMLDocument||(!a.getElementsByClassName||this.brokenGEBCN)&&a.querySelectorAll)break a;if(a.getElementsByClassName&&!this.brokenGEBCN){r=a.getElementsByClassName(n);if(j)return r[0]||null;for(i=0;p=r[i++];)(!c||!l[this.getUID(p)])&&g.push(p)}else{var C=RegExp("(^|\\s)"+m.escapeRegExp(n)+"(\\s|$)");r=a.getElementsByTagName("*");for(i=0;p=r[i++];)if((className=p.className)&&C.test(className)){if(j)return p;(!c||!l[this.getUID(p)])&&g.push(p)}}}}else{if("*"==n&&this.brokenStarGEBTN)break a;r=a.getElementsByTagName(n);if(j)return r[0]||null;for(i=0;p=r[i++];)(!c||!l[this.getUID(p)])&&g.push(p)}c&&this.sort(g);return j?null:g}a:if(a.querySelectorAll&&this.isHTMLDocument&&!f[b]&&!this.brokenMixedCaseQSA&&!(this.brokenCheckedQSA&&-1<b.indexOf(":checked")||this.brokenEmptyAttributeQSA&&e.test(b)||!s&&-1<b.indexOf(",")||m.disableQSA)){i=b;p=a;if(!s){var x=p.getAttribute("id");p.setAttribute("id","slickid__");i="#slickid__ "+i;a=p.parentNode}try{if(j)return a.querySelector(i)||null;r=a.querySelectorAll(i)}catch(F){f[b]=1;break a}finally{s||(x?p.setAttribute("id",x):p.removeAttribute("id"),a=p)}if(this.starSelectsClosedQSA)for(i=0;p=r[i++];)"@"<p.nodeName&&(!c||!l[this.getUID(p)])&&g.push(p);else for(i=0;p=r[i++];)(!c||!l[this.getUID(p)])&&g.push(p);c&&this.sort(g);return g}r=this.Slick.parse(b);if(!r.length)return g}else{if(null==b)return g;if(b.Slick)r=b;else{if(this.contains(a.documentElement||a,b))g?g.push(b):g=b;return g}}this.posNTH={};this.posNTHLast={};this.posNTHType={};this.posNTHTypeLast={};this.push=!c&&(j||1==r.length&&1==r.expressions[0].length)?this.pushArray:this.pushUID;null==g&&(g=[]);var t,B,A,D,v,z,y=r.expressions;i=0;a:for(;z=y[i];i++)for(b=0;v=z[b];b++){x="combinator:"+v.combinator;if(!this[x])continue a;s=this.isXMLDocument?v.tag:v.tag.toUpperCase();p=v.id;n=v.classList;A=v.classes;D=v.attributes;v=v.pseudos;t=b===z.length-1;this.bitUniques={};t?(this.uniques=l,this.found=g):(this.uniques={},this.found=[]);if(0===b){if(this[x](a,s,p,A,D,v,n),j&&t&&g.length)break a}else if(j&&t){t=0;for(B=C.length;t<B;t++)if(this[x](C[t],s,p,A,D,v,n),g.length)break a}else{t=0;for(B=C.length;t<B;t++)this[x](C[t],s,p,A,D,v,n)}C=this.found}(c||1<r.expressions.length)&&this.sort(g);return j?g[0]||null:g};b.uidx=1;b.uidk="slick-uniqueid";b.getUIDXML=function(a){var b=a.getAttribute(this.uidk);b||(b=this.uidx++,a.setAttribute(this.uidk,b));return b};b.getUIDHTML=function(a){return a.uniqueNumber||(a.uniqueNumber=this.uidx++)};b.sort=function(a){if(!this.documentSorter)return a;a.sort(this.documentSorter);return a};b.cacheNTH={};b.matchNTH=/^([+-]?\d*)?([a-z]+)?([+-]\d+)?$/;b.parseNTHArgument=function(a){var b=a.match(this.matchNTH);if(!b)return!1;var c=b[2]||!1,d=b[1]||1;"-"==d&&(d=-1);b=+b[3]||0;b="n"==c?{a:d,b:b}:"odd"==c?{a:2,b:1}:"even"==c?{a:2,b:0}:{a:0,b:d};return this.cacheNTH[a]=b};b.createNTHPseudo=function(a,b,c,d){return function(e,f){var g=this.getUID(e);if(!this[c][g]){var l=e.parentNode;if(!l)return!1;var l=l[a],s=1;if(d){var j=e.nodeName;do l.nodeName==j&&(this[c][this.getUID(l)]=s++);while(l=l[b])}else{do 1==l.nodeType&&(this[c][this.getUID(l)]=s++);while(l=l[b])}}f=f||"n";s=this.cacheNTH[f]||this.parseNTHArgument(f);if(!s)return!1;l=s.a;s=s.b;g=this[c][g];if(0==l)return s==g;if(0<l){if(g<s)return!1}else if(s<g)return!1;return 0==(g-s)%l}};b.pushArray=function(a,b,c,d,e,f){this.matchSelector(a,b,c,d,e,f)&&this.found.push(a)};b.pushUID=function(a,b,c,d,e,f){var g=this.getUID(a);!this.uniques[g]&&this.matchSelector(a,b,c,d,e,f)&&(this.uniques[g]=!0,this.found.push(a))};b.matchNode=function(a,b){if(this.isHTMLDocument&&this.nativeMatchesSelector)try{return this.nativeMatchesSelector.call(a,b.replace(/\[([^=]+)=\s*([^'"\]]+?)\s*\]/g,'[$1="$2"]'))}catch(c){}var d=this.Slick.parse(b);if(!d)return!0;var e=d.expressions,f=0,g;for(g=0;currentExpression=e[g];g++)if(1==currentExpression.length){var l=currentExpression[0];if(this.matchSelector(a,this.isXMLDocument?l.tag:l.tag.toUpperCase(),l.id,l.classes,l.attributes,l.pseudos))return!0;f++}if(f==d.length)return!1;d=this.search(this.document,d);for(g=0;e=d[g++];)if(e===a)return!0;return!1};b.matchPseudo=function(a,b,c){var d="pseudo:"+b;if(this[d])return this[d](a,c);a=this.getAttribute(a,b);return c?c==a:!!a};b.matchSelector=function(a,b,c,d,e,f){if(b){var g=this.isXMLDocument?a.nodeName:a.nodeName.toUpperCase();if("*"==b){if("@">g)return!1}else if(g!=b)return!1}if(c&&a.getAttribute("id")!=c)return!1;if(d)for(b=d.length;b--;)if(c=this.getAttribute(a,"class"),!c||!d[b].regexp.test(c))return!1;if(e)for(b=e.length;b--;)if(d=e[b],d.operator?!d.test(this.getAttribute(a,d.key)):!this.hasAttribute(a,d.key))return!1;if(f)for(b=f.length;b--;)if(d=f[b],!this.matchPseudo(a,d.key,d.value))return!1;return!0};var g={" ":function(a,b,c,d,e,f,g){var l;if(this.isHTMLDocument){if(c){l=this.document.getElementById(c);if(!l&&a.all||this.idGetsName&&l&&l.getAttributeNode("id").nodeValue!=c){g=a.all[c];if(!g)return;g[0]||(g=[g]);for(a=0;l=g[a++];){var s=l.getAttributeNode("id");if(s&&s.nodeValue==c){this.push(l,b,null,d,e,f);break}}return}if(l){if(this.document!==a&&!this.contains(a,l))return;this.push(l,b,null,d,e,f);return}if(this.contains(this.root,a))return}if(d&&a.getElementsByClassName&&!this.brokenGEBCN&&(g=a.getElementsByClassName(g.join(" ")))&&g.length){for(a=0;l=g[a++];)this.push(l,b,c,null,e,f);return}}if((g=a.getElementsByTagName(b))&&g.length){this.brokenStarGEBTN||(b=null);for(a=0;l=g[a++];)this.push(l,b,c,d,e,f)}},">":function(a,b,c,d,e,f){if(a=a.firstChild){do 1==a.nodeType&&this.push(a,b,c,d,e,f);while(a=a.nextSibling)}},"+":function(a,b,c,d,e,f){for(;a=a.nextSibling;)if(1==a.nodeType){this.push(a,b,c,d,e,f);break}},"^":function(a,b,c,d,e,f){if(a=a.firstChild)if(1==a.nodeType)this.push(a,b,c,d,e,f);else this["combinator:+"](a,b,c,d,e,f)},"~":function(a,b,c,d,e,f){for(;a=a.nextSibling;)if(1==a.nodeType){var g=this.getUID(a);if(this.bitUniques[g])break;this.bitUniques[g]=!0;this.push(a,b,c,d,e,f)}},"++":function(a,b,c,d,e,f){this["combinator:+"](a,b,c,d,e,f);this["combinator:!+"](a,b,c,d,e,f)},"~~":function(a,b,c,d,e,f){this["combinator:~"](a,b,c,d,e,f);this["combinator:!~"](a,b,c,d,e,f)},"!":function(a,b,c,d,e,f){for(;a=a.parentNode;)a!==this.document&&this.push(a,b,c,d,e,f)},"!>":function(a,b,c,d,e,f){a=a.parentNode;a!==this.document&&this.push(a,b,c,d,e,f)},"!+":function(a,b,c,d,e,f){for(;a=a.previousSibling;)if(1==a.nodeType){this.push(a,b,c,d,e,f);break}},"!^":function(a,b,c,d,e,f){if(a=a.lastChild)if(1==a.nodeType)this.push(a,b,c,d,e,f);else this["combinator:!+"](a,b,c,d,e,f)},"!~":function(a,b,c,d,e,f){for(;a=a.previousSibling;)if(1==a.nodeType){var g=this.getUID(a);if(this.bitUniques[g])break;this.bitUniques[g]=!0;this.push(a,b,c,d,e,f)}}},i;for(i in g)b["combinator:"+i]=g[i];var g={empty:function(a){var b=a.firstChild;return!(b&&1==b.nodeType)&&!(a.innerText||a.textContent||"").length},not:function(a,b){return!this.matchNode(a,b)},contains:function(a,b){return-1<(a.innerText||a.textContent||"").indexOf(b)},"first-child":function(a){for(;a=a.previousSibling;)if(1==a.nodeType)return!1;return!0},"last-child":function(a){for(;a=a.nextSibling;)if(1==a.nodeType)return!1;return!0},"only-child":function(a){for(var b=a;b=b.previousSibling;)if(1==b.nodeType)return!1;for(;a=a.nextSibling;)if(1==a.nodeType)return!1;return!0},"nth-child":b.createNTHPseudo("firstChild","nextSibling","posNTH"),"nth-last-child":b.createNTHPseudo("lastChild","previousSibling","posNTHLast"),"nth-of-type":b.createNTHPseudo("firstChild","nextSibling","posNTHType",!0),"nth-last-of-type":b.createNTHPseudo("lastChild","previousSibling","posNTHTypeLast",!0),index:function(a,b){return this["pseudo:nth-child"](a,""+(b+1))},even:function(a){return this["pseudo:nth-child"](a,"2n")},odd:function(a){return this["pseudo:nth-child"](a,"2n+1")},"first-of-type":function(a){for(var b=a.nodeName;a=a.previousSibling;)if(a.nodeName==b)return!1;return!0},"last-of-type":function(a){for(var b=a.nodeName;a=a.nextSibling;)if(a.nodeName==b)return!1;return!0},"only-of-type":function(a){for(var b=a,c=a.nodeName;b=b.previousSibling;)if(b.nodeName==c)return!1;for(;a=a.nextSibling;)if(a.nodeName==c)return!1;return!0},enabled:function(a){return!a.disabled},disabled:function(a){return a.disabled},checked:function(a){return a.checked||a.selected},focus:function(a){return this.isHTMLDocument&&this.document.activeElement===a&&(a.href||a.type||this.hasAttribute(a,"tabindex"))},root:function(a){return a===this.root},selected:function(a){return a.selected}},j;for(j in g)b["pseudo:"+j]=g[j];j=b.attributeGetters={"for":function(){return"htmlFor"in this?this.htmlFor:this.getAttribute("for")},href:function(){return"href"in this?this.getAttribute("href",2):this.getAttribute("href")},style:function(){return this.style?this.style.cssText:this.getAttribute("style")},tabindex:function(){var a=this.getAttributeNode("tabindex");return a&&a.specified?a.nodeValue:null},type:function(){return this.getAttribute("type")},maxlength:function(){var a=this.getAttributeNode("maxLength");return a&&a.specified?a.nodeValue:null}};j.MAXLENGTH=j.maxLength=j.maxlength;var m=b.Slick=this.Slick||{};m.version="1.1.7";m.search=function(a,c,d){return b.search(a,c,d)};m.find=function(a,c){return b.search(a,c,null,!0)};m.contains=function(a,c){b.setDocument(a);return b.contains(a,c)};m.getAttribute=function(a,c){b.setDocument(a);return b.getAttribute(a,c)};m.hasAttribute=function(a,c){b.setDocument(a);return b.hasAttribute(a,c)};m.match=function(a,c){if(!a||!c)return!1;if(!c||c===a)return!0;b.setDocument(a);return b.matchNode(a,c)};m.defineAttributeGetter=function(a,c){b.attributeGetters[a]=c;return this};m.lookupAttributeGetter=function(a){return b.attributeGetters[a]};m.definePseudo=function(a,c){b["pseudo:"+
a]=function(a,b){return c.call(a,b)};return this};m.lookupPseudo=function(a){var c=b["pseudo:"+a];return c?function(a){return c.call(this,a)}:null};m.override=function(a,c){b.override(a,c);return this};m.isXML=b.isXML;m.uidOf=function(a){return b.getUIDHTML(a)};this.Slick||(this.Slick=m)}).apply("undefined"!=typeof exports?exports:this);var Element=function(b,a){var c=Element.Constructors[b];if(c)return c(a);if("string"!=typeof b)return document.id(b).set(a);a||(a={});if(!/^[\w-]+$/.test(b)){c=Slick.parse(b).expressions[0][0];b="*"==c.tag?"div":c.tag;c.id&&null==a.id&&(a.id=c.id);var d=c.attributes;if(d)for(var e,f=0,g=d.length;f<g;f++)e=d[f],null==a[e.key]&&(null!=e.value&&"="==e.operator?a[e.key]=e.value:!e.value&&!e.operator&&(a[e.key]=!0));c.classList&&null==a["class"]&&(a["class"]=c.classList.join(" "))}return document.newElement(b,a)};Browser.Element&&(Element.prototype=Browser.Element.prototype,Element.prototype._fireEvent=function(b){return function(a,c){return b.call(this,a,c)}}(Element.prototype.fireEvent));(new Type("Element",Element)).mirror(function(b){if(!Array.prototype[b]){var a={};a[b]=function(){for(var a=[],d=arguments,e=true,f=0,g=this.length;f<g;f++)var i=this[f],i=a[f]=i[b].apply(i,d),e=e&&typeOf(i)=="element";return e?new Elements(a):a};Elements.implement(a)}});Browser.Element||(Element.parent=Object,Element.Prototype={$constructor:Element,$family:Function.from("element").hide()},Element.mirror(function(b,a){Element.Prototype[b]=a}));Element.Constructors={};var IFrame=new Type("IFrame",function(){var b=Array.link(arguments,{properties:Type.isObject,iframe:function(a){return a!=null}}),a=b.properties||{},c;b.iframe&&(c=document.id(b.iframe));var d=a.onload||function(){};delete a.onload;a.id=a.name=[a.id,a.name,c?c.id||c.name:"IFrame_"+String.uniqueID()].pick();c=new Element(c||"iframe",a);b=function(){d.call(c.contentWindow)};window.frames[a.id]?b():c.addListener("load",b);return c}),Elements=this.Elements=function(b){if(b&&b.length)for(var a={},c,d=0;c=b[d++];){var e=Slick.uidOf(c);if(!a[e]){a[e]=true;this.push(c)}}};Elements.prototype={length:0};Elements.parent=Array;(new Type("Elements",Elements)).implement({filter:function(b,a){return!b?this:new Elements(Array.filter(this,typeOf(b)=="string"?function(a){return a.match(b)}:b,a))}.protect(),push:function(){for(var b=this.length,a=0,c=arguments.length;a<c;a++){var d=document.id(arguments[a]);d&&(this[b++]=d)}return this.length=b}.protect(),unshift:function(){for(var b=[],a=0,c=arguments.length;a<c;a++){var d=document.id(arguments[a]);d&&b.push(d)}return Array.prototype.unshift.apply(this,b)}.protect(),concat:function(){for(var b=new Elements(this),a=0,c=arguments.length;a<c;a++){var d=arguments[a];Type.isEnumerable(d)?b.append(d):b.push(d)}return b}.protect(),append:function(b){for(var a=0,c=b.length;a<c;a++)this.push(b[a]);return this}.protect(),empty:function(){for(;this.length;)delete this[--this.length];return this}.protect()});(function(){var b=Array.prototype.splice,a={"0":0,1:1,length:2};b.call(a,1,1);a[1]==1&&Elements.implement("splice",function(){for(var a=this.length,c=b.apply(this,arguments);a>=this.length;)delete this[a--];return c}.protect());Array.forEachMethod(function(a,b){Elements.implement(b,a)});Array.mirror(Elements);var c;try{c=document.createElement("<input name=x>").name=="x"}catch(d){}var e=function(a){return(""+a).replace(/&/g,"&amp;").replace(/"/g,"&quot;")};Document.implement({newElement:function(a,b){if(b&&b.checked!=null)b.defaultChecked=b.checked;if(c&&b){a="<"+a;b.name&&(a=a+(' name="'+e(b.name)+'"'));b.type&&(a=a+(' type="'+e(b.type)+'"'));a=a+">";delete b.name;delete b.type}return this.id(this.createElement(a)).set(b)}})})();(function(){Slick.uidOf(window);Slick.uidOf(document);Document.implement({newTextNode:function(a){return this.createTextNode(a)},getDocument:function(){return this},getWindow:function(){return this.window},id:function(){var a={string:function(b,c,d){return(b=Slick.find(d,"#"+b.replace(/(\W)/g,"\\$1")))?a.element(b,c):null},element:function(a,b){Slick.uidOf(a);if(!b&&!a.$family&&!/^(?:object|embed)$/i.test(a.tagName)){var c=a.fireEvent;a._fireEvent=function(a,b){return c(a,b)};Object.append(a,Element.Prototype)}return a},object:function(b,c,d){return b.toElement?a.element(b.toElement(d),c):null}};a.textnode=a.whitespace=a.window=a.document=function(a){return a};return function(b,c,d){if(b&&b.$family&&b.uniqueNumber)return b;var e=typeOf(b);return a[e]?a[e](b,c,d||document):null}}()});window.$==null&&Window.implement("$",function(a,b){return document.id(a,b,this.document)});Window.implement({getDocument:function(){return this.document},getWindow:function(){return this}});[Document,Element].invoke("implement",{getElements:function(a){return Slick.search(this,a,new Elements)},getElement:function(a){return document.id(Slick.find(this,a))}});var b={contains:function(a){return Slick.contains(this,a)}};document.contains||Document.implement(b);document.createElement("div").contains||Element.implement(b);var a=function(a,b){if(!a)return b;for(var a=Object.clone(Slick.parse(a)),c=a.expressions,d=c.length;d--;)c[d][0].combinator=b;return a};Object.forEach({getNext:"~",getPrevious:"!~",getParent:"!"},function(b,c){Element.implement(c,function(c){return this.getElement(a(c,b))})});Object.forEach({getAllNext:"~",getAllPrevious:"!~",getSiblings:"~~",getChildren:">",getParents:"!"},function(b,c){Element.implement(c,function(c){return this.getElements(a(c,b))})});Element.implement({getFirst:function(b){return document.id(Slick.search(this,a(b,">"))[0])},getLast:function(b){return document.id(Slick.search(this,a(b,">")).getLast())},getWindow:function(){return this.ownerDocument.window},getDocument:function(){return this.ownerDocument},getElementById:function(a){return document.id(Slick.find(this,"#"+(""+a).replace(/(\W)/g,"\\$1")))},match:function(a){return!a||Slick.match(this,a)}});window.$$==null&&Window.implement("$$",function(a){if(arguments.length==1){if(typeof a=="string")return Slick.search(this.document,a,new Elements);if(Type.isEnumerable(a))return new Elements(a)}return new Elements(arguments)});var c={before:function(a,b){var c=b.parentNode;c&&c.insertBefore(a,b)},after:function(a,b){var c=b.parentNode;c&&c.insertBefore(a,b.nextSibling)},bottom:function(a,b){b.appendChild(a)},top:function(a,b){b.insertBefore(a,b.firstChild)}};c.inside=c.bottom;var d={},e={},f={};Array.forEach(["type","value","defaultValue","accessKey","cellPadding","cellSpacing","colSpan","frameBorder","rowSpan","tabIndex","useMap"],function(a){f[a.toLowerCase()]=a});f.html="innerHTML";f.text=document.createElement("div").textContent==null?"innerText":"textContent";Object.forEach(f,function(a,b){e[b]=function(b,c){b[a]=c};d[b]=function(b){return b[a]}});Array.forEach(["compact","nowrap","ismap","declare","noshade","checked","disabled","readOnly","multiple","selected","noresize","defer","defaultChecked","autofocus","controls","autoplay","loop"],function(a){var b=a.toLowerCase();e[b]=function(b,c){b[a]=!!c};d[b]=function(b){return!!b[a]}});Object.append(e,{"class":function(a,b){"className"in a?a.className=b||"":a.setAttribute("class",b)},"for":function(a,b){"htmlFor"in a?a.htmlFor=b:a.setAttribute("for",b)},style:function(a,b){a.style?a.style.cssText=b:a.setAttribute("style",b)},value:function(a,b){a.value=b!=null?b:""}});d["class"]=function(a){return"className"in a?a.className||null:a.getAttribute("class")};b=document.createElement("button");try{b.type="button"}catch(g){}if(b.type!="button")e.type=function(a,b){a.setAttribute("type",b)};b=null;b=document.createElement("input");b.value="t";b.type="submit";if(b.value!="t")e.type=function(a,b){var c=a.value;a.type=b;a.value=c};var b=null,i=function(a){a.random="attribute";return a.getAttribute("random")=="attribute"}(document.createElement("div"));Element.implement({setProperty:function(a,b){var c=e[a.toLowerCase()];if(c)c(this,b);else{if(i)var d=this.retrieve("$attributeWhiteList",{});if(b==null){this.removeAttribute(a);i&&delete d[a]}else{this.setAttribute(a,""+b);i&&(d[a]=true)}}return this},setProperties:function(a){for(var b in a)this.setProperty(b,a[b]);return this},getProperty:function(a){var b=d[a.toLowerCase()];if(b)return b(this);if(i){var c=this.getAttributeNode(a),b=this.retrieve("$attributeWhiteList",{});if(!c)return null;if(c.expando&&!b[a]){c=this.outerHTML;if(c.substr(0,c.search(/\/?['"]?>(?![^<]*<['"])/)).indexOf(a)<0)return null;b[a]=true}}b=Slick.getAttribute(this,a);return!b&&!Slick.hasAttribute(this,a)?null:b},getProperties:function(){var a=Array.from(arguments);return a.map(this.getProperty,this).associate(a)},removeProperty:function(a){return this.setProperty(a,null)},removeProperties:function(){Array.each(arguments,this.removeProperty,this);return this},set:function(a,b){var c=Element.Properties[a];c&&c.set?c.set.call(this,b):this.setProperty(a,b)}.overloadSetter(),get:function(a){var b=Element.Properties[a];return b&&b.get?b.get.apply(this):this.getProperty(a)}.overloadGetter(),erase:function(a){var b=Element.Properties[a];b&&b.erase?b.erase.apply(this):this.removeProperty(a);return this},hasClass:function(a){return this.className.clean().contains(a," ")},addClass:function(a){if(!this.hasClass(a))this.className=(this.className+" "+a).clean();return this},removeClass:function(a){this.className=this.className.replace(RegExp("(^|\\s)"+a+"(?:\\s|$)"),"$1");return this},toggleClass:function(a,b){b==null&&(b=!this.hasClass(a));return b?this.addClass(a):this.removeClass(a)},adopt:function(){var a=this,b,c=Array.flatten(arguments),d=c.length;d>1&&(a=b=document.createDocumentFragment());for(var e=0;e<d;e++){var f=document.id(c[e],true);f&&a.appendChild(f)}b&&this.appendChild(b);return this},appendText:function(a,b){return this.grab(this.getDocument().newTextNode(a),b)},grab:function(a,b){c[b||"bottom"](document.id(a,true),this);return this},inject:function(a,b){c[b||"bottom"](this,document.id(a,true));return this},replaces:function(a){a=document.id(a,true);a.parentNode.replaceChild(this,a);return this},wraps:function(a,b){a=document.id(a,true);return this.replaces(a).grab(a,b)},getSelected:function(){this.selectedIndex;return new Elements(Array.from(this.options).filter(function(a){return a.selected}))},toQueryString:function(){var a=[];this.getElements("input, select, textarea").each(function(b){var c=b.type;if(b.name&&!b.disabled&&!(c=="submit"||c=="reset"||c=="file"||c=="image")){c=b.get("tag")=="select"?b.getSelected().map(function(a){return document.id(a).get("value")}):(c=="radio"||c=="checkbox")&&!b.checked?null:b.get("value");Array.from(c).each(function(c){typeof c!="undefined"&&a.push(encodeURIComponent(b.name)+"="+encodeURIComponent(c))})}});return a.join("&")}});var j={},m={},h=function(a){return m[a]||(m[a]={})},k=function(a){var b=a.uniqueNumber;a.removeEvents&&a.removeEvents();a.clearAttributes&&a.clearAttributes();if(b!=null){delete j[b];delete m[b]}return a},o={input:"checked",option:"selected",textarea:"value"};Element.implement({destroy:function(){var a=k(this).getElementsByTagName("*");Array.each(a,k);Element.dispose(this);return null},empty:function(){Array.from(this.childNodes).each(Element.dispose);return this},dispose:function(){return this.parentNode?this.parentNode.removeChild(this):this},clone:function(a,b){var a=a!==false,c=this.cloneNode(a),d=[c],e=[this],f;if(a){d.append(Array.from(c.getElementsByTagName("*")));e.append(Array.from(this.getElementsByTagName("*")))}for(f=d.length;f--;){var k=d[f],g=e[f];b||k.removeAttribute("id");if(k.clearAttributes){k.clearAttributes();k.mergeAttributes(g);k.removeAttribute("uniqueNumber");if(k.options)for(var j=k.options,m=g.options,h=j.length;h--;)j[h].selected=m[h].selected}(j=o[g.tagName.toLowerCase()])&&g[j]&&(k[j]=g[j])}if(Browser.ie){d=c.getElementsByTagName("object");e=this.getElementsByTagName("object");for(f=d.length;f--;)d[f].outerHTML=e[f].outerHTML}return document.id(c)}});[Element,Window,Document].invoke("implement",{addListener:function(a,b,c){if(a=="unload")var d=b,e=this,b=function(){e.removeListener("unload",b);d()};else j[Slick.uidOf(this)]=this;this.addEventListener?this.addEventListener(a,b,!!c):this.attachEvent("on"+a,b);return this},removeListener:function(a,b,c){this.removeEventListener?this.removeEventListener(a,b,!!c):this.detachEvent("on"+a,b);return this},retrieve:function(a,b){var c=h(Slick.uidOf(this)),d=c[a];b!=null&&d==null&&(d=c[a]=b);return d!=null?d:null},store:function(a,b){h(Slick.uidOf(this))[a]=b;return this},eliminate:function(a){delete h(Slick.uidOf(this))[a];return this}});window.attachEvent&&!window.addEventListener&&window.addListener("unload",function(){Object.each(j,k);window.CollectGarbage&&CollectGarbage()});Element.Properties={};Element.Properties.style={set:function(a){this.style.cssText=a},get:function(){return this.style.cssText},erase:function(){this.style.cssText=""}};Element.Properties.tag={get:function(){return this.tagName.toLowerCase()}};Element.Properties.html={set:function(a){a==null?a="":typeOf(a)=="array"&&(a=a.join(""));this.innerHTML=a},erase:function(){this.innerHTML=""}};b=document.createElement("div");b.innerHTML="<nav></nav>";var q=b.childNodes.length==1;if(!q)for(var b=["abbr","article","aside","audio","canvas","datalist","details","figcaption","figure","footer","header","hgroup","mark","meter","nav","output","progress","section","summary","time","video"],u=document.createDocumentFragment(),r=b.length;r--;)u.createElement(b[r]);b=null;b=Function.attempt(function(){document.createElement("table").innerHTML="<tr><td></td></tr>";return true});r=document.createElement("tr");r.innerHTML="<td></td>";var w=r.innerHTML=="<td></td>",r=null;if(!b||!w||!q)Element.Properties.html.set=function(a){var b={table:[1,"<table>","</table>"],select:[1,"<select>","</select>"],tbody:[2,"<table><tbody>","</tbody></table>"],tr:[3,"<table><tbody><tr>","</tr></tbody></table>"]};b.thead=b.tfoot=b.tbody;return function(c){var d=b[this.get("tag")];!d&&!q&&(d=[0,"",""]);if(!d)return a.call(this,c);var e=d[0],f=document.createElement("div"),k=f;q||u.appendChild(f);for(f.innerHTML=[d[1],c,d[2]].flatten().join("");e--;)k=k.firstChild;this.empty().adopt(k.childNodes);q||u.removeChild(f)}}(Element.Properties.html.set);b=document.createElement("form");b.innerHTML="<select><option>s</option></select>";if(b.firstChild.value!="s")Element.Properties.value={set:function(a){if(this.get("tag")!="select")return this.setProperty("value",a);for(var b=this.getElements("option"),c=0;c<b.length;c++){var d=b[c],e=d.getAttributeNode("value");if((e&&e.specified?d.value:d.get("text"))==a)return d.selected=true}},get:function(){var a=this,b=a.get("tag");if(b!="select"&&b!="option")return this.getProperty("value");if(b=="select"&&!(a=a.getSelected()[0]))return"";return(b=a.getAttributeNode("value"))&&b.specified?a.value:a.get("text")}};b=null;if(document.createElement("div").getAttributeNode("id"))Element.Properties.id={set:function(a){this.id=this.getAttributeNode("id").value=a},get:function(){return this.id||null},erase:function(){this.id=this.getAttributeNode("id").value=""}}})();(function(){var b=document.html,a=document.createElement("div");a.style.color="red";a.style.color=null;var c=a.style.color=="red",a=null;Element.Properties.styles={set:function(a){this.setStyles(a)}};var a=b.style.opacity!=null,d=b.style.filter!=null,e=/alpha\(opacity=([\d.]+)\)/i,f=a?function(a,b){a.style.opacity=b}:d?function(a,b){var c=a.style;if(!a.currentStyle||!a.currentStyle.hasLayout)c.zoom=1;var b=b==null||b==1?"":"alpha(opacity="+(b*100).limit(0,100).round()+")",d=c.filter||a.getComputedStyle("filter")||"";c.filter=e.test(d)?d.replace(e,b):d+b;c.filter||c.removeAttribute("filter")}:function(a,b){a.store("$opacity",b);a.style.visibility=b>0||b==null?"visible":"hidden"},g=a?function(a){a=a.style.opacity||a.getComputedStyle("opacity");return a==""?1:a.toFloat()}:d?function(a){var a=a.style.filter||a.getComputedStyle("filter"),b;a&&(b=a.match(e));return b==null||a==null?1:b[1]/100}:function(a){var b=a.retrieve("$opacity");b==null&&(b=a.style.visibility=="hidden"?0:1);return b},i=b.style.cssFloat==null?"styleFloat":"cssFloat";Element.implement({getComputedStyle:function(a){if(this.currentStyle)return this.currentStyle[a.camelCase()];var b=Element.getDocument(this).defaultView;return(b=b?b.getComputedStyle(this,null):null)?b.getPropertyValue(a==i?"float":a.hyphenate()):null},setStyle:function(a,b){if(a=="opacity"){b!=null&&(b=parseFloat(b));f(this,b);return this}a=(a=="float"?i:a).camelCase();if(typeOf(b)!="string")var d=(Element.Styles[a]||"@").split(" "),b=Array.from(b).map(function(a,b){return!d[b]?"":typeOf(a)=="number"?d[b].replace("@",Math.round(a)):a}).join(" ");else b==""+Number(b)&&(b=Math.round(b));this.style[a]=b;(b==""||b==null)&&c&&this.style.removeAttribute&&this.style.removeAttribute(a);return this},getStyle:function(a){if(a=="opacity")return g(this);var a=(a=="float"?i:a).camelCase(),b=this.style[a];if(!b||a=="zIndex"){var b=[],c;for(c in Element.ShortStyles)if(a==c){for(var d in Element.ShortStyles[c])b.push(this.getStyle(d));return b.join(" ")}b=this.getComputedStyle(a)}if(b){b=""+b;(c=b.match(/rgba?\([\d\s,]+\)/))&&(b=b.replace(c[0],c[0].rgbToHex()))}if(Browser.ie&&isNaN(parseFloat(b))){if(/^(height|width)$/.test(a)){var e=0;(a=="width"?["left","right"]:["top","bottom"]).each(function(a){e=e+(this.getStyle("border-"+a+"-width").toInt()+this.getStyle("padding-"+a).toInt())},this);return this["offset"+a.capitalize()]-e+"px"}if(Browser.opera&&(""+b).indexOf("px")!=-1)return b;if(/^border(.+)Width|margin|padding/.test(a))return"0px"}return b},setStyles:function(a){for(var b in a)this.setStyle(b,a[b]);return this},getStyles:function(){var a={};Array.flatten(arguments).each(function(b){a[b]=this.getStyle(b)},this);return a}});Element.Styles={left:"@px",top:"@px",bottom:"@px",right:"@px",width:"@px",height:"@px",maxWidth:"@px",maxHeight:"@px",minWidth:"@px",minHeight:"@px",backgroundColor:"rgb(@, @, @)",backgroundPosition:"@px @px",color:"rgb(@, @, @)",fontSize:"@px",letterSpacing:"@px",lineHeight:"@px",clip:"rect(@px @px @px @px)",margin:"@px @px @px @px",padding:"@px @px @px @px",border:"@px @ rgb(@, @, @) @px @ rgb(@, @, @) @px @ rgb(@, @, @)",borderWidth:"@px @px @px @px",borderStyle:"@ @ @ @",borderColor:"rgb(@, @, @) rgb(@, @, @) rgb(@, @, @) rgb(@, @, @)",zIndex:"@",zoom:"@",fontWeight:"@",textIndent:"@px",opacity:"@"};Element.ShortStyles={margin:{},padding:{},border:{},borderWidth:{},borderStyle:{},borderColor:{}};["Top","Right","Bottom","Left"].each(function(a){var b=Element.ShortStyles,c=Element.Styles;["margin","padding"].each(function(d){var e=d+a;b[d][e]=c[e]="@px"});var d="border"+a;b.border[d]=c[d]="@px @ rgb(@, @, @)";var e=d+"Width",f=d+"Style",g=d+"Color";b[d]={};b.borderWidth[e]=b[d][e]=c[e]="@px";b.borderStyle[f]=b[d][f]=c[f]="@";b.borderColor[g]=b[d][g]=c[g]="rgb(@, @, @)"})})();(function(){Element.Properties.events={set:function(a){this.addEvents(a)}};[Element,Window,Document].invoke("implement",{addEvent:function(a,b,d){var e=this.retrieve("events",{});e[a]||(e[a]={keys:[],values:[]});if(e[a].keys.contains(b))return this;e[a].keys.push(b);var f=a,g=Element.Events[a],i=b,j=this;if(g){g.onAdd&&g.onAdd.call(this,b,a);g.condition&&(i=function(d){return g.condition.call(this,d,a)?b.call(this,d):true});g.base&&(f=Function.from(g.base).call(this,a))}var m=function(){return b.call(j)},h=Element.NativeEvents[f];if(h){h==2&&(m=function(a){a=new DOMEvent(a,j.getWindow());i.call(j,a)===false&&a.stop()});this.addListener(f,m,d)}e[a].values.push(m);return this},removeEvent:function(a,b,d){var e=this.retrieve("events");if(!e||!e[a])return this;var f=e[a],g=f.keys.indexOf(b);if(g==-1)return this;e=f.values[g];delete f.keys[g];delete f.values[g];if(f=Element.Events[a]){f.onRemove&&f.onRemove.call(this,b,a);f.base&&(a=Function.from(f.base).call(this,a))}return Element.NativeEvents[a]?this.removeListener(a,e,d):this},addEvents:function(a){for(var b in a)this.addEvent(b,a[b]);return this},removeEvents:function(a){var b;if(typeOf(a)=="object"){for(b in a)this.removeEvent(b,a[b]);return this}var d=this.retrieve("events");if(!d)return this;if(a){if(d[a]){d[a].keys.each(function(b){this.removeEvent(a,b)},this);delete d[a]}}else{for(b in d)this.removeEvents(b);this.eliminate("events")}return this},fireEvent:function(a,b,d){var e=this.retrieve("events");if(!e||!e[a])return this;b=Array.from(b);e[a].keys.each(function(a){d?a.delay(d,this,b):a.apply(this,b)},this);return this},cloneEvents:function(a,b){var a=document.id(a),d=a.retrieve("events");if(!d)return this;if(b)d[b]&&d[b].keys.each(function(a){this.addEvent(b,a)},this);else for(var e in d)this.cloneEvents(a,e);return this}});Element.NativeEvents={click:2,dblclick:2,mouseup:2,mousedown:2,contextmenu:2,mousewheel:2,DOMMouseScroll:2,mouseover:2,mouseout:2,mousemove:2,selectstart:2,selectend:2,keydown:2,keypress:2,keyup:2,orientationchange:2,touchstart:2,touchmove:2,touchend:2,touchcancel:2,gesturestart:2,gesturechange:2,gestureend:2,focus:2,blur:2,change:2,reset:2,select:2,submit:2,paste:2,input:2,load:2,unload:1,beforeunload:2,resize:1,move:1,DOMContentLoaded:1,readystatechange:1,error:1,abort:1,scroll:1};Element.Events={mousewheel:{base:Browser.firefox?"DOMMouseScroll":"mousewheel"}};if("onmouseenter"in document.documentElement)Element.NativeEvents.mouseenter=Element.NativeEvents.mouseleave=2;else{var b=function(a){a=a.relatedTarget;return a==null?true:!a?false:a!=this&&a.prefix!="xul"&&typeOf(this)!="document"&&!this.contains(a)};Element.Events.mouseenter={base:"mouseover",condition:b};Element.Events.mouseleave={base:"mouseout",condition:b}}if(!window.addEventListener){Element.NativeEvents.propertychange=2;Element.Events.change={base:function(){var a=this.type;return this.get("tag")=="input"&&(a=="radio"||a=="checkbox")?"propertychange":"change"},condition:function(a){return this.type!="radio"||a.event.propertyName=="checked"&&this.checked}}}})();(function(){var b,a=!!window.addEventListener;Element.NativeEvents.focusin=Element.NativeEvents.focusout=2;var c=function(a,b,c,d,e){for(;e&&e!=a;){if(b(e,d))return c.call(e,d,e);e=document.id(e.parentNode)}},d={mouseenter:{base:"mouseover"},mouseleave:{base:"mouseout"},focus:{base:"focus"+(a?"":"in"),capture:true},blur:{base:a?"blur":"focusout",capture:true}},e=function(a){return{base:"focusin",remove:function(b,c){var d=b.retrieve("$delegation:"+a+"listeners",{})[c];if(d&&d.forms)for(var e=d.forms.length;e--;)d.forms[e].removeEvent(a,d.fns[e])},listen:function(b,d,e,f,g,i){if(f=g.get("tag")=="form"?g:f.target.getParent("form")){var r=b.retrieve("$delegation:"+a+"listeners",{}),w=r[i]||{forms:[],fns:[]},l=w.forms,s=w.fns;if(l.indexOf(f)==-1){l.push(f);l=function(a){c(b,d,e,a,g)};f.addEvent(a,l);s.push(l);r[i]=w;b.store("$delegation:"+a+"listeners",r)}}}}},f=function(a){return{base:"focusin",listen:function(b,d,e,f,g){var i={blur:function(){this.removeEvents(i)}};i[a]=function(a){c(b,d,e,a,g)};f.target.addEvents(i)}}};a||Object.append(d,{submit:e("submit"),reset:e("reset"),change:f("change"),select:f("select")});var a=Element.prototype,g=a.addEvent,i=a.removeEvent,a=function(a,b){return function(c,d,e){if(c.indexOf(":relay")==-1)return a.call(this,c,d,e);var f=Slick.parse(c).expressions[0][0];if(f.pseudos[0].key!="relay")return a.call(this,c,d,e);var g=f.tag;f.pseudos.slice(1).each(function(a){g=g+(":"+a.key+(a.value?"("+a.value+")":""))});a.call(this,c,d);return b.call(this,g,f.pseudos[0].value,d)}};b=function(a,c,e,f){var g=this.retrieve("$delegates",{}),q=g[a];if(!q)return this;if(f){var c=a,e=q[f].delegator,u=d[a]||{},a=u.base||c;u.remove&&u.remove(this,f);delete q[f];g[c]=q;return i.call(this,a,e)}if(e)for(u in q){f=q[u];if(f.match==c&&f.fn==e)return b.call(this,a,c,e,u)}else for(u in q){f=q[u];f.match==c&&b.call(this,a,c,f.fn,u)}return this};[Element,Window,Document].invoke("implement",{addEvent:a(g,function(a,b,e){var f=this.retrieve("$delegates",{}),i=f[a];if(i)for(var q in i)if(i[q].fn==e&&i[q].match==b)return this;q=a;var u=b,r=d[a]||{},a=r.base||q,b=function(a){return Slick.match(a,u)},w=Element.Events[q];if(w&&w.condition)var l=b,s=w.condition,b=function(b,c){return l(b,c)&&s.call(b,c,a)};var p=this,n=String.uniqueID(),w=r.listen?function(a,c){if(!c&&a&&a.target)c=a.target;c&&r.listen(p,b,e,a,c,n)}:function(a,d){if(!d&&a&&a.target)d=a.target;d&&c(p,b,e,a,d)};i||(i={});i[n]={match:u,fn:e,delegator:w};f[q]=i;return g.call(this,a,w,r.capture)}),removeEvent:a(i,b)})})();(function(){function b(a){return h(a,"-moz-box-sizing")=="border-box"}function a(a){return h(a,"border-top-width").toInt()||0}function c(a){return h(a,"border-left-width").toInt()||0}function d(a){return/^(?:body|html)$/i.test(a.tagName)}function e(a){a=a.getDocument();return!a.compatMode||a.compatMode=="CSS1Compat"?a.html:a.body}var f=document.createElement("div"),g=document.createElement("div");f.style.height="0";f.appendChild(g);var i=g.offsetParent===f,f=g=null,j=function(a){return h(a,"position")!="static"||d(a)},m=function(a){return j(a)||/^(?:table|td|th)$/i.test(a.tagName)};Element.implement({scrollTo:function(a,b){if(d(this))this.getWindow().scrollTo(a,b);else{this.scrollLeft=a;this.scrollTop=b}return this},getSize:function(){return d(this)?this.getWindow().getSize():{x:this.offsetWidth,y:this.offsetHeight}},getScrollSize:function(){return d(this)?this.getWindow().getScrollSize():{x:this.scrollWidth,y:this.scrollHeight}},getScroll:function(){return d(this)?this.getWindow().getScroll():{x:this.scrollLeft,y:this.scrollTop}},getScrolls:function(){for(var a=this.parentNode,b={x:0,y:0};a&&!d(a);){b.x=b.x+a.scrollLeft;b.y=b.y+a.scrollTop;a=a.parentNode}return b},getOffsetParent:i?function(){var a=this;if(d(a)||h(a,"position")=="fixed")return null;for(var b=h(a,"position")=="static"?m:j;a=a.parentNode;)if(b(a))return a;return null}:function(){if(d(this)||h(this,"position")=="fixed")return null;try{return this.offsetParent}catch(a){}return null},getOffsets:function(){if(this.getBoundingClientRect&&!Browser.Platform.ios){var e=this.getBoundingClientRect(),f=document.id(this.getDocument().documentElement),g=f.getScroll(),i=this.getScrolls(),j=h(this,"position")=="fixed";return{x:e.left.toInt()+i.x+(j?0:g.x)-f.clientLeft,y:e.top.toInt()+i.y+(j?0:g.y)-f.clientTop}}e=this;f={x:0,y:0};if(d(this))return f;for(;e&&!d(e);){f.x=f.x+e.offsetLeft;f.y=f.y+e.offsetTop;if(Browser.firefox){if(!b(e)){f.x=f.x+c(e);f.y=f.y+a(e)}if((g=e.parentNode)&&h(g,"overflow")!="visible"){f.x=f.x+c(g);f.y=f.y+a(g)}}else if(e!=this&&Browser.safari){f.x=f.x+c(e);f.y=f.y+a(e)}e=e.offsetParent}if(Browser.firefox&&!b(this)){f.x=f.x-c(this);f.y=f.y-a(this)}return f},getPosition:function(b){var d=this.getOffsets(),e=this.getScrolls(),d={x:d.x-e.x,y:d.y-e.y};if(b&&(b=document.id(b))){e=b.getPosition();return{x:d.x-e.x-c(b),y:d.y-e.y-a(b)}}return d},getCoordinates:function(a){if(d(this))return this.getWindow().getCoordinates();var a=this.getPosition(a),b=this.getSize(),a={left:a.x,top:a.y,width:b.x,height:b.y};a.right=a.left+a.width;a.bottom=a.top+a.height;return a},computePosition:function(a){return{left:a.x-(h(this,"margin-left").toInt()||0),top:a.y-(h(this,"margin-top").toInt()||0)}},setPosition:function(a){return this.setStyles(this.computePosition(a))}});[Document,Window].invoke("implement",{getSize:function(){var a=e(this);return{x:a.clientWidth,y:a.clientHeight}},getScroll:function(){var a=this.getWindow(),b=e(this);return{x:a.pageXOffset||b.scrollLeft,y:a.pageYOffset||b.scrollTop}},getScrollSize:function(){var a=e(this),b=this.getSize(),c=this.getDocument().body;return{x:Math.max(a.scrollWidth,c.scrollWidth,b.x),y:Math.max(a.scrollHeight,c.scrollHeight,b.y)}},getPosition:function(){return{x:0,y:0}},getCoordinates:function(){var a=this.getSize();return{top:0,left:0,bottom:a.y,right:a.x,height:a.y,width:a.x}}});var h=Element.getComputedStyle})();Element.alias({position:"setPosition"});[Window,Document,Element].invoke("implement",{getHeight:function(){return this.getSize().y},getWidth:function(){return this.getSize().x},getScrollTop:function(){return this.getScroll().y},getScrollLeft:function(){return this.getScroll().x},getScrollHeight:function(){return this.getScrollSize().y},getScrollWidth:function(){return this.getScrollSize().x},getTop:function(){return this.getPosition().y},getLeft:function(){return this.getPosition().x}});(function(){var b=this.Fx=new Class({Implements:[Chain,Events,Options],options:{fps:60,unit:false,duration:500,frames:null,frameSkip:true,link:"ignore"},initialize:function(a){this.subject=this.subject||this;this.setOptions(a)},getTransition:function(){return function(a){return-(Math.cos(Math.PI*a)-1)/2}},step:function(a){if(this.options.frameSkip){var b=(this.time!=null?a-this.time:0)/this.frameInterval;this.time=a;this.frame=this.frame+b}else this.frame++;if(this.frame<this.frames)this.set(this.compute(this.from,this.to,this.transition(this.frame/this.frames)));else{this.frame=this.frames;this.set(this.compute(this.from,this.to,1));this.stop()}},set:function(a){return a},compute:function(a,c,d){return b.compute(a,c,d)},check:function(){if(!this.isRunning())return true;switch(this.options.link){case"cancel":this.cancel();return true;case"chain":this.chain(this.caller.pass(arguments,this))}return false},start:function(a,c){if(!this.check(a,c))return this;this.from=a;this.to=c;this.frame=this.options.frameSkip?0:-1;this.time=null;this.transition=this.getTransition();var d=this.options.frames,f=this.options.fps,h=this.options.duration;this.duration=b.Durations[h]||h.toInt();this.frameInterval=1E3/f;this.frames=d||Math.round(this.duration/this.frameInterval);this.fireEvent("start",this.subject);e.call(this,f);return this},stop:function(){if(this.isRunning()){this.time=null;f.call(this,this.options.fps);if(this.frames==this.frame){this.fireEvent("complete",this.subject);this.callChain()||this.fireEvent("chainComplete",this.subject)}else this.fireEvent("stop",this.subject)}return this},cancel:function(){if(this.isRunning()){this.time=null;f.call(this,this.options.fps);this.frame=this.frames;this.fireEvent("cancel",this.subject).clearChain()}return this},pause:function(){if(this.isRunning()){this.time=null;f.call(this,this.options.fps)}return this},resume:function(){this.frame<this.frames&&!this.isRunning()&&e.call(this,this.options.fps);return this},isRunning:function(){var b=a[this.options.fps];return b&&b.contains(this)}});b.compute=function(a,b,c){return(b-a)*c+a};b.Durations={"short":250,normal:500,"long":1E3};var a={},c={},d=function(){for(var a=Date.now(),b=this.length;b--;){var c=this[b];c&&c.step(a)}},e=function(b){var e=a[b]||(a[b]=[]);e.push(this);c[b]||(c[b]=d.periodical(Math.round(1E3/b),e))},f=function(b){var d=a[b];if(d){d.erase(this);if(!d.length&&c[b]){delete a[b];c[b]=clearInterval(c[b])}}}})();Fx.CSS=new Class({Extends:Fx,prepare:function(b,a,c){var c=Array.from(c),d=c[0],c=c[1];if(c==null){var c=d,d=b.getStyle(a),e=this.options.unit;if(e&&d.slice(-e.length)!=e&&parseFloat(d)!=0){b.setStyle(a,c+e);var f=b.getComputedStyle(a);if(!/px$/.test(f)){f=b.style[("pixel-"+a).camelCase()];if(f==null){var g=b.style.left;b.style.left=c+e;f=b.style.pixelLeft;b.style.left=g}}d=(c||1)/(parseFloat(f)||1)*(parseFloat(d)||0);b.setStyle(a,d+e)}}return{from:this.parse(d),to:this.parse(c)}},parse:function(b){b=Function.from(b)();b=typeof b=="string"?b.split(" "):Array.from(b);return b.map(function(a){var a=""+a,b=false;Object.each(Fx.CSS.Parsers,function(d){if(!b){var e=d.parse(a);if(e||e===0)b={value:e,parser:d}}});return b=b||{value:a,parser:Fx.CSS.Parsers.String}})},compute:function(b,a,c){var d=[];Math.min(b.length,a.length).times(function(e){d.push({value:b[e].parser.compute(b[e].value,a[e].value,c),parser:b[e].parser})});d.$family=Function.from("fx:css:value");return d},serve:function(b,a){typeOf(b)!="fx:css:value"&&(b=this.parse(b));var c=[];b.each(function(b){c=c.concat(b.parser.serve(b.value,a))});return c},render:function(b,a,c,d){b.setStyle(a,this.serve(c,d))},search:function(b){if(Fx.CSS.Cache[b])return Fx.CSS.Cache[b];var a={},c=RegExp("^"+b.escapeRegExp()+"$");Array.each(document.styleSheets,function(b){var e=b.href;if(!e||!e.contains("://")||e.contains(document.domain))Array.each(b.rules||b.cssRules,function(b){if(b.style){var d=b.selectorText?b.selectorText.replace(/^\w+/,function(a){return a.toLowerCase()}):null;d&&c.test(d)&&Object.each(Element.Styles,function(c,d){if(b.style[d]&&!Element.ShortStyles[d]){c=""+b.style[d];a[d]=/^rgb/.test(c)?c.rgbToHex():c}})}})});return Fx.CSS.Cache[b]=a}});Fx.CSS.Cache={};Fx.CSS.Parsers={Color:{parse:function(b){return b.match(/^#[0-9a-f]{3,6}$/i)?b.hexToRgb(true):(b=b.match(/(\d+),\s*(\d+),\s*(\d+)/))?[b[1],b[2],b[3]]:false},compute:function(b,a,c){return b.map(function(d,e){return Math.round(Fx.compute(b[e],a[e],c))})},serve:function(b){return b.map(Number)}},Number:{parse:parseFloat,compute:Fx.compute,serve:function(b,a){return a?b+a:b}},String:{parse:Function.from(!1),compute:function(b,a){return a},serve:function(b){return b}}};Fx.Tween=new Class({Extends:Fx.CSS,initialize:function(b,a){this.element=this.subject=document.id(b);this.parent(a)},set:function(b,a){if(arguments.length==1){a=b;b=this.property||this.options.property}this.render(this.element,b,a,this.options.unit);return this},start:function(b,a,c){if(!this.check(b,a,c))return this;var d=Array.flatten(arguments);this.property=this.options.property||d.shift();d=this.prepare(this.element,this.property,d);return this.parent(d.from,d.to)}});Element.Properties.tween={set:function(b){this.get("tween").cancel().setOptions(b);return this},get:function(){var b=this.retrieve("tween");if(!b){b=new Fx.Tween(this,{link:"cancel"});this.store("tween",b)}return b}};Element.implement({tween:function(b,a,c){this.get("tween").start(b,a,c);return this},fade:function(b){var a=this.get("tween"),c,d=["opacity"].append(arguments),e;d[1]==null&&(d[1]="toggle");switch(d[1]){case"in":c="start";d[1]=1;break;case"out":c="start";d[1]=0;break;case"show":c="set";d[1]=1;break;case"hide":c="set";d[1]=0;break;case"toggle":e=this.retrieve("fade:flag",this.getStyle("opacity")==1);c="start";d[1]=e?0:1;this.store("fade:flag",!e);e=true;break;default:c="start"}e||this.eliminate("fade:flag");a[c].apply(a,d);d=d[d.length-1];c=="set"||d!=0?this.setStyle("visibility",d==0?"hidden":"visible"):a.chain(function(){this.element.setStyle("visibility","hidden");this.callChain()});return this},highlight:function(b,a){if(!a){a=this.retrieve("highlight:original",this.getStyle("background-color"));a=a=="transparent"?"#fff":a}var c=this.get("tween");c.start("background-color",b||"#ffff88",a).chain(function(){this.setStyle("background-color",this.retrieve("highlight:original"));c.callChain()}.bind(this));return this}});Fx.Morph=new Class({Extends:Fx.CSS,initialize:function(b,a){this.element=this.subject=document.id(b);this.parent(a)},set:function(b){typeof b=="string"&&(b=this.search(b));for(var a in b)this.render(this.element,a,b[a],this.options.unit);return this},compute:function(b,a,c){var d={},e;for(e in b)d[e]=this.parent(b[e],a[e],c);return d},start:function(b){if(!this.check(b))return this;typeof b=="string"&&(b=this.search(b));var a={},c={},d;for(d in b){var e=this.prepare(this.element,d,b[d]);a[d]=e.from;c[d]=e.to}return this.parent(a,c)}});Element.Properties.morph={set:function(b){this.get("morph").cancel().setOptions(b);return this},get:function(){var b=this.retrieve("morph");if(!b){b=new Fx.Morph(this,{link:"cancel"});this.store("morph",b)}return b}};Element.implement({morph:function(b){this.get("morph").start(b);return this}});Fx.implement({getTransition:function(){var b=this.options.transition||Fx.Transitions.Sine.easeInOut;if(typeof b=="string"){var a=b.split(":"),b=Fx.Transitions,b=b[a[0]]||b[a[0].capitalize()];a[1]&&(b=b["ease"+a[1].capitalize()+(a[2]?a[2].capitalize():"")])}return b}});Fx.Transition=function(b,a){var a=Array.from(a),c=function(c){return b(c,a)};return Object.append(c,{easeIn:c,easeOut:function(c){return 1-b(1-c,a)},easeInOut:function(c){return(c<=0.5?b(2*c,a):2-b(2*(1-c),a))/2}})};Fx.Transitions={linear:function(b){return b}};Fx.Transitions.extend=function(b){for(var a in b)Fx.Transitions[a]=new Fx.Transition(b[a])};Fx.Transitions.extend({Pow:function(b,a){return Math.pow(b,a&&a[0]||6)},Expo:function(b){return Math.pow(2,8*(b-1))},Circ:function(b){return 1-Math.sin(Math.acos(b))},Sine:function(b){return 1-Math.cos(b*Math.PI/2)},Back:function(b,a){a=a&&a[0]||1.618;return Math.pow(b,2)*((a+1)*b-a)},Bounce:function(b){for(var a,c=0,d=1;;c=c+d,d=d/2)if(b>=(7-4*c)/11){a=d*d-Math.pow((11-6*c-11*b)/4,2);break}return a},Elastic:function(b,a){return Math.pow(2,10*--b)*Math.cos(20*b*Math.PI*(a&&a[0]||1)/3)}});["Quad","Cubic","Quart","Quint"].each(function(b,a){Fx.Transitions[b]=new Fx.Transition(function(b){return Math.pow(b,a+2)})});(function(){var b=function(){},a="onprogress"in new Browser.Request,c=this.Request=new Class({Implements:[Chain,Events,Options],options:{url:"",data:"",headers:{"X-Requested-With":"XMLHttpRequest",Accept:"text/javascript, text/html, application/xml, text/xml, */*"},async:true,format:false,method:"post",link:"ignore",isSuccess:null,emulation:true,urlEncoded:true,encoding:"utf-8",evalScripts:false,evalResponse:false,timeout:0,noCache:false},initialize:function(a){this.xhr=new Browser.Request;this.setOptions(a);this.headers=this.options.headers},onStateChange:function(){var c=this.xhr;if(c.readyState==4&&this.running){this.running=false;this.status=0;Function.attempt(function(){var a=c.status;this.status=a==1223?204:a}.bind(this));c.onreadystatechange=b;if(a)c.onprogress=c.onloadstart=b;clearTimeout(this.timer);this.response={text:this.xhr.responseText||"",xml:this.xhr.responseXML};this.options.isSuccess.call(this,this.status)?this.success(this.response.text,this.response.xml):this.failure()}},isSuccess:function(){var a=this.status;return a>=200&&a<300},isRunning:function(){return!!this.running},processScripts:function(a){return this.options.evalResponse||/(ecma|java)script/.test(this.getHeader("Content-type"))?Browser.exec(a):a.stripScripts(this.options.evalScripts)},success:function(a,b){this.onSuccess(this.processScripts(a),b)},onSuccess:function(){this.fireEvent("complete",arguments).fireEvent("success",arguments).callChain()},failure:function(){this.onFailure()},onFailure:function(){this.fireEvent("complete").fireEvent("failure",this.xhr)},loadstart:function(a){this.fireEvent("loadstart",[a,this.xhr])},progress:function(a){this.fireEvent("progress",[a,this.xhr])},timeout:function(){this.fireEvent("timeout",this.xhr)},setHeader:function(a,b){this.headers[a]=b;return this},getHeader:function(a){return Function.attempt(function(){return this.xhr.getResponseHeader(a)}.bind(this))},check:function(){if(!this.running)return true;switch(this.options.link){case"cancel":this.cancel();return true;case"chain":this.chain(this.caller.pass(arguments,this))}return false},send:function(b){if(!this.check(b))return this;this.options.isSuccess=this.options.isSuccess||this.isSuccess;this.running=true;var c=typeOf(b);if(c=="string"||c=="element")b={data:b};var c=this.options,b=Object.append({data:c.data,url:c.url,method:c.method},b),c=b.data,d=""+b.url,b=b.method.toLowerCase();switch(typeOf(c)){case"element":c=document.id(c).toQueryString();break;case"object":case"hash":c=Object.toQueryString(c)}if(this.options.format)var i="format="+this.options.format,c=c?i+"&"+c:i;if(this.options.emulation&&!["get","post"].contains(b)){b="_method="+b;c=c?b+"&"+c:b;b="post"}this.options.urlEncoded&&["post","put"].contains(b)&&(this.headers["Content-type"]="application/x-www-form-urlencoded"+(this.options.encoding?"; charset="+this.options.encoding:""));if(!d)d=document.location.pathname;i=d.lastIndexOf("/");if(i>-1&&(i=d.indexOf("#"))>-1)d=d.substr(0,i);this.options.noCache&&(d=d+((d.contains("?")?"&":"?")+String.uniqueID()));if(c&&b=="get"){d=d+((d.contains("?")?"&":"?")+c);c=null}var j=this.xhr;if(a){j.onloadstart=this.loadstart.bind(this);j.onprogress=this.progress.bind(this)}j.open(b.toUpperCase(),d,this.options.async,this.options.user,this.options.password);if(this.options.user&&"withCredentials"in j)j.withCredentials=true;j.onreadystatechange=this.onStateChange.bind(this);Object.each(this.headers,function(a,b){try{j.setRequestHeader(b,a)}catch(c){this.fireEvent("exception",[b,a])}},this);this.fireEvent("request");j.send(c);if(this.options.async){if(this.options.timeout)this.timer=this.timeout.delay(this.options.timeout,this)}else this.onStateChange();return this},cancel:function(){if(!this.running)return this;this.running=false;var c=this.xhr;c.abort();clearTimeout(this.timer);c.onreadystatechange=b;if(a)c.onprogress=c.onloadstart=b;this.xhr=new Browser.Request;this.fireEvent("cancel");return this}}),d={};["get","post","put","delete","GET","POST","PUT","DELETE"].each(function(a){d[a]=function(b){var c={method:a};if(b!=null)c.data=b;return this.send(c)}});c.implement(d);Element.Properties.send={set:function(a){this.get("send").cancel().setOptions(a);return this},get:function(){var a=this.retrieve("send");if(!a){a=new c({data:this,link:"cancel",method:this.get("method")||"post",url:this.get("action")});this.store("send",a)}return a}};Element.implement({send:function(a){var b=this.get("send");b.send({data:this,url:a||b.options.url});return this}})})();Request.HTML=new Class({Extends:Request,options:{update:!1,append:!1,evalScripts:!0,filter:!1,headers:{Accept:"text/html, application/xml, text/xml, */*"}},success:function(b){var a=this.options,c=this.response;c.html=b.stripScripts(function(a){c.javascript=a});if(b=c.html.match(/<body[^>]*>([\s\S]*?)<\/body>/i))c.html=b[1];b=(new Element("div")).set("html",c.html);c.tree=b.childNodes;c.elements=b.getElements(a.filter||"*");if(a.filter)c.tree=c.elements;if(a.update){b=document.id(a.update).empty();a.filter?b.adopt(c.elements):b.set("html",c.html)}else if(a.append){var d=document.id(a.append);a.filter?c.elements.reverse().inject(d):d.adopt(b.getChildren())}a.evalScripts&&Browser.exec(c.javascript);this.onSuccess(c.tree,c.elements,c.html,c.javascript)}});Element.Properties.load={set:function(b){this.get("load").cancel().setOptions(b);return this},get:function(){var b=this.retrieve("load");if(!b){b=new Request.HTML({data:this,link:"cancel",update:this,method:"get"});this.store("load",b)}return b}};Element.implement({load:function(){this.get("load").send(Array.link(arguments,{data:Type.isObject,url:Type.isString}));return this}});"undefined"==typeof JSON&&(this.JSON={});(function(){var b={"\u0008":"\\b","\t":"\\t","\n":"\\n","\u000c":"\\f","\r":"\\r",'"':'\\"',"\\":"\\\\"},a=function(a){return b[a]||"\\u"+("0000"+a.charCodeAt(0).toString(16)).slice(-4)};JSON.validate=function(a){a=a.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,"@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,"]").replace(/(?:^|:|,)(?:\s*\[)+/g,"");return/^[\],:{}\s]*$/.test(a)};JSON.encode=JSON.stringify?function(a){return JSON.stringify(a)}:function(b){b&&b.toJSON&&(b=b.toJSON());switch(typeOf(b)){case"string":return'"'+b.replace(/[\x00-\x1f\\"]/g,a)+'"';case"array":return"["+b.map(JSON.encode).clean()+"]";case"object":case"hash":var d=[];Object.each(b,function(a,b){var c=JSON.encode(a);c&&d.push(JSON.encode(b)+":"+c)});return"{"+d+"}";case"number":case"boolean":return""+b;case"null":return"null"}return null};JSON.decode=function(a,b){if(!a||typeOf(a)!="string")return null;if(b||JSON.secure){if(JSON.parse)return JSON.parse(a);if(!JSON.validate(a))throw Error("JSON could not decode the input; security is enabled and the value is not secure.");}return eval("("+a+")")}})();Request.JSON=new Class({Extends:Request,options:{secure:!0},initialize:function(b){this.parent(b);Object.append(this.headers,{Accept:"application/json","X-Request":"JSON"})},success:function(b){var a;try{a=this.response.json=JSON.decode(b,this.options.secure)}catch(c){this.fireEvent("error",[b,c]);return}if(a==null)this.onFailure();else this.onSuccess(a,b)}});var Cookie=new Class({Implements:Options,options:{path:"/",domain:!1,duration:!1,secure:!1,document:document,encode:!0},initialize:function(b,a){this.key=b;this.setOptions(a)},write:function(b){this.options.encode&&(b=encodeURIComponent(b));this.options.domain&&(b=b+("; domain="+this.options.domain));this.options.path&&(b=b+("; path="+this.options.path));if(this.options.duration){var a=new Date;a.setTime(a.getTime()+this.options.duration*864E5);b=b+("; expires="+a.toGMTString())}this.options.secure&&(b=b+"; secure");this.options.document.cookie=this.key+"="+b;return this},read:function(){var b=this.options.document.cookie.match("(?:^|;)\\s*"+this.key.escapeRegExp()+"=([^;]*)");return b?decodeURIComponent(b[1]):null},dispose:function(){(new Cookie(this.key,Object.merge({},this.options,{duration:-1}))).write("");return this}});Cookie.write=function(b,a,c){return(new Cookie(b,c)).write(a)};Cookie.read=function(b){return(new Cookie(b)).read()};Cookie.dispose=function(b,a){return(new Cookie(b,a)).dispose()};(function(b,a){var c,d,e=[],f,g,i=a.createElement("div"),j=function(){clearTimeout(g);if(!c){Browser.loaded=c=true;a.removeListener("DOMContentLoaded",j).removeListener("readystatechange",m);a.fireEvent("domready");b.fireEvent("domready")}},m=function(){for(var a=e.length;a--;)if(e[a]()){j();return true}return false},h=function(){clearTimeout(g);m()||(g=setTimeout(h,10))};a.addListener("DOMContentLoaded",j);var k=function(){try{i.doScroll();return true}catch(a){}return false};if(i.doScroll&&!k()){e.push(k);f=true}a.readyState&&e.push(function(){var b=a.readyState;return b=="loaded"||b=="complete"});"onreadystatechange"in a?a.addListener("readystatechange",m):f=true;f&&h();Element.Events.domready={onAdd:function(a){c&&a.call(this)}};Element.Events.load={base:"load",onAdd:function(a){d&&this==b&&a.call(this)},condition:function(){if(this==b){j();delete Element.Events.load}return true}};b.addEvent("load",function(){d=true})})(window,document);(function(){var b=this.Swiff=new Class({Implements:Options,options:{id:null,height:1,width:1,container:null,properties:{},params:{quality:"high",allowScriptAccess:"always",wMode:"window",swLiveConnect:true},callBacks:{},vars:{}},toElement:function(){return this.object},initialize:function(a,c){this.instance="Swiff_"+String.uniqueID();this.setOptions(c);var c=this.options,d=this.id=c.id||this.instance,e=document.id(c.container);b.CallBacks[this.instance]={};var f=c.params,g=c.vars,i=c.callBacks,j=Object.append({height:c.height,width:c.width},c.properties),m=this,h;for(h in i){b.CallBacks[this.instance][h]=function(a){return function(){return a.apply(m.object,arguments)}}(i[h]);g[h]="Swiff.CallBacks."+this.instance+"."+h}f.flashVars=Object.toQueryString(g);if(Browser.ie){j.classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000";f.movie=a}else j.type="application/x-shockwave-flash";j.data=a;var d='<object id="'+d+'"',k;for(k in j)d=d+(" "+k+'="'+j[k]+'"');var d=d+">",o;for(o in f)f[o]&&(d=d+('<param name="'+
o+'" value="'+f[o]+'" />'));this.object=(e?e.empty():new Element("div")).set("html",d+"</object>").firstChild},replaces:function(a){a=document.id(a,true);a.parentNode.replaceChild(this.toElement(),a);return this},inject:function(a){document.id(a,true).appendChild(this.toElement());return this},remote:function(){return b.remote.apply(b,[this.toElement()].append(arguments))}});b.CallBacks={};b.remote=function(a,b){var d=a.CallFunction('<invoke name="'+b+'" returntype="javascript">'+__flash__argumentsToXML(arguments,2)+"</invoke>");return eval(d)}})();


/*===============================
/media/system/js/core.js
================================================================================*/;
Joomla=window.Joomla||{},Joomla.editors=Joomla.editors||{},Joomla.editors.instances=Joomla.editors.instances||{},function(e,t){"use strict";e.submitform=function(e,o,n){o||(o=t.getElementById("adminForm")),e&&(o.task.value=e),o.noValidate=!n,o.setAttribute("novalidate",!n);var r=t.createElement("input");r.style.display="none",r.type="submit",o.appendChild(r).click(),o.removeChild(r)},e.submitbutton=function(t){e.submitform(t)},e.JText={strings:{},_:function(t,o){var n=e.getOptions("joomla.jtext");return n&&(this.load(n),e.loadOptions({"joomla.jtext":null})),o=void 0===o?"":o,t=t.toUpperCase(),void 0!==this.strings[t]?this.strings[t]:o},load:function(e){for(var t in e)e.hasOwnProperty(t)&&(this.strings[t.toUpperCase()]=e[t]);return this}},e.optionsStorage=e.optionsStorage||null,e.getOptions=function(t,o){return e.optionsStorage||e.loadOptions(),void 0!==e.optionsStorage[t]?e.optionsStorage[t]:o},e.loadOptions=function(o){if(o)if(e.optionsStorage)for(var n in o)o.hasOwnProperty(n)&&(e.optionsStorage[n]=o[n]);else e.optionsStorage=o;else for(var r,a,i,s=t.querySelectorAll(".joomla-script-options.new"),l=0,d=s.length;d>l;l++)a=s[l],r=a.text||a.textContent,i=JSON.parse(r),i?e.loadOptions(i):null,a.className=a.className.replace(" new"," loaded")},e.replaceTokens=function(e){if(/^[0-9A-F]{32}$/i.test(e)){var o,n,r,a=t.getElementsByTagName("input");for(o=0,r=a.length;r>o;o++)n=a[o],"hidden"==n.type&&"1"==n.value&&32==n.name.length&&(n.name=e)}},e.isEmail=function(e){var t=/^[\w.!#$%&‚Äô*+\/=?^`{|}~-]+@[a-z0-9-]+(?:\.[a-z0-9-]{2,})+$/i;return t.test(e)},e.checkAll=function(e,t){if(!e.form)return!1;t=t?t:"cb";var o,n,r,a=0;for(o=0,r=e.form.elements.length;r>o;o++)n=e.form.elements[o],n.type==e.type&&0===n.id.indexOf(t)&&(n.checked=e.checked,a+=n.checked?1:0);return e.form.boxchecked&&(e.form.boxchecked.value=a),!0},e.renderMessages=function(o){e.removeMessages();var n,r,a,i,s,l,d,c,u=t.getElementById("system-message-container");for(n in o)if(o.hasOwnProperty(n)){r=o[n],a=t.createElement("div"),c="notice"==n?"alert-info":"alert-"+n,c="message"==n?"alert-success":c,a.className="alert "+c;var m=t.createElement("button");for(m.setAttribute("type","button"),m.setAttribute("data-dismiss","alert"),m.className="close",m.innerHTML="×",a.appendChild(m),i=e.JText._(n),"undefined"!=typeof i&&(s=t.createElement("h4"),s.className="alert-heading",s.innerHTML=e.JText._(n),a.appendChild(s)),l=r.length-1;l>=0;l--)d=t.createElement("div"),d.innerHTML=r[l],a.appendChild(d);u.appendChild(a)}},e.removeMessages=function(){for(var e=t.getElementById("system-message-container");e.firstChild;)e.removeChild(e.firstChild);e.style.display="none",e.offsetHeight,e.style.display=""},e.ajaxErrorsMessages=function(t,o){var n={};if("parsererror"===o){for(var r=t.responseText.trim(),a=[],i=r.length-1;i>=0;i--)a.unshift(["&#",r[i].charCodeAt(),";"].join(""));r=a.join(""),n.error=[e.JText._("JLIB_JS_AJAX_ERROR_PARSE").replace("%s",r)]}else"nocontent"===o?n.error=[e.JText._("JLIB_JS_AJAX_ERROR_NO_CONTENT")]:"timeout"===o?n.error=[e.JText._("JLIB_JS_AJAX_ERROR_TIMEOUT")]:"abort"===o?n.error=[e.JText._("JLIB_JS_AJAX_ERROR_CONNECTION_ABORT")]:t.responseJSON&&t.responseJSON.message?n.error=[e.JText._("JLIB_JS_AJAX_ERROR_OTHER").replace("%s",t.status)+" <em>"+t.responseJSON.message+"</em>"]:t.statusText?n.error=[e.JText._("JLIB_JS_AJAX_ERROR_OTHER").replace("%s",t.status)+" <em>"+t.statusText+"</em>"]:n.error=[e.JText._("JLIB_JS_AJAX_ERROR_OTHER").replace("%s",t.status)];return n},e.isChecked=function(e,o){if("undefined"==typeof o&&(o=t.getElementById("adminForm")),o.boxchecked.value=e?parseInt(o.boxchecked.value)+1:parseInt(o.boxchecked.value)-1,o.elements["checkall-toggle"]){var n,r,a,i=!0;for(n=0,a=o.elements.length;a>n;n++)if(r=o.elements[n],"checkbox"==r.type&&"checkall-toggle"!=r.name&&!r.checked){i=!1;break}o.elements["checkall-toggle"].checked=i}},e.popupWindow=function(e,t,o,n,r){var a=(screen.width-o)/2,i=(screen.height-n)/2,s="height="+n+",width="+o+",top="+i+",left="+a+",scrollbars="+r+",resizable";window.open(e,t,s).window.focus()},e.tableOrdering=function(o,n,r,a){"undefined"==typeof a&&(a=t.getElementById("adminForm")),a.filter_order.value=o,a.filter_order_Dir.value=n,e.submitform(r,a)},window.writeDynaList=function(e,o,n,r,a,i){var s,l,d,c="<select "+e+">",u=n==r,m=0;for(l in o)o.hasOwnProperty(l)&&(d=o[l],d[0]==n&&(s="",(u&&a==d[1]||!u&&0===m)&&(s='selected="selected"'),c+='<option value="'+d[1]+'" '+s+">"+d[2]+"</option>",m++));c+="</select>",i?i.innerHTML=c:t.writeln(c)},window.changeDynaList=function(e,o,n,r,a){for(var i,s,l,d,c=t.adminForm[e],u=n==r;c.firstChild;)c.removeChild(c.firstChild);i=0;for(s in o)o.hasOwnProperty(s)&&(l=o[s],l[0]==n&&(d=new Option,d.value=l[1],d.text=l[2],(u&&a==d.value||!u&&0===i)&&(d.selected=!0),c.options[i++]=d));c.length=i},window.radioGetCheckedValue=function(e){if(!e)return"";var t,o=e.length;if(void 0===o)return e.checked?e.value:"";for(t=0;o>t;t++)if(e[t].checked)return e[t].value;return""},window.getSelectedValue=function(e,o){var n=t[e][o],r=n.selectedIndex;return null!==r&&r>-1?n.options[r].value:null},window.listItemTask=function(e,o){var n,r=t.adminForm,a=0,i=r[e];if(!i)return!1;for(;;){if(n=r["cb"+a],!n)break;n.checked=!1,a++}return i.checked=!0,r.boxchecked.value=1,window.submitform(o),!1},window.submitbutton=function(t){e.submitbutton(t)},window.submitform=function(t){e.submitform(t)},window.saveorder=function(e,t){window.checkAll_button(e,t)},window.checkAll_button=function(o,n){n=n?n:"saveorder";var r,a;for(r=0;o>=r;r++){if(a=t.adminForm["cb"+r],!a)return void alert("You cannot change the order of items, as an item in the list is `Checked Out`");a.checked=!0}e.submitform(n)},e.loadingLayer=function(o,n){if(o=o||"show",n=n||t.body,"load"==o){var r=t.getElementsByTagName("body")[0].getAttribute("data-basepath")||"",a=t.createElement("div");a.id="loading-logo",a.style.position="fixed",a.style.top="0",a.style.left="0",a.style.width="100%",a.style.height="100%",a.style.opacity="0.8",a.style.filter="alpha(opacity=80)",a.style.overflow="hidden",a.style["z-index"]="10000",a.style.display="none",a.style["background-color"]="#fff",a.style["background-image"]='url("'+r+'/media/jui/images/ajax-loader.gif")',a.style["background-position"]="center",a.style["background-repeat"]="no-repeat",a.style["background-attachment"]="fixed",n.appendChild(a)}else t.getElementById("loading-logo")||e.loadingLayer("load",n),t.getElementById("loading-logo").style.display="show"==o?"block":"none";return t.getElementById("loading-logo")},e.extend=function(e,t){for(var o in t)t.hasOwnProperty(o)&&(e[o]=t[o]);return e},e.request=function(t){t=e.extend({url:"",method:"GET",data:null,perform:!0},t),t.method=t.data?"POST":t.method;try{var o=window.XMLHttpRequest?new XMLHttpRequest:new ActiveXObject("MSXML2.XMLHTTP.3.0");if(o.open(t.method,t.url,!0),o.setRequestHeader("X-Requested-With","XMLHttpRequest"),o.setRequestHeader("X-Ajax-Engine","Joomla!"),"POST"!==t.method||t.headers&&t.headers["Content-Type"]||o.setRequestHeader("Content-Type","application/x-www-form-urlencoded"),t.headers)for(var n in t.headers)t.headers.hasOwnProperty(n)&&o.setRequestHeader(n,t.headers[n]);if(o.onreadystatechange=function(){4===o.readyState&&(200===o.status?t.onSuccess&&t.onSuccess.call(window,o.responseText,o):t.onError&&t.onError.call(window,o))},t.perform){if(t.onBefore&&t.onBefore.call(window,o)===!1)return o;o.send(t.data)}}catch(r){return window.console?console.log(r):null,!1}return o}}(Joomla,document);


/*===============================
/media/system/js/mootools-more.js
================================================================================*/;
MooTools.More={version:"1.4.0.1",build:"a4244edf2aa97ac8a196fc96082dd35af1abab87"};(function(){Events.Pseudos=function(h,e,f){var d="_monitorEvents:";var c=function(i){return{store:i.store?function(j,k){i.store(d+j,k);}:function(j,k){(i._monitorEvents||(i._monitorEvents={}))[j]=k;},retrieve:i.retrieve?function(j,k){return i.retrieve(d+j,k);}:function(j,k){if(!i._monitorEvents){return k;}return i._monitorEvents[j]||k;}};};var g=function(k){if(k.indexOf(":")==-1||!h){return null;}var j=Slick.parse(k).expressions[0][0],p=j.pseudos,i=p.length,o=[];while(i--){var n=p[i].key,m=h[n];if(m!=null){o.push({event:j.tag,value:p[i].value,pseudo:n,original:k,listener:m});}}return o.length?o:null;};return{addEvent:function(m,p,j){var n=g(m);if(!n){return e.call(this,m,p,j);}var k=c(this),r=k.retrieve(m,[]),i=n[0].event,l=Array.slice(arguments,2),o=p,q=this;n.each(function(s){var t=s.listener,u=o;if(t==false){i+=":"+s.pseudo+"("+s.value+")";}else{o=function(){t.call(q,s,u,arguments,o);};}});r.include({type:i,event:p,monitor:o});k.store(m,r);if(m!=i){e.apply(this,[m,p].concat(l));}return e.apply(this,[i,o].concat(l));},removeEvent:function(m,l){var k=g(m);if(!k){return f.call(this,m,l);}var n=c(this),j=n.retrieve(m);if(!j){return this;}var i=Array.slice(arguments,2);f.apply(this,[m,l].concat(i));j.each(function(o,p){if(!l||o.event==l){f.apply(this,[o.type,o.monitor].concat(i));}delete j[p];},this);n.store(m,j);return this;}};};var b={once:function(e,f,d,c){f.apply(this,d);this.removeEvent(e.event,c).removeEvent(e.original,f);},throttle:function(d,e,c){if(!e._throttled){e.apply(this,c);e._throttled=setTimeout(function(){e._throttled=false;},d.value||250);}},pause:function(d,e,c){clearTimeout(e._pause);e._pause=e.delay(d.value||250,this,c);}};Events.definePseudo=function(c,d){b[c]=d;return this;};Events.lookupPseudo=function(c){return b[c];};var a=Events.prototype;Events.implement(Events.Pseudos(b,a.addEvent,a.removeEvent));["Request","Fx"].each(function(c){if(this[c]){this[c].implement(Events.prototype);}});})();Class.refactor=function(b,a){Object.each(a,function(e,d){var c=b.prototype[d];c=(c&&c.$origin)||c||function(){};b.implement(d,(typeof e=="function")?function(){var f=this.previous;this.previous=c;var g=e.apply(this,arguments);this.previous=f;return g;}:e);});return b;};Class.Mutators.Binds=function(a){if(!this.prototype.initialize){this.implement("initialize",function(){});}return Array.from(a).concat(this.prototype.Binds||[]);};Class.Mutators.initialize=function(a){return function(){Array.from(this.Binds).each(function(b){var c=this[b];if(c){this[b]=c.bind(this);}},this);return a.apply(this,arguments);};};Class.Occlude=new Class({occlude:function(c,b){b=document.id(b||this.element);var a=b.retrieve(c||this.property);if(a&&!this.occluded){return(this.occluded=a);}this.occluded=false;b.store(c||this.property,this);return this.occluded;}});(function(){var a={wait:function(b){return this.chain(function(){this.callChain.delay(b==null?500:b,this);return this;}.bind(this));}};Chain.implement(a);if(this.Fx){Fx.implement(a);}if(this.Element&&Element.implement&&this.Fx){Element.implement({chains:function(b){Array.from(b||["tween","morph","reveal"]).each(function(c){c=this.get(c);if(!c){return;}c.setOptions({link:"chain"});},this);return this;},pauseFx:function(c,b){this.chains(b).get(b||"tween").wait(c);return this;}});}})();(function(a){Array.implement({min:function(){return Math.min.apply(null,this);},max:function(){return Math.max.apply(null,this);},average:function(){return this.length?this.sum()/this.length:0;},sum:function(){var b=0,c=this.length;if(c){while(c--){b+=this[c];}}return b;},unique:function(){return[].combine(this);},shuffle:function(){for(var c=this.length;c&&--c;){var b=this[c],d=Math.floor(Math.random()*(c+1));this[c]=this[d];this[d]=b;}return this;},reduce:function(d,e){for(var c=0,b=this.length;c<b;c++){if(c in this){e=e===a?this[c]:d.call(null,e,this[c],c,this);}}return e;},reduceRight:function(c,d){var b=this.length;while(b--){if(b in this){d=d===a?this[b]:c.call(null,d,this[b],b,this);}}return d;}});})();(function(){var b=function(c){return c!=null;};var a=Object.prototype.hasOwnProperty;Object.extend({getFromPath:function(e,f){if(typeof f=="string"){f=f.split(".");}for(var d=0,c=f.length;d<c;d++){if(a.call(e,f[d])){e=e[f[d]];}else{return null;}}return e;},cleanValues:function(c,e){e=e||b;for(var d in c){if(!e(c[d])){delete c[d];}}return c;},erase:function(c,d){if(a.call(c,d)){delete c[d];}return c;},run:function(d){var c=Array.slice(arguments,1);for(var e in d){if(d[e].apply){d[e].apply(d,c);}}return d;}});})();(function(){var b=null,a={},d={};var c=function(f){if(instanceOf(f,e.Set)){return f;}else{return a[f];}};var e=this.Locale={define:function(f,j,h,i){var g;if(instanceOf(f,e.Set)){g=f.name;if(g){a[g]=f;}}else{g=f;if(!a[g]){a[g]=new e.Set(g);}f=a[g];}if(j){f.define(j,h,i);}if(!b){b=f;}return f;},use:function(f){f=c(f);if(f){b=f;this.fireEvent("change",f);}return this;},getCurrent:function(){return b;},get:function(g,f){return(b)?b.get(g,f):"";},inherit:function(f,g,h){f=c(f);if(f){f.inherit(g,h);}return this;},list:function(){return Object.keys(a);}};Object.append(e,new Events);e.Set=new Class({sets:{},inherits:{locales:[],sets:{}},initialize:function(f){this.name=f||"";},define:function(i,g,h){var f=this.sets[i];if(!f){f={};}if(g){if(typeOf(g)=="object"){f=Object.merge(f,g);}else{f[g]=h;}}this.sets[i]=f;return this;},get:function(r,j,q){var p=Object.getFromPath(this.sets,r);if(p!=null){var m=typeOf(p);if(m=="function"){p=p.apply(null,Array.from(j));}else{if(m=="object"){p=Object.clone(p);}}return p;}var h=r.indexOf("."),o=h<0?r:r.substr(0,h),k=(this.inherits.sets[o]||[]).combine(this.inherits.locales).include("en-US");if(!q){q=[];}for(var g=0,f=k.length;g<f;g++){if(q.contains(k[g])){continue;}q.include(k[g]);var n=a[k[g]];if(!n){continue;}p=n.get(r,j,q);if(p!=null){return p;}}return"";},inherit:function(g,h){g=Array.from(g);if(h&&!this.inherits.sets[h]){this.inherits.sets[h]=[];}var f=g.length;while(f--){(h?this.inherits.sets[h]:this.inherits.locales).unshift(g[f]);}return this;}});})();Locale.define("en-US","Date",{months:["January","February","March","April","May","June","July","August","September","October","November","December"],months_abbr:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],days:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],days_abbr:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],dateOrder:["month","date","year"],shortDate:"%m/%d/%Y",shortTime:"%I:%M%p",AM:"AM",PM:"PM",firstDayOfWeek:0,ordinal:function(a){return(a>3&&a<21)?"th":["th","st","nd","rd","th"][Math.min(a%10,4)];},lessThanMinuteAgo:"less than a minute ago",minuteAgo:"about a minute ago",minutesAgo:"{delta} minutes ago",hourAgo:"about an hour ago",hoursAgo:"about {delta} hours ago",dayAgo:"1 day ago",daysAgo:"{delta} days ago",weekAgo:"1 week ago",weeksAgo:"{delta} weeks ago",monthAgo:"1 month ago",monthsAgo:"{delta} months ago",yearAgo:"1 year ago",yearsAgo:"{delta} years ago",lessThanMinuteUntil:"less than a minute from now",minuteUntil:"about a minute from now",minutesUntil:"{delta} minutes from now",hourUntil:"about an hour from now",hoursUntil:"about {delta} hours from now",dayUntil:"1 day from now",daysUntil:"{delta} days from now",weekUntil:"1 week from now",weeksUntil:"{delta} weeks from now",monthUntil:"1 month from now",monthsUntil:"{delta} months from now",yearUntil:"1 year from now",yearsUntil:"{delta} years from now"});(function(){var a=this.Date;var f=a.Methods={ms:"Milliseconds",year:"FullYear",min:"Minutes",mo:"Month",sec:"Seconds",hr:"Hours"};["Date","Day","FullYear","Hours","Milliseconds","Minutes","Month","Seconds","Time","TimezoneOffset","Week","Timezone","GMTOffset","DayOfYear","LastMonth","LastDayOfMonth","UTCDate","UTCDay","UTCFullYear","AMPM","Ordinal","UTCHours","UTCMilliseconds","UTCMinutes","UTCMonth","UTCSeconds","UTCMilliseconds"].each(function(s){a.Methods[s.toLowerCase()]=s;});var p=function(u,t,s){if(t==1){return u;}return u<Math.pow(10,t-1)?(s||"0")+p(u,t-1,s):u;};a.implement({set:function(u,s){u=u.toLowerCase();var t=f[u]&&"set"+f[u];if(t&&this[t]){this[t](s);}return this;}.overloadSetter(),get:function(t){t=t.toLowerCase();var s=f[t]&&"get"+f[t];if(s&&this[s]){return this[s]();}return null;}.overloadGetter(),clone:function(){return new a(this.get("time"));},increment:function(s,u){s=s||"day";u=u!=null?u:1;switch(s){case"year":return this.increment("month",u*12);case"month":var t=this.get("date");this.set("date",1).set("mo",this.get("mo")+u);return this.set("date",t.min(this.get("lastdayofmonth")));case"week":return this.increment("day",u*7);case"day":return this.set("date",this.get("date")+u);}if(!a.units[s]){throw new Error(s+" is not a supported interval");}return this.set("time",this.get("time")+u*a.units[s]());},decrement:function(s,t){return this.increment(s,-1*(t!=null?t:1));},isLeapYear:function(){return a.isLeapYear(this.get("year"));},clearTime:function(){return this.set({hr:0,min:0,sec:0,ms:0});},diff:function(t,s){if(typeOf(t)=="string"){t=a.parse(t);}return((t-this)/a.units[s||"day"](3,3)).round();},getLastDayOfMonth:function(){return a.daysInMonth(this.get("mo"),this.get("year"));},getDayOfYear:function(){return(a.UTC(this.get("year"),this.get("mo"),this.get("date")+1)-a.UTC(this.get("year"),0,1))/a.units.day();},setDay:function(t,s){if(s==null){s=a.getMsg("firstDayOfWeek");if(s===""){s=1;}}t=(7+a.parseDay(t,true)-s)%7;var u=(7+this.get("day")-s)%7;return this.increment("day",t-u);},getWeek:function(v){if(v==null){v=a.getMsg("firstDayOfWeek");if(v===""){v=1;}}var x=this,u=(7+x.get("day")-v)%7,t=0,w;if(v==1){var y=x.get("month"),s=x.get("date")-u;if(y==11&&s>28){return 1;}if(y==0&&s<-2){x=new a(x).decrement("day",u);u=0;}w=new a(x.get("year"),0,1).get("day")||7;if(w>4){t=-7;}}else{w=new a(x.get("year"),0,1).get("day");}t+=x.get("dayofyear");t+=6-u;t+=(7+w-v)%7;return(t/7);},getOrdinal:function(s){return a.getMsg("ordinal",s||this.get("date"));},getTimezone:function(){return this.toString().replace(/^.*? ([A-Z]{3}).[0-9]{4}.*$/,"$1").replace(/^.*?\(([A-Z])[a-z]+ ([A-Z])[a-z]+ ([A-Z])[a-z]+\)$/,"$1$2$3");},getGMTOffset:function(){var s=this.get("timezoneOffset");return((s>0)?"-":"+")+p((s.abs()/60).floor(),2)+p(s%60,2);},setAMPM:function(s){s=s.toUpperCase();var t=this.get("hr");if(t>11&&s=="AM"){return this.decrement("hour",12);}else{if(t<12&&s=="PM"){return this.increment("hour",12);}}return this;},getAMPM:function(){return(this.get("hr")<12)?"AM":"PM";},parse:function(s){this.set("time",a.parse(s));return this;},isValid:function(s){if(!s){s=this;}return typeOf(s)=="date"&&!isNaN(s.valueOf());},format:function(s){if(!this.isValid()){return"invalid date";}if(!s){s="%x %X";}if(typeof s=="string"){s=g[s.toLowerCase()]||s;}if(typeof s=="function"){return s(this);}var t=this;return s.replace(/%([a-z%])/gi,function(v,u){switch(u){case"a":return a.getMsg("days_abbr")[t.get("day")];case"A":return a.getMsg("days")[t.get("day")];case"b":return a.getMsg("months_abbr")[t.get("month")];case"B":return a.getMsg("months")[t.get("month")];case"c":return t.format("%a %b %d %H:%M:%S %Y");case"d":return p(t.get("date"),2);case"e":return p(t.get("date"),2," ");case"H":return p(t.get("hr"),2);case"I":return p((t.get("hr")%12)||12,2);case"j":return p(t.get("dayofyear"),3);case"k":return p(t.get("hr"),2," ");case"l":return p((t.get("hr")%12)||12,2," ");case"L":return p(t.get("ms"),3);case"m":return p((t.get("mo")+1),2);case"M":return p(t.get("min"),2);case"o":return t.get("ordinal");case"p":return a.getMsg(t.get("ampm"));case"s":return Math.round(t/1000);case"S":return p(t.get("seconds"),2);case"T":return t.format("%H:%M:%S");case"U":return p(t.get("week"),2);case"w":return t.get("day");case"x":return t.format(a.getMsg("shortDate"));case"X":return t.format(a.getMsg("shortTime"));case"y":return t.get("year").toString().substr(2);case"Y":return t.get("year");case"z":return t.get("GMTOffset");case"Z":return t.get("Timezone");}return u;});},toISOString:function(){return this.format("iso8601");}}).alias({toJSON:"toISOString",compare:"diff",strftime:"format"});var k=["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],h=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];var g={db:"%Y-%m-%d %H:%M:%S",compact:"%Y%m%dT%H%M%S","short":"%d %b %H:%M","long":"%B %d, %Y %H:%M",rfc822:function(s){return k[s.get("day")]+s.format(", %d ")+h[s.get("month")]+s.format(" %Y %H:%M:%S %Z");},rfc2822:function(s){return k[s.get("day")]+s.format(", %d ")+h[s.get("month")]+s.format(" %Y %H:%M:%S %z");},iso8601:function(s){return(s.getUTCFullYear()+"-"+p(s.getUTCMonth()+1,2)+"-"+p(s.getUTCDate(),2)+"T"+p(s.getUTCHours(),2)+":"+p(s.getUTCMinutes(),2)+":"+p(s.getUTCSeconds(),2)+"."+p(s.getUTCMilliseconds(),3)+"Z");}};var c=[],n=a.parse;var r=function(v,x,u){var t=-1,w=a.getMsg(v+"s");switch(typeOf(x)){case"object":t=w[x.get(v)];break;case"number":t=w[x];if(!t){throw new Error("Invalid "+v+" index: "+x);}break;case"string":var s=w.filter(function(y){return this.test(y);},new RegExp("^"+x,"i"));if(!s.length){throw new Error("Invalid "+v+" string");}if(s.length>1){throw new Error("Ambiguous "+v);}t=s[0];}return(u)?w.indexOf(t):t;};var i=1900,o=70;a.extend({getMsg:function(t,s){return Locale.get("Date."+t,s);},units:{ms:Function.from(1),second:Function.from(1000),minute:Function.from(60000),hour:Function.from(3600000),day:Function.from(86400000),week:Function.from(608400000),month:function(t,s){var u=new a;return a.daysInMonth(t!=null?t:u.get("mo"),s!=null?s:u.get("year"))*86400000;},year:function(s){s=s||new a().get("year");return a.isLeapYear(s)?31622400000:31536000000;}},daysInMonth:function(t,s){return[31,a.isLeapYear(s)?29:28,31,30,31,30,31,31,30,31,30,31][t];},isLeapYear:function(s){return((s%4===0)&&(s%100!==0))||(s%400===0);},parse:function(v){var u=typeOf(v);if(u=="number"){return new a(v);}if(u!="string"){return v;}v=v.clean();if(!v.length){return null;}var s;c.some(function(w){var t=w.re.exec(v);return(t)?(s=w.handler(t)):false;});if(!(s&&s.isValid())){s=new a(n(v));if(!(s&&s.isValid())){s=new a(v.toInt());}}return s;},parseDay:function(s,t){return r("day",s,t);},parseMonth:function(t,s){return r("month",t,s);},parseUTC:function(t){var s=new a(t);var u=a.UTC(s.get("year"),s.get("mo"),s.get("date"),s.get("hr"),s.get("min"),s.get("sec"),s.get("ms"));return new a(u);},orderIndex:function(s){return a.getMsg("dateOrder").indexOf(s)+1;},defineFormat:function(s,t){g[s]=t;return this;},defineParser:function(s){c.push((s.re&&s.handler)?s:l(s));return this;},defineParsers:function(){Array.flatten(arguments).each(a.defineParser);return this;},define2DigitYearStart:function(s){o=s%100;i=s-o;return this;}}).extend({defineFormats:a.defineFormat.overloadSetter()});var d=function(s){return new RegExp("(?:"+a.getMsg(s).map(function(t){return t.substr(0,3);}).join("|")+")[a-z]*");};var m=function(s){switch(s){case"T":return"%H:%M:%S";case"x":return((a.orderIndex("month")==1)?"%m[-./]%d":"%d[-./]%m")+"([-./]%y)?";case"X":return"%H([.:]%M)?([.:]%S([.:]%s)?)? ?%p? ?%z?";}return null;};var j={d:/[0-2]?[0-9]|3[01]/,H:/[01]?[0-9]|2[0-3]/,I:/0?[1-9]|1[0-2]/,M:/[0-5]?\d/,s:/\d+/,o:/[a-z]*/,p:/[ap]\.?m\.?/,y:/\d{2}|\d{4}/,Y:/\d{4}/,z:/Z|[+-]\d{2}(?::?\d{2})?/};j.m=j.I;j.S=j.M;var e;var b=function(s){e=s;j.a=j.A=d("days");j.b=j.B=d("months");c.each(function(u,t){if(u.format){c[t]=l(u.format);}});};var l=function(u){if(!e){return{format:u};}var s=[];var t=(u.source||u).replace(/%([a-z])/gi,function(w,v){return m(v)||w;}).replace(/\((?!\?)/g,"(?:").replace(/ (?!\?|\*)/g,",? ").replace(/%([a-z%])/gi,function(w,v){var x=j[v];if(!x){return v;}s.push(v);return"("+x.source+")";}).replace(/\[a-z\]/gi,"[a-z\\u00c0-\\uffff;&]");return{format:u,re:new RegExp("^"+t+"$","i"),handler:function(y){y=y.slice(1).associate(s);var v=new a().clearTime(),x=y.y||y.Y;if(x!=null){q.call(v,"y",x);}if("d"in y){q.call(v,"d",1);}if("m"in y||y.b||y.B){q.call(v,"m",1);}for(var w in y){q.call(v,w,y[w]);}return v;}};};var q=function(s,t){if(!t){return this;}switch(s){case"a":case"A":return this.set("day",a.parseDay(t,true));case"b":case"B":return this.set("mo",a.parseMonth(t,true));case"d":return this.set("date",t);case"H":case"I":return this.set("hr",t);case"m":return this.set("mo",t-1);case"M":return this.set("min",t);case"p":return this.set("ampm",t.replace(/\./g,""));case"S":return this.set("sec",t);case"s":return this.set("ms",("0."+t)*1000);case"w":return this.set("day",t);case"Y":return this.set("year",t);case"y":t=+t;if(t<100){t+=i+(t<o?100:0);}return this.set("year",t);case"z":if(t=="Z"){t="+00";}var u=t.match(/([+-])(\d{2}):?(\d{2})?/);u=(u[1]+"1")*(u[2]*60+(+u[3]||0))+this.getTimezoneOffset();return this.set("time",this-u*60000);}return this;};a.defineParsers("%Y([-./]%m([-./]%d((T| )%X)?)?)?","%Y%m%d(T%H(%M%S?)?)?","%x( %X)?","%d%o( %b( %Y)?)?( %X)?","%b( %d%o)?( %Y)?( %X)?","%Y %b( %d%o( %X)?)?","%o %b %d %X %z %Y","%T","%H:%M( ?%p)?");Locale.addEvent("change",function(s){if(Locale.get("Date")){b(s);}}).fireEvent("change",Locale.getCurrent());})();Date.implement({timeDiffInWords:function(a){return Date.distanceOfTimeInWords(this,a||new Date);},timeDiff:function(f,c){if(f==null){f=new Date;}var h=((f-this)/1000).floor().abs();var e=[],a=[60,60,24,365,0],d=["s","m","h","d","y"],g,b;for(var i=0;i<a.length;i++){if(i&&!h){break;}g=h;if((b=a[i])){g=(h%b);h=(h/b).floor();}e.unshift(g+(d[i]||""));}return e.join(c||":");}}).extend({distanceOfTimeInWords:function(b,a){return Date.getTimePhrase(((a-b)/1000).toInt());},getTimePhrase:function(f){var d=(f<0)?"Until":"Ago";if(f<0){f*=-1;}var b={minute:60,hour:60,day:24,week:7,month:52/12,year:12,eon:Infinity};var e="lessThanMinute";for(var c in b){var a=b[c];if(f<1.5*a){if(f>0.75*a){e=c;}break;}f/=a;e=c+"s";}f=f.round();return Date.getMsg(e+d,f).substitute({delta:f});}}).defineParsers({re:/^(?:tod|tom|yes)/i,handler:function(a){var b=new Date().clearTime();switch(a[0]){case"tom":return b.increment();case"yes":return b.decrement();default:return b;}}},{re:/^(next|last) ([a-z]+)$/i,handler:function(e){var f=new Date().clearTime();var b=f.getDay();var c=Date.parseDay(e[2],true);var a=c-b;if(c<=b){a+=7;}if(e[1]=="last"){a-=7;}return f.set("date",f.getDate()+a);}}).alias("timeAgoInWords","timeDiffInWords");Locale.define("en-US","Number",{decimal:".",group:",",currency:{prefix:"$ "}});Number.implement({format:function(q){var n=this;q=q?Object.clone(q):{};var a=function(i){if(q[i]!=null){return q[i];}return Locale.get("Number."+i);};var f=n<0,h=a("decimal"),k=a("precision"),o=a("group"),c=a("decimals");if(f){var e=a("negative")||{};if(e.prefix==null&&e.suffix==null){e.prefix="-";}["prefix","suffix"].each(function(i){if(e[i]){q[i]=a(i)+e[i];}});n=-n;}var l=a("prefix"),p=a("suffix");if(c!==""&&c>=0&&c<=20){n=n.toFixed(c);}if(k>=1&&k<=21){n=(+n).toPrecision(k);}n+="";var m;if(a("scientific")===false&&n.indexOf("e")>-1){var j=n.split("e"),b=+j[1];n=j[0].replace(".","");if(b<0){b=-b-1;m=j[0].indexOf(".");if(m>-1){b-=m-1;}while(b--){n="0"+n;}n="0."+n;}else{m=j[0].lastIndexOf(".");if(m>-1){b-=j[0].length-m-1;}while(b--){n+="0";}}}if(h!="."){n=n.replace(".",h);}if(o){m=n.lastIndexOf(h);m=(m>-1)?m:n.length;var d=n.substring(m),g=m;while(g--){if((m-g-1)%3==0&&g!=(m-1)){d=o+d;}d=n.charAt(g)+d;}n=d;}if(l){n=l+n;}if(p){n+=p;}return n;},formatCurrency:function(b){var a=Locale.get("Number.currency")||{};if(a.scientific==null){a.scientific=false;}a.decimals=b!=null?b:(a.decimals==null?2:a.decimals);return this.format(a);},formatPercentage:function(b){var a=Locale.get("Number.percentage")||{};if(a.suffix==null){a.suffix="%";}a.decimals=b!=null?b:(a.decimals==null?2:a.decimals);return this.format(a);}});(function(){var c={a:/[àáâãäåăą]/g,A:/[ÀÁÂÃÄÅĂĄ]/g,c:/[ćčç]/g,C:/[ĆČÇ]/g,d:/[ďđ]/g,D:/[ĎÐ]/g,e:/[èéêëěę]/g,E:/[ÈÉÊËĚĘ]/g,g:/[ğ]/g,G:/[Ğ]/g,i:/[ìíîï]/g,I:/[ÌÍÎÏ]/g,l:/[ĺľł]/g,L:/[ĹĽŁ]/g,n:/[ñňń]/g,N:/[ÑŇŃ]/g,o:/[òóôõöøő]/g,O:/[ÒÓÔÕÖØ]/g,r:/[řŕ]/g,R:/[ŘŔ]/g,s:/[ššş]/g,S:/[ŠŞŚ]/g,t:/[ťţ]/g,T:/[ŤŢ]/g,ue:/[ü]/g,UE:/[Ü]/g,u:/[ùúûůµ]/g,U:/[ÙÚÛŮ]/g,y:/[ÿý]/g,Y:/[ŸÝ]/g,z:/[žźż]/g,Z:/[ŽŹŻ]/g,th:/[þ]/g,TH:/[Þ]/g,dh:/[ð]/g,DH:/[Ð]/g,ss:/[ß]/g,oe:/[œ]/g,OE:/[Œ]/g,ae:/[æ]/g,AE:/[Æ]/g},b={" ":/[\xa0\u2002\u2003\u2009]/g,"*":/[\xb7]/g,"'":/[\u2018\u2019]/g,'"':/[\u201c\u201d]/g,"...":/[\u2026]/g,"-":/[\u2013]/g,"&raquo;":/[\uFFFD]/g};var a=function(f,h){var e=f,g;for(g in h){e=e.replace(h[g],g);}return e;};var d=function(e,g){e=e||"";var h=g?"<"+e+"(?!\\w)[^>]*>([\\s\\S]*?)</"+e+"(?!\\w)>":"</?"+e+"([^>]+)?>",f=new RegExp(h,"gi");return f;};String.implement({standardize:function(){return a(this,c);},repeat:function(e){return new Array(e+1).join(this);},pad:function(e,h,g){if(this.length>=e){return this;}var f=(h==null?" ":""+h).repeat(e-this.length).substr(0,e-this.length);if(!g||g=="right"){return this+f;}if(g=="left"){return f+this;}return f.substr(0,(f.length/2).floor())+this+f.substr(0,(f.length/2).ceil());},getTags:function(e,f){return this.match(d(e,f))||[];},stripTags:function(e,f){return this.replace(d(e,f),"");},tidy:function(){return a(this,b);},truncate:function(e,f,i){var h=this;if(f==null&&arguments.length==1){f="…";}if(h.length>e){h=h.substring(0,e);if(i){var g=h.lastIndexOf(i);if(g!=-1){h=h.substr(0,g);}}if(f){h+=f;}}return h;}});})();String.implement({parseQueryString:function(d,a){if(d==null){d=true;}if(a==null){a=true;}var c=this.split(/[&;]/),b={};if(!c.length){return b;}c.each(function(i){var e=i.indexOf("=")+1,g=e?i.substr(e):"",f=e?i.substr(0,e-1).match(/([^\]\[]+|(\B)(?=\]))/g):[i],h=b;if(!f){return;}if(a){g=decodeURIComponent(g);}f.each(function(k,j){if(d){k=decodeURIComponent(k);}var l=h[k];if(j<f.length-1){h=h[k]=l||{};}else{if(typeOf(l)=="array"){l.push(g);}else{h[k]=l!=null?[l,g]:g;}}});});return b;},cleanQueryString:function(a){return this.split("&").filter(function(e){var b=e.indexOf("="),c=b<0?"":e.substr(0,b),d=e.substr(b+1);return a?a.call(null,c,d):(d||d===0);}).join("&");}});(function(){var b=function(){return this.get("value");};var a=this.URI=new Class({Implements:Options,options:{},regex:/^(?:(\w+):)?(?:\/\/(?:(?:([^:@\/]*):?([^:@\/]*))?@)?([^:\/?#]*)(?::(\d*))?)?(\.\.?$|(?:[^?#\/]*\/)*)([^?#]*)(?:\?([^#]*))?(?:#(.*))?/,parts:["scheme","user","password","host","port","directory","file","query","fragment"],schemes:{http:80,https:443,ftp:21,rtsp:554,mms:1755,file:0},initialize:function(d,c){this.setOptions(c);var e=this.options.base||a.base;if(!d){d=e;}if(d&&d.parsed){this.parsed=Object.clone(d.parsed);}else{this.set("value",d.href||d.toString(),e?new a(e):false);}},parse:function(e,d){var c=e.match(this.regex);if(!c){return false;}c.shift();return this.merge(c.associate(this.parts),d);},merge:function(d,c){if((!d||!d.scheme)&&(!c||!c.scheme)){return false;}if(c){this.parts.every(function(e){if(d[e]){return false;}d[e]=c[e]||"";return true;});}d.port=d.port||this.schemes[d.scheme.toLowerCase()];d.directory=d.directory?this.parseDirectory(d.directory,c?c.directory:""):"/";return d;},parseDirectory:function(d,e){d=(d.substr(0,1)=="/"?"":(e||"/"))+d;if(!d.test(a.regs.directoryDot)){return d;}var c=[];d.replace(a.regs.endSlash,"").split("/").each(function(f){if(f==".."&&c.length>0){c.pop();}else{if(f!="."){c.push(f);}}});return c.join("/")+"/";},combine:function(c){return c.value||c.scheme+"://"+(c.user?c.user+(c.password?":"+c.password:"")+"@":"")+(c.host||"")+(c.port&&c.port!=this.schemes[c.scheme]?":"+c.port:"")+(c.directory||"/")+(c.file||"")+(c.query?"?"+c.query:"")+(c.fragment?"#"+c.fragment:"");},set:function(d,f,e){if(d=="value"){var c=f.match(a.regs.scheme);if(c){c=c[1];}if(c&&this.schemes[c.toLowerCase()]==null){this.parsed={scheme:c,value:f};}else{this.parsed=this.parse(f,(e||this).parsed)||(c?{scheme:c,value:f}:{value:f});}}else{if(d=="data"){this.setData(f);}else{this.parsed[d]=f;}}return this;},get:function(c,d){switch(c){case"value":return this.combine(this.parsed,d?d.parsed:false);case"data":return this.getData();}return this.parsed[c]||"";},go:function(){document.location.href=this.toString();},toURI:function(){return this;},getData:function(e,d){var c=this.get(d||"query");if(!(c||c===0)){return e?null:{};}var f=c.parseQueryString();return e?f[e]:f;},setData:function(c,f,d){if(typeof c=="string"){var e=this.getData();e[arguments[0]]=arguments[1];c=e;}else{if(f){c=Object.merge(this.getData(),c);}}return this.set(d||"query",Object.toQueryString(c));},clearData:function(c){return this.set(c||"query","");},toString:b,valueOf:b});a.regs={endSlash:/\/$/,scheme:/^(\w+):/,directoryDot:/\.\/|\.$/};a.base=new a(Array.from(document.getElements("base[href]",true)).getLast(),{base:document.location});String.implement({toURI:function(c){return new a(this,c);}});})();URI=Class.refactor(URI,{combine:function(f,e){if(!e||f.scheme!=e.scheme||f.host!=e.host||f.port!=e.port){return this.previous.apply(this,arguments);}var a=f.file+(f.query?"?"+f.query:"")+(f.fragment?"#"+f.fragment:"");if(!e.directory){return(f.directory||(f.file?"":"./"))+a;}var d=e.directory.split("/"),c=f.directory.split("/"),g="",h;var b=0;for(h=0;h<d.length&&h<c.length&&d[h]==c[h];h++){}for(b=0;b<d.length-h-1;b++){g+="../";}for(b=h;b<c.length-1;b++){g+=c[b]+"/";}return(g||(f.file?"":"./"))+a;},toAbsolute:function(a){a=new URI(a);if(a){a.set("directory","").set("file","");}return this.toRelative(a);},toRelative:function(a){return this.get("value",new URI(a));}});(function(){if(this.Hash){return;}var a=this.Hash=new Type("Hash",function(b){if(typeOf(b)=="hash"){b=Object.clone(b.getClean());}for(var c in b){this[c]=b[c];}return this;});this.$H=function(b){return new a(b);};a.implement({forEach:function(b,c){Object.forEach(this,b,c);},getClean:function(){var c={};for(var b in this){if(this.hasOwnProperty(b)){c[b]=this[b];}}return c;},getLength:function(){var c=0;for(var b in this){if(this.hasOwnProperty(b)){c++;}}return c;}});a.alias("each","forEach");a.implement({has:Object.prototype.hasOwnProperty,keyOf:function(b){return Object.keyOf(this,b);},hasValue:function(b){return Object.contains(this,b);},extend:function(b){a.each(b||{},function(d,c){a.set(this,c,d);},this);return this;},combine:function(b){a.each(b||{},function(d,c){a.include(this,c,d);},this);return this;},erase:function(b){if(this.hasOwnProperty(b)){delete this[b];}return this;},get:function(b){return(this.hasOwnProperty(b))?this[b]:null;},set:function(b,c){if(!this[b]||this.hasOwnProperty(b)){this[b]=c;}return this;},empty:function(){a.each(this,function(c,b){delete this[b];},this);return this;},include:function(b,c){if(this[b]==undefined){this[b]=c;}return this;},map:function(b,c){return new a(Object.map(this,b,c));},filter:function(b,c){return new a(Object.filter(this,b,c));},every:function(b,c){return Object.every(this,b,c);},some:function(b,c){return Object.some(this,b,c);},getKeys:function(){return Object.keys(this);},getValues:function(){return Object.values(this);},toQueryString:function(b){return Object.toQueryString(this,b);}});a.alias({indexOf:"keyOf",contains:"hasValue"});})();Hash.implement({getFromPath:function(a){return Object.getFromPath(this,a);},cleanValues:function(a){return new Hash(Object.cleanValues(this,a));},run:function(){Object.run(arguments);}});Element.implement({tidy:function(){this.set("value",this.get("value").tidy());},getTextInRange:function(b,a){return this.get("value").substring(b,a);},getSelectedText:function(){if(this.setSelectionRange){return this.getTextInRange(this.getSelectionStart(),this.getSelectionEnd());}return document.selection.createRange().text;},getSelectedRange:function(){if(this.selectionStart!=null){return{start:this.selectionStart,end:this.selectionEnd};}var e={start:0,end:0};var a=this.getDocument().selection.createRange();if(!a||a.parentElement()!=this){return e;}var c=a.duplicate();if(this.type=="text"){e.start=0-c.moveStart("character",-100000);e.end=e.start+a.text.length;}else{var b=this.get("value");var d=b.length;c.moveToElementText(this);c.setEndPoint("StartToEnd",a);if(c.text.length){d-=b.match(/[\n\r]*$/)[0].length;}e.end=d-c.text.length;c.setEndPoint("StartToStart",a);e.start=d-c.text.length;}return e;},getSelectionStart:function(){return this.getSelectedRange().start;},getSelectionEnd:function(){return this.getSelectedRange().end;},setCaretPosition:function(a){if(a=="end"){a=this.get("value").length;}this.selectRange(a,a);return this;},getCaretPosition:function(){return this.getSelectedRange().start;},selectRange:function(e,a){if(this.setSelectionRange){this.focus();this.setSelectionRange(e,a);}else{var c=this.get("value");var d=c.substr(e,a-e).replace(/\r/g,"").length;e=c.substr(0,e).replace(/\r/g,"").length;var b=this.createTextRange();b.collapse(true);b.moveEnd("character",e+d);b.moveStart("character",e);b.select();}return this;},insertAtCursor:function(b,a){var d=this.getSelectedRange();var c=this.get("value");this.set("value",c.substring(0,d.start)+b+c.substring(d.end,c.length));if(a!==false){this.selectRange(d.start,d.start+b.length);}else{this.setCaretPosition(d.start+b.length);}return this;},insertAroundCursor:function(b,a){b=Object.append({before:"",defaultMiddle:"",after:""},b);var c=this.getSelectedText()||b.defaultMiddle;var g=this.getSelectedRange();var f=this.get("value");if(g.start==g.end){this.set("value",f.substring(0,g.start)+b.before+c+b.after+f.substring(g.end,f.length));this.selectRange(g.start+b.before.length,g.end+b.before.length+c.length);}else{var d=f.substring(g.start,g.end);this.set("value",f.substring(0,g.start)+b.before+d+b.after+f.substring(g.end,f.length));var e=g.start+b.before.length;if(a!==false){this.selectRange(e,e+d.length);}else{this.setCaretPosition(e+f.length);}}return this;}});Elements.from=function(e,d){if(d||d==null){e=e.stripScripts();}var b,c=e.match(/^\s*<(t[dhr]|tbody|tfoot|thead)/i);if(c){b=new Element("table");var a=c[1].toLowerCase();if(["td","th","tr"].contains(a)){b=new Element("tbody").inject(b);if(a!="tr"){b=new Element("tr").inject(b);}}}return(b||new Element("div")).set("html",e).getChildren();};(function(){var d={relay:false},c=["once","throttle","pause"],b=c.length;while(b--){d[c[b]]=Events.lookupPseudo(c[b]);}DOMEvent.definePseudo=function(e,f){d[e]=f;return this;};var a=Element.prototype;[Element,Window,Document].invoke("implement",Events.Pseudos(d,a.addEvent,a.removeEvent));})();(function(){var a="$moo:keys-pressed",b="$moo:keys-keyup";DOMEvent.definePseudo("keys",function(d,e,c){var g=c[0],f=[],h=this.retrieve(a,[]);f.append(d.value.replace("++",function(){f.push("+");return"";}).split("+"));h.include(g.key);if(f.every(function(j){return h.contains(j);})){e.apply(this,c);}this.store(a,h);if(!this.retrieve(b)){var i=function(j){(function(){h=this.retrieve(a,[]).erase(j.key);this.store(a,h);}).delay(0,this);};this.store(b,i).addEvent("keyup",i);}});DOMEvent.defineKeys({"16":"shift","17":"control","18":"alt","20":"capslock","33":"pageup","34":"pagedown","35":"end","36":"home","144":"numlock","145":"scrolllock","186":";","187":"=","188":",","190":".","191":"/","192":"`","219":"[","220":"\\","221":"]","222":"'","107":"+"}).defineKey(Browser.firefox?109:189,"-");})();(function(){var b=function(e,d){var f=[];Object.each(d,function(g){Object.each(g,function(h){e.each(function(i){f.push(i+"-"+h+(i=="border"?"-width":""));});});});return f;};var c=function(f,e){var d=0;Object.each(e,function(h,g){if(g.test(f)){d=d+h.toInt();}});return d;};var a=function(d){return!!(!d||d.offsetHeight||d.offsetWidth);};Element.implement({measure:function(h){if(a(this)){return h.call(this);}var g=this.getParent(),e=[];while(!a(g)&&g!=document.body){e.push(g.expose());g=g.getParent();}var f=this.expose(),d=h.call(this);f();e.each(function(i){i();});return d;},expose:function(){if(this.getStyle("display")!="none"){return function(){};}var d=this.style.cssText;this.setStyles({display:"block",position:"absolute",visibility:"hidden"});return function(){this.style.cssText=d;}.bind(this);},getDimensions:function(d){d=Object.merge({computeSize:false},d);var i={x:0,y:0};var h=function(j,e){return(e.computeSize)?j.getComputedSize(e):j.getSize();};var f=this.getParent("body");if(f&&this.getStyle("display")=="none"){i=this.measure(function(){return h(this,d);});}else{if(f){try{i=h(this,d);}catch(g){}}}return Object.append(i,(i.x||i.x===0)?{width:i.x,height:i.y}:{x:i.width,y:i.height});},getComputedSize:function(d){d=Object.merge({styles:["padding","border"],planes:{height:["top","bottom"],width:["left","right"]},mode:"both"},d);var g={},e={width:0,height:0},f;if(d.mode=="vertical"){delete e.width;delete d.planes.width;}else{if(d.mode=="horizontal"){delete e.height;delete d.planes.height;}}b(d.styles,d.planes).each(function(h){g[h]=this.getStyle(h).toInt();},this);Object.each(d.planes,function(i,h){var k=h.capitalize(),j=this.getStyle(h);if(j=="auto"&&!f){f=this.getDimensions();}j=g[h]=(j=="auto")?f[h]:j.toInt();e["total"+k]=j;i.each(function(m){var l=c(m,g);e["computed"+m.capitalize()]=l;e["total"+k]+=l;});},this);return Object.append(e,g);}});})();(function(){var a=false,b=false;var c=function(){var d=new Element("div").setStyles({position:"fixed",top:0,right:0}).inject(document.body);a=(d.offsetTop===0);d.dispose();b=true;};Element.implement({pin:function(h,f){if(!b){c();}if(this.getStyle("display")=="none"){return this;}var j,k=window.getScroll(),l,e;if(h!==false){j=this.getPosition(a?document.body:this.getOffsetParent());if(!this.retrieve("pin:_pinned")){var g={top:j.y-k.y,left:j.x-k.x};if(a&&!f){this.setStyle("position","fixed").setStyles(g);}else{l=this.getOffsetParent();var i=this.getPosition(l),m=this.getStyles("left","top");if(l&&m.left=="auto"||m.top=="auto"){this.setPosition(i);}if(this.getStyle("position")=="static"){this.setStyle("position","absolute");}i={x:m.left.toInt()-k.x,y:m.top.toInt()-k.y};e=function(){if(!this.retrieve("pin:_pinned")){return;}var n=window.getScroll();this.setStyles({left:i.x+n.x,top:i.y+n.y});}.bind(this);this.store("pin:_scrollFixer",e);window.addEvent("scroll",e);}this.store("pin:_pinned",true);}}else{if(!this.retrieve("pin:_pinned")){return this;}l=this.getParent();var d=(l.getComputedStyle("position")!="static"?l:l.getOffsetParent());j=this.getPosition(d);this.store("pin:_pinned",false);e=this.retrieve("pin:_scrollFixer");if(!e){this.setStyles({position:"absolute",top:j.y+k.y,left:j.x+k.x});}else{this.store("pin:_scrollFixer",null);window.removeEvent("scroll",e);}this.removeClass("isPinned");}return this;},unpin:function(){return this.pin(false);},togglePin:function(){return this.pin(!this.retrieve("pin:_pinned"));}});})();(function(b){var a=Element.Position={options:{relativeTo:document.body,position:{x:"center",y:"center"},offset:{x:0,y:0}},getOptions:function(d,c){c=Object.merge({},a.options,c);a.setPositionOption(c);a.setEdgeOption(c);a.setOffsetOption(d,c);a.setDimensionsOption(d,c);return c;},setPositionOption:function(c){c.position=a.getCoordinateFromValue(c.position);},setEdgeOption:function(d){var c=a.getCoordinateFromValue(d.edge);d.edge=c?c:(d.position.x=="center"&&d.position.y=="center")?{x:"center",y:"center"}:{x:"left",y:"top"};},setOffsetOption:function(f,d){var c={x:0,y:0},g=f.measure(function(){return document.id(this.getOffsetParent());}),e=g.getScroll();if(!g||g==f.getDocument().body){return;}c=g.measure(function(){var i=this.getPosition();if(this.getStyle("position")=="fixed"){var h=window.getScroll();i.x+=h.x;i.y+=h.y;}return i;});d.offset={parentPositioned:g!=document.id(d.relativeTo),x:d.offset.x-c.x+e.x,y:d.offset.y-c.y+e.y};},setDimensionsOption:function(d,c){c.dimensions=d.getDimensions({computeSize:true,styles:["padding","border","margin"]});},getPosition:function(e,d){var c={};d=a.getOptions(e,d);var f=document.id(d.relativeTo)||document.body;a.setPositionCoordinates(d,c,f);if(d.edge){a.toEdge(c,d);}var g=d.offset;c.left=((c.x>=0||g.parentPositioned||d.allowNegative)?c.x:0).toInt();c.top=((c.y>=0||g.parentPositioned||d.allowNegative)?c.y:0).toInt();a.toMinMax(c,d);if(d.relFixedPosition||f.getStyle("position")=="fixed"){a.toRelFixedPosition(f,c);}if(d.ignoreScroll){a.toIgnoreScroll(f,c);}if(d.ignoreMargins){a.toIgnoreMargins(c,d);}c.left=Math.ceil(c.left);c.top=Math.ceil(c.top);delete c.x;delete c.y;return c;},setPositionCoordinates:function(k,g,d){var f=k.offset.y,h=k.offset.x,e=(d==document.body)?window.getScroll():d.getPosition(),j=e.y,c=e.x,i=window.getSize();switch(k.position.x){case"left":g.x=c+h;break;case"right":g.x=c+h+d.offsetWidth;break;default:g.x=c+((d==document.body?i.x:d.offsetWidth)/2)+h;break;}switch(k.position.y){case"top":g.y=j+f;break;case"bottom":g.y=j+f+d.offsetHeight;break;default:g.y=j+((d==document.body?i.y:d.offsetHeight)/2)+f;break;}},toMinMax:function(c,d){var f={left:"x",top:"y"},e;["minimum","maximum"].each(function(g){["left","top"].each(function(h){e=d[g]?d[g][f[h]]:null;if(e!=null&&((g=="minimum")?c[h]<e:c[h]>e)){c[h]=e;}});});},toRelFixedPosition:function(e,c){var d=window.getScroll();c.top+=d.y;c.left+=d.x;},toIgnoreScroll:function(e,d){var c=e.getScroll();d.top-=c.y;d.left-=c.x;},toIgnoreMargins:function(c,d){c.left+=d.edge.x=="right"?d.dimensions["margin-right"]:(d.edge.x!="center"?-d.dimensions["margin-left"]:-d.dimensions["margin-left"]+((d.dimensions["margin-right"]+d.dimensions["margin-left"])/2));c.top+=d.edge.y=="bottom"?d.dimensions["margin-bottom"]:(d.edge.y!="center"?-d.dimensions["margin-top"]:-d.dimensions["margin-top"]+((d.dimensions["margin-bottom"]+d.dimensions["margin-top"])/2));},toEdge:function(c,d){var e={},g=d.dimensions,f=d.edge;switch(f.x){case"left":e.x=0;break;case"right":e.x=-g.x-g.computedRight-g.computedLeft;break;default:e.x=-(Math.round(g.totalWidth/2));break;}switch(f.y){case"top":e.y=0;break;case"bottom":e.y=-g.y-g.computedTop-g.computedBottom;break;default:e.y=-(Math.round(g.totalHeight/2));break;}c.x+=e.x;c.y+=e.y;},getCoordinateFromValue:function(c){if(typeOf(c)!="string"){return c;}c=c.toLowerCase();return{x:c.test("left")?"left":(c.test("right")?"right":"center"),y:c.test(/upper|top/)?"top":(c.test("bottom")?"bottom":"center")};}};Element.implement({position:function(d){if(d&&(d.x!=null||d.y!=null)){return(b?b.apply(this,arguments):this);}var c=this.setStyle("position","absolute").calculatePosition(d);return(d&&d.returnPos)?c:this.setStyles(c);},calculatePosition:function(c){return a.getPosition(this,c);}});})(Element.prototype.position);Element.implement({isDisplayed:function(){return this.getStyle("display")!="none";},isVisible:function(){var a=this.offsetWidth,b=this.offsetHeight;return(a==0&&b==0)?false:(a>0&&b>0)?true:this.style.display!="none";},toggle:function(){return this[this.isDisplayed()?"hide":"show"]();},hide:function(){var b;try{b=this.getStyle("display");}catch(a){}if(b=="none"){return this;}return this.store("element:_originalDisplay",b||"").setStyle("display","none");},show:function(a){if(!a&&this.isDisplayed()){return this;}a=a||this.retrieve("element:_originalDisplay")||"block";return this.setStyle("display",(a=="none")?"block":a);},swapClass:function(a,b){return this.removeClass(a).addClass(b);}});Document.implement({clearSelection:function(){if(window.getSelection){var a=window.getSelection();if(a&&a.removeAllRanges){a.removeAllRanges();}}else{if(document.selection&&document.selection.empty){try{document.selection.empty();}catch(b){}}}}});var IframeShim=new Class({Implements:[Options,Events,Class.Occlude],options:{className:"iframeShim",src:'javascript:false;document.write("");',display:false,zIndex:null,margin:0,offset:{x:0,y:0},browsers:(Browser.ie6||(Browser.firefox&&Browser.version<3&&Browser.Platform.mac))},property:"IframeShim",initialize:function(b,a){this.element=document.id(b);if(this.occlude()){return this.occluded;}this.setOptions(a);this.makeShim();return this;},makeShim:function(){if(this.options.browsers){var c=this.element.getStyle("zIndex").toInt();if(!c){c=1;var b=this.element.getStyle("position");if(b=="static"||!b){this.element.setStyle("position","relative");}this.element.setStyle("zIndex",c);}c=((this.options.zIndex!=null||this.options.zIndex===0)&&c>this.options.zIndex)?this.options.zIndex:c-1;if(c<0){c=1;}this.shim=new Element("iframe",{src:this.options.src,scrolling:"no",frameborder:0,styles:{zIndex:c,position:"absolute",border:"none",filter:"progid:DXImageTransform.Microsoft.Alpha(style=0,opacity=0)"},"class":this.options.className}).store("IframeShim",this);var a=(function(){this.shim.inject(this.element,"after");this[this.options.display?"show":"hide"]();this.fireEvent("inject");}).bind(this);if(!IframeShim.ready){window.addEvent("load",a);}else{a();}}else{this.position=this.hide=this.show=this.dispose=Function.from(this);}},position:function(){if(!IframeShim.ready||!this.shim){return this;}var a=this.element.measure(function(){return this.getSize();});if(this.options.margin!=undefined){a.x=a.x-(this.options.margin*2);a.y=a.y-(this.options.margin*2);this.options.offset.x+=this.options.margin;this.options.offset.y+=this.options.margin;}this.shim.set({width:a.x,height:a.y}).position({relativeTo:this.element,offset:this.options.offset});return this;},hide:function(){if(this.shim){this.shim.setStyle("display","none");}return this;},show:function(){if(this.shim){this.shim.setStyle("display","block");}return this.position();},dispose:function(){if(this.shim){this.shim.dispose();}return this;},destroy:function(){if(this.shim){this.shim.destroy();}return this;}});window.addEvent("load",function(){IframeShim.ready=true;});var Mask=new Class({Implements:[Options,Events],Binds:["position"],options:{style:{},"class":"mask",maskMargins:false,useIframeShim:true,iframeShimOptions:{}},initialize:function(b,a){this.target=document.id(b)||document.id(document.body);this.target.store("mask",this);this.setOptions(a);this.render();this.inject();},render:function(){this.element=new Element("div",{"class":this.options["class"],id:this.options.id||"mask-"+String.uniqueID(),styles:Object.merge({},this.options.style,{display:"none"}),events:{click:function(a){this.fireEvent("click",a);if(this.options.hideOnClick){this.hide();}}.bind(this)}});this.hidden=true;},toElement:function(){return this.element;},inject:function(b,a){a=a||(this.options.inject?this.options.inject.where:"")||this.target==document.body?"inside":"after";b=b||(this.options.inject&&this.options.inject.target)||this.target;this.element.inject(b,a);if(this.options.useIframeShim){this.shim=new IframeShim(this.element,this.options.iframeShimOptions);this.addEvents({show:this.shim.show.bind(this.shim),hide:this.shim.hide.bind(this.shim),destroy:this.shim.destroy.bind(this.shim)});}},position:function(){this.resize(this.options.width,this.options.height);this.element.position({relativeTo:this.target,position:"topLeft",ignoreMargins:!this.options.maskMargins,ignoreScroll:this.target==document.body});return this;},resize:function(a,e){var b={styles:["padding","border"]};if(this.options.maskMargins){b.styles.push("margin");}var d=this.target.getComputedSize(b);if(this.target==document.body){this.element.setStyles({width:0,height:0});var c=window.getScrollSize();if(d.totalHeight<c.y){d.totalHeight=c.y;}if(d.totalWidth<c.x){d.totalWidth=c.x;}}this.element.setStyles({width:Array.pick([a,d.totalWidth,d.x]),height:Array.pick([e,d.totalHeight,d.y])});return this;},show:function(){if(!this.hidden){return this;}window.addEvent("resize",this.position);this.position();this.showMask.apply(this,arguments);return this;},showMask:function(){this.element.setStyle("display","block");this.hidden=false;this.fireEvent("show");},hide:function(){if(this.hidden){return this;}window.removeEvent("resize",this.position);this.hideMask.apply(this,arguments);if(this.options.destroyOnHide){return this.destroy();}return this;},hideMask:function(){this.element.setStyle("display","none");this.hidden=true;this.fireEvent("hide");},toggle:function(){this[this.hidden?"show":"hide"]();},destroy:function(){this.hide();this.element.destroy();this.fireEvent("destroy");this.target.eliminate("mask");}});Element.Properties.mask={set:function(b){var a=this.retrieve("mask");if(a){a.destroy();}return this.eliminate("mask").store("mask:options",b);},get:function(){var a=this.retrieve("mask");if(!a){a=new Mask(this,this.retrieve("mask:options"));this.store("mask",a);}return a;}};Element.implement({mask:function(a){if(a){this.set("mask",a);}this.get("mask").show();return this;},unmask:function(){this.get("mask").hide();return this;}});var Spinner=new Class({Extends:Mask,Implements:Chain,options:{"class":"spinner",containerPosition:{},content:{"class":"spinner-content"},messageContainer:{"class":"spinner-msg"},img:{"class":"spinner-img"},fxOptions:{link:"chain"}},initialize:function(c,a){this.target=document.id(c)||document.id(document.body);this.target.store("spinner",this);this.setOptions(a);this.render();this.inject();var b=function(){this.active=false;}.bind(this);this.addEvents({hide:b,show:b});},render:function(){this.parent();this.element.set("id",this.options.id||"spinner-"+String.uniqueID());this.content=document.id(this.options.content)||new Element("div",this.options.content);this.content.inject(this.element);if(this.options.message){this.msg=document.id(this.options.message)||new Element("p",this.options.messageContainer).appendText(this.options.message);this.msg.inject(this.content);}if(this.options.img){this.img=document.id(this.options.img)||new Element("div",this.options.img);this.img.inject(this.content);}this.element.set("tween",this.options.fxOptions);},show:function(a){if(this.active){return this.chain(this.show.bind(this));}if(!this.hidden){this.callChain.delay(20,this);return this;}this.active=true;return this.parent(a);},showMask:function(a){var b=function(){this.content.position(Object.merge({relativeTo:this.element},this.options.containerPosition));}.bind(this);if(a){this.parent();b();}else{if(!this.options.style.opacity){this.options.style.opacity=this.element.getStyle("opacity").toFloat();}this.element.setStyles({display:"block",opacity:0}).tween("opacity",this.options.style.opacity);b();this.hidden=false;this.fireEvent("show");this.callChain();}},hide:function(a){if(this.active){return this.chain(this.hide.bind(this));}if(this.hidden){this.callChain.delay(20,this);return this;}this.active=true;return this.parent(a);},hideMask:function(a){if(a){return this.parent();}this.element.tween("opacity",0).get("tween").chain(function(){this.element.setStyle("display","none");this.hidden=true;this.fireEvent("hide");this.callChain();}.bind(this));},destroy:function(){this.content.destroy();this.parent();this.target.eliminate("spinner");}});Request=Class.refactor(Request,{options:{useSpinner:false,spinnerOptions:{},spinnerTarget:false},initialize:function(a){this._send=this.send;this.send=function(b){var c=this.getSpinner();if(c){c.chain(this._send.pass(b,this)).show();}else{this._send(b);}return this;};this.previous(a);},getSpinner:function(){if(!this.spinner){var b=document.id(this.options.spinnerTarget)||document.id(this.options.update);if(this.options.useSpinner&&b){b.set("spinner",this.options.spinnerOptions);var a=this.spinner=b.get("spinner");["complete","exception","cancel"].each(function(c){this.addEvent(c,a.hide.bind(a));},this);}}return this.spinner;}});Element.Properties.spinner={set:function(a){var b=this.retrieve("spinner");if(b){b.destroy();}return this.eliminate("spinner").store("spinner:options",a);},get:function(){var a=this.retrieve("spinner");if(!a){a=new Spinner(this,this.retrieve("spinner:options"));this.store("spinner",a);}return a;}};Element.implement({spin:function(a){if(a){this.set("spinner",a);}this.get("spinner").show();return this;},unspin:function(){this.get("spinner").hide();return this;}});if(!window.Form){window.Form={};}(function(){Form.Request=new Class({Binds:["onSubmit","onFormValidate"],Implements:[Options,Events,Class.Occlude],options:{requestOptions:{evalScripts:true,useSpinner:true,emulation:false,link:"ignore"},sendButtonClicked:true,extraData:{},resetForm:true},property:"form.request",initialize:function(b,c,a){this.element=document.id(b);if(this.occlude()){return this.occluded;}this.setOptions(a).setTarget(c).attach();},setTarget:function(a){this.target=document.id(a);if(!this.request){this.makeRequest();}else{this.request.setOptions({update:this.target});}return this;},toElement:function(){return this.element;},makeRequest:function(){var a=this;this.request=new Request.HTML(Object.merge({update:this.target,emulation:false,spinnerTarget:this.element,method:this.element.get("method")||"post"},this.options.requestOptions)).addEvents({success:function(c,e,d,b){["complete","success"].each(function(f){a.fireEvent(f,[a.target,c,e,d,b]);});},failure:function(){a.fireEvent("complete",arguments).fireEvent("failure",arguments);},exception:function(){a.fireEvent("failure",arguments);}});return this.attachReset();},attachReset:function(){if(!this.options.resetForm){return this;}this.request.addEvent("success",function(){Function.attempt(function(){this.element.reset();}.bind(this));if(window.OverText){OverText.update();}}.bind(this));return this;},attach:function(a){var c=(a!=false)?"addEvent":"removeEvent";this.element[c]("click:relay(button, input[type=submit])",this.saveClickedButton.bind(this));var b=this.element.retrieve("validator");if(b){b[c]("onFormValidate",this.onFormValidate);}else{this.element[c]("submit",this.onSubmit);}return this;},detach:function(){return this.attach(false);},enable:function(){return this.attach();},disable:function(){return this.detach();},onFormValidate:function(c,b,a){if(!a){return;}var d=this.element.retrieve("validator");if(c||(d&&!d.options.stopOnFailure)){a.stop();this.send();}},onSubmit:function(a){var b=this.element.retrieve("validator");if(b){this.element.removeEvent("submit",this.onSubmit);b.addEvent("onFormValidate",this.onFormValidate);this.element.validate();return;}if(a){a.stop();}this.send();},saveClickedButton:function(b,c){var a=c.get("name");if(!a||!this.options.sendButtonClicked){return;}this.options.extraData[a]=c.get("value")||true;this.clickedCleaner=function(){delete this.options.extraData[a];this.clickedCleaner=function(){};}.bind(this);},clickedCleaner:function(){},send:function(){var b=this.element.toQueryString().trim(),a=Object.toQueryString(this.options.extraData);if(b){b+="&"+a;}else{b=a;}this.fireEvent("send",[this.element,b.parseQueryString()]);this.request.send({data:b,url:this.options.requestOptions.url||this.element.get("action")});this.clickedCleaner();return this;}});Element.implement("formUpdate",function(c,b){var a=this.retrieve("form.request");if(!a){a=new Form.Request(this,c,b);}else{if(c){a.setTarget(c);}if(b){a.setOptions(b).makeRequest();}}a.send();return this;});})();(function(){var a=function(d){var b=d.options.hideInputs;if(window.OverText){var c=[null];OverText.each(function(e){c.include("."+e.options.labelClass);});if(c){b+=c.join(", ");}}return(b)?d.element.getElements(b):null;};Fx.Reveal=new Class({Extends:Fx.Morph,options:{link:"cancel",styles:["padding","border","margin"],transitionOpacity:!Browser.ie6,mode:"vertical",display:function(){return this.element.get("tag")!="tr"?"block":"table-row";},opacity:1,hideInputs:Browser.ie?"select, input, textarea, object, embed":null},dissolve:function(){if(!this.hiding&&!this.showing){if(this.element.getStyle("display")!="none"){this.hiding=true;this.showing=false;this.hidden=true;this.cssText=this.element.style.cssText;var d=this.element.getComputedSize({styles:this.options.styles,mode:this.options.mode});if(this.options.transitionOpacity){d.opacity=this.options.opacity;}var c={};Object.each(d,function(f,e){c[e]=[f,0];});this.element.setStyles({display:Function.from(this.options.display).call(this),overflow:"hidden"});var b=a(this);if(b){b.setStyle("visibility","hidden");}this.$chain.unshift(function(){if(this.hidden){this.hiding=false;this.element.style.cssText=this.cssText;this.element.setStyle("display","none");if(b){b.setStyle("visibility","visible");}}this.fireEvent("hide",this.element);this.callChain();}.bind(this));this.start(c);}else{this.callChain.delay(10,this);this.fireEvent("complete",this.element);this.fireEvent("hide",this.element);}}else{if(this.options.link=="chain"){this.chain(this.dissolve.bind(this));}else{if(this.options.link=="cancel"&&!this.hiding){this.cancel();this.dissolve();}}}return this;},reveal:function(){if(!this.showing&&!this.hiding){if(this.element.getStyle("display")=="none"){this.hiding=false;this.showing=true;this.hidden=false;this.cssText=this.element.style.cssText;var d;this.element.measure(function(){d=this.element.getComputedSize({styles:this.options.styles,mode:this.options.mode});}.bind(this));if(this.options.heightOverride!=null){d.height=this.options.heightOverride.toInt();}if(this.options.widthOverride!=null){d.width=this.options.widthOverride.toInt();}if(this.options.transitionOpacity){this.element.setStyle("opacity",0);d.opacity=this.options.opacity;}var c={height:0,display:Function.from(this.options.display).call(this)};Object.each(d,function(f,e){c[e]=0;});c.overflow="hidden";this.element.setStyles(c);var b=a(this);if(b){b.setStyle("visibility","hidden");}this.$chain.unshift(function(){this.element.style.cssText=this.cssText;this.element.setStyle("display",Function.from(this.options.display).call(this));if(!this.hidden){this.showing=false;}if(b){b.setStyle("visibility","visible");}this.callChain();this.fireEvent("show",this.element);}.bind(this));this.start(d);}else{this.callChain();this.fireEvent("complete",this.element);this.fireEvent("show",this.element);}}else{if(this.options.link=="chain"){this.chain(this.reveal.bind(this));}else{if(this.options.link=="cancel"&&!this.showing){this.cancel();this.reveal();}}}return this;},toggle:function(){if(this.element.getStyle("display")=="none"){this.reveal();}else{this.dissolve();}return this;},cancel:function(){this.parent.apply(this,arguments);if(this.cssText!=null){this.element.style.cssText=this.cssText;}this.hiding=false;this.showing=false;return this;}});Element.Properties.reveal={set:function(b){this.get("reveal").cancel().setOptions(b);return this;},get:function(){var b=this.retrieve("reveal");if(!b){b=new Fx.Reveal(this);this.store("reveal",b);}return b;}};Element.Properties.dissolve=Element.Properties.reveal;Element.implement({reveal:function(b){this.get("reveal").setOptions(b).reveal();return this;},dissolve:function(b){this.get("reveal").setOptions(b).dissolve();return this;},nix:function(b){var c=Array.link(arguments,{destroy:Type.isBoolean,options:Type.isObject});this.get("reveal").setOptions(b).dissolve().chain(function(){this[c.destroy?"destroy":"dispose"]();}.bind(this));return this;},wink:function(){var c=Array.link(arguments,{duration:Type.isNumber,options:Type.isObject});var b=this.get("reveal").setOptions(c.options);b.reveal().chain(function(){(function(){b.dissolve();}).delay(c.duration||2000);});}});})();Form.Request.Append=new Class({Extends:Form.Request,options:{useReveal:true,revealOptions:{},inject:"bottom"},makeRequest:function(){this.request=new Request.HTML(Object.merge({url:this.element.get("action"),method:this.element.get("method")||"post",spinnerTarget:this.element},this.options.requestOptions,{evalScripts:false})).addEvents({success:function(b,g,f,a){var c;var d=Elements.from(f);if(d.length==1){c=d[0];}else{c=new Element("div",{styles:{display:"none"}}).adopt(d);}c.inject(this.target,this.options.inject);if(this.options.requestOptions.evalScripts){Browser.exec(a);}this.fireEvent("beforeEffect",c);var e=function(){this.fireEvent("success",[c,this.target,b,g,f,a]);}.bind(this);if(this.options.useReveal){c.set("reveal",this.options.revealOptions).get("reveal").chain(e);c.reveal();}else{e();}}.bind(this),failure:function(a){this.fireEvent("failure",a);}.bind(this)});this.attachReset();}});Locale.define("en-US","FormValidator",{required:"This field is required.",length:"Please enter {length} characters (you entered {elLength} characters)",minLength:"Please enter at least {minLength} characters (you entered {length} characters).",maxLength:"Please enter no more than {maxLength} characters (you entered {length} characters).",integer:"Please enter an integer in this field. Numbers with decimals (e.g. 1.25) are not permitted.",numeric:'Please enter only numeric values in this field (i.e. "1" or "1.1" or "-1" or "-1.1").',digits:"Please use numbers and punctuation only in this field (for example, a phone number with dashes or dots is permitted).",alpha:"Please use only letters (a-z) within this field. No spaces or other characters are allowed.",alphanum:"Please use only letters (a-z) or numbers (0-9) in this field. No spaces or other characters are allowed.",dateSuchAs:"Please enter a valid date such as {date}",dateInFormatMDY:'Please enter a valid date such as MM/DD/YYYY (i.e. "12/31/1999")',email:'Please enter a valid email address. For example "fred@domain.com".',url:"Please enter a valid URL such as http://www.example.com.",currencyDollar:"Please enter a valid $ amount. For example $100.00 .",oneRequired:"Please enter something for at least one of these inputs.",errorPrefix:"Error: ",warningPrefix:"Warning: ",noSpace:"There can be no spaces in this input.",reqChkByNode:"No items are selected.",requiredChk:"This field is required.",reqChkByName:"Please select a {label}.",match:"This field needs to match the {matchName} field",startDate:"the start date",endDate:"the end date",currendDate:"the current date",afterDate:"The date should be the same or after {label}.",beforeDate:"The date should be the same or before {label}.",startMonth:"Please select a start month",sameMonth:"These two dates must be in the same month - you must change one or the other.",creditcard:"The credit card number entered is invalid. Please check the number and try again. {length} digits entered."});if(!window.Form){window.Form={};}var InputValidator=this.InputValidator=new Class({Implements:[Options],options:{errorMsg:"Validation failed.",test:Function.from(true)},initialize:function(b,a){this.setOptions(a);this.className=b;},test:function(b,a){b=document.id(b);return(b)?this.options.test(b,a||this.getProps(b)):false;},getError:function(c,a){c=document.id(c);var b=this.options.errorMsg;if(typeOf(b)=="function"){b=b(c,a||this.getProps(c));}return b;},getProps:function(a){a=document.id(a);return(a)?a.get("validatorProps"):{};}});Element.Properties.validators={get:function(){return(this.get("data-validators")||this.className).clean().split(" ");}};Element.Properties.validatorProps={set:function(a){return this.eliminate("$moo:validatorProps").store("$moo:validatorProps",a);},get:function(a){if(a){this.set(a);}if(this.retrieve("$moo:validatorProps")){return this.retrieve("$moo:validatorProps");}if(this.getProperty("data-validator-properties")||this.getProperty("validatorProps")){try{this.store("$moo:validatorProps",JSON.decode(this.getProperty("validatorProps")||this.getProperty("data-validator-properties")));}catch(c){return{};}}else{var b=this.get("validators").filter(function(d){return d.test(":");});if(!b.length){this.store("$moo:validatorProps",{});}else{a={};b.each(function(d){var f=d.split(":");if(f[1]){try{a[f[0]]=JSON.decode(f[1]);}catch(g){}}});this.store("$moo:validatorProps",a);}}return this.retrieve("$moo:validatorProps");}};Form.Validator=new Class({Implements:[Options,Events],Binds:["onSubmit"],options:{fieldSelectors:"input, select, textarea",ignoreHidden:true,ignoreDisabled:true,useTitles:false,evaluateOnSubmit:true,evaluateFieldsOnBlur:true,evaluateFieldsOnChange:true,serial:true,stopOnFailure:true,warningPrefix:function(){return Form.Validator.getMsg("warningPrefix")||"Warning: ";},errorPrefix:function(){return Form.Validator.getMsg("errorPrefix")||"Error: ";}},initialize:function(b,a){this.setOptions(a);this.element=document.id(b);this.element.store("validator",this);this.warningPrefix=Function.from(this.options.warningPrefix)();this.errorPrefix=Function.from(this.options.errorPrefix)();if(this.options.evaluateOnSubmit){this.element.addEvent("submit",this.onSubmit);}if(this.options.evaluateFieldsOnBlur||this.options.evaluateFieldsOnChange){this.watchFields(this.getFields());}},toElement:function(){return this.element;},getFields:function(){return(this.fields=this.element.getElements(this.options.fieldSelectors));},watchFields:function(a){a.each(function(b){if(this.options.evaluateFieldsOnBlur){b.addEvent("blur",this.validationMonitor.pass([b,false],this));}if(this.options.evaluateFieldsOnChange){b.addEvent("change",this.validationMonitor.pass([b,true],this));}},this);},validationMonitor:function(){clearTimeout(this.timer);this.timer=this.validateField.delay(50,this,arguments);},onSubmit:function(a){if(this.validate(a)){this.reset();}},reset:function(){this.getFields().each(this.resetField,this);return this;},validate:function(b){var a=this.getFields().map(function(c){return this.validateField(c,true);},this).every(function(c){return c;});this.fireEvent("formValidate",[a,this.element,b]);if(this.options.stopOnFailure&&!a&&b){b.preventDefault();}return a;},validateField:function(j,b){if(this.paused){return true;}j=document.id(j);var f=!j.hasClass("validation-failed");var g,i;if(this.options.serial&&!b){g=this.element.getElement(".validation-failed");i=this.element.getElement(".warning");}if(j&&(!g||b||j.hasClass("validation-failed")||(g&&!this.options.serial))){var a=j.get("validators");var d=a.some(function(k){return this.getValidator(k);},this);var h=[];a.each(function(k){if(k&&!this.test(k,j)){h.include(k);}},this);f=h.length===0;if(d&&!this.hasValidator(j,"warnOnly")){if(f){j.addClass("validation-passed").removeClass("validation-failed");this.fireEvent("elementPass",[j]);}else{j.addClass("validation-failed").removeClass("validation-passed");this.fireEvent("elementFail",[j,h]);}}if(!i){var e=a.some(function(k){if(k.test("^warn")){return this.getValidator(k.replace(/^warn-/,""));}else{return null;}},this);j.removeClass("warning");var c=a.map(function(k){if(k.test("^warn")){return this.test(k.replace(/^warn-/,""),j,true);}else{return null;}},this);}}return f;},test:function(b,d,e){d=document.id(d);if((this.options.ignoreHidden&&!d.isVisible())||(this.options.ignoreDisabled&&d.get("disabled"))){return true;}var a=this.getValidator(b);if(e!=null){e=false;}if(this.hasValidator(d,"warnOnly")){e=true;}var c=this.hasValidator(d,"ignoreValidation")||(a?a.test(d):true);if(a&&d.isVisible()){this.fireEvent("elementValidate",[c,d,b,e]);}if(e){return true;}return c;},hasValidator:function(b,a){return b.get("validators").contains(a);},resetField:function(a){a=document.id(a);if(a){a.get("validators").each(function(b){if(b.test("^warn-")){b=b.replace(/^warn-/,"");}a.removeClass("validation-failed");a.removeClass("warning");a.removeClass("validation-passed");},this);}return this;},stop:function(){this.paused=true;return this;},start:function(){this.paused=false;return this;},ignoreField:function(a,b){a=document.id(a);if(a){this.enforceField(a);if(b){a.addClass("warnOnly");}else{a.addClass("ignoreValidation");}}return this;},enforceField:function(a){a=document.id(a);if(a){a.removeClass("warnOnly").removeClass("ignoreValidation");}return this;}});Form.Validator.getMsg=function(a){return Locale.get("FormValidator."+a);};Form.Validator.adders={validators:{},add:function(b,a){this.validators[b]=new InputValidator(b,a);if(!this.initialize){this.implement({validators:this.validators});}},addAllThese:function(a){Array.from(a).each(function(b){this.add(b[0],b[1]);},this);},getValidator:function(a){return this.validators[a.split(":")[0]];}};Object.append(Form.Validator,Form.Validator.adders);Form.Validator.implement(Form.Validator.adders);Form.Validator.add("IsEmpty",{errorMsg:false,test:function(a){if(a.type=="select-one"||a.type=="select"){return!(a.selectedIndex>=0&&a.options[a.selectedIndex].value!="");}else{return((a.get("value")==null)||(a.get("value").length==0));}}});Form.Validator.addAllThese([["required",{errorMsg:function(){return Form.Validator.getMsg("required");},test:function(a){return!Form.Validator.getValidator("IsEmpty").test(a);}}],["length",{errorMsg:function(a,b){if(typeOf(b.length)!="null"){return Form.Validator.getMsg("length").substitute({length:b.length,elLength:a.get("value").length});}else{return"";}},test:function(a,b){if(typeOf(b.length)!="null"){return(a.get("value").length==b.length||a.get("value").length==0);}else{return true;}}}],["minLength",{errorMsg:function(a,b){if(typeOf(b.minLength)!="null"){return Form.Validator.getMsg("minLength").substitute({minLength:b.minLength,length:a.get("value").length});}else{return"";}},test:function(a,b){if(typeOf(b.minLength)!="null"){return(a.get("value").length>=(b.minLength||0));}else{return true;}}}],["maxLength",{errorMsg:function(a,b){if(typeOf(b.maxLength)!="null"){return Form.Validator.getMsg("maxLength").substitute({maxLength:b.maxLength,length:a.get("value").length});}else{return"";}},test:function(a,b){return a.get("value").length<=(b.maxLength||10000);}}],["validate-integer",{errorMsg:Form.Validator.getMsg.pass("integer"),test:function(a){return Form.Validator.getValidator("IsEmpty").test(a)||(/^(-?[1-9]\d*|0)$/).test(a.get("value"));}}],["validate-numeric",{errorMsg:Form.Validator.getMsg.pass("numeric"),test:function(a){return Form.Validator.getValidator("IsEmpty").test(a)||(/^-?(?:0$0(?=\d*\.)|[1-9]|0)\d*(\.\d+)?$/).test(a.get("value"));}}],["validate-digits",{errorMsg:Form.Validator.getMsg.pass("digits"),test:function(a){return Form.Validator.getValidator("IsEmpty").test(a)||(/^[\d() .:\-\+#]+$/.test(a.get("value")));}}],["validate-alpha",{errorMsg:Form.Validator.getMsg.pass("alpha"),test:function(a){return Form.Validator.getValidator("IsEmpty").test(a)||(/^[a-zA-Z]+$/).test(a.get("value"));}}],["validate-alphanum",{errorMsg:Form.Validator.getMsg.pass("alphanum"),test:function(a){return Form.Validator.getValidator("IsEmpty").test(a)||!(/\W/).test(a.get("value"));}}],["validate-date",{errorMsg:function(a,b){if(Date.parse){var c=b.dateFormat||"%x";return Form.Validator.getMsg("dateSuchAs").substitute({date:new Date().format(c)});}else{return Form.Validator.getMsg("dateInFormatMDY");}},test:function(e,g){if(Form.Validator.getValidator("IsEmpty").test(e)){return true;}var a=Locale.getCurrent().sets.Date,b=new RegExp([a.days,a.days_abbr,a.months,a.months_abbr].flatten().join("|"),"i"),i=e.get("value"),f=i.match(/[a-z]+/gi);if(f&&!f.every(b.exec,b)){return false;}var c=Date.parse(i),h=g.dateFormat||"%x",d=c.format(h);if(d!="invalid date"){e.set("value",d);}return c.isValid();}}],["validate-email",{errorMsg:Form.Validator.getMsg.pass("email"),test:function(a){return Form.Validator.getValidator("IsEmpty").test(a)||(/^(?:[a-z0-9!#$%&'*+\/=?^_`{|}~-]\.?){0,63}[a-z0-9!#$%&'*+\/=?^_`{|}~-]@(?:(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)*[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\])$/i).test(a.get("value"));}}],["validate-url",{errorMsg:Form.Validator.getMsg.pass("url"),test:function(a){return Form.Validator.getValidator("IsEmpty").test(a)||(/^(https?|ftp|rmtp|mms):\/\/(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)(:(\d+))?\/?/i).test(a.get("value"));}}],["validate-currency-dollar",{errorMsg:Form.Validator.getMsg.pass("currencyDollar"),test:function(a){return Form.Validator.getValidator("IsEmpty").test(a)||(/^\$?\-?([1-9]{1}[0-9]{0,2}(\,[0-9]{3})*(\.[0-9]{0,2})?|[1-9]{1}\d*(\.[0-9]{0,2})?|0(\.[0-9]{0,2})?|(\.[0-9]{1,2})?)$/).test(a.get("value"));}}],["validate-one-required",{errorMsg:Form.Validator.getMsg.pass("oneRequired"),test:function(a,b){var c=document.id(b["validate-one-required"])||a.getParent(b["validate-one-required"]);return c.getElements("input").some(function(d){if(["checkbox","radio"].contains(d.get("type"))){return d.get("checked");}return d.get("value");});}}]]);Element.Properties.validator={set:function(a){this.get("validator").setOptions(a);},get:function(){var a=this.retrieve("validator");if(!a){a=new Form.Validator(this);this.store("validator",a);}return a;}};Element.implement({validate:function(a){if(a){this.set("validator",a);}return this.get("validator").validate();}});Form.Validator.Inline=new Class({Extends:Form.Validator,options:{showError:function(a){if(a.reveal){a.reveal();}else{a.setStyle("display","block");}},hideError:function(a){if(a.dissolve){a.dissolve();}else{a.setStyle("display","none");}},scrollToErrorsOnSubmit:true,scrollToErrorsOnBlur:false,scrollToErrorsOnChange:false,scrollFxOptions:{transition:"quad:out",offset:{y:-20}}},initialize:function(b,a){this.parent(b,a);this.addEvent("onElementValidate",function(g,f,e,h){var d=this.getValidator(e);if(!g&&d.getError(f)){if(h){f.addClass("warning");}var c=this.makeAdvice(e,f,d.getError(f),h);this.insertAdvice(c,f);this.showAdvice(e,f);}else{this.hideAdvice(e,f);}});},makeAdvice:function(d,f,c,g){var e=(g)?this.warningPrefix:this.errorPrefix;e+=(this.options.useTitles)?f.title||c:c;var a=(g)?"warning-advice":"validation-advice";var b=this.getAdvice(d,f);if(b){b=b.set("html",e);}else{b=new Element("div",{html:e,styles:{display:"none"},id:"advice-"+d.split(":")[0]+"-"+this.getFieldId(f)}).addClass(a);}f.store("$moo:advice-"+d,b);return b;},getFieldId:function(a){return a.id?a.id:a.id="input_"+a.name;},showAdvice:function(b,c){var a=this.getAdvice(b,c);if(a&&!c.retrieve("$moo:"+this.getPropName(b))&&(a.getStyle("display")=="none"||a.getStyle("visiblity")=="hidden"||a.getStyle("opacity")==0)){c.store("$moo:"+this.getPropName(b),true);this.options.showError(a);this.fireEvent("showAdvice",[c,a,b]);}},hideAdvice:function(b,c){var a=this.getAdvice(b,c);if(a&&c.retrieve("$moo:"+this.getPropName(b))){c.store("$moo:"+this.getPropName(b),false);this.options.hideError(a);this.fireEvent("hideAdvice",[c,a,b]);}},getPropName:function(a){return"advice"+a;},resetField:function(a){a=document.id(a);if(!a){return this;}this.parent(a);a.get("validators").each(function(b){this.hideAdvice(b,a);},this);return this;},getAllAdviceMessages:function(d,c){var b=[];if(d.hasClass("ignoreValidation")&&!c){return b;}var a=d.get("validators").some(function(g){var e=g.test("^warn-")||d.hasClass("warnOnly");if(e){g=g.replace(/^warn-/,"");}var f=this.getValidator(g);if(!f){return;}b.push({message:f.getError(d),warnOnly:e,passed:f.test(),validator:f});},this);return b;},getAdvice:function(a,b){return b.retrieve("$moo:advice-"+a);},insertAdvice:function(a,c){var b=c.get("validatorProps");if(!b.msgPos||!document.id(b.msgPos)){if(c.type&&c.type.toLowerCase()=="radio"){c.getParent().adopt(a);}else{a.inject(document.id(c),"after");}}else{document.id(b.msgPos).grab(a);}},validateField:function(g,f,b){var a=this.parent(g,f);if(((this.options.scrollToErrorsOnSubmit&&b==null)||b)&&!a){var c=document.id(this).getElement(".validation-failed");var d=document.id(this).getParent();while(d!=document.body&&d.getScrollSize().y==d.getSize().y){d=d.getParent();}var e=d.retrieve("$moo:fvScroller");if(!e&&window.Fx&&Fx.Scroll){e=new Fx.Scroll(d,this.options.scrollFxOptions);d.store("$moo:fvScroller",e);}if(c){if(e){e.toElement(c);}else{d.scrollTo(d.getScroll().x,c.getPosition(d).y-20);}}}return a;},watchFields:function(a){a.each(function(b){if(this.options.evaluateFieldsOnBlur){b.addEvent("blur",this.validationMonitor.pass([b,false,this.options.scrollToErrorsOnBlur],this));}if(this.options.evaluateFieldsOnChange){b.addEvent("change",this.validationMonitor.pass([b,true,this.options.scrollToErrorsOnChange],this));}},this);}});Form.Validator.addAllThese([["validate-enforce-oncheck",{test:function(a,b){var c=a.getParent("form").retrieve("validator");if(!c){return true;}(b.toEnforce||document.id(b.enforceChildrenOf).getElements("input, select, textarea")).map(function(d){if(a.checked){c.enforceField(d);}else{c.ignoreField(d);c.resetField(d);}});return true;}}],["validate-ignore-oncheck",{test:function(a,b){var c=a.getParent("form").retrieve("validator");if(!c){return true;}(b.toIgnore||document.id(b.ignoreChildrenOf).getElements("input, select, textarea")).each(function(d){if(a.checked){c.ignoreField(d);c.resetField(d);}else{c.enforceField(d);}});return true;}}],["validate-nospace",{errorMsg:function(){return Form.Validator.getMsg("noSpace");},test:function(a,b){return!a.get("value").test(/\s/);}}],["validate-toggle-oncheck",{test:function(b,c){var d=b.getParent("form").retrieve("validator");if(!d){return true;}var a=c.toToggle||document.id(c.toToggleChildrenOf).getElements("input, select, textarea");if(!b.checked){a.each(function(e){d.ignoreField(e);d.resetField(e);});}else{a.each(function(e){d.enforceField(e);});}return true;}}],["validate-reqchk-bynode",{errorMsg:function(){return Form.Validator.getMsg("reqChkByNode");},test:function(a,b){return(document.id(b.nodeId).getElements(b.selector||"input[type=checkbox], input[type=radio]")).some(function(c){return c.checked;});}}],["validate-required-check",{errorMsg:function(a,b){return b.useTitle?a.get("title"):Form.Validator.getMsg("requiredChk");},test:function(a,b){return!!a.checked;}}],["validate-reqchk-byname",{errorMsg:function(a,b){return Form.Validator.getMsg("reqChkByName").substitute({label:b.label||a.get("type")});},test:function(b,d){var c=d.groupName||b.get("name");var a=$$(document.getElementsByName(c)).some(function(g,f){return g.checked;});var e=b.getParent("form").retrieve("validator");if(a&&e){e.resetField(b);}return a;}}],["validate-match",{errorMsg:function(a,b){return Form.Validator.getMsg("match").substitute({matchName:b.matchName||document.id(b.matchInput).get("name")});},test:function(b,c){var d=b.get("value");var a=document.id(c.matchInput)&&document.id(c.matchInput).get("value");return d&&a?d==a:true;}}],["validate-after-date",{errorMsg:function(a,b){return Form.Validator.getMsg("afterDate").substitute({label:b.afterLabel||(b.afterElement?Form.Validator.getMsg("startDate"):Form.Validator.getMsg("currentDate"))});},test:function(b,c){var d=document.id(c.afterElement)?Date.parse(document.id(c.afterElement).get("value")):new Date();var a=Date.parse(b.get("value"));return a&&d?a>=d:true;}}],["validate-before-date",{errorMsg:function(a,b){return Form.Validator.getMsg("beforeDate").substitute({label:b.beforeLabel||(b.beforeElement?Form.Validator.getMsg("endDate"):Form.Validator.getMsg("currentDate"))});},test:function(b,c){var d=Date.parse(b.get("value"));var a=document.id(c.beforeElement)?Date.parse(document.id(c.beforeElement).get("value")):new Date();return a&&d?a>=d:true;}}],["validate-custom-required",{errorMsg:function(){return Form.Validator.getMsg("required");},test:function(a,b){return a.get("value")!=b.emptyValue;}}],["validate-same-month",{errorMsg:function(a,b){var c=document.id(b.sameMonthAs)&&document.id(b.sameMonthAs).get("value");var d=a.get("value");if(d!=""){return Form.Validator.getMsg(c?"sameMonth":"startMonth");}},test:function(a,b){var d=Date.parse(a.get("value"));var c=Date.parse(document.id(b.sameMonthAs)&&document.id(b.sameMonthAs).get("value"));return d&&c?d.format("%B")==c.format("%B"):true;}}],["validate-cc-num",{errorMsg:function(a){var b=a.get("value").replace(/[^0-9]/g,"");return Form.Validator.getMsg("creditcard").substitute({length:b.length});},test:function(c){if(Form.Validator.getValidator("IsEmpty").test(c)){return true;}var g=c.get("value");g=g.replace(/[^0-9]/g,"");var a=false;if(g.test(/^4[0-9]{12}([0-9]{3})?$/)){a="Visa";}else{if(g.test(/^5[1-5]([0-9]{14})$/)){a="Master Card";}else{if(g.test(/^3[47][0-9]{13}$/)){a="American Express";}else{if(g.test(/^6011[0-9]{12}$/)){a="Discover";}}}}if(a){var d=0;var e=0;for(var b=g.length-1;b>=0;--b){e=g.charAt(b).toInt();if(e==0){continue;}if((g.length-b)%2==0){e+=e;}if(e>9){e=e.toString().charAt(0).toInt()+e.toString().charAt(1).toInt();}d+=e;}if((d%10)==0){return true;}}var f="";while(g!=""){f+=" "+g.substr(0,4);g=g.substr(4);}c.getParent("form").retrieve("validator").ignoreField(c);c.set("value",f.clean());c.getParent("form").retrieve("validator").enforceField(c);return false;}}]]);var OverText=new Class({Implements:[Options,Events,Class.Occlude],Binds:["reposition","assert","focus","hide"],options:{element:"label",labelClass:"overTxtLabel",positionOptions:{position:"upperLeft",edge:"upperLeft",offset:{x:4,y:2}},poll:false,pollInterval:250,wrap:false},property:"OverText",initialize:function(b,a){b=this.element=document.id(b);if(this.occlude()){return this.occluded;}this.setOptions(a);this.attach(b);OverText.instances.push(this);if(this.options.poll){this.poll();}},toElement:function(){return this.element;},attach:function(){var b=this.element,a=this.options,c=a.textOverride||b.get("alt")||b.get("title");if(!c){return this;}var d=this.text=new Element(a.element,{"class":a.labelClass,styles:{lineHeight:"normal",position:"absolute",cursor:"text"},html:c,events:{click:this.hide.pass(a.element=="label",this)}}).inject(b,"after");if(a.element=="label"){if(!b.get("id")){b.set("id","input_"+String.uniqueID());}d.set("for",b.get("id"));}if(a.wrap){this.textHolder=new Element("div.overTxtWrapper",{styles:{lineHeight:"normal",position:"relative"}}).grab(d).inject(b,"before");}return this.enable();},destroy:function(){this.element.eliminate(this.property);this.disable();if(this.text){this.text.destroy();}if(this.textHolder){this.textHolder.destroy();}return this;},disable:function(){this.element.removeEvents({focus:this.focus,blur:this.assert,change:this.assert});window.removeEvent("resize",this.reposition);this.hide(true,true);return this;},enable:function(){this.element.addEvents({focus:this.focus,blur:this.assert,change:this.assert});window.addEvent("resize",this.reposition);this.reposition();return this;},wrap:function(){if(this.options.element=="label"){if(!this.element.get("id")){this.element.set("id","input_"+String.uniqueID());}this.text.set("for",this.element.get("id"));}},startPolling:function(){this.pollingPaused=false;return this.poll();},poll:function(a){if(this.poller&&!a){return this;}if(a){clearInterval(this.poller);}else{this.poller=(function(){if(!this.pollingPaused){this.assert(true);}}).periodical(this.options.pollInterval,this);}return this;},stopPolling:function(){this.pollingPaused=true;return this.poll(true);},focus:function(){if(this.text&&(!this.text.isDisplayed()||this.element.get("disabled"))){return this;}return this.hide();},hide:function(c,a){if(this.text&&(this.text.isDisplayed()&&(!this.element.get("disabled")||a))){this.text.hide();this.fireEvent("textHide",[this.text,this.element]);this.pollingPaused=true;if(!c){try{this.element.fireEvent("focus");this.element.focus();}catch(b){}}}return this;},show:function(){if(this.text&&!this.text.isDisplayed()){this.text.show();this.reposition();this.fireEvent("textShow",[this.text,this.element]);this.pollingPaused=false;}return this;},test:function(){return!this.element.get("value");},assert:function(a){return this[this.test()?"show":"hide"](a);},reposition:function(){this.assert(true);if(!this.element.isVisible()){return this.stopPolling().hide();}if(this.text&&this.test()){this.text.position(Object.merge(this.options.positionOptions,{relativeTo:this.element}));}return this;}});OverText.instances=[];Object.append(OverText,{each:function(a){return OverText.instances.each(function(c,b){if(c.element&&c.text){a.call(OverText,c,b);}});},update:function(){return OverText.each(function(a){return a.reposition();});},hideAll:function(){return OverText.each(function(a){return a.hide(true,true);});},showAll:function(){return OverText.each(function(a){return a.show();});}});Fx.Elements=new Class({Extends:Fx.CSS,initialize:function(b,a){this.elements=this.subject=$$(b);this.parent(a);},compute:function(g,h,j){var c={};for(var d in g){var a=g[d],e=h[d],f=c[d]={};for(var b in a){f[b]=this.parent(a[b],e[b],j);}}return c;},set:function(b){for(var c in b){if(!this.elements[c]){continue;}var a=b[c];for(var d in a){this.render(this.elements[c],d,a[d],this.options.unit);}}return this;},start:function(c){if(!this.check(c)){return this;}var h={},j={};for(var d in c){if(!this.elements[d]){continue;}var f=c[d],a=h[d]={},g=j[d]={};for(var b in f){var e=this.prepare(this.elements[d],b,f[b]);a[b]=e.from;g[b]=e.to;}}return this.parent(h,j);}});Fx.Accordion=new Class({Extends:Fx.Elements,options:{fixedHeight:false,fixedWidth:false,display:0,show:false,height:true,width:false,opacity:true,alwaysHide:false,trigger:"click",initialDisplayFx:true,resetHeight:true},initialize:function(){var g=function(h){return h!=null;};var f=Array.link(arguments,{container:Type.isElement,options:Type.isObject,togglers:g,elements:g});this.parent(f.elements,f.options);var b=this.options,e=this.togglers=$$(f.togglers);this.previous=-1;this.internalChain=new Chain();if(b.alwaysHide){this.options.link="chain";}if(b.show||this.options.show===0){b.display=false;this.previous=b.show;}if(b.start){b.display=false;b.show=false;}var d=this.effects={};if(b.opacity){d.opacity="fullOpacity";}if(b.width){d.width=b.fixedWidth?"fullWidth":"offsetWidth";}if(b.height){d.height=b.fixedHeight?"fullHeight":"scrollHeight";}for(var c=0,a=e.length;c<a;c++){this.addSection(e[c],this.elements[c]);}this.elements.each(function(j,h){if(b.show===h){this.fireEvent("active",[e[h],j]);}else{for(var k in d){j.setStyle(k,0);}}},this);if(b.display||b.display===0||b.initialDisplayFx===false){this.display(b.display,b.initialDisplayFx);}if(b.fixedHeight!==false){b.resetHeight=false;}this.addEvent("complete",this.internalChain.callChain.bind(this.internalChain));},addSection:function(g,d){g=document.id(g);d=document.id(d);this.togglers.include(g);this.elements.include(d);var f=this.togglers,c=this.options,h=f.contains(g),a=f.indexOf(g),b=this.display.pass(a,this);g.store("accordion:display",b).addEvent(c.trigger,b);if(c.height){d.setStyles({"padding-top":0,"border-top":"none","padding-bottom":0,"border-bottom":"none"});}if(c.width){d.setStyles({"padding-left":0,"border-left":"none","padding-right":0,"border-right":"none"});}d.fullOpacity=1;if(c.fixedWidth){d.fullWidth=c.fixedWidth;}if(c.fixedHeight){d.fullHeight=c.fixedHeight;}d.setStyle("overflow","hidden");if(!h){for(var e in this.effects){d.setStyle(e,0);}}return this;},removeSection:function(f,b){var e=this.togglers,a=e.indexOf(f),c=this.elements[a];var d=function(){e.erase(f);this.elements.erase(c);this.detach(f);}.bind(this);if(this.now==a||b!=null){this.display(b!=null?b:(a-1>=0?a-1:0)).chain(d);}else{d();}return this;},detach:function(b){var a=function(c){c.removeEvent(this.options.trigger,c.retrieve("accordion:display"));}.bind(this);if(!b){this.togglers.each(a);}else{a(b);}return this;},display:function(b,c){if(!this.check(b,c)){return this;}var h={},g=this.elements,a=this.options,f=this.effects;if(c==null){c=true;}if(typeOf(b)=="element"){b=g.indexOf(b);}if(b==this.previous&&!a.alwaysHide){return this;}if(a.resetHeight){var e=g[this.previous];if(e&&!this.selfHidden){for(var d in f){e.setStyle(d,e[f[d]]);}}}if((this.timer&&a.link=="chain")||(b===this.previous&&!a.alwaysHide)){return this;}this.previous=b;this.selfHidden=false;g.each(function(l,k){h[k]={};var j;if(k!=b){j=true;}else{if(a.alwaysHide&&((l.offsetHeight>0&&a.height)||l.offsetWidth>0&&a.width)){j=true;this.selfHidden=true;}}this.fireEvent(j?"background":"active",[this.togglers[k],l]);for(var m in f){h[k][m]=j?0:l[f[m]];}if(!c&&!j&&a.resetHeight){h[k].height="auto";}},this);this.internalChain.clearChain();this.internalChain.chain(function(){if(a.resetHeight&&!this.selfHidden){var i=g[b];if(i){i.setStyle("height","auto");}}}.bind(this));return c?this.start(h):this.set(h).internalChain.callChain();}});Fx.Move=new Class({Extends:Fx.Morph,options:{relativeTo:document.body,position:"center",edge:false,offset:{x:0,y:0}},start:function(a){var b=this.element,c=b.getStyles("top","left");if(c.top=="auto"||c.left=="auto"){b.setPosition(b.getPosition(b.getOffsetParent()));}return this.parent(b.position(Object.merge({},this.options,a,{returnPos:true})));}});Element.Properties.move={set:function(a){this.get("move").cancel().setOptions(a);return this;},get:function(){var a=this.retrieve("move");if(!a){a=new Fx.Move(this,{link:"cancel"});this.store("move",a);}return a;}};Element.implement({move:function(a){this.get("move").start(a);return this;}});(function(){Fx.Scroll=new Class({Extends:Fx,options:{offset:{x:0,y:0},wheelStops:true},initialize:function(c,b){this.element=this.subject=document.id(c);this.parent(b);if(typeOf(this.element)!="element"){this.element=document.id(this.element.getDocument().body);}if(this.options.wheelStops){var d=this.element,e=this.cancel.pass(false,this);this.addEvent("start",function(){d.addEvent("mousewheel",e);},true);this.addEvent("complete",function(){d.removeEvent("mousewheel",e);},true);}},set:function(){var b=Array.flatten(arguments);if(Browser.firefox){b=[Math.round(b[0]),Math.round(b[1])];}this.element.scrollTo(b[0],b[1]);return this;},compute:function(d,c,b){return[0,1].map(function(e){return Fx.compute(d[e],c[e],b);});},start:function(c,d){if(!this.check(c,d)){return this;}var b=this.element.getScroll();return this.parent([b.x,b.y],[c,d]);},calculateScroll:function(g,f){var d=this.element,b=d.getScrollSize(),h=d.getScroll(),j=d.getSize(),c=this.options.offset,i={x:g,y:f};for(var e in i){if(!i[e]&&i[e]!==0){i[e]=h[e];}if(typeOf(i[e])!="number"){i[e]=b[e]-j[e];}i[e]+=c[e];}return[i.x,i.y];},toTop:function(){return this.start.apply(this,this.calculateScroll(false,0));},toLeft:function(){return this.start.apply(this,this.calculateScroll(0,false));},toRight:function(){return this.start.apply(this,this.calculateScroll("right",false));},toBottom:function(){return this.start.apply(this,this.calculateScroll(false,"bottom"));},toElement:function(d,e){e=e?Array.from(e):["x","y"];var c=a(this.element)?{x:0,y:0}:this.element.getScroll();var b=Object.map(document.id(d).getPosition(this.element),function(g,f){return e.contains(f)?g+c[f]:false;});return this.start.apply(this,this.calculateScroll(b.x,b.y));},toElementEdge:function(d,g,e){g=g?Array.from(g):["x","y"];d=document.id(d);var i={},f=d.getPosition(this.element),j=d.getSize(),h=this.element.getScroll(),b=this.element.getSize(),c={x:f.x+j.x,y:f.y+j.y};["x","y"].each(function(k){if(g.contains(k)){if(c[k]>h[k]+b[k]){i[k]=c[k]-b[k];}if(f[k]<h[k]){i[k]=f[k];}}if(i[k]==null){i[k]=h[k];}if(e&&e[k]){i[k]=i[k]+e[k];}},this);if(i.x!=h.x||i.y!=h.y){this.start(i.x,i.y);}return this;},toElementCenter:function(e,f,h){f=f?Array.from(f):["x","y"];e=document.id(e);var i={},c=e.getPosition(this.element),d=e.getSize(),b=this.element.getScroll(),g=this.element.getSize();["x","y"].each(function(j){if(f.contains(j)){i[j]=c[j]-(g[j]-d[j])/2;}if(i[j]==null){i[j]=b[j];}if(h&&h[j]){i[j]=i[j]+h[j];}},this);if(i.x!=b.x||i.y!=b.y){this.start(i.x,i.y);}return this;}});function a(b){return(/^(?:body|html)$/i).test(b.tagName);}})();Fx.Slide=new Class({Extends:Fx,options:{mode:"vertical",wrapper:false,hideOverflow:true,resetHeight:false},initialize:function(b,a){b=this.element=this.subject=document.id(b);this.parent(a);a=this.options;var d=b.retrieve("wrapper"),c=b.getStyles("margin","position","overflow");if(a.hideOverflow){c=Object.append(c,{overflow:"hidden"});}if(a.wrapper){d=document.id(a.wrapper).setStyles(c);}if(!d){d=new Element("div",{styles:c}).wraps(b);}b.store("wrapper",d).setStyle("margin",0);if(b.getStyle("overflow")=="visible"){b.setStyle("overflow","hidden");}this.now=[];this.open=true;this.wrapper=d;this.addEvent("complete",function(){this.open=(d["offset"+this.layout.capitalize()]!=0);if(this.open&&this.options.resetHeight){d.setStyle("height","");}},true);},vertical:function(){this.margin="margin-top";this.layout="height";this.offset=this.element.offsetHeight;},horizontal:function(){this.margin="margin-left";this.layout="width";this.offset=this.element.offsetWidth;},set:function(a){this.element.setStyle(this.margin,a[0]);this.wrapper.setStyle(this.layout,a[1]);return this;},compute:function(c,b,a){return[0,1].map(function(d){return Fx.compute(c[d],b[d],a);});},start:function(b,e){if(!this.check(b,e)){return this;}this[e||this.options.mode]();var d=this.element.getStyle(this.margin).toInt(),c=this.wrapper.getStyle(this.layout).toInt(),a=[[d,c],[0,this.offset]],g=[[d,c],[-this.offset,0]],f;switch(b){case"in":f=a;break;case"out":f=g;break;case"toggle":f=(c==0)?a:g;}return this.parent(f[0],f[1]);},slideIn:function(a){return this.start("in",a);},slideOut:function(a){return this.start("out",a);},hide:function(a){this[a||this.options.mode]();this.open=false;return this.set([-this.offset,0]);},show:function(a){this[a||this.options.mode]();this.open=true;return this.set([0,this.offset]);},toggle:function(a){return this.start("toggle",a);}});Element.Properties.slide={set:function(a){this.get("slide").cancel().setOptions(a);return this;},get:function(){var a=this.retrieve("slide");if(!a){a=new Fx.Slide(this,{link:"cancel"});this.store("slide",a);}return a;}};Element.implement({slide:function(d,e){d=d||"toggle";var b=this.get("slide"),a;switch(d){case"hide":b.hide(e);break;case"show":b.show(e);break;case"toggle":var c=this.retrieve("slide:flag",b.open);b[c?"slideOut":"slideIn"](e);this.store("slide:flag",!c);a=true;break;default:b.start(d,e);}if(!a){this.eliminate("slide:flag");}return this;}});Fx.SmoothScroll=new Class({Extends:Fx.Scroll,options:{axes:["x","y"]},initialize:function(c,d){d=d||document;this.doc=d.getDocument();this.parent(this.doc,c);var e=d.getWindow(),a=e.location.href.match(/^[^#]*/)[0]+"#",b=$$(this.options.links||this.doc.links);b.each(function(g){if(g.href.indexOf(a)!=0){return;}var f=g.href.substr(a.length);if(f){this.useLink(g,f);}},this);this.addEvent("complete",function(){e.location.hash=this.anchor;this.element.scrollTo(this.to[0],this.to[1]);},true);},useLink:function(b,a){b.addEvent("click",function(d){var c=document.id(a)||this.doc.getElement("a[name="+a+"]");if(!c){return;}d.preventDefault();this.toElement(c,this.options.axes).chain(function(){this.fireEvent("scrolledTo",[b,c]);}.bind(this));this.anchor=a;}.bind(this));return this;}});Fx.Sort=new Class({Extends:Fx.Elements,options:{mode:"vertical"},initialize:function(b,a){this.parent(b,a);this.elements.each(function(c){if(c.getStyle("position")=="static"){c.setStyle("position","relative");}});this.setDefaultOrder();},setDefaultOrder:function(){this.currentOrder=this.elements.map(function(b,a){return a;});},sort:function(){if(!this.check(arguments)){return this;}var e=Array.flatten(arguments);var i=0,a=0,c={},h={},d=this.options.mode=="vertical";var f=this.elements.map(function(m,k){var l=m.getComputedSize({styles:["border","padding","margin"]});var n;if(d){n={top:i,margin:l["margin-top"],height:l.totalHeight};i+=n.height-l["margin-top"];}else{n={left:a,margin:l["margin-left"],width:l.totalWidth};a+=n.width;}var j=d?"top":"left";h[k]={};var o=m.getStyle(j).toInt();h[k][j]=o||0;return n;},this);this.set(h);e=e.map(function(j){return j.toInt();});if(e.length!=this.elements.length){this.currentOrder.each(function(j){if(!e.contains(j)){e.push(j);}});if(e.length>this.elements.length){e.splice(this.elements.length-1,e.length-this.elements.length);}}var b=0;i=a=0;e.each(function(k){var j={};if(d){j.top=i-f[k].top-b;i+=f[k].height;}else{j.left=a-f[k].left;a+=f[k].width;}b=b+f[k].margin;c[k]=j;},this);var g={};Array.clone(e).sort().each(function(j){g[j]=c[j];});this.start(g);this.currentOrder=e;return this;},rearrangeDOM:function(a){a=a||this.currentOrder;var b=this.elements[0].getParent();var c=[];this.elements.setStyle("opacity",0);a.each(function(d){c.push(this.elements[d].inject(b).setStyles({top:0,left:0}));},this);this.elements.setStyle("opacity",1);this.elements=$$(c);this.setDefaultOrder();return this;},getDefaultOrder:function(){return this.elements.map(function(b,a){return a;});},getCurrentOrder:function(){return this.currentOrder;},forward:function(){return this.sort(this.getDefaultOrder());},backward:function(){return this.sort(this.getDefaultOrder().reverse());},reverse:function(){return this.sort(this.currentOrder.reverse());},sortByElements:function(a){return this.sort(a.map(function(b){return this.elements.indexOf(b);},this));},swap:function(c,b){if(typeOf(c)=="element"){c=this.elements.indexOf(c);}if(typeOf(b)=="element"){b=this.elements.indexOf(b);}var a=Array.clone(this.currentOrder);a[this.currentOrder.indexOf(c)]=b;a[this.currentOrder.indexOf(b)]=c;return this.sort(a);}});var Drag=new Class({Implements:[Events,Options],options:{snap:6,unit:"px",grid:false,style:true,limit:false,handle:false,invert:false,preventDefault:false,stopPropagation:false,modifiers:{x:"left",y:"top"}},initialize:function(){var b=Array.link(arguments,{options:Type.isObject,element:function(c){return c!=null;}});this.element=document.id(b.element);this.document=this.element.getDocument();this.setOptions(b.options||{});var a=typeOf(this.options.handle);this.handles=((a=="array"||a=="collection")?$$(this.options.handle):document.id(this.options.handle))||this.element;this.mouse={now:{},pos:{}};this.value={start:{},now:{}};this.selection=(Browser.ie)?"selectstart":"mousedown";if(Browser.ie&&!Drag.ondragstartFixed){document.ondragstart=Function.from(false);Drag.ondragstartFixed=true;}this.bound={start:this.start.bind(this),check:this.check.bind(this),drag:this.drag.bind(this),stop:this.stop.bind(this),cancel:this.cancel.bind(this),eventStop:Function.from(false)};this.attach();},attach:function(){this.handles.addEvent("mousedown",this.bound.start);return this;},detach:function(){this.handles.removeEvent("mousedown",this.bound.start);return this;},start:function(a){var j=this.options;if(a.rightClick){return;}if(j.preventDefault){a.preventDefault();}if(j.stopPropagation){a.stopPropagation();}this.mouse.start=a.page;this.fireEvent("beforeStart",this.element);var c=j.limit;this.limit={x:[],y:[]};var e,g;for(e in j.modifiers){if(!j.modifiers[e]){continue;}var b=this.element.getStyle(j.modifiers[e]);if(b&&!b.match(/px$/)){if(!g){g=this.element.getCoordinates(this.element.getOffsetParent());}b=g[j.modifiers[e]];}if(j.style){this.value.now[e]=(b||0).toInt();}else{this.value.now[e]=this.element[j.modifiers[e]];}if(j.invert){this.value.now[e]*=-1;}this.mouse.pos[e]=a.page[e]-this.value.now[e];if(c&&c[e]){var d=2;while(d--){var f=c[e][d];if(f||f===0){this.limit[e][d]=(typeof f=="function")?f():f;}}}}if(typeOf(this.options.grid)=="number"){this.options.grid={x:this.options.grid,y:this.options.grid};}var h={mousemove:this.bound.check,mouseup:this.bound.cancel};h[this.selection]=this.bound.eventStop;this.document.addEvents(h);},check:function(a){if(this.options.preventDefault){a.preventDefault();}var b=Math.round(Math.sqrt(Math.pow(a.page.x-this.mouse.start.x,2)+Math.pow(a.page.y-this.mouse.start.y,2)));if(b>this.options.snap){this.cancel();this.document.addEvents({mousemove:this.bound.drag,mouseup:this.bound.stop});this.fireEvent("start",[this.element,a]).fireEvent("snap",this.element);}},drag:function(b){var a=this.options;if(a.preventDefault){b.preventDefault();}this.mouse.now=b.page;for(var c in a.modifiers){if(!a.modifiers[c]){continue;}this.value.now[c]=this.mouse.now[c]-this.mouse.pos[c];if(a.invert){this.value.now[c]*=-1;}if(a.limit&&this.limit[c]){if((this.limit[c][1]||this.limit[c][1]===0)&&(this.value.now[c]>this.limit[c][1])){this.value.now[c]=this.limit[c][1];}else{if((this.limit[c][0]||this.limit[c][0]===0)&&(this.value.now[c]<this.limit[c][0])){this.value.now[c]=this.limit[c][0];}}}if(a.grid[c]){this.value.now[c]-=((this.value.now[c]-(this.limit[c][0]||0))%a.grid[c]);}if(a.style){this.element.setStyle(a.modifiers[c],this.value.now[c]+a.unit);}else{this.element[a.modifiers[c]]=this.value.now[c];}}this.fireEvent("drag",[this.element,b]);},cancel:function(a){this.document.removeEvents({mousemove:this.bound.check,mouseup:this.bound.cancel});if(a){this.document.removeEvent(this.selection,this.bound.eventStop);this.fireEvent("cancel",this.element);}},stop:function(b){var a={mousemove:this.bound.drag,mouseup:this.bound.stop};a[this.selection]=this.bound.eventStop;this.document.removeEvents(a);if(b){this.fireEvent("complete",[this.element,b]);}}});Element.implement({makeResizable:function(a){var b=new Drag(this,Object.merge({modifiers:{x:"width",y:"height"}},a));this.store("resizer",b);return b.addEvent("drag",function(){this.fireEvent("resize",b);}.bind(this));}});Drag.Move=new Class({Extends:Drag,options:{droppables:[],container:false,precalculate:false,includeMargins:true,checkDroppables:true},initialize:function(b,a){this.parent(b,a);b=this.element;this.droppables=$$(this.options.droppables);this.container=document.id(this.options.container);if(this.container&&typeOf(this.container)!="element"){this.container=document.id(this.container.getDocument().body);}if(this.options.style){if(this.options.modifiers.x=="left"&&this.options.modifiers.y=="top"){var c=b.getOffsetParent(),d=b.getStyles("left","top");if(c&&(d.left=="auto"||d.top=="auto")){b.setPosition(b.getPosition(c));}}if(b.getStyle("position")=="static"){b.setStyle("position","absolute");}}this.addEvent("start",this.checkDroppables,true);this.overed=null;},start:function(a){if(this.container){this.options.limit=this.calculateLimit();}if(this.options.precalculate){this.positions=this.droppables.map(function(b){return b.getCoordinates();});}this.parent(a);},calculateLimit:function(){var j=this.element,e=this.container,d=document.id(j.getOffsetParent())||document.body,h=e.getCoordinates(d),c={},b={},k={},g={},m={};["top","right","bottom","left"].each(function(q){c[q]=j.getStyle("margin-"+q).toInt();b[q]=j.getStyle("border-"+q).toInt();k[q]=e.getStyle("margin-"+q).toInt();g[q]=e.getStyle("border-"+q).toInt();m[q]=d.getStyle("padding-"+q).toInt();},this);var f=j.offsetWidth+c.left+c.right,p=j.offsetHeight+c.top+c.bottom,i=0,l=0,o=h.right-g.right-f,a=h.bottom-g.bottom-p;if(this.options.includeMargins){i+=c.left;l+=c.top;}else{o+=c.right;a+=c.bottom;}if(j.getStyle("position")=="relative"){var n=j.getCoordinates(d);n.left-=j.getStyle("left").toInt();n.top-=j.getStyle("top").toInt();i-=n.left;l-=n.top;if(e.getStyle("position")!="relative"){i+=g.left;l+=g.top;}o+=c.left-n.left;a+=c.top-n.top;if(e!=d){i+=k.left+m.left;l+=((Browser.ie6||Browser.ie7)?0:k.top)+m.top;}}else{i-=c.left;l-=c.top;if(e!=d){i+=h.left+g.left;l+=h.top+g.top;}}return{x:[i,o],y:[l,a]};},getDroppableCoordinates:function(c){var b=c.getCoordinates();if(c.getStyle("position")=="fixed"){var a=window.getScroll();b.left+=a.x;b.right+=a.x;b.top+=a.y;b.bottom+=a.y;}return b;},checkDroppables:function(){var a=this.droppables.filter(function(d,c){d=this.positions?this.positions[c]:this.getDroppableCoordinates(d);var b=this.mouse.now;return(b.x>d.left&&b.x<d.right&&b.y<d.bottom&&b.y>d.top);},this).getLast();if(this.overed!=a){if(this.overed){this.fireEvent("leave",[this.element,this.overed]);}if(a){this.fireEvent("enter",[this.element,a]);}this.overed=a;}},drag:function(a){this.parent(a);if(this.options.checkDroppables&&this.droppables.length){this.checkDroppables();}},stop:function(a){this.checkDroppables();this.fireEvent("drop",[this.element,this.overed,a]);this.overed=null;return this.parent(a);}});Element.implement({makeDraggable:function(a){var b=new Drag.Move(this,a);this.store("dragger",b);return b;}});var Slider=new Class({Implements:[Events,Options],Binds:["clickedElement","draggedKnob","scrolledElement"],options:{onTick:function(a){this.setKnobPosition(a);},initialStep:0,snap:false,offset:0,range:false,wheel:false,steps:100,mode:"horizontal"},initialize:function(f,a,e){this.setOptions(e);e=this.options;this.element=document.id(f);a=this.knob=document.id(a);this.previousChange=this.previousEnd=this.step=-1;var b={},d={x:false,y:false};switch(e.mode){case"vertical":this.axis="y";this.property="top";this.offset="offsetHeight";break;case"horizontal":this.axis="x";this.property="left";this.offset="offsetWidth";}this.setSliderDimensions();this.setRange(e.range);if(a.getStyle("position")=="static"){a.setStyle("position","relative");}a.setStyle(this.property,-e.offset);d[this.axis]=this.property;b[this.axis]=[-e.offset,this.full-e.offset];var c={snap:0,limit:b,modifiers:d,onDrag:this.draggedKnob,onStart:this.draggedKnob,onBeforeStart:(function(){this.isDragging=true;}).bind(this),onCancel:function(){this.isDragging=false;}.bind(this),onComplete:function(){this.isDragging=false;this.draggedKnob();this.end();}.bind(this)};if(e.snap){this.setSnap(c);}this.drag=new Drag(a,c);this.attach();if(e.initialStep!=null){this.set(e.initialStep);}},attach:function(){this.element.addEvent("mousedown",this.clickedElement);if(this.options.wheel){this.element.addEvent("mousewheel",this.scrolledElement);}this.drag.attach();return this;},detach:function(){this.element.removeEvent("mousedown",this.clickedElement).removeEvent("mousewheel",this.scrolledElement);this.drag.detach();return this;},autosize:function(){this.setSliderDimensions().setKnobPosition(this.toPosition(this.step));this.drag.options.limit[this.axis]=[-this.options.offset,this.full-this.options.offset];if(this.options.snap){this.setSnap();}return this;},setSnap:function(a){if(!a){a=this.drag.options;}a.grid=Math.ceil(this.stepWidth);a.limit[this.axis][1]=this.full;return this;},setKnobPosition:function(a){if(this.options.snap){a=this.toPosition(this.step);}this.knob.setStyle(this.property,a);return this;},setSliderDimensions:function(){this.full=this.element.measure(function(){this.half=this.knob[this.offset]/2;return this.element[this.offset]-this.knob[this.offset]+(this.options.offset*2);}.bind(this));return this;},set:function(a){if(!((this.range>0)^(a<this.min))){a=this.min;}if(!((this.range>0)^(a>this.max))){a=this.max;}this.step=Math.round(a);return this.checkStep().fireEvent("tick",this.toPosition(this.step)).end();},setRange:function(a,b){this.min=Array.pick([a[0],0]);this.max=Array.pick([a[1],this.options.steps]);this.range=this.max-this.min;this.steps=this.options.steps||this.full;this.stepSize=Math.abs(this.range)/this.steps;this.stepWidth=this.stepSize*this.full/Math.abs(this.range);if(a){this.set(Array.pick([b,this.step]).floor(this.min).max(this.max));}return this;},clickedElement:function(c){if(this.isDragging||c.target==this.knob){return;}var b=this.range<0?-1:1,a=c.page[this.axis]-this.element.getPosition()[this.axis]-this.half;a=a.limit(-this.options.offset,this.full-this.options.offset);this.step=Math.round(this.min+b*this.toStep(a));this.checkStep().fireEvent("tick",a).end();},scrolledElement:function(a){var b=(this.options.mode=="horizontal")?(a.wheel<0):(a.wheel>0);this.set(this.step+(b?-1:1)*this.stepSize);a.stop();},draggedKnob:function(){var b=this.range<0?-1:1,a=this.drag.value.now[this.axis];a=a.limit(-this.options.offset,this.full-this.options.offset);this.step=Math.round(this.min+b*this.toStep(a));this.checkStep();},checkStep:function(){var a=this.step;if(this.previousChange!=a){this.previousChange=a;this.fireEvent("change",a);}return this;},end:function(){var a=this.step;if(this.previousEnd!==a){this.previousEnd=a;this.fireEvent("complete",a+"");}return this;},toStep:function(a){var b=(a+this.options.offset)*this.stepSize/this.full*this.steps;return this.options.steps?Math.round(b-=b%this.stepSize):b;},toPosition:function(a){return(this.full*Math.abs(this.min-a))/(this.steps*this.stepSize)-this.options.offset;}});var Sortables=new Class({Implements:[Events,Options],options:{opacity:1,clone:false,revert:false,handle:false,dragOptions:{}},initialize:function(a,b){this.setOptions(b);this.elements=[];this.lists=[];this.idle=true;this.addLists($$(document.id(a)||a));if(!this.options.clone){this.options.revert=false;}if(this.options.revert){this.effect=new Fx.Morph(null,Object.merge({duration:250,link:"cancel"},this.options.revert));}},attach:function(){this.addLists(this.lists);return this;},detach:function(){this.lists=this.removeLists(this.lists);return this;},addItems:function(){Array.flatten(arguments).each(function(a){this.elements.push(a);var b=a.retrieve("sortables:start",function(c){this.start.call(this,c,a);}.bind(this));(this.options.handle?a.getElement(this.options.handle)||a:a).addEvent("mousedown",b);},this);return this;},addLists:function(){Array.flatten(arguments).each(function(a){this.lists.include(a);this.addItems(a.getChildren());},this);return this;},removeItems:function(){return $$(Array.flatten(arguments).map(function(a){this.elements.erase(a);var b=a.retrieve("sortables:start");(this.options.handle?a.getElement(this.options.handle)||a:a).removeEvent("mousedown",b);return a;},this));},removeLists:function(){return $$(Array.flatten(arguments).map(function(a){this.lists.erase(a);this.removeItems(a.getChildren());return a;},this));},getClone:function(b,a){if(!this.options.clone){return new Element(a.tagName).inject(document.body);}if(typeOf(this.options.clone)=="function"){return this.options.clone.call(this,b,a,this.list);}var c=a.clone(true).setStyles({margin:0,position:"absolute",visibility:"hidden",width:a.getStyle("width")}).addEvent("mousedown",function(d){a.fireEvent("mousedown",d);});if(c.get("html").test("radio")){c.getElements("input[type=radio]").each(function(d,e){d.set("name","clone_"+e);if(d.get("checked")){a.getElements("input[type=radio]")[e].set("checked",true);}});}return c.inject(this.list).setPosition(a.getPosition(a.getOffsetParent()));},getDroppables:function(){var a=this.list.getChildren().erase(this.clone).erase(this.element);if(!this.options.constrain){a.append(this.lists).erase(this.list);}return a;},insert:function(c,b){var a="inside";if(this.lists.contains(b)){this.list=b;this.drag.droppables=this.getDroppables();}else{a=this.element.getAllPrevious().contains(b)?"before":"after";}this.element.inject(b,a);this.fireEvent("sort",[this.element,this.clone]);},start:function(b,a){if(!this.idle||b.rightClick||["button","input","a","textarea"].contains(b.target.get("tag"))){return;}this.idle=false;this.element=a;this.opacity=a.getStyle("opacity");this.list=a.getParent();this.clone=this.getClone(b,a);this.drag=new Drag.Move(this.clone,Object.merge({droppables:this.getDroppables()},this.options.dragOptions)).addEvents({onSnap:function(){b.stop();this.clone.setStyle("visibility","visible");this.element.setStyle("opacity",this.options.opacity||0);this.fireEvent("start",[this.element,this.clone]);}.bind(this),onEnter:this.insert.bind(this),onCancel:this.end.bind(this),onComplete:this.end.bind(this)});this.clone.inject(this.element,"before");this.drag.start(b);},end:function(){this.drag.detach();this.element.setStyle("opacity",this.opacity);if(this.effect){var b=this.element.getStyles("width","height"),d=this.clone,c=d.computePosition(this.element.getPosition(this.clone.getOffsetParent()));var a=function(){this.removeEvent("cancel",a);d.destroy();};this.effect.element=d;this.effect.start({top:c.top,left:c.left,width:b.width,height:b.height,opacity:0.25}).addEvent("cancel",a).chain(a);}else{this.clone.destroy();}this.reset();},reset:function(){this.idle=true;this.fireEvent("complete",this.element);},serialize:function(){var c=Array.link(arguments,{modifier:Type.isFunction,index:function(d){return d!=null;}});var b=this.lists.map(function(d){return d.getChildren().map(c.modifier||function(e){return e.get("id");},this);},this);var a=c.index;if(this.lists.length==1){a=0;}return(a||a===0)&&a>=0&&a<this.lists.length?b[a]:b;}});Request.JSONP=new Class({Implements:[Chain,Events,Options],options:{onRequest:function(a){if(this.options.log&&window.console&&console.log){console.log("JSONP retrieving script with url:"+a);}},onError:function(a){if(this.options.log&&window.console&&console.warn){console.warn("JSONP "+a+" will fail in Internet Explorer, which enforces a 2083 bytes length limit on URIs");}},url:"",callbackKey:"callback",injectScript:document.head,data:"",link:"ignore",timeout:0,log:false},initialize:function(a){this.setOptions(a);},send:function(c){if(!Request.prototype.check.call(this,c)){return this;}this.running=true;var d=typeOf(c);if(d=="string"||d=="element"){c={data:c};}c=Object.merge(this.options,c||{});var e=c.data;switch(typeOf(e)){case"element":e=document.id(e).toQueryString();break;case"object":case"hash":e=Object.toQueryString(e);}var b=this.index=Request.JSONP.counter++;var f=c.url+(c.url.test("\\?")?"&":"?")+(c.callbackKey)+"=Request.JSONP.request_map.request_"+b+(e?"&"+e:"");if(f.length>2083){this.fireEvent("error",f);}Request.JSONP.request_map["request_"+b]=function(){this.success(arguments,b);}.bind(this);var a=this.getScript(f).inject(c.injectScript);this.fireEvent("request",[f,a]);if(c.timeout){this.timeout.delay(c.timeout,this);}return this;},getScript:function(a){if(!this.script){this.script=new Element("script",{type:"text/javascript",async:true,src:a});}return this.script;},success:function(b,a){if(!this.running){return;}this.clear().fireEvent("complete",b).fireEvent("success",b).callChain();},cancel:function(){if(this.running){this.clear().fireEvent("cancel");}return this;},isRunning:function(){return!!this.running;},clear:function(){this.running=false;if(this.script){this.script.destroy();this.script=null;}return this;},timeout:function(){if(this.running){this.running=false;this.fireEvent("timeout",[this.script.get("src"),this.script]).fireEvent("failure").cancel();}return this;}});Request.JSONP.counter=0;Request.JSONP.request_map={};Request.Queue=new Class({Implements:[Options,Events],Binds:["attach","request","complete","cancel","success","failure","exception"],options:{stopOnFailure:true,autoAdvance:true,concurrent:1,requests:{}},initialize:function(a){var b;if(a){b=a.requests;delete a.requests;}this.setOptions(a);this.requests={};this.queue=[];this.reqBinders={};if(b){this.addRequests(b);}},addRequest:function(a,b){this.requests[a]=b;this.attach(a,b);return this;},addRequests:function(a){Object.each(a,function(c,b){this.addRequest(b,c);},this);return this;},getName:function(a){return Object.keyOf(this.requests,a);},attach:function(a,b){if(b._groupSend){return this;}["request","complete","cancel","success","failure","exception"].each(function(c){if(!this.reqBinders[a]){this.reqBinders[a]={};}this.reqBinders[a][c]=function(){this["on"+c.capitalize()].apply(this,[a,b].append(arguments));}.bind(this);b.addEvent(c,this.reqBinders[a][c]);},this);b._groupSend=b.send;b.send=function(c){this.send(a,c);return b;}.bind(this);return this;},removeRequest:function(b){var a=typeOf(b)=="object"?this.getName(b):b;if(!a&&typeOf(a)!="string"){return this;}b=this.requests[a];if(!b){return this;}["request","complete","cancel","success","failure","exception"].each(function(c){b.removeEvent(c,this.reqBinders[a][c]);},this);b.send=b._groupSend;delete b._groupSend;return this;},getRunning:function(){return Object.filter(this.requests,function(a){return a.running;});},isRunning:function(){return!!(Object.keys(this.getRunning()).length);},send:function(b,a){var c=function(){this.requests[b]._groupSend(a);this.queue.erase(c);}.bind(this);c.name=b;if(Object.keys(this.getRunning()).length>=this.options.concurrent||(this.error&&this.options.stopOnFailure)){this.queue.push(c);}else{c();}return this;},hasNext:function(a){return(!a)?!!this.queue.length:!!this.queue.filter(function(b){return b.name==a;}).length;},resume:function(){this.error=false;(this.options.concurrent-Object.keys(this.getRunning()).length).times(this.runNext,this);return this;},runNext:function(a){if(!this.queue.length){return this;}if(!a){this.queue[0]();}else{var b;this.queue.each(function(c){if(!b&&c.name==a){b=true;c();}});}return this;},runAll:function(){this.queue.each(function(a){a();});return this;},clear:function(a){if(!a){this.queue.empty();}else{this.queue=this.queue.map(function(b){if(b.name!=a){return b;}else{return false;}}).filter(function(b){return b;});}return this;},cancel:function(a){this.requests[a].cancel();return this;},onRequest:function(){this.fireEvent("request",arguments);},onComplete:function(){this.fireEvent("complete",arguments);if(!this.queue.length){this.fireEvent("end");}},onCancel:function(){if(this.options.autoAdvance&&!this.error){this.runNext();}this.fireEvent("cancel",arguments);},onSuccess:function(){if(this.options.autoAdvance&&!this.error){this.runNext();}this.fireEvent("success",arguments);},onFailure:function(){this.error=true;if(!this.options.stopOnFailure&&this.options.autoAdvance){this.runNext();}this.fireEvent("failure",arguments);},onException:function(){this.error=true;if(!this.options.stopOnFailure&&this.options.autoAdvance){this.runNext();}this.fireEvent("exception",arguments);}});Request.implement({options:{initialDelay:5000,delay:5000,limit:60000},startTimer:function(b){var a=function(){if(!this.running){this.send({data:b});}};this.lastDelay=this.options.initialDelay;this.timer=a.delay(this.lastDelay,this);this.completeCheck=function(c){clearTimeout(this.timer);this.lastDelay=(c)?this.options.delay:(this.lastDelay+this.options.delay).min(this.options.limit);this.timer=a.delay(this.lastDelay,this);};return this.addEvent("complete",this.completeCheck);},stopTimer:function(){clearTimeout(this.timer);return this.removeEvent("complete",this.completeCheck);}});var Asset={javascript:function(d,b){if(!b){b={};}var a=new Element("script",{src:d,type:"text/javascript"}),e=b.document||document,c=b.onload||b.onLoad;delete b.onload;delete b.onLoad;delete b.document;if(c){if(typeof a.onreadystatechange!="undefined"){a.addEvent("readystatechange",function(){if(["loaded","complete"].contains(this.readyState)){c.call(this);}});}else{a.addEvent("load",c);}}return a.set(b).inject(e.head);},css:function(d,a){if(!a){a={};}var b=new Element("link",{rel:"stylesheet",media:"screen",type:"text/css",href:d});var c=a.onload||a.onLoad,e=a.document||document;delete a.onload;delete a.onLoad;delete a.document;if(c){b.addEvent("load",c);}return b.set(a).inject(e.head);},image:function(c,b){if(!b){b={};}var d=new Image(),a=document.id(d)||new Element("img");["load","abort","error"].each(function(e){var g="on"+e,f="on"+e.capitalize(),h=b[g]||b[f]||function(){};delete b[f];delete b[g];d[g]=function(){if(!d){return;}if(!a.parentNode){a.width=d.width;a.height=d.height;}d=d.onload=d.onabort=d.onerror=null;h.delay(1,a,a);a.fireEvent(e,a,1);};});d.src=a.src=c;if(d&&d.complete){d.onload.delay(1);}return a.set(b);},images:function(c,b){c=Array.from(c);var d=function(){},a=0;b=Object.merge({onComplete:d,onProgress:d,onError:d,properties:{}},b);return new Elements(c.map(function(f,e){return Asset.image(f,Object.append(b.properties,{onload:function(){a++;b.onProgress.call(this,a,e,f);if(a==c.length){b.onComplete();}},onerror:function(){a++;b.onError.call(this,a,e,f);if(a==c.length){b.onComplete();}}}));}));}};(function(){var a=this.Color=new Type("Color",function(c,d){if(arguments.length>=3){d="rgb";c=Array.slice(arguments,0,3);}else{if(typeof c=="string"){if(c.match(/rgb/)){c=c.rgbToHex().hexToRgb(true);}else{if(c.match(/hsb/)){c=c.hsbToRgb();}else{c=c.hexToRgb(true);}}}}d=d||"rgb";switch(d){case"hsb":var b=c;c=c.hsbToRgb();c.hsb=b;break;case"hex":c=c.hexToRgb(true);break;}c.rgb=c.slice(0,3);c.hsb=c.hsb||c.rgbToHsb();c.hex=c.rgbToHex();return Object.append(c,this);});a.implement({mix:function(){var b=Array.slice(arguments);var d=(typeOf(b.getLast())=="number")?b.pop():50;var c=this.slice();b.each(function(e){e=new a(e);for(var f=0;f<3;f++){c[f]=Math.round((c[f]/100*(100-d))+(e[f]/100*d));}});return new a(c,"rgb");},invert:function(){return new a(this.map(function(b){return 255-b;}));},setHue:function(b){return new a([b,this.hsb[1],this.hsb[2]],"hsb");},setSaturation:function(b){return new a([this.hsb[0],b,this.hsb[2]],"hsb");},setBrightness:function(b){return new a([this.hsb[0],this.hsb[1],b],"hsb");}});this.$RGB=function(e,d,c){return new a([e,d,c],"rgb");};this.$HSB=function(e,d,c){return new a([e,d,c],"hsb");};this.$HEX=function(b){return new a(b,"hex");};Array.implement({rgbToHsb:function(){var c=this[0],d=this[1],k=this[2],h=0;var j=Math.max(c,d,k),f=Math.min(c,d,k);var l=j-f;var i=j/255,g=(j!=0)?l/j:0;if(g!=0){var e=(j-c)/l;var b=(j-d)/l;var m=(j-k)/l;if(c==j){h=m-b;}else{if(d==j){h=2+e-m;}else{h=4+b-e;}}h/=6;if(h<0){h++;}}return[Math.round(h*360),Math.round(g*100),Math.round(i*100)];},hsbToRgb:function(){var d=Math.round(this[2]/100*255);if(this[1]==0){return[d,d,d];}else{var b=this[0]%360;var g=b%60;var h=Math.round((this[2]*(100-this[1]))/10000*255);var e=Math.round((this[2]*(6000-this[1]*g))/600000*255);var c=Math.round((this[2]*(6000-this[1]*(60-g)))/600000*255);switch(Math.floor(b/60)){case 0:return[d,c,h];case 1:return[e,d,h];case 2:return[h,d,c];case 3:return[h,e,d];case 4:return[c,h,d];case 5:return[d,h,e];}}return false;}});String.implement({rgbToHsb:function(){var b=this.match(/\d{1,3}/g);return(b)?b.rgbToHsb():null;},hsbToRgb:function(){var b=this.match(/\d{1,3}/g);return(b)?b.hsbToRgb():null;}});})();(function(){this.Group=new Class({initialize:function(){this.instances=Array.flatten(arguments);},addEvent:function(e,d){var g=this.instances,a=g.length,f=a,c=new Array(a),b=this;g.each(function(h,j){h.addEvent(e,function(){if(!c[j]){f--;}c[j]=arguments;if(!f){d.call(b,g,h,c);f=a;c=new Array(a);}});});}});})();Hash.Cookie=new Class({Extends:Cookie,options:{autoSave:true},initialize:function(b,a){this.parent(b,a);this.load();},save:function(){var a=JSON.encode(this.hash);if(!a||a.length>4096){return false;}if(a=="{}"){this.dispose();}else{this.write(a);}return true;},load:function(){this.hash=new Hash(JSON.decode(this.read(),true));return this;}});Hash.each(Hash.prototype,function(b,a){if(typeof b=="function"){Hash.Cookie.implement(a,function(){var c=b.apply(this.hash,arguments);if(this.options.autoSave){this.save();}return c;});}});(function(){var a=this.Table=function(){this.length=0;var c=[],b=[];this.set=function(e,g){var d=c.indexOf(e);if(d==-1){var f=c.length;c[f]=e;b[f]=g;this.length++;}else{b[d]=g;}return this;};this.get=function(e){var d=c.indexOf(e);return(d==-1)?null:b[d];};this.erase=function(e){var d=c.indexOf(e);if(d!=-1){this.length--;c.splice(d,1);return b.splice(d,1)[0];}return null;};this.each=this.forEach=function(f,g){for(var e=0,d=this.length;e<d;e++){f.call(g,c[e],b[e],this);}};};if(this.Type){new Type("Table",a);}})();var HtmlTable=new Class({Implements:[Options,Events,Class.Occlude],options:{properties:{cellpadding:0,cellspacing:0,border:0},rows:[],headers:[],footers:[]},property:"HtmlTable",initialize:function(){var a=Array.link(arguments,{options:Type.isObject,table:Type.isElement,id:Type.isString});this.setOptions(a.options);if(!a.table&&a.id){a.table=document.id(a.id);}this.element=a.table||new Element("table",this.options.properties);if(this.occlude()){return this.occluded;}this.build();},build:function(){this.element.store("HtmlTable",this);this.body=document.id(this.element.tBodies[0])||new Element("tbody").inject(this.element);$$(this.body.rows);if(this.options.headers.length){this.setHeaders(this.options.headers);}else{this.thead=document.id(this.element.tHead);}if(this.thead){this.head=this.getHead();}if(this.options.footers.length){this.setFooters(this.options.footers);}this.tfoot=document.id(this.element.tFoot);if(this.tfoot){this.foot=document.id(this.tfoot.rows[0]);}this.options.rows.each(function(a){this.push(a);},this);},toElement:function(){return this.element;},empty:function(){this.body.empty();return this;},set:function(e,a){var d=(e=="headers")?"tHead":"tFoot",b=d.toLowerCase();this[b]=(document.id(this.element[d])||new Element(b).inject(this.element,"top")).empty();var c=this.push(a,{},this[b],e=="headers"?"th":"td");if(e=="headers"){this.head=this.getHead();}else{this.foot=this.getHead();}return c;},getHead:function(){var a=this.thead.rows;return a.length>1?$$(a):a.length?document.id(a[0]):false;},setHeaders:function(a){this.set("headers",a);return this;},setFooters:function(a){this.set("footers",a);return this;},update:function(d,e,a){var b=d.getChildren(a||"td"),c=b.length-1;e.each(function(i,f){var j=b[f]||new Element(a||"td").inject(d),h=(i?i.content:"")||i,g=typeOf(h);if(i&&i.properties){j.set(i.properties);}if(/(element(s?)|array|collection)/.test(g)){j.empty().adopt(h);}else{j.set("html",h);}if(f>c){b.push(j);}else{b[f]=j;}});return{tr:d,tds:b};},push:function(e,c,d,a,b){if(typeOf(e)=="element"&&e.get("tag")=="tr"){e.inject(d||this.body,b);return{tr:e,tds:e.getChildren("td")};}return this.update(new Element("tr",c).inject(d||this.body,b),e,a);},pushMany:function(d,c,e,a,b){return d.map(function(f){return this.push(f,c,e,a,b);},this);}});["adopt","inject","wraps","grab","replaces","dispose"].each(function(a){HtmlTable.implement(a,function(){this.element[a].apply(this.element,arguments);return this;});});HtmlTable=Class.refactor(HtmlTable,{options:{classZebra:"table-tr-odd",zebra:true,zebraOnlyVisibleRows:true},initialize:function(){this.previous.apply(this,arguments);if(this.occluded){return this.occluded;}if(this.options.zebra){this.updateZebras();}},updateZebras:function(){var a=0;Array.each(this.body.rows,function(b){if(!this.options.zebraOnlyVisibleRows||b.isDisplayed()){this.zebra(b,a++);}},this);},setRowStyle:function(b,a){if(this.previous){this.previous(b,a);}this.zebra(b,a);},zebra:function(b,a){return b[((a%2)?"remove":"add")+"Class"](this.options.classZebra);},push:function(){var a=this.previous.apply(this,arguments);if(this.options.zebra){this.updateZebras();}return a;}});HtmlTable=Class.refactor(HtmlTable,{options:{sortIndex:0,sortReverse:false,parsers:[],defaultParser:"string",classSortable:"table-sortable",classHeadSort:"table-th-sort",classHeadSortRev:"table-th-sort-rev",classNoSort:"table-th-nosort",classGroupHead:"table-tr-group-head",classGroup:"table-tr-group",classCellSort:"table-td-sort",classSortSpan:"table-th-sort-span",sortable:false,thSelector:"th"},initialize:function(){this.previous.apply(this,arguments);if(this.occluded){return this.occluded;}this.sorted={index:null,dir:1};if(!this.bound){this.bound={};}this.bound.headClick=this.headClick.bind(this);this.sortSpans=new Elements();if(this.options.sortable){this.enableSort();if(this.options.sortIndex!=null){this.sort(this.options.sortIndex,this.options.sortReverse);}}},attachSorts:function(a){this.detachSorts();if(a!==false){this.element.addEvent("click:relay("+this.options.thSelector+")",this.bound.headClick);}},detachSorts:function(){this.element.removeEvents("click:relay("+this.options.thSelector+")");},setHeaders:function(){this.previous.apply(this,arguments);if(this.sortEnabled){this.setParsers();}},setParsers:function(){this.parsers=this.detectParsers();},detectParsers:function(){return this.head&&this.head.getElements(this.options.thSelector).flatten().map(this.detectParser,this);},detectParser:function(a,b){if(a.hasClass(this.options.classNoSort)||a.retrieve("htmltable-parser")){return a.retrieve("htmltable-parser");}var c=new Element("div");c.adopt(a.childNodes).inject(a);var f=new Element("span",{"class":this.options.classSortSpan}).inject(c,"top");this.sortSpans.push(f);var g=this.options.parsers[b],e=this.body.rows,d;switch(typeOf(g)){case"function":g={convert:g};d=true;break;case"string":g=g;d=true;break;}if(!d){HtmlTable.ParserPriority.some(function(k){var o=HtmlTable.Parsers[k],m=o.match;if(!m){return false;}for(var n=0,l=e.length;n<l;n++){var h=document.id(e[n].cells[b]),p=h?h.get("html").clean():"";if(p&&m.test(p)){g=o;return true;}}});}if(!g){g=this.options.defaultParser;}a.store("htmltable-parser",g);return g;},headClick:function(b,a){if(!this.head||a.hasClass(this.options.classNoSort)){return;}return this.sort(Array.indexOf(this.head.getElements(this.options.thSelector).flatten(),a)%this.body.rows[0].cells.length);},serialize:function(){var a=this.previous.apply(this,arguments)||{};if(this.options.sortable){a.sortIndex=this.sorted.index;a.sortReverse=this.sorted.reverse;}return a;},restore:function(a){if(this.options.sortable&&a.sortIndex){this.sort(a.sortIndex,a.sortReverse);}this.previous.apply(this,arguments);},setSortedState:function(b,a){if(a!=null){this.sorted.reverse=a;}else{if(this.sorted.index==b){this.sorted.reverse=!this.sorted.reverse;}else{this.sorted.reverse=this.sorted.index==null;}}if(b!=null){this.sorted.index=b;}},setHeadSort:function(a){var b=$$(!this.head.length?this.head.cells[this.sorted.index]:this.head.map(function(c){return c.getElements(this.options.thSelector)[this.sorted.index];},this).clean());if(!b.length){return;}if(a){b.addClass(this.options.classHeadSort);if(this.sorted.reverse){b.addClass(this.options.classHeadSortRev);}else{b.removeClass(this.options.classHeadSortRev);}}else{b.removeClass(this.options.classHeadSort).removeClass(this.options.classHeadSortRev);}},setRowSort:function(b,a){var e=b.length,d=this.body,g,f;while(e){var h=b[--e],c=h.position,i=d.rows[c];if(i.disabled){continue;}if(!a){g=this.setGroupSort(g,i,h);this.setRowStyle(i,e);}d.appendChild(i);for(f=0;f<e;f++){if(b[f].position>c){b[f].position--;}}}},setRowStyle:function(b,a){this.previous(b,a);b.cells[this.sorted.index].addClass(this.options.classCellSort);},setGroupSort:function(b,c,a){if(b==a.value){c.removeClass(this.options.classGroupHead).addClass(this.options.classGroup);}else{c.removeClass(this.options.classGroup).addClass(this.options.classGroupHead);}return a.value;},getParser:function(){var a=this.parsers[this.sorted.index];return typeOf(a)=="string"?HtmlTable.Parsers[a]:a;},sort:function(c,b,e){if(!this.head){return;}if(!e){this.clearSort();this.setSortedState(c,b);this.setHeadSort(true);}var f=this.getParser();if(!f){return;}var a;if(!Browser.ie){a=this.body.getParent();this.body.dispose();}var d=this.parseData(f).sort(function(h,g){if(h.value===g.value){return 0;}return h.value>g.value?1:-1;});if(this.sorted.reverse==(f==HtmlTable.Parsers["input-checked"])){d.reverse(true);}this.setRowSort(d,e);if(a){a.grab(this.body);}this.fireEvent("stateChanged");return this.fireEvent("sort",[this.body,this.sorted.index]);},parseData:function(a){return Array.map(this.body.rows,function(d,b){var c=a.convert.call(document.id(d.cells[this.sorted.index]));return{position:b,value:c};},this);},clearSort:function(){this.setHeadSort(false);this.body.getElements("td").removeClass(this.options.classCellSort);},reSort:function(){if(this.sortEnabled){this.sort.call(this,this.sorted.index,this.sorted.reverse);}return this;},enableSort:function(){this.element.addClass(this.options.classSortable);this.attachSorts(true);this.setParsers();this.sortEnabled=true;return this;},disableSort:function(){this.element.removeClass(this.options.classSortable);this.attachSorts(false);this.sortSpans.each(function(a){a.destroy();});this.sortSpans.empty();this.sortEnabled=false;return this;}});HtmlTable.ParserPriority=["date","input-checked","input-value","float","number"];HtmlTable.Parsers={date:{match:/^\d{2}[-\/ ]\d{2}[-\/ ]\d{2,4}$/,convert:function(){var a=Date.parse(this.get("text").stripTags());return(typeOf(a)=="date")?a.format("db"):"";},type:"date"},"input-checked":{match:/ type="(radio|checkbox)" /,convert:function(){return this.getElement("input").checked;}},"input-value":{match:/<input/,convert:function(){return this.getElement("input").value;}},number:{match:/^\d+[^\d.,]*$/,convert:function(){return this.get("text").stripTags().toInt();},number:true},numberLax:{match:/^[^\d]+\d+$/,convert:function(){return this.get("text").replace(/[^-?^0-9]/,"").stripTags().toInt();},number:true},"float":{match:/^[\d]+\.[\d]+/,convert:function(){return this.get("text").replace(/[^-?^\d.]/,"").stripTags().toFloat();},number:true},floatLax:{match:/^[^\d]+[\d]+\.[\d]+$/,convert:function(){return this.get("text").replace(/[^-?^\d.]/,"").stripTags();},number:true},string:{match:null,convert:function(){return this.get("text").stripTags().toLowerCase();}},title:{match:null,convert:function(){return this.title;}}};HtmlTable.defineParsers=function(a){HtmlTable.Parsers=Object.append(HtmlTable.Parsers,a);for(var b in a){HtmlTable.ParserPriority.unshift(b);}};(function(){var a=this.Keyboard=new Class({Extends:Events,Implements:[Options],options:{defaultEventType:"keydown",active:false,manager:null,events:{},nonParsedEvents:["activate","deactivate","onactivate","ondeactivate","changed","onchanged"]},initialize:function(f){if(f&&f.manager){this._manager=f.manager;delete f.manager;}this.setOptions(f);this._setup();},addEvent:function(h,g,f){return this.parent(a.parse(h,this.options.defaultEventType,this.options.nonParsedEvents),g,f);},removeEvent:function(g,f){return this.parent(a.parse(g,this.options.defaultEventType,this.options.nonParsedEvents),f);},toggleActive:function(){return this[this.isActive()?"deactivate":"activate"]();},activate:function(f){if(f){if(f.isActive()){return this;}if(this._activeKB&&f!=this._activeKB){this.previous=this._activeKB;this.previous.fireEvent("deactivate");}this._activeKB=f.fireEvent("activate");a.manager.fireEvent("changed");}else{if(this._manager){this._manager.activate(this);}}return this;},isActive:function(){return this._manager?(this._manager._activeKB==this):(a.manager==this);},deactivate:function(f){if(f){if(f===this._activeKB){this._activeKB=null;f.fireEvent("deactivate");a.manager.fireEvent("changed");}}else{if(this._manager){this._manager.deactivate(this);}}return this;},relinquish:function(){if(this.isActive()&&this._manager&&this._manager.previous){this._manager.activate(this._manager.previous);}else{this.deactivate();}return this;},manage:function(f){if(f._manager){f._manager.drop(f);}this._instances.push(f);f._manager=this;if(!this._activeKB){this.activate(f);}return this;},drop:function(f){f.relinquish();this._instances.erase(f);if(this._activeKB==f){if(this.previous&&this._instances.contains(this.previous)){this.activate(this.previous);}else{this._activeKB=this._instances[0];}}return this;},trace:function(){a.trace(this);},each:function(f){a.each(this,f);},_instances:[],_disable:function(f){if(this._activeKB==f){this._activeKB=null;}},_setup:function(){this.addEvents(this.options.events);if(a.manager&&!this._manager){a.manager.manage(this);}if(this.options.active){this.activate();}else{this.relinquish();}},_handle:function(h,g){if(h.preventKeyboardPropagation){return;}var f=!!this._manager;if(f&&this._activeKB){this._activeKB._handle(h,g);if(h.preventKeyboardPropagation){return;}}this.fireEvent(g,h);if(!f&&this._activeKB){this._activeKB._handle(h,g);}}});var b={};var c=["shift","control","alt","meta"];var e=/^(?:shift|control|ctrl|alt|meta)$/;a.parse=function(h,g,k){if(k&&k.contains(h.toLowerCase())){return h;}h=h.toLowerCase().replace(/^(keyup|keydown):/,function(m,l){g=l;return"";});if(!b[h]){var f,j={};h.split("+").each(function(l){if(e.test(l)){j[l]=true;}else{f=l;}});j.control=j.control||j.ctrl;var i=[];c.each(function(l){if(j[l]){i.push(l);}});if(f){i.push(f);}b[h]=i.join("+");}return g+":keys("+b[h]+")";};a.each=function(f,g){var h=f||a.manager;while(h){g.run(h);h=h._activeKB;}};a.stop=function(f){f.preventKeyboardPropagation=true;};a.manager=new a({active:true});a.trace=function(f){f=f||a.manager;var g=window.console&&console.log;if(g){console.log("the following items have focus: ");}a.each(f,function(h){if(g){console.log(document.id(h.widget)||h.wiget||h);}});};var d=function(g){var f=[];c.each(function(h){if(g[h]){f.push(h);}});if(!e.test(g.key)){f.push(g.key);}a.manager._handle(g,g.type+":keys("+f.join("+")+")");};document.addEvents({keyup:d,keydown:d});})();Keyboard.prototype.options.nonParsedEvents.combine(["rebound","onrebound"]);Keyboard.implement({addShortcut:function(b,a){this._shortcuts=this._shortcuts||[];this._shortcutIndex=this._shortcutIndex||{};a.getKeyboard=Function.from(this);a.name=b;this._shortcutIndex[b]=a;this._shortcuts.push(a);if(a.keys){this.addEvent(a.keys,a.handler);}return this;},addShortcuts:function(b){for(var a in b){this.addShortcut(a,b[a]);}return this;},removeShortcut:function(b){var a=this.getShortcut(b);if(a&&a.keys){this.removeEvent(a.keys,a.handler);delete this._shortcutIndex[b];this._shortcuts.erase(a);}return this;},removeShortcuts:function(a){a.each(this.removeShortcut,this);return this;},getShortcuts:function(){return this._shortcuts||[];},getShortcut:function(a){return(this._shortcutIndex||{})[a];}});Keyboard.rebind=function(b,a){Array.from(a).each(function(c){c.getKeyboard().removeEvent(c.keys,c.handler);c.getKeyboard().addEvent(b,c.handler);c.keys=b;c.getKeyboard().fireEvent("rebound");});};Keyboard.getActiveShortcuts=function(b){var a=[],c=[];Keyboard.each(b,[].push.bind(a));a.each(function(d){c.extend(d.getShortcuts());});return c;};Keyboard.getShortcut=function(c,b,d){d=d||{};var a=d.many?[]:null,e=d.many?function(g){var f=g.getShortcut(c);if(f){a.push(f);}}:function(f){if(!a){a=f.getShortcut(c);}};Keyboard.each(b,e);return a;};Keyboard.getShortcuts=function(b,a){return Keyboard.getShortcut(b,a,{many:true});};HtmlTable=Class.refactor(HtmlTable,{options:{useKeyboard:true,classRowSelected:"table-tr-selected",classRowHovered:"table-tr-hovered",classSelectable:"table-selectable",shiftForMultiSelect:true,allowMultiSelect:true,selectable:false,selectHiddenRows:false},initialize:function(){this.previous.apply(this,arguments);if(this.occluded){return this.occluded;}this.selectedRows=new Elements();if(!this.bound){this.bound={};}this.bound.mouseleave=this.mouseleave.bind(this);this.bound.clickRow=this.clickRow.bind(this);this.bound.activateKeyboard=function(){if(this.keyboard&&this.selectEnabled){this.keyboard.activate();}}.bind(this);if(this.options.selectable){this.enableSelect();}},empty:function(){this.selectNone();return this.previous();},enableSelect:function(){this.selectEnabled=true;this.attachSelects();this.element.addClass(this.options.classSelectable);return this;},disableSelect:function(){this.selectEnabled=false;this.attachSelects(false);this.element.removeClass(this.options.classSelectable);return this;},push:function(){var a=this.previous.apply(this,arguments);this.updateSelects();return a;},toggleRow:function(a){return this[(this.isSelected(a)?"de":"")+"selectRow"](a);},selectRow:function(b,a){if(this.isSelected(b)||(!a&&!this.body.getChildren().contains(b))){return;}if(!this.options.allowMultiSelect){this.selectNone();}if(!this.isSelected(b)){this.selectedRows.push(b);b.addClass(this.options.classRowSelected);this.fireEvent("rowFocus",[b,this.selectedRows]);this.fireEvent("stateChanged");}this.focused=b;document.clearSelection();return this;},isSelected:function(a){return this.selectedRows.contains(a);},getSelected:function(){return this.selectedRows;},getSelected:function(){return this.selectedRows;},serialize:function(){var a=this.previous.apply(this,arguments)||{};if(this.options.selectable){a.selectedRows=this.selectedRows.map(function(b){return Array.indexOf(this.body.rows,b);}.bind(this));}return a;},restore:function(a){if(this.options.selectable&&a.selectedRows){a.selectedRows.each(function(b){this.selectRow(this.body.rows[b]);}.bind(this));}this.previous.apply(this,arguments);},deselectRow:function(b,a){if(!this.isSelected(b)||(!a&&!this.body.getChildren().contains(b))){return;}this.selectedRows=new Elements(Array.from(this.selectedRows).erase(b));b.removeClass(this.options.classRowSelected);this.fireEvent("rowUnfocus",[b,this.selectedRows]);this.fireEvent("stateChanged");return this;},selectAll:function(a){if(!a&&!this.options.allowMultiSelect){return;}this.selectRange(0,this.body.rows.length,a);return this;},selectNone:function(){return this.selectAll(true);},selectRange:function(b,a,f){if(!this.options.allowMultiSelect&&!f){return;}var g=f?"deselectRow":"selectRow",e=Array.clone(this.body.rows);if(typeOf(b)=="element"){b=e.indexOf(b);}if(typeOf(a)=="element"){a=e.indexOf(a);}a=a<e.length-1?a:e.length-1;if(a<b){var d=b;b=a;a=d;}for(var c=b;c<=a;c++){if(this.options.selectHiddenRows||e[c].isDisplayed()){this[g](e[c],true);}}return this;},deselectRange:function(b,a){this.selectRange(b,a,true);},getSelected:function(){return this.selectedRows;},enterRow:function(a){if(this.hovered){this.hovered=this.leaveRow(this.hovered);}this.hovered=a.addClass(this.options.classRowHovered);},leaveRow:function(a){a.removeClass(this.options.classRowHovered);},updateSelects:function(){Array.each(this.body.rows,function(a){var b=a.retrieve("binders");if(!b&&!this.selectEnabled){return;}if(!b){b={mouseenter:this.enterRow.pass([a],this),mouseleave:this.leaveRow.pass([a],this)};a.store("binders",b);}if(this.selectEnabled){a.addEvents(b);}else{a.removeEvents(b);}},this);},shiftFocus:function(b,a){if(!this.focused){return this.selectRow(this.body.rows[0],a);}var c=this.getRowByOffset(b,this.options.selectHiddenRows);if(c===null||this.focused==this.body.rows[c]){return this;}this.toggleRow(this.body.rows[c],a);},clickRow:function(a,b){var c=(a.shift||a.meta||a.control)&&this.options.shiftForMultiSelect;if(!c&&!(a.rightClick&&this.isSelected(b)&&this.options.allowMultiSelect)){this.selectNone();}if(a.rightClick){this.selectRow(b);}else{this.toggleRow(b);}if(a.shift){this.selectRange(this.rangeStart||this.body.rows[0],b,this.rangeStart?!this.isSelected(b):true);this.focused=b;}this.rangeStart=b;},getRowByOffset:function(e,d){if(!this.focused){return 0;}var b=Array.indexOf(this.body.rows,this.focused);if((b==0&&e<0)||(b==this.body.rows.length-1&&e>0)){return null;}if(d){b+=e;}else{var a=0,c=0;if(e>0){while(c<e&&b<this.body.rows.length-1){if(this.body.rows[++b].isDisplayed()){c++;}}}else{while(c>e&&b>0){if(this.body.rows[--b].isDisplayed()){c--;}}}}return b;},attachSelects:function(d){d=d!=null?d:true;var g=d?"addEvents":"removeEvents";this.element[g]({mouseleave:this.bound.mouseleave,click:this.bound.activateKeyboard});this.body[g]({"click:relay(tr)":this.bound.clickRow,"contextmenu:relay(tr)":this.bound.clickRow});if(this.options.useKeyboard||this.keyboard){if(!this.keyboard){this.keyboard=new Keyboard();}if(!this.selectKeysDefined){this.selectKeysDefined=true;var f,e;var c=function(i){var h=function(j){clearTimeout(f);j.preventDefault();var k=this.body.rows[this.getRowByOffset(i,this.options.selectHiddenRows)];if(j.shift&&k&&this.isSelected(k)){this.deselectRow(this.focused);this.focused=k;}else{if(k&&(!this.options.allowMultiSelect||!j.shift)){this.selectNone();}this.shiftFocus(i,j);}if(e){f=h.delay(100,this,j);}else{f=(function(){e=true;h(j);}).delay(400);}}.bind(this);return h;}.bind(this);var b=function(){clearTimeout(f);e=false;};this.keyboard.addEvents({"keydown:shift+up":c(-1),"keydown:shift+down":c(1),"keyup:shift+up":b,"keyup:shift+down":b,"keyup:up":b,"keyup:down":b});var a="";if(this.options.allowMultiSelect&&this.options.shiftForMultiSelect&&this.options.useKeyboard){a=" (Shift multi-selects).";}this.keyboard.addShortcuts({"Select Previous Row":{keys:"up",shortcut:"up arrow",handler:c(-1),description:"Select the previous row in the table."+a},"Select Next Row":{keys:"down",shortcut:"down arrow",handler:c(1),description:"Select the next row in the table."+a}});}this.keyboard[d?"activate":"deactivate"]();}this.updateSelects();},mouseleave:function(){if(this.hovered){this.leaveRow(this.hovered);}}});var Scroller=new Class({Implements:[Events,Options],options:{area:20,velocity:1,onChange:function(a,b){this.element.scrollTo(a,b);},fps:50},initialize:function(b,a){this.setOptions(a);this.element=document.id(b);this.docBody=document.id(this.element.getDocument().body);this.listener=(typeOf(this.element)!="element")?this.docBody:this.element;this.timer=null;this.bound={attach:this.attach.bind(this),detach:this.detach.bind(this),getCoords:this.getCoords.bind(this)};},start:function(){this.listener.addEvents({mouseover:this.bound.attach,mouseleave:this.bound.detach});return this;},stop:function(){this.listener.removeEvents({mouseover:this.bound.attach,mouseleave:this.bound.detach});this.detach();this.timer=clearInterval(this.timer);return this;},attach:function(){this.listener.addEvent("mousemove",this.bound.getCoords);},detach:function(){this.listener.removeEvent("mousemove",this.bound.getCoords);this.timer=clearInterval(this.timer);},getCoords:function(a){this.page=(this.listener.get("tag")=="body")?a.client:a.page;if(!this.timer){this.timer=this.scroll.periodical(Math.round(1000/this.options.fps),this);}},scroll:function(){var c=this.element.getSize(),a=this.element.getScroll(),h=this.element!=this.docBody?this.element.getOffsets():{x:0,y:0},d=this.element.getScrollSize(),g={x:0,y:0},e=this.options.area.top||this.options.area,b=this.options.area.bottom||this.options.area;for(var f in this.page){if(this.page[f]<(e+h[f])&&a[f]!=0){g[f]=(this.page[f]-e-h[f])*this.options.velocity;}else{if(this.page[f]+b>(c[f]+h[f])&&a[f]+c[f]!=d[f]){g[f]=(this.page[f]-c[f]+b-h[f])*this.options.velocity;}}g[f]=g[f].round();}if(g.y||g.x){this.fireEvent("change",[a.x+g.x,a.y+g.y]);}}});(function(){var a=function(c,b){return(c)?(typeOf(c)=="function"?c(b):b.get(c)):"";};this.Tips=new Class({Implements:[Events,Options],options:{onShow:function(){this.tip.setStyle("display","block");},onHide:function(){this.tip.setStyle("display","none");},title:"title",text:function(b){return b.get("rel")||b.get("href");},showDelay:100,hideDelay:100,className:"tip-wrap",offset:{x:16,y:16},windowPadding:{x:0,y:0},fixed:false,waiAria:true},initialize:function(){var b=Array.link(arguments,{options:Type.isObject,elements:function(c){return c!=null;}});this.setOptions(b.options);if(b.elements){this.attach(b.elements);}this.container=new Element("div",{"class":"tip"});if(this.options.id){this.container.set("id",this.options.id);if(this.options.waiAria){this.attachWaiAria();}}},toElement:function(){if(this.tip){return this.tip;}this.tip=new Element("div",{"class":this.options.className,styles:{position:"absolute",top:0,left:0}}).adopt(new Element("div",{"class":"tip-top"}),this.container,new Element("div",{"class":"tip-bottom"}));return this.tip;},attachWaiAria:function(){var b=this.options.id;this.container.set("role","tooltip");if(!this.waiAria){this.waiAria={show:function(c){if(b){c.set("aria-describedby",b);}this.container.set("aria-hidden","false");},hide:function(c){if(b){c.erase("aria-describedby");}this.container.set("aria-hidden","true");}};}this.addEvents(this.waiAria);},detachWaiAria:function(){if(this.waiAria){this.container.erase("role");this.container.erase("aria-hidden");this.removeEvents(this.waiAria);}},attach:function(b){$$(b).each(function(d){var f=a(this.options.title,d),e=a(this.options.text,d);d.set("title","").store("tip:native",f).retrieve("tip:title",f);d.retrieve("tip:text",e);this.fireEvent("attach",[d]);var c=["enter","leave"];if(!this.options.fixed){c.push("move");}c.each(function(h){var g=d.retrieve("tip:"+h);if(!g){g=function(i){this["element"+h.capitalize()].apply(this,[i,d]);}.bind(this);}d.store("tip:"+h,g).addEvent("mouse"+h,g);},this);},this);return this;},detach:function(b){$$(b).each(function(d){["enter","leave","move"].each(function(e){d.removeEvent("mouse"+e,d.retrieve("tip:"+e)).eliminate("tip:"+e);});this.fireEvent("detach",[d]);if(this.options.title=="title"){var c=d.retrieve("tip:native");if(c){d.set("title",c);}}},this);return this;},elementEnter:function(c,b){clearTimeout(this.timer);this.timer=(function(){this.container.empty();["title","text"].each(function(e){var d=b.retrieve("tip:"+e);var f=this["_"+e+"Element"]=new Element("div",{"class":"tip-"+e}).inject(this.container);if(d){this.fill(f,d);}},this);this.show(b);this.position((this.options.fixed)?{page:b.getPosition()}:c);}).delay(this.options.showDelay,this);},elementLeave:function(c,b){clearTimeout(this.timer);this.timer=this.hide.delay(this.options.hideDelay,this,b);this.fireForParent(c,b);},setTitle:function(b){if(this._titleElement){this._titleElement.empty();this.fill(this._titleElement,b);}return this;},setText:function(b){if(this._textElement){this._textElement.empty();this.fill(this._textElement,b);}return this;},fireForParent:function(c,b){b=b.getParent();if(!b||b==document.body){return;}if(b.retrieve("tip:enter")){b.fireEvent("mouseenter",c);}else{this.fireForParent(c,b);}},elementMove:function(c,b){this.position(c);},position:function(f){if(!this.tip){document.id(this);}var c=window.getSize(),b=window.getScroll(),g={x:this.tip.offsetWidth,y:this.tip.offsetHeight},d={x:"left",y:"top"},e={y:false,x2:false,y2:false,x:false},h={};for(var i in d){h[d[i]]=f.page[i]+this.options.offset[i];if(h[d[i]]<0){e[i]=true;}if((h[d[i]]+g[i]-b[i])>c[i]-this.options.windowPadding[i]){h[d[i]]=f.page[i]-this.options.offset[i]-g[i];e[i+"2"]=true;}}this.fireEvent("bound",e);this.tip.setStyles(h);},fill:function(b,c){if(typeof c=="string"){b.set("html",c);}else{b.adopt(c);}},show:function(b){if(!this.tip){document.id(this);}if(!this.tip.getParent()){this.tip.inject(document.body);}this.fireEvent("show",[this.tip,b]);},hide:function(b){if(!this.tip){document.id(this);}this.fireEvent("hide",[this.tip,b]);}});})();(function(){var a={json:JSON.decode};Locale.Set.defineParser=function(b,c){a[b]=c;};Locale.Set.from=function(d,c){if(instanceOf(d,Locale.Set)){return d;}if(!c&&typeOf(d)=="string"){c="json";}if(a[c]){d=a[c](d);}var b=new Locale.Set;b.sets=d.sets||{};if(d.inherits){b.inherits.locales=Array.from(d.inherits.locales);b.inherits.sets=d.inherits.sets||{};}return b;};})();Locale.define("ar","Date",{dateOrder:["date","month","year"],shortDate:"%d/%m/%Y",shortTime:"%H:%M"});Locale.define("ar","FormValidator",{required:"هذا الحقل مطلوب.",minLength:"رجاءً إدخال {minLength} أحرف على الأقل (تم إدخال {length} أحرف).",maxLength:"الرجاء عدم إدخال أكثر من {maxLength} أحرف (تم إدخال {length} أحرف).",integer:"الرجاء إدخال عدد صحيح في هذا الحقل. أي رقم ذو كسر عشري أو مئوي (مثال 1.25 ) غير مسموح.",numeric:'الرجاء إدخال قيم رقمية في هذا الحقل (مثال "1" أو "1.1" أو "-1" أو "-1.1").',digits:"الرجاء أستخدام قيم رقمية وعلامات ترقيمية فقط في هذا الحقل (مثال, رقم هاتف مع نقطة أو شحطة)",alpha:"الرجاء أستخدام أحرف فقط (ا-ي) في هذا الحقل. أي فراغات أو علامات غير مسموحة.",alphanum:"الرجاء أستخدام أحرف فقط (ا-ي) أو أرقام (0-9) فقط في هذا الحقل. أي فراغات أو علامات غير مسموحة.",dateSuchAs:"الرجاء إدخال تاريخ صحيح كالتالي {date}",dateInFormatMDY:"الرجاء إدخال تاريخ صحيح (مثال, 31-12-1999)",email:"الرجاء إدخال بريد إلكتروني صحيح.",url:"الرجاء إدخال عنوان إلكتروني صحيح مثل http://www.example.com",currencyDollar:"الرجاء إدخال قيمة $ صحيحة. مثال, 100.00$",oneRequired:"الرجاء إدخال قيمة في أحد هذه الحقول على الأقل.",errorPrefix:"خطأ: ",warningPrefix:"تحذير: "});Locale.define("ca-CA","Date",{months:["Gener","Febrer","Març","Abril","Maig","Juny","Juli","Agost","Setembre","Octubre","Novembre","Desembre"],months_abbr:["gen.","febr.","març","abr.","maig","juny","jul.","ag.","set.","oct.","nov.","des."],days:["Diumenge","Dilluns","Dimarts","Dimecres","Dijous","Divendres","Dissabte"],days_abbr:["dg","dl","dt","dc","dj","dv","ds"],dateOrder:["date","month","year"],shortDate:"%d/%m/%Y",shortTime:"%H:%M",AM:"AM",PM:"PM",firstDayOfWeek:0,ordinal:"",lessThanMinuteAgo:"fa menys d`un minut",minuteAgo:"fa un minut",minutesAgo:"fa {delta} minuts",hourAgo:"fa un hora",hoursAgo:"fa unes {delta} hores",dayAgo:"fa un dia",daysAgo:"fa {delta} dies",lessThanMinuteUntil:"menys d`un minut des d`ara",minuteUntil:"un minut des d`ara",minutesUntil:"{delta} minuts des d`ara",hourUntil:"un hora des d`ara",hoursUntil:"unes {delta} hores des d`ara",dayUntil:"1 dia des d`ara",daysUntil:"{delta} dies des d`ara"});Locale.define("ca-CA","FormValidator",{required:"Aquest camp es obligatori.",minLength:"Per favor introdueix al menys {minLength} caracters (has introduit {length} caracters).",maxLength:"Per favor introdueix no mes de {maxLength} caracters (has introduit {length} caracters).",integer:"Per favor introdueix un nombre enter en aquest camp. Nombres amb decimals (p.e. 1,25) no estan permesos.",numeric:'Per favor introdueix sols valors numerics en aquest camp (p.e. "1" o "1,1" o "-1" o "-1,1").',digits:"Per favor usa sols numeros i puntuacio en aquest camp (per exemple, un nombre de telefon amb guions i punts no esta permes).",alpha:"Per favor utilitza lletres nomes (a-z) en aquest camp. No s´admiteixen espais ni altres caracters.",alphanum:"Per favor, utilitza nomes lletres (a-z) o numeros (0-9) en aquest camp. No s´admiteixen espais ni altres caracters.",dateSuchAs:"Per favor introdueix una data valida com {date}",dateInFormatMDY:'Per favor introdueix una data valida com DD/MM/YYYY (p.e. "31/12/1999")',email:'Per favor, introdueix una adreça de correu electronic valida. Per exemple, "fred@domain.com".',url:"Per favor introdueix una URL valida com http://www.example.com.",currencyDollar:"Per favor introdueix una quantitat valida de €. Per exemple €100,00 .",oneRequired:"Per favor introdueix alguna cosa per al menys una d´aquestes entrades.",errorPrefix:"Error: ",warningPrefix:"Avis: ",noSpace:"No poden haver espais en aquesta entrada.",reqChkByNode:"No hi han elements seleccionats.",requiredChk:"Aquest camp es obligatori.",reqChkByName:"Per favor selecciona una {label}.",match:"Aquest camp necessita coincidir amb el camp {matchName}",startDate:"la data de inici",endDate:"la data de fi",currendDate:"la data actual",afterDate:"La data deu ser igual o posterior a {label}.",beforeDate:"La data deu ser igual o anterior a {label}.",startMonth:"Per favor selecciona un mes d´orige",sameMonth:"Aquestes dos dates deuen estar dins del mateix mes - deus canviar una o altra."});(function(){var a=function(e,d,c,b){if(e==1){return d;}else{if(e==2||e==3||e==4){return c;}else{return b;}}};Locale.define("cs-CZ","Date",{months:["Leden","Únor","Březen","Duben","Květen","Červen","Červenec","Srpen","Září","Říjen","Listopad","Prosinec"],months_abbr:["ledna","února","března","dubna","května","června","července","srpna","září","října","listopadu","prosince"],days:["Neděle","Pondělí","Úterý","Středa","Čtvrtek","Pátek","Sobota"],days_abbr:["ne","po","út","st","čt","pá","so"],dateOrder:["date","month","year"],shortDate:"%d.%m.%Y",shortTime:"%H:%M",AM:"dop.",PM:"odp.",firstDayOfWeek:1,ordinal:".",lessThanMinuteAgo:"před chvílí",minuteAgo:"přibližně před minutou",minutesAgo:function(b){return"před {delta} "+a(b,"minutou","minutami","minutami");},hourAgo:"přibližně před hodinou",hoursAgo:function(b){return"před {delta} "+a(b,"hodinou","hodinami","hodinami");},dayAgo:"před dnem",daysAgo:function(b){return"před {delta} "+a(b,"dnem","dny","dny");},weekAgo:"před týdnem",weeksAgo:function(b){return"před {delta} "+a(b,"týdnem","týdny","týdny");},monthAgo:"před měsícem",monthsAgo:function(b){return"před {delta} "+a(b,"měsícem","měsíci","měsíci");},yearAgo:"před rokem",yearsAgo:function(b){return"před {delta} "+a(b,"rokem","lety","lety");},lessThanMinuteUntil:"za chvíli",minuteUntil:"přibližně za minutu",minutesUntil:function(b){return"za {delta} "+a(b,"minutu","minuty","minut");},hourUntil:"přibližně za hodinu",hoursUntil:function(b){return"za {delta} "+a(b,"hodinu","hodiny","hodin");},dayUntil:"za den",daysUntil:function(b){return"za {delta} "+a(b,"den","dny","dnů");},weekUntil:"za týden",weeksUntil:function(b){return"za {delta} "+a(b,"týden","týdny","týdnů");},monthUntil:"za měsíc",monthsUntil:function(b){return"za {delta} "+a(b,"měsíc","měsíce","měsíců");},yearUntil:"za rok",yearsUntil:function(b){return"za {delta} "+a(b,"rok","roky","let");}});})();Locale.define("cs-CZ","FormValidator",{required:"Tato položka je povinná.",minLength:"Zadejte prosím alespoň {minLength} znaků (napsáno {length} znaků).",maxLength:"Zadejte prosím méně než {maxLength} znaků (nápsáno {length} znaků).",integer:"Zadejte prosím celé číslo. Desetinná čísla (např. 1.25) nejsou povolena.",numeric:'Zadejte jen číselné hodnoty (tj. "1" nebo "1.1" nebo "-1" nebo "-1.1").',digits:"Zadejte prosím pouze čísla a interpunkční znaménka(například telefonní číslo s pomlčkami nebo tečkami je povoleno).",alpha:"Zadejte prosím pouze písmena (a-z). Mezery nebo jiné znaky nejsou povoleny.",alphanum:"Zadejte prosím pouze písmena (a-z) nebo číslice (0-9). Mezery nebo jiné znaky nejsou povoleny.",dateSuchAs:"Zadejte prosím platné datum jako {date}",dateInFormatMDY:'Zadejte prosím platné datum jako MM / DD / RRRR (tj. "12/31/1999")',email:'Zadejte prosím platnou e-mailovou adresu. Například "fred@domain.com".',url:"Zadejte prosím platnou URL adresu jako http://www.example.com.",currencyDollar:"Zadejte prosím platnou částku. Například $100.00.",oneRequired:"Zadejte prosím alespoň jednu hodnotu pro tyto položky.",errorPrefix:"Chyba: ",warningPrefix:"Upozornění: ",noSpace:"V této položce nejsou povoleny mezery",reqChkByNode:"Nejsou vybrány žádné položky.",requiredChk:"Tato položka je vyžadována.",reqChkByName:"Prosím vyberte {label}.",match:"Tato položka se musí shodovat s položkou {matchName}",startDate:"datum zahájení",endDate:"datum ukončení",currendDate:"aktuální datum",afterDate:"Datum by mělo být stejné nebo větší než {label}.",beforeDate:"Datum by mělo být stejné nebo menší než {label}.",startMonth:"Vyberte počáteční měsíc.",sameMonth:"Tyto dva datumy musí být ve stejném měsíci - změňte jeden z nich.",creditcard:"Zadané číslo kreditní karty je neplatné. Prosím opravte ho. Bylo zadáno {length} čísel."});Locale.define("da-DK","Date",{months:["Januar","Februar","Marts","April","Maj","Juni","Juli","August","September","Oktober","November","December"],months_abbr:["jan.","feb.","mar.","apr.","maj.","jun.","jul.","aug.","sep.","okt.","nov.","dec."],days:["Søndag","Mandag","Tirsdag","Onsdag","Torsdag","Fredag","Lørdag"],days_abbr:["søn","man","tir","ons","tor","fre","lør"],dateOrder:["date","month","year"],shortDate:"%d-%m-%Y",shortTime:"%H:%M",AM:"AM",PM:"PM",firstDayOfWeek:1,ordinal:".",lessThanMinuteAgo:"mindre end et minut siden",minuteAgo:"omkring et minut siden",minutesAgo:"{delta} minutter siden",hourAgo:"omkring en time siden",hoursAgo:"omkring {delta} timer siden",dayAgo:"1 dag siden",daysAgo:"{delta} dage siden",weekAgo:"1 uge siden",weeksAgo:"{delta} uger siden",monthAgo:"1 måned siden",monthsAgo:"{delta} måneder siden",yearAgo:"1 år siden",yearsAgo:"{delta} år siden",lessThanMinuteUntil:"mindre end et minut fra nu",minuteUntil:"omkring et minut fra nu",minutesUntil:"{delta} minutter fra nu",hourUntil:"omkring en time fra nu",hoursUntil:"omkring {delta} timer fra nu",dayUntil:"1 dag fra nu",daysUntil:"{delta} dage fra nu",weekUntil:"1 uge fra nu",weeksUntil:"{delta} uger fra nu",monthUntil:"1 måned fra nu",monthsUntil:"{delta} måneder fra nu",yearUntil:"1 år fra nu",yearsUntil:"{delta} år fra nu"});Locale.define("da-DK","FormValidator",{required:"Feltet skal udfyldes.",minLength:"Skriv mindst {minLength} tegn (du skrev {length} tegn).",maxLength:"Skriv maksimalt {maxLength} tegn (du skrev {length} tegn).",integer:"Skriv et tal i dette felt. Decimal tal (f.eks. 1.25) er ikke tilladt.",numeric:'Skriv kun tal i dette felt (i.e. "1" eller "1.1" eller "-1" eller "-1.1").',digits:"Skriv kun tal og tegnsætning i dette felt (eksempel, et telefon nummer med bindestreg eller punktum er tilladt).",alpha:"Skriv kun bogstaver (a-z) i dette felt. Mellemrum og andre tegn er ikke tilladt.",alphanum:"Skriv kun bogstaver (a-z) eller tal (0-9) i dette felt. Mellemrum og andre tegn er ikke tilladt.",dateSuchAs:"Skriv en gyldig dato som {date}",dateInFormatMDY:'Skriv dato i formatet DD-MM-YYYY (f.eks. "31-12-1999")',email:'Skriv en gyldig e-mail adresse. F.eks "fred@domain.com".',url:'Skriv en gyldig URL adresse. F.eks "http://www.example.com".',currencyDollar:"Skriv et gldigt beløb. F.eks Kr.100.00 .",oneRequired:"Et eller flere af felterne i denne formular skal udfyldes.",errorPrefix:"Fejl: ",warningPrefix:"Advarsel: ",noSpace:"Der må ikke benyttes mellemrum i dette felt.",reqChkByNode:"Foretag et valg.",requiredChk:"Dette felt skal udfyldes.",reqChkByName:"Vælg en {label}.",match:"Dette felt skal matche {matchName} feltet",startDate:"start dato",endDate:"slut dato",currendDate:"dags dato",afterDate:"Datoen skal være større end eller lig med {label}.",beforeDate:"Datoen skal være mindre end eller lig med {label}.",startMonth:"Vælg en start måned",sameMonth:"De valgte datoer skal være i samme måned - skift en af dem."});Locale.define("de-DE","Date",{months:["Januar","Februar","März","April","Mai","Juni","Juli","August","September","Oktober","November","Dezember"],months_abbr:["Jan","Feb","Mär","Apr","Mai","Jun","Jul","Aug","Sep","Okt","Nov","Dez"],days:["Sonntag","Montag","Dienstag","Mittwoch","Donnerstag","Freitag","Samstag"],days_abbr:["So.","Mo.","Di.","Mi.","Do.","Fr.","Sa."],dateOrder:["date","month","year"],shortDate:"%d.%m.%Y",shortTime:"%H:%M",AM:"vormittags",PM:"nachmittags",firstDayOfWeek:1,ordinal:".",lessThanMinuteAgo:"vor weniger als einer Minute",minuteAgo:"vor einer Minute",minutesAgo:"vor {delta} Minuten",hourAgo:"vor einer Stunde",hoursAgo:"vor {delta} Stunden",dayAgo:"vor einem Tag",daysAgo:"vor {delta} Tagen",weekAgo:"vor einer Woche",weeksAgo:"vor {delta} Wochen",monthAgo:"vor einem Monat",monthsAgo:"vor {delta} Monaten",yearAgo:"vor einem Jahr",yearsAgo:"vor {delta} Jahren",lessThanMinuteUntil:"in weniger als einer Minute",minuteUntil:"in einer Minute",minutesUntil:"in {delta} Minuten",hourUntil:"in ca. einer Stunde",hoursUntil:"in ca. {delta} Stunden",dayUntil:"in einem Tag",daysUntil:"in {delta} Tagen",weekUntil:"in einer Woche",weeksUntil:"in {delta} Wochen",monthUntil:"in einem Monat",monthsUntil:"in {delta} Monaten",yearUntil:"in einem Jahr",yearsUntil:"in {delta} Jahren"});Locale.define("de-CH").inherit("de-DE","Date");Locale.define("de-CH","FormValidator",{required:"Dieses Feld ist obligatorisch.",minLength:"Geben Sie bitte mindestens {minLength} Zeichen ein (Sie haben {length} Zeichen eingegeben).",maxLength:"Bitte geben Sie nicht mehr als {maxLength} Zeichen ein (Sie haben {length} Zeichen eingegeben).",integer:"Geben Sie bitte eine ganze Zahl ein. Dezimalzahlen (z.B. 1.25) sind nicht erlaubt.",numeric:"Geben Sie bitte nur Zahlenwerte in dieses Eingabefeld ein (z.B. &quot;1&quot;, &quot;1.1&quot;, &quot;-1&quot; oder &quot;-1.1&quot;).",digits:"Benutzen Sie bitte nur Zahlen und Satzzeichen in diesem Eingabefeld (erlaubt ist z.B. eine Telefonnummer mit Bindestrichen und Punkten).",alpha:"Benutzen Sie bitte nur Buchstaben (a-z) in diesem Feld. Leerzeichen und andere Zeichen sind nicht erlaubt.",alphanum:"Benutzen Sie bitte nur Buchstaben (a-z) und Zahlen (0-9) in diesem Eingabefeld. Leerzeichen und andere Zeichen sind nicht erlaubt.",dateSuchAs:"Geben Sie bitte ein g&uuml;ltiges Datum ein. Wie zum Beispiel {date}",dateInFormatMDY:"Geben Sie bitte ein g&uuml;ltiges Datum ein. Wie zum Beispiel TT.MM.JJJJ (z.B. &quot;31.12.1999&quot;)",email:"Geben Sie bitte eine g&uuml;ltige E-Mail Adresse ein. Wie zum Beispiel &quot;maria@bernasconi.ch&quot;.",url:"Geben Sie bitte eine g&uuml;ltige URL ein. Wie zum Beispiel http://www.example.com.",currencyDollar:"Geben Sie bitte einen g&uuml;ltigen Betrag in Schweizer Franken ein. Wie zum Beispiel 100.00 CHF .",oneRequired:"Machen Sie f&uuml;r mindestens eines der Eingabefelder einen Eintrag.",errorPrefix:"Fehler: ",warningPrefix:"Warnung: ",noSpace:"In diesem Eingabefeld darf kein Leerzeichen sein.",reqChkByNode:"Es wurden keine Elemente gew&auml;hlt.",requiredChk:"Dieses Feld ist obligatorisch.",reqChkByName:"Bitte w&auml;hlen Sie ein {label}.",match:"Dieses Eingabefeld muss mit dem Feld {matchName} &uuml;bereinstimmen.",startDate:"Das Anfangsdatum",endDate:"Das Enddatum",currendDate:"Das aktuelle Datum",afterDate:"Das Datum sollte zur gleichen Zeit oder sp&auml;ter sein {label}.",beforeDate:"Das Datum sollte zur gleichen Zeit oder fr&uuml;her sein {label}.",startMonth:"W&auml;hlen Sie bitte einen Anfangsmonat",sameMonth:"Diese zwei Datumsangaben m&uuml;ssen im selben Monat sein - Sie m&uuml;ssen eine von beiden ver&auml;ndern.",creditcard:"Die eingegebene Kreditkartennummer ist ung&uuml;ltig. Bitte &uuml;berpr&uuml;fen Sie diese und versuchen Sie es erneut. {length} Zahlen eingegeben."});Locale.define("de-DE","FormValidator",{required:"Dieses Eingabefeld muss ausgefüllt werden.",minLength:"Geben Sie bitte mindestens {minLength} Zeichen ein (Sie haben nur {length} Zeichen eingegeben).",maxLength:"Geben Sie bitte nicht mehr als {maxLength} Zeichen ein (Sie haben {length} Zeichen eingegeben).",integer:'Geben Sie in diesem Eingabefeld bitte eine ganze Zahl ein. Dezimalzahlen (z.B. "1.25") sind nicht erlaubt.',numeric:'Geben Sie in diesem Eingabefeld bitte nur Zahlenwerte (z.B. "1", "1.1", "-1" oder "-1.1") ein.',digits:"Geben Sie in diesem Eingabefeld bitte nur Zahlen und Satzzeichen ein (z.B. eine Telefonnummer mit Bindestrichen und Punkten ist erlaubt).",alpha:"Geben Sie in diesem Eingabefeld bitte nur Buchstaben (a-z) ein. Leerzeichen und andere Zeichen sind nicht erlaubt.",alphanum:"Geben Sie in diesem Eingabefeld bitte nur Buchstaben (a-z) und Zahlen (0-9) ein. Leerzeichen oder andere Zeichen sind nicht erlaubt.",dateSuchAs:'Geben Sie bitte ein gültiges Datum ein (z.B. "{date}").',dateInFormatMDY:'Geben Sie bitte ein gültiges Datum im Format TT.MM.JJJJ ein (z.B. "31.12.1999").',email:'Geben Sie bitte eine gültige E-Mail-Adresse ein (z.B. "max@mustermann.de").',url:'Geben Sie bitte eine gültige URL ein (z.B. "http://www.example.com").',currencyDollar:"Geben Sie bitte einen gültigen Betrag in EURO ein (z.B. 100.00€).",oneRequired:"Bitte füllen Sie mindestens ein Eingabefeld aus.",errorPrefix:"Fehler: ",warningPrefix:"Warnung: ",noSpace:"Es darf kein Leerzeichen in diesem Eingabefeld sein.",reqChkByNode:"Es wurden keine Elemente gewählt.",requiredChk:"Dieses Feld muss ausgefüllt werden.",reqChkByName:"Bitte wählen Sie ein {label}.",match:"Dieses Eingabefeld muss mit dem {matchName} Eingabefeld übereinstimmen.",startDate:"Das Anfangsdatum",endDate:"Das Enddatum",currendDate:"Das aktuelle Datum",afterDate:"Das Datum sollte zur gleichen Zeit oder später sein als {label}.",beforeDate:"Das Datum sollte zur gleichen Zeit oder früher sein als {label}.",startMonth:"Wählen Sie bitte einen Anfangsmonat",sameMonth:"Diese zwei Datumsangaben müssen im selben Monat sein - Sie müssen eines von beiden verändern.",creditcard:"Die eingegebene Kreditkartennummer ist ungültig. Bitte überprüfen Sie diese und versuchen Sie es erneut. {length} Zahlen eingegeben."});Locale.define("EU","Number",{decimal:",",group:".",currency:{prefix:"€ "}});Locale.define("de-DE").inherit("EU","Number");Locale.define("en-GB","Date",{dateOrder:["date","month","year"],shortDate:"%d/%m/%Y",shortTime:"%H:%M"}).inherit("en-US","Date");Locale.define("es-ES","Date",{months:["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],months_abbr:["ene","feb","mar","abr","may","jun","jul","ago","sep","oct","nov","dic"],days:["Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado"],days_abbr:["dom","lun","mar","mié","juv","vie","sáb"],dateOrder:["date","month","year"],shortDate:"%d/%m/%Y",shortTime:"%H:%M",AM:"AM",PM:"PM",firstDayOfWeek:1,ordinal:"",lessThanMinuteAgo:"hace menos de un minuto",minuteAgo:"hace un minuto",minutesAgo:"hace {delta} minutos",hourAgo:"hace una hora",hoursAgo:"hace unas {delta} horas",dayAgo:"hace un día",daysAgo:"hace {delta} días",weekAgo:"hace una semana",weeksAgo:"hace unas {delta} semanas",monthAgo:"hace un mes",monthsAgo:"hace {delta} meses",yearAgo:"hace un año",yearsAgo:"hace {delta} años",lessThanMinuteUntil:"menos de un minuto desde ahora",minuteUntil:"un minuto desde ahora",minutesUntil:"{delta} minutos desde ahora",hourUntil:"una hora desde ahora",hoursUntil:"unas {delta} horas desde ahora",dayUntil:"un día desde ahora",daysUntil:"{delta} días desde ahora",weekUntil:"una semana desde ahora",weeksUntil:"unas {delta} semanas desde ahora",monthUntil:"un mes desde ahora",monthsUntil:"{delta} meses desde ahora",yearUntil:"un año desde ahora",yearsUntil:"{delta} años desde ahora"});Locale.define("es-AR").inherit("es-ES","Date");Locale.define("es-AR","FormValidator",{required:"Este campo es obligatorio.",minLength:"Por favor ingrese al menos {minLength} caracteres (ha ingresado {length} caracteres).",maxLength:"Por favor no ingrese más de {maxLength} caracteres (ha ingresado {length} caracteres).",integer:"Por favor ingrese un número entero en este campo. Números con decimales (p.e. 1,25) no se permiten.",numeric:'Por favor ingrese solo valores numéricos en este campo (p.e. "1" o "1,1" o "-1" o "-1,1").',digits:"Por favor use sólo números y puntuación en este campo (por ejemplo, un número de teléfono con guiones y/o puntos no está permitido).",alpha:"Por favor use sólo letras (a-z) en este campo. No se permiten espacios ni otros caracteres.",alphanum:"Por favor, usa sólo letras (a-z) o números (0-9) en este campo. No se permiten espacios u otros caracteres.",dateSuchAs:"Por favor ingrese una fecha válida como {date}",dateInFormatMDY:'Por favor ingrese una fecha válida, utulizando el formato DD/MM/YYYY (p.e. "31/12/1999")',email:'Por favor, ingrese una dirección de e-mail válida. Por ejemplo, "fred@dominio.com".',url:"Por favor ingrese una URL válida como http://www.example.com.",currencyDollar:"Por favor ingrese una cantidad válida de pesos. Por ejemplo $100,00 .",oneRequired:"Por favor ingrese algo para por lo menos una de estas entradas.",errorPrefix:"Error: ",warningPrefix:"Advertencia: ",noSpace:"No se permiten espacios en este campo.",reqChkByNode:"No hay elementos seleccionados.",requiredChk:"Este campo es obligatorio.",reqChkByName:"Por favor selecciona una {label}.",match:"Este campo necesita coincidir con el campo {matchName}",startDate:"la fecha de inicio",endDate:"la fecha de fin",currendDate:"la fecha actual",afterDate:"La fecha debe ser igual o posterior a {label}.",beforeDate:"La fecha debe ser igual o anterior a {label}.",startMonth:"Por favor selecciona un mes de origen",sameMonth:"Estas dos fechas deben estar en el mismo mes - debes cambiar una u otra."});Locale.define("es-ES","FormValidator",{required:"Este campo es obligatorio.",minLength:"Por favor introduce al menos {minLength} caracteres (has introducido {length} caracteres).",maxLength:"Por favor introduce no m&aacute;s de {maxLength} caracteres (has introducido {length} caracteres).",integer:"Por favor introduce un n&uacute;mero entero en este campo. N&uacute;meros con decimales (p.e. 1,25) no se permiten.",numeric:'Por favor introduce solo valores num&eacute;ricos en este campo (p.e. "1" o "1,1" o "-1" o "-1,1").',digits:"Por favor usa solo n&uacute;meros y puntuaci&oacute;n en este campo (por ejemplo, un n&uacute;mero de tel&eacute;fono con guiones y puntos no esta permitido).",alpha:"Por favor usa letras solo (a-z) en este campo. No se admiten espacios ni otros caracteres.",alphanum:"Por favor, usa solo letras (a-z) o n&uacute;meros (0-9) en este campo. No se admiten espacios ni otros caracteres.",dateSuchAs:"Por favor introduce una fecha v&aacute;lida como {date}",dateInFormatMDY:'Por favor introduce una fecha v&aacute;lida como DD/MM/YYYY (p.e. "31/12/1999")',email:'Por favor, introduce una direcci&oacute;n de email v&aacute;lida. Por ejemplo, "fred@domain.com".',url:"Por favor introduce una URL v&aacute;lida como http://www.example.com.",currencyDollar:"Por favor introduce una cantidad v&aacute;lida de €. Por ejemplo €100,00 .",oneRequired:"Por favor introduce algo para por lo menos una de estas entradas.",errorPrefix:"Error: ",warningPrefix:"Aviso: ",noSpace:"No pueden haber espacios en esta entrada.",reqChkByNode:"No hay elementos seleccionados.",requiredChk:"Este campo es obligatorio.",reqChkByName:"Por favor selecciona una {label}.",match:"Este campo necesita coincidir con el campo {matchName}",startDate:"la fecha de inicio",endDate:"la fecha de fin",currendDate:"la fecha actual",afterDate:"La fecha debe ser igual o posterior a {label}.",beforeDate:"La fecha debe ser igual o anterior a {label}.",startMonth:"Por favor selecciona un mes de origen",sameMonth:"Estas dos fechas deben estar en el mismo mes - debes cambiar una u otra."});Locale.define("et-EE","Date",{months:["jaanuar","veebruar","märts","aprill","mai","juuni","juuli","august","september","oktoober","november","detsember"],months_abbr:["jaan","veebr","märts","apr","mai","juuni","juuli","aug","sept","okt","nov","dets"],days:["pühapäev","esmaspäev","teisipäev","kolmapäev","neljapäev","reede","laupäev"],days_abbr:["pühap","esmasp","teisip","kolmap","neljap","reede","laup"],dateOrder:["month","date","year"],shortDate:"%m.%d.%Y",shortTime:"%H:%M",AM:"AM",PM:"PM",firstDayOfWeek:1,ordinal:"",lessThanMinuteAgo:"vähem kui minut aega tagasi",minuteAgo:"umbes minut aega tagasi",minutesAgo:"{delta} minutit tagasi",hourAgo:"umbes tund aega tagasi",hoursAgo:"umbes {delta} tundi tagasi",dayAgo:"1 päev tagasi",daysAgo:"{delta} päeva tagasi",weekAgo:"1 nädal tagasi",weeksAgo:"{delta} nädalat tagasi",monthAgo:"1 kuu tagasi",monthsAgo:"{delta} kuud tagasi",yearAgo:"1 aasta tagasi",yearsAgo:"{delta} aastat tagasi",lessThanMinuteUntil:"vähem kui minuti aja pärast",minuteUntil:"umbes minuti aja pärast",minutesUntil:"{delta} minuti pärast",hourUntil:"umbes tunni aja pärast",hoursUntil:"umbes {delta} tunni pärast",dayUntil:"1 päeva pärast",daysUntil:"{delta} päeva pärast",weekUntil:"1 nädala pärast",weeksUntil:"{delta} nädala pärast",monthUntil:"1 kuu pärast",monthsUntil:"{delta} kuu pärast",yearUntil:"1 aasta pärast",yearsUntil:"{delta} aasta pärast"});Locale.define("et-EE","FormValidator",{required:"Väli peab olema täidetud.",minLength:"Palun sisestage vähemalt {minLength} tähte (te sisestasite {length} tähte).",maxLength:"Palun ärge sisestage rohkem kui {maxLength} tähte (te sisestasite {length} tähte).",integer:"Palun sisestage väljale täisarv. Kümnendarvud (näiteks 1.25) ei ole lubatud.",numeric:'Palun sisestage ainult numbreid väljale (näiteks "1", "1.1", "-1" või "-1.1").',digits:"Palun kasutage ainult numbreid ja kirjavahemärke (telefoninumbri sisestamisel on lubatud kasutada kriipse ja punkte).",alpha:"Palun kasutage ainult tähti (a-z). Tühikud ja teised sümbolid on keelatud.",alphanum:"Palun kasutage ainult tähti (a-z) või numbreid (0-9). Tühikud ja teised sümbolid on keelatud.",dateSuchAs:"Palun sisestage kehtiv kuupäev kujul {date}",dateInFormatMDY:'Palun sisestage kehtiv kuupäev kujul MM.DD.YYYY (näiteks: "12.31.1999").',email:'Palun sisestage kehtiv e-maili aadress (näiteks: "fred@domain.com").',url:"Palun sisestage kehtiv URL (näiteks: http://www.example.com).",currencyDollar:"Palun sisestage kehtiv $ summa (näiteks: $100.00).",oneRequired:"Palun sisestage midagi vähemalt ühele antud väljadest.",errorPrefix:"Viga: ",warningPrefix:"Hoiatus: ",noSpace:"Väli ei tohi sisaldada tühikuid.",reqChkByNode:"Ükski väljadest pole valitud.",requiredChk:"Välja täitmine on vajalik.",reqChkByName:"Palun valige üks {label}.",match:"Väli peab sobima {matchName} väljaga",startDate:"algkuupäev",endDate:"lõppkuupäev",currendDate:"praegune kuupäev",afterDate:"Kuupäev peab olema võrdne või pärast {label}.",beforeDate:"Kuupäev peab olema võrdne või enne {label}.",startMonth:"Palun valige algkuupäev.",sameMonth:"Antud kaks kuupäeva peavad olema samas kuus - peate muutma ühte kuupäeva."});Locale.define("fa","Date",{months:["ژانویه","فوریه","مارس","آپریل","مه","ژوئن","ژوئیه","آگوست","سپتامبر","اکتبر","نوامبر","دسامبر"],months_abbr:["1","2","3","4","5","6","7","8","9","10","11","12"],days:["یکشنبه","دوشنبه","سه شنبه","چهارشنبه","پنجشنبه","جمعه","شنبه"],days_abbr:["ي","د","س","چ","پ","ج","ش"],dateOrder:["month","date","year"],shortDate:"%m/%d/%Y",shortTime:"%I:%M%p",AM:"ق.ظ",PM:"ب.ظ",ordinal:"ام",lessThanMinuteAgo:"کمتر از یک دقیقه پیش",minuteAgo:"حدود یک دقیقه پیش",minutesAgo:"{delta} دقیقه پیش",hourAgo:"حدود یک ساعت پیش",hoursAgo:"حدود {delta} ساعت پیش",dayAgo:"1 روز پیش",daysAgo:"{delta} روز پیش",weekAgo:"1 هفته پیش",weeksAgo:"{delta} هفته پیش",monthAgo:"1 ماه پیش",monthsAgo:"{delta} ماه پیش",yearAgo:"1 سال پیش",yearsAgo:"{delta} سال پیش",lessThanMinuteUntil:"کمتر از یک دقیقه از حالا",minuteUntil:"حدود یک دقیقه از حالا",minutesUntil:"{delta} دقیقه از حالا",hourUntil:"حدود یک ساعت از حالا",hoursUntil:"حدود {delta} ساعت از حالا",dayUntil:"1 روز از حالا",daysUntil:"{delta} روز از حالا",weekUntil:"1 هفته از حالا",weeksUntil:"{delta} هفته از حالا",monthUntil:"1 ماه از حالا",monthsUntil:"{delta} ماه از حالا",yearUntil:"1 سال از حالا",yearsUntil:"{delta} سال از حالا"});Locale.define("fa","FormValidator",{required:"این فیلد الزامی است.",minLength:"شما باید حداقل {minLength} حرف وارد کنید ({length} حرف وارد کرده اید).",maxLength:"لطفا حداکثر {maxLength} حرف وارد کنید (شما {length} حرف وارد کرده اید).",integer:"لطفا از عدد صحیح استفاده کنید. اعداد اعشاری (مانند 1.25) مجاز نیستند.",numeric:'لطفا فقط داده عددی وارد کنید (مانند "1" یا "1.1" یا "1-" یا "1.1-").',digits:"لطفا فقط از اعداد و علامتها در این فیلد استفاده کنید (برای مثال شماره تلفن با خط تیره و نقطه قابل قبول است).",alpha:"لطفا فقط از حروف الفباء برای این بخش استفاده کنید. کاراکترهای دیگر و فاصله مجاز نیستند.",alphanum:"لطفا فقط از حروف الفباء و اعداد در این بخش استفاده کنید. کاراکترهای دیگر و فاصله مجاز نیستند.",dateSuchAs:"لطفا یک تاریخ معتبر مانند {date} وارد کنید.",dateInFormatMDY:'لطفا یک تاریخ معتبر به شکل MM/DD/YYYY وارد کنید (مانند "12/31/1999").',email:'لطفا یک آدرس ایمیل معتبر وارد کنید. برای مثال "fred@domain.com".',url:"لطفا یک URL معتبر مانند http://www.example.com وارد کنید.",currencyDollar:"لطفا یک محدوده معتبر برای این بخش وارد کنید مانند 100.00$ .",oneRequired:"لطفا حداقل یکی از فیلدها را پر کنید.",errorPrefix:"خطا: ",warningPrefix:"هشدار: ",noSpace:"استفاده از فاصله در این بخش مجاز نیست.",reqChkByNode:"موردی انتخاب نشده است.",requiredChk:"این فیلد الزامی است.",reqChkByName:"لطفا یک {label} را انتخاب کنید.",match:"این فیلد باید با فیلد {matchName} مطابقت داشته باشد.",startDate:"تاریخ شروع",endDate:"تاریخ پایان",currendDate:"تاریخ کنونی",afterDate:"تاریخ میبایست برابر یا بعد از {label} باشد",beforeDate:"تاریخ میبایست برابر یا قبل از {label} باشد",startMonth:"لطفا ماه شروع را انتخاب کنید",sameMonth:"این دو تاریخ باید در یک ماه باشند - شما باید یکی یا هر دو را تغییر دهید.",creditcard:"شماره کارت اعتباری که وارد کرده اید معتبر نیست. لطفا شماره را بررسی کنید و مجددا تلاش کنید. {length} رقم وارد شده است."});Locale.define("fi-FI","Date",{months:["tammikuu","helmikuu","maaliskuu","huhtikuu","toukokuu","kesäkuu","heinäkuu","elokuu","syyskuu","lokakuu","marraskuu","joulukuu"],months_abbr:["tammik.","helmik.","maalisk.","huhtik.","toukok.","kesäk.","heinäk.","elok.","syysk.","lokak.","marrask.","jouluk."],days:["sunnuntai","maanantai","tiistai","keskiviikko","torstai","perjantai","lauantai"],days_abbr:["su","ma","ti","ke","to","pe","la"],dateOrder:["date","month","year"],shortDate:"%d.%m.%Y",shortTime:"%H:%M",AM:"AM",PM:"PM",firstDayOfWeek:1,ordinal:".",lessThanMinuteAgo:"vajaa minuutti sitten",minuteAgo:"noin minuutti sitten",minutesAgo:"{delta} minuuttia sitten",hourAgo:"noin tunti sitten",hoursAgo:"noin {delta} tuntia sitten",dayAgo:"päivä sitten",daysAgo:"{delta} päivää sitten",weekAgo:"viikko sitten",weeksAgo:"{delta} viikkoa sitten",monthAgo:"kuukausi sitten",monthsAgo:"{delta} kuukautta sitten",yearAgo:"vuosi sitten",yearsAgo:"{delta} vuotta sitten",lessThanMinuteUntil:"vajaan minuutin kuluttua",minuteUntil:"noin minuutin kuluttua",minutesUntil:"{delta} minuutin kuluttua",hourUntil:"noin tunnin kuluttua",hoursUntil:"noin {delta} tunnin kuluttua",dayUntil:"päivän kuluttua",daysUntil:"{delta} päivän kuluttua",weekUntil:"viikon kuluttua",weeksUntil:"{delta} viikon kuluttua",monthUntil:"kuukauden kuluttua",monthsUntil:"{delta} kuukauden kuluttua",yearUntil:"vuoden kuluttua",yearsUntil:"{delta} vuoden kuluttua"});Locale.define("fi-FI","FormValidator",{required:"Tämä kenttä on pakollinen.",minLength:"Ole hyvä ja anna vähintään {minLength} merkkiä (annoit {length} merkkiä).",maxLength:"Älä anna enempää kuin {maxLength} merkkiä (annoit {length} merkkiä).",integer:"Ole hyvä ja anna kokonaisluku. Luvut, joissa on desimaaleja (esim. 1.25) eivät ole sallittuja.",numeric:'Anna tähän kenttään lukuarvo (kuten "1" tai "1.1" tai "-1" tai "-1.1").',digits:"Käytä pelkästään numeroita ja välimerkkejä tässä kentässä (syötteet, kuten esim. puhelinnumero, jossa on väliviivoja, pilkkuja tai pisteitä, kelpaa).",alpha:"Anna tähän kenttään vain kirjaimia (a-z). Välilyönnit tai muut merkit eivät ole sallittuja.",alphanum:"Anna tähän kenttään vain kirjaimia (a-z) tai numeroita (0-9). Välilyönnit tai muut merkit eivät ole sallittuja.",dateSuchAs:"Ole hyvä ja anna kelvollinen päivmäärä, kuten esimerkiksi {date}",dateInFormatMDY:'Ole hyvä ja anna kelvollinen päivämäärä muodossa pp/kk/vvvv (kuten "12/31/1999")',email:'Ole hyvä ja anna kelvollinen sähköpostiosoite (kuten esimerkiksi "matti@meikalainen.com").',url:"Ole hyvä ja anna kelvollinen URL, kuten esimerkiksi http://www.example.com.",currencyDollar:"Ole hyvä ja anna kelvollinen eurosumma (kuten esimerkiksi 100,00 EUR) .",oneRequired:"Ole hyvä ja syötä jotakin ainakin johonkin näistä kentistä.",errorPrefix:"Virhe: ",warningPrefix:"Varoitus: ",noSpace:"Tässä syötteessä ei voi olla välilyöntejä",reqChkByNode:"Ei valintoja.",requiredChk:"Tämä kenttä on pakollinen.",reqChkByName:"Ole hyvä ja valitse {label}.",match:"Tämän kentän tulee vastata kenttää {matchName}",startDate:"alkupäivämäärä",endDate:"loppupäivämäärä",currendDate:"nykyinen päivämäärä",afterDate:"Päivämäärän tulisi olla sama tai myöhäisempi ajankohta kuin {label}.",beforeDate:"Päivämäärän tulisi olla sama tai aikaisempi ajankohta kuin {label}.",startMonth:"Ole hyvä ja valitse aloituskuukausi",sameMonth:"Näiden kahden päivämäärän tulee olla saman kuun sisällä -- sinun pitää muuttaa jompaa kumpaa.",creditcard:"Annettu luottokortin numero ei kelpaa. Ole hyvä ja tarkista numero sekä yritä uudelleen. {length} numeroa syötetty."});Locale.define("fi-FI","Number",{group:" "}).inherit("EU","Number");Locale.define("fr-FR","Date",{months:["Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre"],months_abbr:["janv.","févr.","mars","avr.","mai","juin","juil.","août","sept.","oct.","nov.","déc."],days:["Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi"],days_abbr:["dim.","lun.","mar.","mer.","jeu.","ven.","sam."],dateOrder:["date","month","year"],shortDate:"%d/%m/%Y",shortTime:"%H:%M",AM:"AM",PM:"PM",firstDayOfWeek:1,ordinal:function(a){return(a>1)?"":"er";},lessThanMinuteAgo:"il y a moins d'une minute",minuteAgo:"il y a une minute",minutesAgo:"il y a {delta} minutes",hourAgo:"il y a une heure",hoursAgo:"il y a {delta} heures",dayAgo:"il y a un jour",daysAgo:"il y a {delta} jours",weekAgo:"il y a une semaine",weeksAgo:"il y a {delta} semaines",monthAgo:"il y a 1 mois",monthsAgo:"il y a {delta} mois",yearthAgo:"il y a 1 an",yearsAgo:"il y a {delta} ans",lessThanMinuteUntil:"dans moins d'une minute",minuteUntil:"dans une minute",minutesUntil:"dans {delta} minutes",hourUntil:"dans une heure",hoursUntil:"dans {delta} heures",dayUntil:"dans un jour",daysUntil:"dans {delta} jours",weekUntil:"dans 1 semaine",weeksUntil:"dans {delta} semaines",monthUntil:"dans 1 mois",monthsUntil:"dans {delta} mois",yearUntil:"dans 1 an",yearsUntil:"dans {delta} ans"});Locale.define("fr-FR","FormValidator",{required:"Ce champ est obligatoire.",length:"Veuillez saisir {length} caract&egrave;re(s) (vous avez saisi {elLength} caract&egrave;re(s)",minLength:"Veuillez saisir un minimum de {minLength} caract&egrave;re(s) (vous avez saisi {length} caract&egrave;re(s)).",maxLength:"Veuillez saisir un maximum de {maxLength} caract&egrave;re(s) (vous avez saisi {length} caract&egrave;re(s)).",integer:'Veuillez saisir un nombre entier dans ce champ. Les nombres d&eacute;cimaux (ex : "1,25") ne sont pas autoris&eacute;s.',numeric:'Veuillez saisir uniquement des chiffres dans ce champ (ex : "1" ou "1,1" ou "-1" ou "-1,1").',digits:"Veuillez saisir uniquement des chiffres et des signes de ponctuation dans ce champ (ex : un num&eacute;ro de t&eacute;l&eacute;phone avec des traits d'union est autoris&eacute;).",alpha:"Veuillez saisir uniquement des lettres (a-z) dans ce champ. Les espaces ou autres caract&egrave;res ne sont pas autoris&eacute;s.",alphanum:"Veuillez saisir uniquement des lettres (a-z) ou des chiffres (0-9) dans ce champ. Les espaces ou autres caract&egrave;res ne sont pas autoris&eacute;s.",dateSuchAs:"Veuillez saisir une date correcte comme {date}",dateInFormatMDY:'Veuillez saisir une date correcte, au format JJ/MM/AAAA (ex : "31/11/1999").',email:'Veuillez saisir une adresse de courrier &eacute;lectronique. Par example "fred@domaine.com".',url:"Veuillez saisir une URL, comme http://www.example.com.",currencyDollar:"Veuillez saisir une quantit&eacute; correcte. Par example 100,00&euro;.",oneRequired:"Veuillez s&eacute;lectionner au moins une de ces options.",errorPrefix:"Erreur : ",warningPrefix:"Attention : ",noSpace:"Ce champ n'accepte pas les espaces.",reqChkByNode:"Aucun &eacute;l&eacute;ment n'est s&eacute;lectionn&eacute;.",requiredChk:"Ce champ est obligatoire.",reqChkByName:"Veuillez s&eacute;lectionner un(e) {label}.",match:"Ce champ doit correspondre avec le champ {matchName}.",startDate:"date de d&eacute;but",endDate:"date de fin",currendDate:"date actuelle",afterDate:"La date doit &ecirc;tre identique ou post&eacute;rieure &agrave; {label}.",beforeDate:"La date doit &ecirc;tre identique ou ant&eacute;rieure &agrave; {label}.",startMonth:"Veuillez s&eacute;lectionner un mois de d&eacute;but.",sameMonth:"Ces deux dates doivent &ecirc;tre dans le m&ecirc;me mois - vous devez en modifier une.",creditcard:"Le num&eacute;ro de carte de cr&eacute;dit est invalide. Merci de v&eacute;rifier le num&eacute;ro et de r&eacute;essayer. Vous avez entr&eacute; {length} chiffre(s)."});Locale.define("fr-FR","Number",{group:" "}).inherit("EU","Number");Locale.define("he-IL","Date",{months:["ינואר","פברואר","מרץ","אפריל","מאי","יוני","יולי","אוגוסט","ספטמבר","אוקטובר","נובמבר","דצמבר"],months_abbr:["ינואר","פברואר","מרץ","אפריל","מאי","יוני","יולי","אוגוסט","ספטמבר","אוקטובר","נובמבר","דצמבר"],days:["ראשון","שני","שלישי","רביעי","חמישי","שישי","שבת"],days_abbr:["ראשון","שני","שלישי","רביעי","חמישי","שישי","שבת"],dateOrder:["date","month","year"],shortDate:"%d/%m/%Y",shortTime:"%H:%M",AM:"AM",PM:"PM",firstDayOfWeek:0,ordinal:"",lessThanMinuteAgo:"לפני פחות מדקה",minuteAgo:"לפני כדקה",minutesAgo:"לפני {delta} דקות",hourAgo:"לפני כשעה",hoursAgo:"לפני {delta} שעות",dayAgo:"לפני יום",daysAgo:"לפני {delta} ימים",weekAgo:"לפני שבוע",weeksAgo:"לפני {delta} שבועות",monthAgo:"לפני חודש",monthsAgo:"לפני {delta} חודשים",yearAgo:"לפני שנה",yearsAgo:"לפני {delta} שנים",lessThanMinuteUntil:"בעוד פחות מדקה",minuteUntil:"בעוד כדקה",minutesUntil:"בעוד {delta} דקות",hourUntil:"בעוד כשעה",hoursUntil:"בעוד {delta} שעות",dayUntil:"בעוד יום",daysUntil:"בעוד {delta} ימים",weekUntil:"בעוד שבוע",weeksUntil:"בעוד {delta} שבועות",monthUntil:"בעוד חודש",monthsUntil:"בעוד {delta} חודשים",yearUntil:"בעוד שנה",yearsUntil:"בעוד {delta} שנים"});Locale.define("he-IL","FormValidator",{required:"נא למלא שדה זה.",minLength:"נא להזין לפחות {minLength} תווים (הזנת {length} תווים).",maxLength:"נא להזין עד {maxLength} תווים (הזנת {length} תווים).",integer:"נא להזין מספר שלם לשדה זה. מספרים עשרוניים (כמו 1.25) אינם חוקיים.",numeric:'נא להזין ערך מספרי בלבד בשדה זה (כמו "1", "1.1", "-1" או "-1.1").',digits:"נא להזין רק ספרות וסימני הפרדה בשדה זה (למשל, מספר טלפון עם מקפים או נקודות הוא חוקי).",alpha:"נא להזין רק אותיות באנגלית (a-z) בשדה זה. רווחים או תווים אחרים אינם חוקיים.",alphanum:"נא להזין רק אותריות באנגלית (a-z) או ספרות (0-9) בשדה זה. אווחרים או תווים אחרים אינם חוקיים.",dateSuchAs:"נא להזין תאריך חוקי, כמו {date}",dateInFormatMDY:'נא להזין תאריך חוקי בפורמט MM/DD/YYYY (כמו "12/31/1999")',email:'נא להזין כתובת אימייל חוקית. לדוגמה: "fred@domain.com".',url:"נא להזין כתובת אתר חוקית, כמו http://www.example.com.",currencyDollar:"נא להזין סכום דולרי חוקי. לדוגמה $100.00.",oneRequired:"נא לבחור לפחות בשדה אחד.",errorPrefix:"שגיאה: ",warningPrefix:"אזהרה: ",noSpace:"אין להזין רווחים בשדה זה.",reqChkByNode:"נא לבחור אחת מהאפשרויות.",requiredChk:"שדה זה נדרש.",reqChkByName:"נא לבחור {label}.",match:"שדה זה צריך להתאים לשדה {matchName}",startDate:"תאריך ההתחלה",endDate:"תאריך הסיום",currendDate:"התאריך הנוכחי",afterDate:"התאריך צריך להיות זהה או אחרי {label}.",beforeDate:"התאריך צריך להיות זהה או לפני {label}.",startMonth:"נא לבחור חודש התחלה",sameMonth:"שני תאריכים אלה צריכים להיות באותו חודש - נא לשנות אחד התאריכים.",creditcard:"מספר כרטיס האשראי שהוזן אינו חוקי. נא לבדוק שנית. הוזנו {length} ספרות."});Locale.define("he-IL","Number",{decimal:".",group:",",currency:{suffix:" ₪"}});Locale.define("hu-HU","Date",{months:["Január","Február","Március","Április","Május","Június","Július","Augusztus","Szeptember","Október","November","December"],months_abbr:["jan.","febr.","márc.","ápr.","máj.","jún.","júl.","aug.","szept.","okt.","nov.","dec."],days:["Vasárnap","Hétfő","Kedd","Szerda","Csütörtök","Péntek","Szombat"],days_abbr:["V","H","K","Sze","Cs","P","Szo"],dateOrder:["year","month","date"],shortDate:"%Y.%m.%d.",shortTime:"%I:%M",AM:"de.",PM:"du.",firstDayOfWeek:1,ordinal:".",lessThanMinuteAgo:"alig egy perce",minuteAgo:"egy perce",minutesAgo:"{delta} perce",hourAgo:"egy órája",hoursAgo:"{delta} órája",dayAgo:"1 napja",daysAgo:"{delta} napja",weekAgo:"1 hete",weeksAgo:"{delta} hete",monthAgo:"1 hónapja",monthsAgo:"{delta} hónapja",yearAgo:"1 éve",yearsAgo:"{delta} éve",lessThanMinuteUntil:"alig egy perc múlva",minuteUntil:"egy perc múlva",minutesUntil:"{delta} perc múlva",hourUntil:"egy óra múlva",hoursUntil:"{delta} óra múlva",dayUntil:"1 nap múlva",daysUntil:"{delta} nap múlva",weekUntil:"1 hét múlva",weeksUntil:"{delta} hét múlva",monthUntil:"1 hónap múlva",monthsUntil:"{delta} hónap múlva",yearUntil:"1 év múlva",yearsUntil:"{delta} év múlva"});Locale.define("hu-HU","FormValidator",{required:"A mező kitöltése kötelező.",minLength:"Legalább {minLength} karakter megadása szükséges (megadva {length} karakter).",maxLength:"Legfeljebb {maxLength} karakter megadása lehetséges (megadva {length} karakter).",integer:"Egész szám megadása szükséges. A tizedesjegyek (pl. 1.25) nem engedélyezettek.",numeric:'Szám megadása szükséges (pl. "1" vagy "1.1" vagy "-1" vagy "-1.1").',digits:"Csak számok és írásjelek megadása lehetséges (pl. telefonszám kötőjelek és/vagy perjelekkel).",alpha:"Csak betűk (a-z) megadása lehetséges. Szóköz és egyéb karakterek nem engedélyezettek.",alphanum:"Csak betűk (a-z) vagy számok (0-9) megadása lehetséges. Szóköz és egyéb karakterek nem engedélyezettek.",dateSuchAs:"Valós dátum megadása szükséges (pl. {date}).",dateInFormatMDY:'Valós dátum megadása szükséges ÉÉÉÉ.HH.NN. formában. (pl. "1999.12.31.")',email:'Valós e-mail cím megadása szükséges (pl. "fred@domain.hu").',url:"Valós URL megadása szükséges (pl. http://www.example.com).",currencyDollar:"Valós pénzösszeg megadása szükséges (pl. 100.00 Ft.).",oneRequired:"Az alábbi mezők legalább egyikének kitöltése kötelező.",errorPrefix:"Hiba: ",warningPrefix:"Figyelem: ",noSpace:"A mező nem tartalmazhat szóközöket.",reqChkByNode:"Nincs egyetlen kijelölt elem sem.",requiredChk:"A mező kitöltése kötelező.",reqChkByName:"Egy {label} kiválasztása szükséges.",match:"A mezőnek egyeznie kell a(z) {matchName} mezővel.",startDate:"a kezdet dátuma",endDate:"a vég dátuma",currendDate:"jelenlegi dátum",afterDate:"A dátum nem lehet kisebb, mint {label}.",beforeDate:"A dátum nem lehet nagyobb, mint {label}.",startMonth:"Kezdeti hónap megadása szükséges.",sameMonth:"A két dátumnak ugyanazon hónapban kell lennie.",creditcard:"A megadott bankkártyaszám nem valódi (megadva {length} számjegy)."});Locale.define("it-IT","Date",{months:["Gennaio","Febbraio","Marzo","Aprile","Maggio","Giugno","Luglio","Agosto","Settembre","Ottobre","Novembre","Dicembre"],months_abbr:["gen","feb","mar","apr","mag","giu","lug","ago","set","ott","nov","dic"],days:["Domenica","Lunedì","Martedì","Mercoledì","Giovedì","Venerdì","Sabato"],days_abbr:["dom","lun","mar","mer","gio","ven","sab"],dateOrder:["date","month","year"],shortDate:"%d/%m/%Y",shortTime:"%H.%M",AM:"AM",PM:"PM",firstDayOfWeek:1,ordinal:"º",lessThanMinuteAgo:"meno di un minuto fa",minuteAgo:"circa un minuto fa",minutesAgo:"circa {delta} minuti fa",hourAgo:"circa un'ora fa",hoursAgo:"circa {delta} ore fa",dayAgo:"circa 1 giorno fa",daysAgo:"circa {delta} giorni fa",weekAgo:"una settimana fa",weeksAgo:"{delta} settimane fa",monthAgo:"un mese fa",monthsAgo:"{delta} mesi fa",yearAgo:"un anno fa",yearsAgo:"{delta} anni fa",lessThanMinuteUntil:"tra meno di un minuto",minuteUntil:"tra circa un minuto",minutesUntil:"tra circa {delta} minuti",hourUntil:"tra circa un'ora",hoursUntil:"tra circa {delta} ore",dayUntil:"tra circa un giorno",daysUntil:"tra circa {delta} giorni",weekUntil:"tra una settimana",weeksUntil:"tra {delta} settimane",monthUntil:"tra un mese",monthsUntil:"tra {delta} mesi",yearUntil:"tra un anno",yearsUntil:"tra {delta} anni"});Locale.define("it-IT","FormValidator",{required:"Il campo &egrave; obbligatorio.",minLength:"Inserire almeno {minLength} caratteri (ne sono stati inseriti {length}).",maxLength:"Inserire al massimo {maxLength} caratteri (ne sono stati inseriti {length}).",integer:"Inserire un numero intero. Non sono consentiti decimali (es.: 1.25).",numeric:'Inserire solo valori numerici (es.: "1" oppure "1.1" oppure "-1" oppure "-1.1").',digits:"Inserire solo numeri e caratteri di punteggiatura. Per esempio &egrave; consentito un numero telefonico con trattini o punti.",alpha:"Inserire solo lettere (a-z). Non sono consentiti spazi o altri caratteri.",alphanum:"Inserire solo lettere (a-z) o numeri (0-9). Non sono consentiti spazi o altri caratteri.",dateSuchAs:"Inserire una data valida del tipo {date}",dateInFormatMDY:'Inserire una data valida nel formato MM/GG/AAAA (es.: "12/31/1999")',email:'Inserire un indirizzo email valido. Per esempio "nome@dominio.com".',url:'Inserire un indirizzo valido. Per esempio "http://www.example.com".',currencyDollar:'Inserire un importo valido. Per esempio "$100.00".',oneRequired:"Completare almeno uno dei campi richiesti.",errorPrefix:"Errore: ",warningPrefix:"Attenzione: ",noSpace:"Non sono consentiti spazi.",reqChkByNode:"Nessuna voce selezionata.",requiredChk:"Il campo &egrave; obbligatorio.",reqChkByName:"Selezionare un(a) {label}.",match:"Il valore deve corrispondere al campo {matchName}",startDate:"data d'inizio",endDate:"data di fine",currendDate:"data attuale",afterDate:"La data deve corrispondere o essere successiva al {label}.",beforeDate:"La data deve corrispondere o essere precedente al {label}.",startMonth:"Selezionare un mese d'inizio",sameMonth:"Le due date devono essere dello stesso mese - occorre modificarne una."});Locale.define("ja-JP","Date",{months:["1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月"],months_abbr:["1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月"],days:["日曜日","月曜日","火曜日","水曜日","木曜日","金曜日","土曜日"],days_abbr:["日","月","火","水","木","金","土"],dateOrder:["year","month","date"],shortDate:"%Y/%m/%d",shortTime:"%H:%M",AM:"午前",PM:"午後",firstDayOfWeek:0,ordinal:"",lessThanMinuteAgo:"1分以内前",minuteAgo:"約1分前",minutesAgo:"約{delta}分前",hourAgo:"約1時間前",hoursAgo:"約{delta}時間前",dayAgo:"1日前",daysAgo:"{delta}日前",weekAgo:"1週間前",weeksAgo:"{delta}週間前",monthAgo:"1ヶ月前",monthsAgo:"{delta}ヶ月前",yearAgo:"1年前",yearsAgo:"{delta}年前",lessThanMinuteUntil:"今から約1分以内",minuteUntil:"今から約1分",minutesUntil:"今から約{delta}分",hourUntil:"今から約1時間",hoursUntil:"今から約{delta}時間",dayUntil:"今から1日間",daysUntil:"今から{delta}日間",weekUntil:"今から1週間",weeksUntil:"今から{delta}週間",monthUntil:"今から1ヶ月",monthsUntil:"今から{delta}ヶ月",yearUntil:"今から1年",yearsUntil:"今から{delta}年"});Locale.define("ja-JP","FormValidator",{required:"入力は必須です。",minLength:"入力文字数は{minLength}以上にしてください。({length}文字)",maxLength:"入力文字数は{maxLength}以下にしてください。({length}文字)",integer:"整数を入力してください。",numeric:'入力できるのは数値だけです。(例: "1", "1.1", "-1", "-1.1"....)',digits:"入力できるのは数値と句読記号です。 (例: -や+を含む電話番号など).",alpha:"入力できるのは半角英字だけです。それ以外の文字は入力できません。",alphanum:"入力できるのは半角英数字だけです。それ以外の文字は入力できません。",dateSuchAs:"有効な日付を入力してください。{date}",dateInFormatMDY:'日付の書式に誤りがあります。YYYY/MM/DD (i.e. "1999/12/31")',email:"メールアドレスに誤りがあります。",url:"URLアドレスに誤りがあります。",currencyDollar:"金額に誤りがあります。",oneRequired:"ひとつ以上入力してください。",errorPrefix:"エラー: ",warningPrefix:"警告: ",noSpace:"スペースは入力できません。",reqChkByNode:"選択されていません。",requiredChk:"この項目は必須です。",reqChkByName:"{label}を選択してください。",match:"{matchName}が入力されている場合必須です。",startDate:"開始日",endDate:"終了日",currendDate:"今日",afterDate:"{label}以降の日付にしてください。",beforeDate:"{label}以前の日付にしてください。",startMonth:"開始月を選択してください。",sameMonth:"日付が同一です。どちらかを変更してください。"});Locale.define("ja-JP","Number",{decimal:".",group:",",currency:{decimals:0,prefix:"\\"}});Locale.define("nl-NL","Date",{months:["januari","februari","maart","april","mei","juni","juli","augustus","september","oktober","november","december"],months_abbr:["jan","feb","mrt","apr","mei","jun","jul","aug","sep","okt","nov","dec"],days:["zondag","maandag","dinsdag","woensdag","donderdag","vrijdag","zaterdag"],days_abbr:["zo","ma","di","wo","do","vr","za"],dateOrder:["date","month","year"],shortDate:"%d-%m-%Y",shortTime:"%H:%M",AM:"AM",PM:"PM",firstDayOfWeek:1,ordinal:"e",lessThanMinuteAgo:"minder dan een minuut geleden",minuteAgo:"ongeveer een minuut geleden",minutesAgo:"{delta} minuten geleden",hourAgo:"ongeveer een uur geleden",hoursAgo:"ongeveer {delta} uur geleden",dayAgo:"een dag geleden",daysAgo:"{delta} dagen geleden",weekAgo:"een week geleden",weeksAgo:"{delta} weken geleden",monthAgo:"een maand geleden",monthsAgo:"{delta} maanden geleden",yearAgo:"een jaar geleden",yearsAgo:"{delta} jaar geleden",lessThanMinuteUntil:"over minder dan een minuut",minuteUntil:"over ongeveer een minuut",minutesUntil:"over {delta} minuten",hourUntil:"over ongeveer een uur",hoursUntil:"over {delta} uur",dayUntil:"over ongeveer een dag",daysUntil:"over {delta} dagen",weekUntil:"over een week",weeksUntil:"over {delta} weken",monthUntil:"over een maand",monthsUntil:"over {delta} maanden",yearUntil:"over een jaar",yearsUntil:"over {delta} jaar"});Locale.define("nl-NL","FormValidator",{required:"Dit veld is verplicht.",length:"Vul precies {length} karakters in (je hebt {elLength} karakters ingevoerd).",minLength:"Vul minimaal {minLength} karakters in (je hebt {length} karakters ingevoerd).",maxLength:"Vul niet meer dan {maxLength} karakters in (je hebt {length} karakters ingevoerd).",integer:"Vul een getal in. Getallen met decimalen (bijvoorbeeld 1.25) zijn niet toegestaan.",numeric:'Vul alleen numerieke waarden in (bijvoorbeeld "1" of "1.1" of "-1" of "-1.1").',digits:"Vul alleen nummers en leestekens in (bijvoorbeeld een telefoonnummer met streepjes is toegestaan).",alpha:"Vul alleen letters in (a-z). Spaties en andere karakters zijn niet toegestaan.",alphanum:"Vul alleen letters (a-z) of nummers (0-9) in. Spaties en andere karakters zijn niet toegestaan.",dateSuchAs:"Vul een geldige datum in, zoals {date}",dateInFormatMDY:'Vul een geldige datum, in het formaat MM/DD/YYYY (bijvoorbeeld "12/31/1999")',email:'Vul een geldig e-mailadres in. Bijvoorbeeld "fred@domein.nl".',url:"Vul een geldige URL in, zoals http://www.example.com.",currencyDollar:"Vul een geldig $ bedrag in. Bijvoorbeeld $100.00 .",oneRequired:"Vul iets in bij in ieder geval een van deze velden.",warningPrefix:"Waarschuwing: ",errorPrefix:"Fout: ",noSpace:"Spaties zijn niet toegestaan in dit veld.",reqChkByNode:"Er zijn geen items geselecteerd.",requiredChk:"Dit veld is verplicht.",reqChkByName:"Selecteer een {label}.",match:"Dit veld moet overeen komen met het {matchName} veld",startDate:"de begin datum",endDate:"de eind datum",currendDate:"de huidige datum",afterDate:"De datum moet hetzelfde of na {label} zijn.",beforeDate:"De datum moet hetzelfde of voor {label} zijn.",startMonth:"Selecteer een begin maand",sameMonth:"Deze twee data moeten in dezelfde maand zijn - u moet een van beide aanpassen.",creditcard:"Het ingevulde creditcardnummer is niet geldig. Controleer het nummer en probeer opnieuw. {length} getallen ingevuld."});Locale.define("nl-NL").inherit("EU","Number");Locale.define("no-NO","Date",{dateOrder:["date","month","year"],shortDate:"%d.%m.%Y",shortTime:"%H:%M",AM:"AM",PM:"PM",firstDayOfWeek:1,lessThanMinuteAgo:"kortere enn et minutt siden",minuteAgo:"omtrent et minutt siden",minutesAgo:"{delta} minutter siden",hourAgo:"omtrent en time siden",hoursAgo:"omtrent {delta} timer siden",dayAgo:"{delta} dag siden",daysAgo:"{delta} dager siden"});Locale.define("no-NO","FormValidator",{required:"Dette feltet er pÃ¥krevd.",minLength:"Vennligst skriv inn minst {minLength} tegn (du skrev {length} tegn).",maxLength:"Vennligst skriv inn maksimalt {maxLength} tegn (du skrev {length} tegn).",integer:"Vennligst skriv inn et tall i dette feltet. Tall med desimaler (for eksempel 1,25) er ikke tillat.",numeric:'Vennligst skriv inn kun numeriske verdier i dette feltet (for eksempel "1", "1.1", "-1" eller "-1.1").',digits:"Vennligst bruk kun nummer og skilletegn i dette feltet.",alpha:"Vennligst bruk kun bokstaver (a-z) i dette feltet. Ingen mellomrom eller andre tegn er tillat.",alphanum:"Vennligst bruk kun bokstaver (a-z) eller nummer (0-9) i dette feltet. Ingen mellomrom eller andre tegn er tillat.",dateSuchAs:"Vennligst skriv inn en gyldig dato, som {date}",dateInFormatMDY:'Vennligst skriv inn en gyldig dato, i formatet MM/DD/YYYY (for eksempel "12/31/1999")',email:'Vennligst skriv inn en gyldig epost-adresse. For eksempel "espen@domene.no".',url:"Vennligst skriv inn en gyldig URL, for eksempel http://www.example.com.",currencyDollar:"Vennligst fyll ut et gyldig $ belÃ¸p. For eksempel $100.00 .",oneRequired:"Vennligst fyll ut noe i minst ett av disse feltene.",errorPrefix:"Feil: ",warningPrefix:"Advarsel: "});Locale.define("pl-PL","Date",{months:["Styczeń","Luty","Marzec","Kwiecień","Maj","Czerwiec","Lipiec","Sierpień","Wrzesień","Październik","Listopad","Grudzień"],months_abbr:["sty","lut","mar","kwi","maj","cze","lip","sie","wrz","paź","lis","gru"],days:["Niedziela","Poniedziałek","Wtorek","Środa","Czwartek","Piątek","Sobota"],days_abbr:["niedz.","pon.","wt.","śr.","czw.","pt.","sob."],dateOrder:["year","month","date"],shortDate:"%Y-%m-%d",shortTime:"%H:%M",AM:"nad ranem",PM:"po południu",firstDayOfWeek:1,ordinal:function(a){return(a>3&&a<21)?"ty":["ty","szy","gi","ci","ty"][Math.min(a%10,4)];},lessThanMinuteAgo:"mniej niż minute temu",minuteAgo:"około minutę temu",minutesAgo:"{delta} minut temu",hourAgo:"około godzinę temu",hoursAgo:"około {delta} godzin temu",dayAgo:"Wczoraj",daysAgo:"{delta} dni temu",lessThanMinuteUntil:"za niecałą minutę",minuteUntil:"za około minutę",minutesUntil:"za {delta} minut",hourUntil:"za około godzinę",hoursUntil:"za około {delta} godzin",dayUntil:"za 1 dzień",daysUntil:"za {delta} dni"});Locale.define("pl-PL","FormValidator",{required:"To pole jest wymagane.",minLength:"Wymagane jest przynajmniej {minLength} znaków (wpisanych zostało tylko {length}).",maxLength:"Dozwolone jest nie więcej niż {maxLength} znaków (wpisanych zostało {length})",integer:"To pole wymaga liczb całych. Liczby dziesiętne (np. 1.25) są niedozwolone.",numeric:'Prosimy używać tylko numerycznych wartości w tym polu (np. "1", "1.1", "-1" lub "-1.1").',digits:"Prosimy używać liczb oraz zankow punktuacyjnych w typ polu (dla przykładu, przy numerze telefonu myślniki i kropki są dozwolone).",alpha:"Prosimy używać tylko liter (a-z) w tym polu. Spacje oraz inne znaki są niedozwolone.",alphanum:"Prosimy używać tylko liter (a-z) lub liczb (0-9) w tym polu. Spacje oraz inne znaki są niedozwolone.",dateSuchAs:"Prosimy podać prawidłową datę w formacie: {date}",dateInFormatMDY:'Prosimy podać poprawną date w formacie DD.MM.RRRR (i.e. "12.01.2009")',email:'Prosimy podać prawidłowy adres e-mail, np. "jan@domena.pl".',url:"Prosimy podać prawidłowy adres URL, np. http://www.example.com.",currencyDollar:"Prosimy podać prawidłową sumę w PLN. Dla przykładu: 100.00 PLN.",oneRequired:"Prosimy wypełnić chociaż jedno z pól.",errorPrefix:"Błąd: ",warningPrefix:"Uwaga: ",noSpace:"W tym polu nie mogą znajdować się spacje.",reqChkByNode:"Brak zaznaczonych elementów.",requiredChk:"To pole jest wymagane.",reqChkByName:"Prosimy wybrać z {label}.",match:"To pole musi być takie samo jak {matchName}",startDate:"data początkowa",endDate:"data końcowa",currendDate:"aktualna data",afterDate:"Podana data poinna być taka sama lub po {label}.",beforeDate:"Podana data poinna być taka sama lub przed {label}.",startMonth:"Prosimy wybrać początkowy miesiąc.",sameMonth:"Te dwie daty muszą być w zakresie tego samego miesiąca - wymagana jest zmiana któregoś z pól."});Locale.define("pt-PT","Date",{months:["Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],months_abbr:["Jan","Fev","Mar","Abr","Mai","Jun","Jul","Ago","Set","Out","Nov","Dez"],days:["Domingo","Segunda-feira","Terça-feira","Quarta-feira","Quinta-feira","Sexta-feira","Sábado"],days_abbr:["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],dateOrder:["date","month","year"],shortDate:"%d-%m-%Y",shortTime:"%H:%M",AM:"AM",PM:"PM",firstDayOfWeek:1,ordinal:"º",lessThanMinuteAgo:"há menos de um minuto",minuteAgo:"há cerca de um minuto",minutesAgo:"há {delta} minutos",hourAgo:"há cerca de uma hora",hoursAgo:"há cerca de {delta} horas",dayAgo:"há um dia",daysAgo:"há {delta} dias",weekAgo:"há uma semana",weeksAgo:"há {delta} semanas",monthAgo:"há um mês",monthsAgo:"há {delta} meses",yearAgo:"há um ano",yearsAgo:"há {delta} anos",lessThanMinuteUntil:"em menos de um minuto",minuteUntil:"em um minuto",minutesUntil:"em {delta} minutos",hourUntil:"em uma hora",hoursUntil:"em {delta} horas",dayUntil:"em um dia",daysUntil:"em {delta} dias",weekUntil:"em uma semana",weeksUntil:"em {delta} semanas",monthUntil:"em um mês",monthsUntil:"em {delta} meses",yearUntil:"em um ano",yearsUntil:"em {delta} anos"});Locale.define("pt-BR","Date",{shortDate:"%d/%m/%Y"}).inherit("pt-PT","Date");Locale.define("pt-BR","FormValidator",{required:"Este campo é obrigatório.",minLength:"Digite pelo menos {minLength} caracteres (tamanho atual: {length}).",maxLength:"Não digite mais de {maxLength} caracteres (tamanho atual: {length}).",integer:"Por favor digite apenas um número inteiro neste campo. Não são permitidos números decimais (por exemplo, 1,25).",numeric:'Por favor digite apenas valores numéricos neste campo (por exemplo, "1" ou "1.1" ou "-1" ou "-1,1").',digits:"Por favor use apenas números e pontuação neste campo (por exemplo, um número de telefone com traços ou pontos é permitido).",alpha:"Por favor use somente letras (a-z). Espaço e outros caracteres não são permitidos.",alphanum:"Use somente letras (a-z) ou números (0-9) neste campo. Espaço e outros caracteres não são permitidos.",dateSuchAs:"Digite uma data válida, como {date}",dateInFormatMDY:'Digite uma data válida, como DD/MM/YYYY (por exemplo, "31/12/1999")',email:'Digite um endereço de email válido. Por exemplo "nome@dominio.com".',url:"Digite uma URL válida. Exemplo: http://www.example.com.",currencyDollar:"Digite um valor em dinheiro válido. Exemplo: R$100,00 .",oneRequired:"Digite algo para pelo menos um desses campos.",errorPrefix:"Erro: ",warningPrefix:"Aviso: ",noSpace:"Não é possível digitar espaços neste campo.",reqChkByNode:"Não foi selecionado nenhum item.",requiredChk:"Este campo é obrigatório.",reqChkByName:"Por favor digite um {label}.",match:"Este campo deve ser igual ao campo {matchName}.",startDate:"a data inicial",endDate:"a data final",currendDate:"a data atual",afterDate:"A data deve ser igual ou posterior a {label}.",beforeDate:"A data deve ser igual ou anterior a {label}.",startMonth:"Por favor selecione uma data inicial.",sameMonth:"Estas duas datas devem ter o mesmo mês - você deve modificar uma das duas.",creditcard:"O número do cartão de crédito informado é inválido. Por favor verifique o valor e tente novamente. {length} números informados."});Locale.define("pt-PT","FormValidator",{required:"Este campo é necessário.",minLength:"Digite pelo menos{minLength} caracteres (comprimento {length} caracteres).",maxLength:"Não insira mais de {maxLength} caracteres (comprimento {length} caracteres).",integer:"Digite um número inteiro neste domínio. Com números decimais (por exemplo, 1,25), não são permitidas.",numeric:'Digite apenas valores numéricos neste domínio (p.ex., "1" ou "1.1" ou "-1" ou "-1,1").',digits:"Por favor, use números e pontuação apenas neste campo (p.ex., um número de telefone com traços ou pontos é permitida).",alpha:"Por favor use somente letras (a-z), com nesta área. Não utilize espaços nem outros caracteres são permitidos.",alphanum:"Use somente letras (a-z) ou números (0-9) neste campo. Não utilize espaços nem outros caracteres são permitidos.",dateSuchAs:"Digite uma data válida, como {date}",dateInFormatMDY:'Digite uma data válida, como DD/MM/YYYY (p.ex. "31/12/1999")',email:'Digite um endereço de email válido. Por exemplo "fred@domain.com".',url:"Digite uma URL válida, como http://www.example.com.",currencyDollar:"Digite um valor válido $. Por exemplo $ 100,00. ",oneRequired:"Digite algo para pelo menos um desses insumos.",errorPrefix:"Erro: ",warningPrefix:"Aviso: "});(function(){var a=function(h,e,d,g,b){var c=h%10,f=h%100;if(c==1&&f!=11){return e;}else{if((c==2||c==3||c==4)&&!(f==12||f==13||f==14)){return d;}else{if(c==0||(c==5||c==6||c==7||c==8||c==9)||(f==11||f==12||f==13||f==14)){return g;}else{return b;}}}};Locale.define("ru-RU","Date",{months:["Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"],months_abbr:["янв","февр","март","апр","май","июнь","июль","авг","сент","окт","нояб","дек"],days:["Воскресенье","Понедельник","Вторник","Среда","Четверг","Пятница","Суббота"],days_abbr:["Вс","Пн","Вт","Ср","Чт","Пт","Сб"],dateOrder:["date","month","year"],shortDate:"%d.%m.%Y",shortTime:"%H:%M",AM:"AM",PM:"PM",firstDayOfWeek:1,ordinal:"",lessThanMinuteAgo:"меньше минуты назад",minuteAgo:"минуту назад",minutesAgo:function(b){return"{delta} "+a(b,"минуту","минуты","минут")+" назад";},hourAgo:"час назад",hoursAgo:function(b){return"{delta} "+a(b,"час","часа","часов")+" назад";},dayAgo:"вчера",daysAgo:function(b){return"{delta} "+a(b,"день","дня","дней")+" назад";},weekAgo:"неделю назад",weeksAgo:function(b){return"{delta} "+a(b,"неделя","недели","недель")+" назад";},monthAgo:"месяц назад",monthsAgo:function(b){return"{delta} "+a(b,"месяц","месяца","месецев")+" назад";},yearAgo:"год назад",yearsAgo:function(b){return"{delta} "+a(b,"год","года","лет")+" назад";},lessThanMinuteUntil:"меньше чем через минуту",minuteUntil:"через минуту",minutesUntil:function(b){return"через {delta} "+a(b,"час","часа","часов")+"";},hourUntil:"через час",hoursUntil:function(b){return"через {delta} "+a(b,"час","часа","часов")+"";},dayUntil:"завтра",daysUntil:function(b){return"через {delta} "+a(b,"день","дня","дней")+"";},weekUntil:"через неделю",weeksUntil:function(b){return"через {delta} "+a(b,"неделю","недели","недель")+"";},monthUntil:"через месяц",monthsUntil:function(b){return"через {delta} "+a(b,"месяц","месяца","месецев")+"";},yearUntil:"через",yearsUntil:function(b){return"через {delta} "+a(b,"год","года","лет")+"";}});})();Locale.define("ru-RU","FormValidator",{required:"Это поле обязательно к заполнению.",minLength:"Пожалуйста, введите хотя бы {minLength} символов (Вы ввели {length}).",maxLength:"Пожалуйста, введите не больше {maxLength} символов (Вы ввели {length}).",integer:"Пожалуйста, введите в это поле число. Дробные числа (например 1.25) тут не разрешены.",numeric:'Пожалуйста, введите в это поле число (например "1" или "1.1", или "-1", или "-1.1").',digits:"В этом поле Вы можете использовать только цифры и знаки пунктуации (например, телефонный номер со знаками дефиса или с точками).",alpha:"В этом поле можно использовать только латинские буквы (a-z). Пробелы и другие символы запрещены.",alphanum:"В этом поле можно использовать только латинские буквы (a-z) и цифры (0-9). Пробелы и другие символы запрещены.",dateSuchAs:"Пожалуйста, введите корректную дату {date}",dateInFormatMDY:'Пожалуйста, введите дату в формате ММ/ДД/ГГГГ (например "12/31/1999")',email:'Пожалуйста, введите корректный емейл-адрес. Для примера "fred@domain.com".',url:"Пожалуйста, введите правильную ссылку вида http://www.example.com.",currencyDollar:"Пожалуйста, введите сумму в долларах. Например: $100.00 .",oneRequired:"Пожалуйста, выберите хоть что-нибудь в одном из этих полей.",errorPrefix:"Ошибка: ",warningPrefix:"Внимание: "});(function(){var a=function(f,d,c,e,b){return(f>=1&&f<=3)?arguments[f]:b;};Locale.define("si-SI","Date",{months:["januar","februar","marec","april","maj","junij","julij","avgust","september","oktober","november","december"],months_abbr:["jan","feb","mar","apr","maj","jun","jul","avg","sep","okt","nov","dec"],days:["nedelja","ponedeljek","torek","sreda","četrtek","petek","sobota"],days_abbr:["ned","pon","tor","sre","čet","pet","sob"],dateOrder:["date","month","year"],shortDate:"%d.%m.%Y",shortTime:"%H.%M",AM:"AM",PM:"PM",firstDayOfWeek:1,ordinal:".",lessThanMinuteAgo:"manj kot minuto nazaj",minuteAgo:"minuto nazaj",minutesAgo:function(b){return"{delta} "+a(b,"minuto","minuti","minute","minut")+" nazaj";},hourAgo:"uro nazaj",hoursAgo:function(b){return"{delta} "+a(b,"uro","uri","ure","ur")+" nazaj";},dayAgo:"dan nazaj",daysAgo:function(b){return"{delta} "+a(b,"dan","dneva","dni","dni")+" nazaj";},weekAgo:"teden nazaj",weeksAgo:function(b){return"{delta} "+a(b,"teden","tedna","tedne","tednov")+" nazaj";},monthAgo:"mesec nazaj",monthsAgo:function(b){return"{delta} "+a(b,"mesec","meseca","mesece","mesecov")+" nazaj";},yearthAgo:"leto nazaj",yearsAgo:function(b){return"{delta} "+a(b,"leto","leti","leta","let")+" nazaj";},lessThanMinuteUntil:"še manj kot minuto",minuteUntil:"še minuta",minutesUntil:function(b){return"še {delta} "+a(b,"minuta","minuti","minute","minut");},hourUntil:"še ura",hoursUntil:function(b){return"še {delta} "+a(b,"ura","uri","ure","ur");},dayUntil:"še dan",daysUntil:function(b){return"še {delta} "+a(b,"dan","dneva","dnevi","dni");},weekUntil:"še tedn",weeksUntil:function(b){return"še {delta} "+a(b,"teden","tedna","tedni","tednov");},monthUntil:"še mesec",monthsUntil:function(b){return"še {delta} "+a(b,"mesec","meseca","meseci","mesecov");},yearUntil:"še leto",yearsUntil:function(b){return"še {delta} "+a(b,"leto","leti","leta","let");}});})();Locale.define("si-SI","FormValidator",{required:"To polje je obvezno",minLength:"Prosim, vnesite vsaj {minLength} znakov (vnesli ste {length} znakov).",maxLength:"Prosim, ne vnesite več kot {maxLength} znakov (vnesli ste {length} znakov).",integer:"Prosim, vnesite celo število. Decimalna števila (kot 1,25) niso dovoljena.",numeric:'Prosim, vnesite samo numerične vrednosti (kot "1" ali "1.1" ali "-1" ali "-1.1").',digits:"Prosim, uporabite številke in ločila le na tem polju (na primer, dovoljena je telefonska številka z pomišlaji ali pikami).",alpha:"Prosim, uporabite le črke v tem plju. Presledki in drugi znaki niso dovoljeni.",alphanum:"Prosim, uporabite samo črke ali številke v tem polju. Presledki in drugi znaki niso dovoljeni.",dateSuchAs:"Prosim, vnesite pravilen datum kot {date}",dateInFormatMDY:'Prosim, vnesite pravilen datum kot MM.DD.YYYY (primer "12.31.1999")',email:'Prosim, vnesite pravilen email naslov. Na primer "fred@domain.com".',url:"Prosim, vnesite pravilen URL kot http://www.example.com.",currencyDollar:"Prosim, vnesit epravilno vrednost €. Primer 100,00€ .",oneRequired:"Prosimo, vnesite nekaj za vsaj eno izmed teh polj.",errorPrefix:"Napaka: ",warningPrefix:"Opozorilo: ",noSpace:"To vnosno polje ne dopušča presledkov.",reqChkByNode:"Nič niste izbrali.",requiredChk:"To polje je obvezno",reqChkByName:"Prosim, izberite {label}.",match:"To polje se mora ujemati z poljem {matchName}",startDate:"datum začetka",endDate:"datum konca",currendDate:"trenuten datum",afterDate:"Datum bi moral biti isti ali po {label}.",beforeDate:"Datum bi moral biti isti ali pred {label}.",startMonth:"Prosim, vnesite začetni datum",sameMonth:"Ta dva datuma morata biti v istem mesecu - premeniti morate eno ali drugo.",creditcard:"Številka kreditne kartice ni pravilna. Preverite številko ali poskusite še enkrat. Vnešenih {length} znakov."});Locale.define("sv-SE","Date",{months:["januari","februari","mars","april","maj","juni","juli","augusti","september","oktober","november","december"],months_abbr:["jan","feb","mar","apr","maj","jun","jul","aug","sep","okt","nov","dec"],days:["söndag","måndag","tisdag","onsdag","torsdag","fredag","lördag"],days_abbr:["sön","mån","tis","ons","tor","fre","lör"],dateOrder:["year","month","date"],shortDate:"%Y-%m-%d",shortTime:"%H:%M",AM:"",PM:"",firstDayOfWeek:1,ordinal:"",lessThanMinuteAgo:"mindre än en minut sedan",minuteAgo:"ungefär en minut sedan",minutesAgo:"{delta} minuter sedan",hourAgo:"ungefär en timme sedan",hoursAgo:"ungefär {delta} timmar sedan",dayAgo:"1 dag sedan",daysAgo:"{delta} dagar sedan",lessThanMinuteUntil:"mindre än en minut sedan",minuteUntil:"ungefär en minut sedan",minutesUntil:"{delta} minuter sedan",hourUntil:"ungefär en timme sedan",hoursUntil:"ungefär {delta} timmar sedan",dayUntil:"1 dag sedan",daysUntil:"{delta} dagar sedan"});Locale.define("sv-SE","FormValidator",{required:"Fältet är obligatoriskt.",minLength:"Ange minst {minLength} tecken (du angav {length} tecken).",maxLength:"Ange högst {maxLength} tecken (du angav {length} tecken). ",integer:"Ange ett heltal i fältet. Tal med decimaler (t.ex. 1,25) är inte tillåtna.",numeric:'Ange endast numeriska värden i detta fält (t.ex. "1" eller "1.1" eller "-1" eller "-1,1").',digits:"Använd endast siffror och skiljetecken i detta fält (till exempel ett telefonnummer med bindestreck tillåtet).",alpha:"Använd endast bokstäver (a-ö) i detta fält. Inga mellanslag eller andra tecken är tillåtna.",alphanum:"Använd endast bokstäver (a-ö) och siffror (0-9) i detta fält. Inga mellanslag eller andra tecken är tillåtna.",dateSuchAs:"Ange ett giltigt datum som t.ex. {date}",dateInFormatMDY:'Ange ett giltigt datum som t.ex. YYYY-MM-DD (i.e. "1999-12-31")',email:'Ange en giltig e-postadress. Till exempel "erik@domain.com".',url:"Ange en giltig webbadress som http://www.example.com.",currencyDollar:"Ange en giltig belopp. Exempelvis 100,00.",oneRequired:"Vänligen ange minst ett av dessa alternativ.",errorPrefix:"Fel: ",warningPrefix:"Varning: ",noSpace:"Det får inte finnas några mellanslag i detta fält.",reqChkByNode:"Inga objekt är valda.",requiredChk:"Detta är ett obligatoriskt fält.",reqChkByName:"Välj en {label}.",match:"Detta fält måste matcha {matchName}",startDate:"startdatumet",endDate:"slutdatum",currendDate:"dagens datum",afterDate:"Datumet bör vara samma eller senare än {label}.",beforeDate:"Datumet bör vara samma eller tidigare än {label}.",startMonth:"Välj en start månad",sameMonth:"Dessa två datum måste vara i samma månad - du måste ändra det ena eller det andra."});(function(){var a=function(j,e,c,i,b){var h=(j/10).toInt(),g=j%10,f=(j/100).toInt();if(h==1&&j>10){return i;}if(g==1){return e;}if(g>0&&g<5){return c;}return i;};Locale.define("uk-UA","Date",{months:["Січень","Лютий","Березень","Квітень","Травень","Червень","Липень","Серпень","Вересень","Жовтень","Листопад","Грудень"],months_abbr:["Січ","Лют","Бер","Квіт","Трав","Черв","Лип","Серп","Вер","Жовт","Лист","Груд"],days:["Неділя","Понеділок","Вівторок","Середа","Четвер","П'ятниця","Субота"],days_abbr:["Нд","Пн","Вт","Ср","Чт","Пт","Сб"],dateOrder:["date","month","year"],shortDate:"%d/%m/%Y",shortTime:"%H:%M",AM:"до полудня",PM:"по полудню",firstDayOfWeek:1,ordinal:"",lessThanMinuteAgo:"меньше хвилини тому",minuteAgo:"хвилину тому",minutesAgo:function(b){return"{delta} "+a(b,"хвилину","хвилини","хвилин")+" тому";},hourAgo:"годину тому",hoursAgo:function(b){return"{delta} "+a(b,"годину","години","годин")+" тому";},dayAgo:"вчора",daysAgo:function(b){return"{delta} "+a(b,"день","дня","днів")+" тому";},weekAgo:"тиждень тому",weeksAgo:function(b){return"{delta} "+a(b,"тиждень","тижні","тижнів")+" тому";},monthAgo:"місяць тому",monthsAgo:function(b){return"{delta} "+a(b,"місяць","місяці","місяців")+" тому";},yearAgo:"рік тому",yearsAgo:function(b){return"{delta} "+a(b,"рік","роки","років")+" тому";},lessThanMinuteUntil:"за мить",minuteUntil:"через хвилину",minutesUntil:function(b){return"через {delta} "+a(b,"хвилину","хвилини","хвилин");},hourUntil:"через годину",hoursUntil:function(b){return"через {delta} "+a(b,"годину","години","годин");},dayUntil:"завтра",daysUntil:function(b){return"через {delta} "+a(b,"день","дня","днів");},weekUntil:"через тиждень",weeksUntil:function(b){return"через {delta} "+a(b,"тиждень","тижні","тижнів");},monthUntil:"через місяць",monthesUntil:function(b){return"через {delta} "+a(b,"місяць","місяці","місяців");},yearUntil:"через рік",yearsUntil:function(b){return"через {delta} "+a(b,"рік","роки","років");}});})();Locale.define("uk-UA","FormValidator",{required:"Це поле повинне бути заповненим.",minLength:"Введіть хоча б {minLength} символів (Ви ввели {length}).",maxLength:"Кількість символів не може бути більше {maxLength} (Ви ввели {length}).",integer:"Введіть в це поле число. Дробові числа (наприклад 1.25) не дозволені.",numeric:'Введіть в це поле число (наприклад "1" або "1.1", або "-1", або "-1.1").',digits:"В цьому полі ви можете використовувати лише цифри і знаки пунктіації (наприклад, телефонний номер з знаками дефізу або з крапками).",alpha:"В цьому полі можна використовувати лише латинські літери (a-z). Пробіли і інші символи заборонені.",alphanum:"В цьому полі можна використовувати лише латинські літери (a-z) і цифри (0-9). Пробіли і інші символи заборонені.",dateSuchAs:"Введіть коректну дату {date}.",dateInFormatMDY:'Введіть дату в форматі ММ/ДД/РРРР (наприклад "12/31/2009").',email:'Введіть коректну адресу електронної пошти (наприклад "name@domain.com").',url:"Введіть коректне інтернет-посилання (наприклад http://www.example.com).",currencyDollar:'Введіть суму в доларах (наприклад "$100.00").',oneRequired:"Заповніть одне з полів.",errorPrefix:"Помилка: ",warningPrefix:"Увага: ",noSpace:"Пробіли заборонені.",reqChkByNode:"Не відмічено жодного варіанту.",requiredChk:"Це поле повинне бути віміченим.",reqChkByName:"Будь ласка, відмітьте {label}.",match:"Це поле повинно відповідати {matchName}",startDate:"початкова дата",endDate:"кінцева дата",currendDate:"сьогоднішня дата",afterDate:"Ця дата повинна бути такою ж, або пізнішою за {label}.",beforeDate:"Ця дата повинна бути такою ж, або ранішою за {label}.",startMonth:"Будь ласка, виберіть початковий місяць",sameMonth:"Ці дати повинні відноситись одного і того ж місяця. Будь ласка, змініть одну з них.",creditcard:"Номер кредитної карти введений неправильно. Будь ласка, перевірте його. Введено {length} символів."});Locale.define("zh-CHS","Date",{months:["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],months_abbr:["一","二","三","四","五","六","七","八","九","十","十一","十二"],days:["星期日","星期一","星期二","星期三","星期四","星期五","星期六"],days_abbr:["日","一","二","三","四","五","六"],dateOrder:["year","month","date"],shortDate:"%Y-%m-%d",shortTime:"%I:%M%p",AM:"AM",PM:"PM",firstDayOfWeek:1,ordinal:"",lessThanMinuteAgo:"不到1分钟前",minuteAgo:"大约1分钟前",minutesAgo:"{delta}分钟之前",hourAgo:"大约1小时前",hoursAgo:"大约{delta}小时前",dayAgo:"1天前",daysAgo:"{delta}天前",weekAgo:"1星期前",weeksAgo:"{delta}星期前",monthAgo:"1个月前",monthsAgo:"{delta}个月前",yearAgo:"1年前",yearsAgo:"{delta}年前",lessThanMinuteUntil:"从现在开始不到1分钟",minuteUntil:"从现在开始約1分钟",minutesUntil:"从现在开始约{delta}分钟",hourUntil:"从现在开始1小时",hoursUntil:"从现在开始约{delta}小时",dayUntil:"从现在开始1天",daysUntil:"从现在开始{delta}天",weekUntil:"从现在开始1星期",weeksUntil:"从现在开始{delta}星期",monthUntil:"从现在开始一个月",monthsUntil:"从现在开始{delta}个月",yearUntil:"从现在开始1年",yearsUntil:"从现在开始{delta}年"});Locale.define("zh-CHT","Date",{months:["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],months_abbr:["一","二","三","四","五","六","七","八","九","十","十一","十二"],days:["星期日","星期一","星期二","星期三","星期四","星期五","星期六"],days_abbr:["日","一","二","三","四","五","六"],dateOrder:["year","month","date"],shortDate:"%Y-%m-%d",shortTime:"%I:%M%p",AM:"AM",PM:"PM",firstDayOfWeek:1,ordinal:"",lessThanMinuteAgo:"不到1分鐘前",minuteAgo:"大約1分鐘前",minutesAgo:"{delta}分鐘之前",hourAgo:"大約1小時前",hoursAgo:"大約{delta}小時前",dayAgo:"1天前",daysAgo:"{delta}天前",weekAgo:"1星期前",weeksAgo:"{delta}星期前",monthAgo:"1个月前",monthsAgo:"{delta}个月前",yearAgo:"1年前",yearsAgo:"{delta}年前",lessThanMinuteUntil:"從現在開始不到1分鐘",minuteUntil:"從現在開始約1分鐘",minutesUntil:"從現在開始約{delta}分鐘",hourUntil:"從現在開始1小時",hoursUntil:"從現在開始約{delta}小時",dayUntil:"從現在開始1天",daysUntil:"從現在開始{delta}天",weekUntil:"從現在開始1星期",weeksUntil:"從現在開始{delta}星期",monthUntil:"從現在開始一個月",monthsUntil:"從現在開始{delta}個月",yearUntil:"從現在開始1年",yearsUntil:"從現在開始{delta}年"});Locale.define("zh-CHS","FormValidator",{required:"此项必填。",minLength:"请至少输入 {minLength} 个字符 (已输入 {length} 个)。",maxLength:"最多只能输入 {maxLength} 个字符 (已输入 {length} 个)。",integer:'请输入一个整数，不能包含小数点。例如："1", "200"。',numeric:'请输入一个数字，例如："1", "1.1", "-1", "-1.1"。',digits:"请输入由数字和标点符号组成的内容。例如电话号码。",alpha:"请输入 A-Z 的 26 个字母，不能包含空格或任何其他字符。",alphanum:"请输入 A-Z 的 26 个字母或 0-9 的 10 个数字，不能包含空格或任何其他字符。",dateSuchAs:"请输入合法的日期格式，如：{date}。",dateInFormatMDY:'请输入合法的日期格式，例如：YYYY-MM-DD ("2010-12-31")。',email:'请输入合法的电子信箱地址，例如："fred@domain.com"。',url:"请输入合法的 Url 地址，例如：http://www.example.com。",currencyDollar:"请输入合法的货币符号，例如：￥100.0",oneRequired:"请至少选择一项。",errorPrefix:"错误：",warningPrefix:"警告：",noSpace:"不能包含空格。",reqChkByNode:"未选择任何内容。",requiredChk:"此项必填。",reqChkByName:"请选择 {label}.",match:"必须与{matchName}相匹配",startDate:"起始日期",endDate:"结束日期",currendDate:"当前日期",afterDate:"日期必须等于或晚于 {label}.",beforeDate:"日期必须早于或等于 {label}.",startMonth:"请选择起始月份",sameMonth:"您必须修改两个日期中的一个，以确保它们在同一月份。",creditcard:"您输入的信用卡号码不正确。当前已输入{length}个字符。"});Locale.define("zh-CHT","FormValidator",{required:"此項必填。 ",minLength:"請至少輸入{minLength} 個字符(已輸入{length} 個)。 ",maxLength:"最多只能輸入{maxLength} 個字符(已輸入{length} 個)。 ",integer:'請輸入一個整數，不能包含小數點。例如："1", "200"。 ',numeric:'請輸入一個數字，例如："1", "1.1", "-1", "-1.1"。 ',digits:"請輸入由數字和標點符號組成的內容。例如電話號碼。 ",alpha:"請輸入AZ 的26 個字母，不能包含空格或任何其他字符。 ",alphanum:"請輸入AZ 的26 個字母或0-9 的10 個數字，不能包含空格或任何其他字符。 ",dateSuchAs:"請輸入合法的日期格式，如：{date}。 ",dateInFormatMDY:'請輸入合法的日期格式，例如：YYYY-MM-DD ("2010-12-31")。 ',email:'請輸入合法的電子信箱地址，例如："fred@domain.com"。 ',url:"請輸入合法的Url 地址，例如：http://www.example.com。 ",currencyDollar:"請輸入合法的貨幣符號，例如：￥100.0",oneRequired:"請至少選擇一項。 ",errorPrefix:"錯誤：",warningPrefix:"警告：",noSpace:"不能包含空格。 ",reqChkByNode:"未選擇任何內容。 ",requiredChk:"此項必填。 ",reqChkByName:"請選擇 {label}.",match:"必須與{matchName}相匹配",startDate:"起始日期",endDate:"結束日期",currendDate:"當前日期",afterDate:"日期必須等於或晚於{label}.",beforeDate:"日期必須早於或等於{label}.",startMonth:"請選擇起始月份",sameMonth:"您必須修改兩個日期中的一個，以確保它們在同一月份。 ",creditcard:"您輸入的信用卡號碼不正確。當前已輸入{length}個字符。 "});Form.Validator.add("validate-currency-yuan",{errorMsg:function(){return Form.Validator.getMsg("currencyYuan");},test:function(a){return Form.Validator.getValidator("IsEmpty").test(a)||(/^￥?\-?([1-9]{1}[0-9]{0,2}(\,[0-9]{3})*(\.[0-9]{0,2})?|[1-9]{1}\d*(\.[0-9]{0,2})?|0(\.[0-9]{0,2})?|(\.[0-9]{1,2})?)$/).test(a.get("value"));}});


/*===============================
http://iasitvlife.ro/modules/mod_news_pro_gk5/interface/scripts/engine.mootools.js
================================================================================*/;
var NSP5=new Class({animation:true,anim_arts:false,anim_lists:false,arts:null,arts_block_width:0,arts_current:0,arts_pages:null,arts_per_page:null,arts_scroller:null,config:null,hover_anim:null,links:null,links_block_width:0,links_pages:null,links_pages_amount:null,links_scroller:null,links_current:0,modInterface:null,module:null,swipe_max_time:500,swipe_min_move:30,initialize:function(module){this.init_fields(module);this.init_interface();},init_fields:function(module){this.module=module;this.module.addClass('activated');this.config=JSON.decode(this.module.get('data-config'));this.config['animation_function']=eval(this.config['animation_function']);this.arts=this.module.getElements('.nspArt');this.arts_pages=this.module.getElements('.nspArtPage');this.arts_per_page=this.config['news_column']*this.config['news_rows'];this.hover_anim=this.module.hasClass('hover');this.links=(this.module.getElement('.nspLinkScroll1'))?this.module.getElement('.nspLinkScroll1').getElements('li'):[];this.links_pages=this.module.getElements('.nspList');this.links_pages_amount=Math.ceil(Math.ceil(this.links.length/this.config['links_amount'])/this.config['links_columns_amount']);this.modInterface={top:this.module.getElement('.nspTopInterface'),bottom:this.module.getElement('.nspBotInterface')};this.pages_amount=Math.ceil(this.arts.length/this.arts_per_page);},init_interface:function(){var $this=this;if(this.arts.length>0){this.arts_block_width=100;this.arts_scroller=new Fx.Tween($this.module.getElement('.nspArtScroll2'),{duration:$this.config['animation_speed'],wait:false,property:'margin-left',unit:'%',transition:$this.config['animation_function']});}
this.module.addEvents({'mouseenter':function(){if(!$this.module.hasClass('onhover'))$this.module.addClass('onhover');},'mouseleave':function(){if($this.module.hasClass('onhover'))$this.module.removeClass('onhover');}});if(this.links.length>0){this.links_block_width=100;this.links_scroller=new Fx.Tween($this.module.getElement('.nspLinkScroll2'),{duration:$this.config['animation_speed'],wait:false,property:'margin-left',unit:'%',transition:$this.config['animation_function']});}
this.nsp_art_list(0,'top');this.nsp_art_list(0,'bottom');if(this.modInterface.top&&this.modInterface.top.getElement('.nspPagination')){this.modInterface.top.getElement('.nspPagination').getElements('li').each(function(item,i){item.addEvent($this.hover_anim?'mouseenter':'click',function(e){e.preventDefault();$this.arts_anim(i);});});}
if(this.modInterface.top&&this.modInterface.top.getElement('.nspPrev')){this.modInterface.top.getElement('.nspPrev').addEvent("click",function(e){e.preventDefault();$this.arts_anim('prev');});this.modInterface.top.getElement('.nspNext').addEvent("click",function(e){e.preventDefault();$this.arts_anim('next');});}
if(this.modInterface.bottom&&this.modInterface.bottom.getElement('.nspPagination')){this.modInterface.bottom.getElement('.nspPagination').getElements('li').each(function(item,i){item.addEvent($this.hover_anim?'mouseenter':'click',function(e){e.preventDefault();$this.lists_anim(i);});});}
if(this.modInterface.bottom&&this.modInterface.bottom.getElement('.nspPrev')){this.modInterface.bottom.getElement('.nspPrev').addEvent("click",function(e){e.preventDefault();$this.lists_anim('prev');});this.modInterface.bottom.getElement('.nspNext').addEvent("click",function(e){e.preventDefault();$this.lists_anim('next');});}
if(this.module.hasClass('autoanim')){(function(){$this.nsp_gk5_autoanim();}).delay($this.config['animation_interval']);}
var arts_wrap=this.module.getElement('.nspArts');if(arts_wrap){var arts_pos_start_x=0;var arts_pos_start_y=0;var arts_time_start=0;var arts_swipe=false;arts_wrap.addEvent('touchstart',function(e){arts_swipe=true;if(e.changedTouches.length>0){arts_pos_start_x=e.changedTouches[0].pageX;arts_pos_start_y=e.changedTouches[0].pageY;arts_time_start=new Date().getTime();}});arts_wrap.addEvent('touchmove',function(e){if(e.changedTouches.length>0&&arts_swipe){if(Math.abs(e.changedTouches[0].pageX-arts_pos_start_x)>Math.abs(e.changedTouches[0].pageY-arts_pos_start_y)){e.preventDefault();}else{arts_swipe=false;}}});arts_wrap.addEvent('touchend',function(e){if(e.changedTouches.length>0&&arts_swipe){if(Math.abs(e.changedTouches[0].pageX-arts_pos_start_x)>=$this.swipe_min_move&&new Date().getTime()-arts_time_start<=$this.swipe_max_time){if(e.changedTouches[0].pageX-arts_pos_start_x>0){$this.arts_anim('prev');}else{$this.arts_anim('next');}}}});}
var links_wrap=this.module.getElement('.nspLinksWrap');if(links_wrap){var links_pos_start_x=0;var links_pos_start_y=0;var links_time_start=0;var links_swipe=false;links_wrap.addEvent('touchstart',function(e){links_swipe=true;if(e.changedTouches.length>0){links_pos_start_x=e.changedTouches[0].pageX;links_pos_start_y=e.changedTouches[0].pageY;links_time_start=new Date().getTime();}});links_wrap.addEvent('touchmove',function(e){if(e.changedTouches.length>0&&links_swipe){if(Math.abs(e.changedTouches[0].pageX-links_pos_start_x)>Math.abs(e.changedTouches[0].pageY-links_pos_start_y)){e.preventDefault();}else{links_swipe=false;}}});links_wrap.addEvent('touchend',function(e){if(e.changedTouches.length>0&&links_swipe){if(Math.abs(e.changedTouches[0].pageX-links_pos_start_x)>=$this.swipe_min_move&&new Date().getTime()-links_time_start<=$this.swipe_max_time){if(e.changedTouches[0].pageX-links_pos_start_x>0){$this.lists_anim('prev');}else{$this.lists_anim('next');}}}});}},nsp_art_list:function(i,pos){var num=(i!==null)?i:(pos=='top')?this.arts_current:this.links_current;if(this.modInterface[pos]&&this.modInterface[pos].getElement('.nspPagination')){var pagination=this.modInterface[pos].getElement('.nspPagination');pagination.getElements('li').setProperty('class','');pagination.getElements('li')[num].setProperty('class','active');}},arts_anim:function(dir){if(!this.anim_arts){var $this=this;this.anim_arts=true;this.arts_pages[this.arts_current].removeClass('active');if(dir=='next'){this.arts_current=(this.arts_current==this.pages_amount-1)?0:this.arts_current+1;}else if(dir=='prev'){this.arts_current=(this.arts_current==0)?this.pages_amount-1:this.arts_current-1;}else{this.arts_current=dir;}
this.arts_scroller.start(-1*this.arts_current*this.arts_block_width);(function(){$this.arts_pages[$this.arts_current].addClass('active');}).delay(this.config['animation_speed']*0.5);(function(){$this.anim_arts=false;}).delay(this.config['animation_speed']);this.nsp_art_list(this.arts_current,'top');this.animation=false;(function(){$this.animation=true;}).delay(this.config['animation_interval']*0.8);}},lists_anim:function(dir){if(!this.anim_lists){var $this=this;this.anim_lists=true;for(var x=0;x<this.config['links_columns_amount'];x++){var item=this.links_pages[this.links_current*this.config['links_columns_amount']+x];if(item)item.removeClass('active');}
if(dir=='next'){this.links_current=(this.links_current==this.links_pages_amount-1)?0:this.links_current+1;}else if(dir=='prev'){this.links_current=(this.links_current==0)?this.links_pages_amount-1:this.links_current-1;}else{$this.links_current=dir;}
(function(){$this.anim_lists=false;}).delay(this.config['animation_speed']);(function(){for(var x=0;x<$this.config['links_columns_amount'];x++){var item=$this.links_pages[$this.links_current*$this.config['links_columns_amount']+x];if(item)item.addClass('active');}}).delay(this.config['animation_speed']*0.5);this.links_scroller.start(-1*this.links_current*this.links_block_width);this.nsp_art_list(null,'bottom');}},nsp_gk5_autoanim:function(){var $this=this;if(!this.module.hasClass('onhover')){this.arts_anim('next');}
(function(){$this.nsp_gk5_autoanim();}).delay($this.config['animation_interval']);}});window.addEvent("load",function(){$$('.nspMain').each(function(module){if(!module.hasClass('activated')){new NSP5(module);}});});


/*===============================
http://iasitvlife.ro/modules/mod_tabs_gk5/scripts/engine.mootools.js
================================================================================*/;
window.addEvent('load',function(){document.getElements('.gkTabsGK5').each(function(el,i){var config=JSON.decode(el.get('data-config'));config['module_id']=el.getProperty('id');var tabs=el.getElements('.gkTabsItem');var items=el.getElements('.gkTabsNav .gkTab');var tabs_wrapper=el.getElement('.gkTabsContainer');var animation=(config['animation']==0)?true:false;var amount=tabs.length;var timer=false;config['current_tab']=config['active_tab']-1;config['previous_tab']=null;config['blank']=false;config['falsy_click']=false;config['hover']=false;var animation_type=config['animation_type'];var tab_animation=[];el.addEvent('mouseenter',function(){config['hover']=true;});el.addEvent('mouseleave',function(){config['hover']=false;});tabs.each(function(tab,i){tab_animation[i]=new Fx.Morph(tab,{duration:config['animation_speed'],wait:'ignore'});});tabs_wrapper.setStyle('height','auto');if(config['auto_height']=='0'){tabs_wrapper.setStyle('height',config['module_height']+'px');}
items.each(function(item,i){item.addEvent(config['activator'],function(){tabsGK5Animation(i,tabs_wrapper,tab_animation,tabs,items,config);});});if(el.getElement('.gkTabsButtonNext')){el.getElement('.gkTabsButtonNext').addEvent('click',function(){if(config['current_tab']<amount-1){tabsGK5Animation(config['current_tab']+1,tabs_wrapper,tab_animation,tabs,items,config);}else{tabsGK5Animation(0,tabs_wrapper,tab_animation,tabs,items,config);}});el.getElement('.gkTabsButtonPrev').addEvent('click',function(){if(config['current_tab']>0){tabsGK5Animation(config['current_tab']-1,tabs_wrapper,tab_animation,tabs,items,config);}else{tabsGK5Animation(amount-1,tabs_wrapper,tab_animation,tabs,items,config);}});}
if(config["animation"]==1){timer=(function(){if(config['hover']){config['blank']=true;}
if(!config['blank']){config['falsy_click']=true;if(config['current_tab']<amount-1){tabsGK5Animation(config['current_tab']+1,tabs_wrapper,tab_animation,tabs,items,config);}else{tabsGK5Animation(0,tabs_wrapper,tab_animation,tabs,items,config);}}else{config['blank']=false;}}).periodical(config["animation_interval"]);}
if(el.get('data-swipe')=='1'){var links_pos_start_x=0;var links_pos_start_y=0;var links_time_start=0;var links_swipe=false;el.addEvent('touchstart',function(e){links_swipe=true;if(e.changedTouches.length>0){links_pos_start_x=e.changedTouches[0].pageX;links_pos_start_y=e.changedTouches[0].pageY;links_time_start=new Date().getTime();}});el.addEvent('touchmove',function(e){if(e.changedTouches.length>0&&links_swipe){if(Math.abs(e.changedTouches[0].pageX-links_pos_start_x)>Math.abs(e.changedTouches[0].pageY-links_pos_start_y)){e.preventDefault();}else{links_swipe=false;}}});el.addEvent('touchend',function(e){if(e.changedTouches.length>0&&links_swipe){if(Math.abs(e.changedTouches[0].pageX-links_pos_start_x)>=30&&new Date().getTime()-links_time_start<=500){if(e.changedTouches[0].pageX-links_pos_start_x>0){if(config['current_tab']>0){tabsGK5Animation(config['current_tab']-1,tabs_wrapper,tab_animation,tabs,items,config);}else{tabsGK5Animation(amount-1,tabs_wrapper,tab_animation,tabs,items,config);}}else{if(config['current_tab']<amount-1){tabsGK5Animation(config['current_tab']+1,tabs_wrapper,tab_animation,tabs,items,config);}else{tabsGK5Animation(0,tabs_wrapper,tab_animation,tabs,items,config);}}}}});}});});var tabsGK5Animation=function(i,tabs_wrapper,tab_animation,tabs,items,config){var direction=(config['rtl']==0)?'left':'right';if(i!=config['current_tab']){config['previous_tab']=config['current_tab'];config['current_tab']=i;if(config['auto_height']=='1'){tabs_wrapper.setStyle('min-height',tabs_wrapper.getSize().y+'px');}
tabs.removeClass('gk-active');tabs[i].addClass('gk-active');tabs[config['previous_tab']].removeClass('gk-active');tabs[config['previous_tab']].addClass('gk-hidden');tabs[i].removeClass('gk-hide');tabs[i].removeClass('gk-hidden');tabs[i].addClass('gk-active');items[config['previous_tab']].removeClass('active');items[i].addClass('active');var prev=config['previous_tab'];setTimeout(function(){if(tabs[prev].hasClass('gk-hidden')&&!tabs[prev].hasClass('gk-active')){tabs[prev].removeClass('gk-hidden');tabs[prev].addClass('gk-hide');}},350);if(config['auto_height']=='1'){tabs_wrapper.setStyle('min-height',tabs[i].getSize().y+"px");setTimeout(function(){tabs_wrapper.setStyle('height','auto');},350);}
if(typeof gkTabEventTrigger!=='undefined'){gkTabEventTrigger(i,i,config['module_id']);}
if(!config['falsy_click']){config['blank']=true;}else{config['falsy_click']=false;}
if(config['cookie_save']==1){Cookie.write('gktab-'+config['module_id'],i+1,{domain:'/',duration:256});}}};


/*===============================
http://iasitvlife.ro/modules/mod_news_pro_gk5/tmpl/portal_modes/videolist/script.jquery.js
================================================================================*/;
jQuery(window).load(function(){setTimeout(function(){jQuery(document).find('.gkNspPM-VideoList').each(function(i,module){module=jQuery(module);if(!module.hasClass('active')){module.addClass('active');}
var mod=new GKNSPVideoList();mod.init(module);});},1000);});var GKNSPVideoList=function(module){var API={currentPage:0,blank:false,pagesAmount:0,itemsAmount:0,perPage:0,pages:null,pagination:null,init:function(module){var $this=this;this.pagesAmount=module.find('.gkItemsPage').length;this.pages=module.find('.gkItemsPage');this.itemsAmount=module.find('.gkItem').length;this.perPage=Math.ceil(this.itemsAmount/this.pagesAmount);this.pagination=module.find('.gkBottomNavPagination li');module.find('.gkItem').each(function(i,item){item=jQuery(item);item.find('.gkImageWrap').click(function(e){e.preventDefault();var target_img=item.find('img');var url=target_img.attr('data-url');if(url!='#'){SqueezeBox.open(url,{handler:'iframe',size:{x:target_img.attr('data-x'),y:target_img.attr('data-y')}});}else{window.location.href=item.find('h3 a').attr('href');}});});if(this.pagination.length>0){jQuery(module.find('.gkBottomNavNext')).click(function(e){e.preventDefault();$this.next();});jQuery(module.find('.gkBottomNavPrev')).click(function(e){e.preventDefault();$this.prev();});jQuery(this.pagination).each(function(i,item){jQuery(item).click(function(e){e.preventDefault();$this.page(i);});});}},next:function(){var num=(this.currentPage<this.pagesAmount-1)?this.currentPage+1:0;this.page(num);},prev:function(){var num=(this.currentPage>0)?this.currentPage-1:this.pagesAmount-1;this.page(num);},page:function(num){var $this=this;var prev=jQuery(this.pages[this.currentPage]);prev.find('.gkItem').each(function(i,item){setTimeout(function(){jQuery(item).removeClass('active');},i*50);});var prevNum=this.currentPage;prev.addClass('to-hide');this.currentPage=num;if(this.pagination){jQuery(this.pagination[prevNum]).removeClass('active');jQuery(this.pagination[this.currentPage]).addClass('active');}
setTimeout(function(){prev.removeClass('active');jQuery($this.pages[$this.currentPage]).addClass('active');jQuery($this.pages[$this.currentPage]).find('.gkItem').each(function(i,item){setTimeout(function(){jQuery(item).addClass('active');},i*50);});prev.removeClass('to-hide');},500);}};return API;};


/*===============================
http://iasitvlife.ro/modules/mod_bm_slider_for_k2/assets/js/jquery.no-conflict.js
================================================================================*/;
if(typeof jQuery==='undefined'){alert('Has no jQuery loaded for the module!');}
else
{var jqbm=jQuery.noConflict(true);jQuery=jqbm;}


/*===============================
http://iasitvlife.ro/modules/mod_bm_slider_for_k2/assets/js/jquery.easing.1.3.js
================================================================================*/;
jqbm.easing['jswing']=jqbm.easing['swing'];jqbm.extend(jqbm.easing,{def:'easeOutQuad',swing:function(x,t,b,c,d){return-c*(t/=d)*(t-2)+b;},easeInQuad:function(x,t,b,c,d){return c*(t/=d)*t+b;},easeOutQuad:function(x,t,b,c,d){return-c*(t/=d)*(t-2)+b;},easeInOutQuad:function(x,t,b,c,d){if((t/=d/2)<1)return c/2*t*t+b;return-c/2*((--t)*(t-2)-1)+b;},easeInCubic:function(x,t,b,c,d){return c*(t/=d)*t*t+b;},easeOutCubic:function(x,t,b,c,d){return c*((t=t/d-1)*t*t+1)+b;},easeInOutCubic:function(x,t,b,c,d){if((t/=d/2)<1)return c/2*t*t*t+b;return c/2*((t-=2)*t*t+2)+b;},easeInQuart:function(x,t,b,c,d){return c*(t/=d)*t*t*t+b;},easeOutQuart:function(x,t,b,c,d){return-c*((t=t/d-1)*t*t*t-1)+b;},easeInOutQuart:function(x,t,b,c,d){if((t/=d/2)<1)return c/2*t*t*t*t+b;return-c/2*((t-=2)*t*t*t-2)+b;},easeInQuint:function(x,t,b,c,d){return c*(t/=d)*t*t*t*t+b;},easeOutQuint:function(x,t,b,c,d){return c*((t=t/d-1)*t*t*t*t+1)+b;},easeInOutQuint:function(x,t,b,c,d){if((t/=d/2)<1)return c/2*t*t*t*t*t+b;return c/2*((t-=2)*t*t*t*t+2)+b;},easeInSine:function(x,t,b,c,d){return-c*Math.cos(t/d*(Math.PI/2))+c+b;},easeOutSine:function(x,t,b,c,d){return c*Math.sin(t/d*(Math.PI/2))+b;},easeInOutSine:function(x,t,b,c,d){return-c/2*(Math.cos(Math.PI*t/d)-1)+b;},easeInExpo:function(x,t,b,c,d){return(t==0)?b:c*Math.pow(2,10*(t/d-1))+b;},easeOutExpo:function(x,t,b,c,d){return(t==d)?b+c:c*(-Math.pow(2,-10*t/d)+1)+b;},easeInOutExpo:function(x,t,b,c,d){if(t==0)return b;if(t==d)return b+c;if((t/=d/2)<1)return c/2*Math.pow(2,10*(t-1))+b;return c/2*(-Math.pow(2,-10*--t)+2)+b;},easeInCirc:function(x,t,b,c,d){return-c*(Math.sqrt(1-(t/=d)*t)-1)+b;},easeOutCirc:function(x,t,b,c,d){return c*Math.sqrt(1-(t=t/d-1)*t)+b;},easeInOutCirc:function(x,t,b,c,d){if((t/=d/2)<1)return-c/2*(Math.sqrt(1-t*t)-1)+b;return c/2*(Math.sqrt(1-(t-=2)*t)+1)+b;},easeInElastic:function(x,t,b,c,d){var s=1.70158;var p=0;var a=c;if(t==0)return b;if((t/=d)==1)return b+c;if(!p)p=d*.3;if(a<Math.abs(c)){a=c;var s=p/4;}
else var s=p/(2*Math.PI)*Math.asin(c/a);return-(a*Math.pow(2,10*(t-=1))*Math.sin((t*d-s)*(2*Math.PI)/p))+b;},easeOutElastic:function(x,t,b,c,d){var s=1.70158;var p=0;var a=c;if(t==0)return b;if((t/=d)==1)return b+c;if(!p)p=d*.3;if(a<Math.abs(c)){a=c;var s=p/4;}
else var s=p/(2*Math.PI)*Math.asin(c/a);return a*Math.pow(2,-10*t)*Math.sin((t*d-s)*(2*Math.PI)/p)+c+b;},easeInOutElastic:function(x,t,b,c,d){var s=1.70158;var p=0;var a=c;if(t==0)return b;if((t/=d/2)==2)return b+c;if(!p)p=d*(.3*1.5);if(a<Math.abs(c)){a=c;var s=p/4;}
else var s=p/(2*Math.PI)*Math.asin(c/a);if(t<1)return-.5*(a*Math.pow(2,10*(t-=1))*Math.sin((t*d-s)*(2*Math.PI)/p))+b;return a*Math.pow(2,-10*(t-=1))*Math.sin((t*d-s)*(2*Math.PI)/p)*.5+c+b;},easeInBack:function(x,t,b,c,d,s){if(s==undefined)s=1.70158;return c*(t/=d)*t*((s+1)*t-s)+b;},easeOutBack:function(x,t,b,c,d,s){if(s==undefined)s=1.70158;return c*((t=t/d-1)*t*((s+1)*t+s)+1)+b;},easeInOutBack:function(x,t,b,c,d,s){if(s==undefined)s=1.70158;if((t/=d/2)<1)return c/2*(t*t*(((s*=(1.525))+1)*t-s))+b;return c/2*((t-=2)*t*(((s*=(1.525))+1)*t+s)+2)+b;},easeInBounce:function(x,t,b,c,d){return c-jqbm.easing.easeOutBounce(x,d-t,0,c,d)+b;},easeOutBounce:function(x,t,b,c,d){if((t/=d)<(1/2.75)){return c*(7.5625*t*t)+b;}else if(t<(2/2.75)){return c*(7.5625*(t-=(1.5/2.75))*t+.75)+b;}else if(t<(2.5/2.75)){return c*(7.5625*(t-=(2.25/2.75))*t+.9375)+b;}else{return c*(7.5625*(t-=(2.625/2.75))*t+.984375)+b;}},easeInOutBounce:function(x,t,b,c,d){if(t<d/2)return jqbm.easing.easeInBounce(x,t*2,0,c,d)*.5+b;return jqbm.easing.easeOutBounce(x,t*2-d,0,c,d)*.5+c*.5+b;}});


/*===============================
http://iasitvlife.ro/modules/mod_bm_slider_for_k2/assets/js/jquery.cycle2.js
================================================================================*/;
;
/*!
* jQuery Cycle2; build: v20131005
* http://jquery.malsup.com/cycle2/
* Copyright (c) 2013 M. Alsup; Dual licensed: MIT/GPL
*/
/*! core engine; version: 20131003 */
(function($){"use strict";var version='20131003';$.fn.cycle=function(options){var o;if(this.length===0&&!$.isReady){o={s:this.selector,c:this.context};$.fn.cycle.log('requeuing slideshow (dom not ready)');$(function(){$(o.s,o.c).cycle(options);});return this;}
return this.each(function(){var data,opts,shortName,val;var container=$(this);var log=$.fn.cycle.log;if(container.data('cycle.opts'))
return;if(container.data('cycle-log')===false||(options&&options.log===false)||(opts&&opts.log===false)){log=$.noop;}
log('--c2 init--');data=container.data();for(var p in data){if(data.hasOwnProperty(p)&&/^cycle[A-Z]+/.test(p)){val=data[p];shortName=p.match(/^cycle(.*)/)[1].replace(/^[A-Z]/,lowerCase);log(shortName+':',val,'('+typeof val+')');data[shortName]=val;}}
opts=$.extend({},$.fn.cycle.defaults,data,options||{});opts.timeoutId=0;opts.paused=opts.paused||false;opts.container=container;opts._maxZ=opts.maxZ;opts.API=$.extend({_container:container},$.fn.cycle.API);opts.API.log=log;opts.API.trigger=function(eventName,args){opts.container.trigger(eventName,args);return opts.API;};container.data('cycle.opts',opts);container.data('cycle.API',opts.API);opts.API.trigger('cycle-bootstrap',[opts,opts.API]);opts.API.addInitialSlides();opts.API.preInitSlideshow();if(opts.slides.length)
opts.API.initSlideshow();});};$.fn.cycle.API={opts:function(){return this._container.data('cycle.opts');},addInitialSlides:function(){var opts=this.opts();var slides=opts.slides;opts.slideCount=0;opts.slides=$();slides=slides.jquery?slides:opts.container.find(slides);if(opts.random){slides.sort(function(){return Math.random()-0.5;});}
opts.API.add(slides);},preInitSlideshow:function(){var opts=this.opts();opts.API.trigger('cycle-pre-initialize',[opts]);var tx=$.fn.cycle.transitions[opts.fx];if(tx&&$.isFunction(tx.preInit))
tx.preInit(opts);opts._preInitialized=true;},postInitSlideshow:function(){var opts=this.opts();opts.API.trigger('cycle-post-initialize',[opts]);var tx=$.fn.cycle.transitions[opts.fx];if(tx&&$.isFunction(tx.postInit))
tx.postInit(opts);},initSlideshow:function(){var opts=this.opts();var pauseObj=opts.container;var slideOpts;opts.API.calcFirstSlide();if(opts.container.css('position')=='static')
opts.container.css('position','relative');$(opts.slides[opts.currSlide]).css('opacity',1).show();opts.API.stackSlides(opts.slides[opts.currSlide],opts.slides[opts.nextSlide],!opts.reverse);if(opts.pauseOnHover){if(opts.pauseOnHover!==true)
pauseObj=$(opts.pauseOnHover);pauseObj.hover(function(){opts.API.pause(true);},function(){opts.API.resume(true);});}
if(opts.timeout){slideOpts=opts.API.getSlideOpts(opts.currSlide);opts.API.queueTransition(slideOpts,slideOpts.timeout+opts.delay);}
opts._initialized=true;opts.API.updateView(true);opts.API.trigger('cycle-initialized',[opts]);opts.API.postInitSlideshow();},pause:function(hover){var opts=this.opts(),slideOpts=opts.API.getSlideOpts(),alreadyPaused=opts.hoverPaused||opts.paused;if(hover)
opts.hoverPaused=true;else
opts.paused=true;if(!alreadyPaused){opts.container.addClass('cycle-paused');opts.API.trigger('cycle-paused',[opts]).log('cycle-paused');if(slideOpts.timeout){clearTimeout(opts.timeoutId);opts.timeoutId=0;opts._remainingTimeout-=($.now()-opts._lastQueue);if(opts._remainingTimeout<0||isNaN(opts._remainingTimeout))
opts._remainingTimeout=undefined;}}},resume:function(hover){var opts=this.opts(),alreadyResumed=!opts.hoverPaused&&!opts.paused,remaining;if(hover)
opts.hoverPaused=false;else
opts.paused=false;if(!alreadyResumed){opts.container.removeClass('cycle-paused');if(opts.slides.filter(':animated').length===0)
opts.API.queueTransition(opts.API.getSlideOpts(),opts._remainingTimeout);opts.API.trigger('cycle-resumed',[opts,opts._remainingTimeout]).log('cycle-resumed');}},add:function(slides,prepend){var opts=this.opts();var oldSlideCount=opts.slideCount;var startSlideshow=false;var len;if($.type(slides)=='string')
slides=$.trim(slides);$(slides).each(function(i){var slideOpts;var slide=$(this);if(prepend)
opts.container.prepend(slide);else
opts.container.append(slide);opts.slideCount++;slideOpts=opts.API.buildSlideOpts(slide);if(prepend)
opts.slides=$(slide).add(opts.slides);else
opts.slides=opts.slides.add(slide);opts.API.initSlide(slideOpts,slide,--opts._maxZ);slide.data('cycle.opts',slideOpts);opts.API.trigger('cycle-slide-added',[opts,slideOpts,slide]);});opts.API.updateView(true);startSlideshow=opts._preInitialized&&(oldSlideCount<2&&opts.slideCount>=1);if(startSlideshow){if(!opts._initialized)
opts.API.initSlideshow();else if(opts.timeout){len=opts.slides.length;opts.nextSlide=opts.reverse?len-1:1;if(!opts.timeoutId){opts.API.queueTransition(opts);}}}},calcFirstSlide:function(){var opts=this.opts();var firstSlideIndex;firstSlideIndex=parseInt(opts.startingSlide||0,10);if(firstSlideIndex>=opts.slides.length||firstSlideIndex<0)
firstSlideIndex=0;opts.currSlide=firstSlideIndex;if(opts.reverse){opts.nextSlide=firstSlideIndex-1;if(opts.nextSlide<0)
opts.nextSlide=opts.slides.length-1;}
else{opts.nextSlide=firstSlideIndex+1;if(opts.nextSlide==opts.slides.length)
opts.nextSlide=0;}},calcNextSlide:function(){var opts=this.opts();var roll;if(opts.reverse){roll=(opts.nextSlide-1)<0;opts.nextSlide=roll?opts.slideCount-1:opts.nextSlide-1;opts.currSlide=roll?0:opts.nextSlide+1;}
else{roll=(opts.nextSlide+1)==opts.slides.length;opts.nextSlide=roll?0:opts.nextSlide+1;opts.currSlide=roll?opts.slides.length-1:opts.nextSlide-1;}},calcTx:function(slideOpts,manual){var opts=slideOpts;var tx;if(manual&&opts.manualFx)
tx=$.fn.cycle.transitions[opts.manualFx];if(!tx)
tx=$.fn.cycle.transitions[opts.fx];if(!tx){tx=$.fn.cycle.transitions.fade;opts.API.log('Transition "'+opts.fx+'" not found.  Using fade.');}
return tx;},prepareTx:function(manual,fwd){var opts=this.opts();var after,curr,next,slideOpts,tx;if(opts.slideCount<2){opts.timeoutId=0;return;}
if(manual&&(!opts.busy||opts.manualTrump)){opts.API.stopTransition();opts.busy=false;clearTimeout(opts.timeoutId);opts.timeoutId=0;}
if(opts.busy)
return;if(opts.timeoutId===0&&!manual)
return;curr=opts.slides[opts.currSlide];next=opts.slides[opts.nextSlide];slideOpts=opts.API.getSlideOpts(opts.nextSlide);tx=opts.API.calcTx(slideOpts,manual);opts._tx=tx;if(manual&&slideOpts.manualSpeed!==undefined)
slideOpts.speed=slideOpts.manualSpeed;if(opts.nextSlide!=opts.currSlide&&(manual||(!opts.paused&&!opts.hoverPaused&&opts.timeout))){opts.API.trigger('cycle-before',[slideOpts,curr,next,fwd]);if(tx.before)
tx.before(slideOpts,curr,next,fwd);after=function(){opts.busy=false;if(!opts.container.data('cycle.opts'))
return;if(tx.after)
tx.after(slideOpts,curr,next,fwd);opts.API.trigger('cycle-after',[slideOpts,curr,next,fwd]);opts.API.queueTransition(slideOpts);opts.API.updateView(true);};opts.busy=true;if(tx.transition)
tx.transition(slideOpts,curr,next,fwd,after);else
opts.API.doTransition(slideOpts,curr,next,fwd,after);opts.API.calcNextSlide();opts.API.updateView();}else{opts.API.queueTransition(slideOpts);}},doTransition:function(slideOpts,currEl,nextEl,fwd,callback){var opts=slideOpts;var curr=$(currEl),next=$(nextEl);var fn=function(){next.animate(opts.animIn||{opacity:1},opts.speed,opts.easeIn||opts.easing,callback);};next.css(opts.cssBefore||{});curr.animate(opts.animOut||{},opts.speed,opts.easeOut||opts.easing,function(){curr.css(opts.cssAfter||{});if(!opts.sync){fn();}});if(opts.sync){fn();}},queueTransition:function(slideOpts,specificTimeout){var opts=this.opts();var timeout=specificTimeout!==undefined?specificTimeout:slideOpts.timeout;if(opts.nextSlide===0&&--opts.loop===0){opts.API.log('terminating; loop=0');opts.timeout=0;if(timeout){setTimeout(function(){opts.API.trigger('cycle-finished',[opts]);},timeout);}
else{opts.API.trigger('cycle-finished',[opts]);}
opts.nextSlide=opts.currSlide;return;}
if(timeout){opts._lastQueue=$.now();if(specificTimeout===undefined)
opts._remainingTimeout=slideOpts.timeout;if(!opts.paused&&!opts.hoverPaused){opts.timeoutId=setTimeout(function(){opts.API.prepareTx(false,!opts.reverse);},timeout);}}},stopTransition:function(){var opts=this.opts();if(opts.slides.filter(':animated').length){opts.slides.stop(false,true);opts.API.trigger('cycle-transition-stopped',[opts]);}
if(opts._tx&&opts._tx.stopTransition)
opts._tx.stopTransition(opts);},advanceSlide:function(val){var opts=this.opts();clearTimeout(opts.timeoutId);opts.timeoutId=0;opts.nextSlide=opts.currSlide+val;if(opts.nextSlide<0)
opts.nextSlide=opts.slides.length-1;else if(opts.nextSlide>=opts.slides.length)
opts.nextSlide=0;opts.API.prepareTx(true,val>=0);return false;},buildSlideOpts:function(slide){var opts=this.opts();var val,shortName;var slideOpts=slide.data()||{};for(var p in slideOpts){if(slideOpts.hasOwnProperty(p)&&/^cycle[A-Z]+/.test(p)){val=slideOpts[p];shortName=p.match(/^cycle(.*)/)[1].replace(/^[A-Z]/,lowerCase);opts.API.log('['+(opts.slideCount-1)+']',shortName+':',val,'('+typeof val+')');slideOpts[shortName]=val;}}
slideOpts=$.extend({},$.fn.cycle.defaults,opts,slideOpts);slideOpts.slideNum=opts.slideCount;try{delete slideOpts.API;delete slideOpts.slideCount;delete slideOpts.currSlide;delete slideOpts.nextSlide;delete slideOpts.slides;}catch(e){}
return slideOpts;},getSlideOpts:function(index){var opts=this.opts();if(index===undefined)
index=opts.currSlide;var slide=opts.slides[index];var slideOpts=$(slide).data('cycle.opts');return $.extend({},opts,slideOpts);},initSlide:function(slideOpts,slide,suggestedZindex){var opts=this.opts();slide.css(slideOpts.slideCss||{});if(suggestedZindex>0)
slide.css('zIndex',suggestedZindex);if(isNaN(slideOpts.speed))
slideOpts.speed=$.fx.speeds[slideOpts.speed]||$.fx.speeds._default;if(!slideOpts.sync)
slideOpts.speed=slideOpts.speed/2;slide.addClass(opts.slideClass);},updateView:function(isAfter,isDuring){var opts=this.opts();if(!opts._initialized)
return;var slideOpts=opts.API.getSlideOpts();var currSlide=opts.slides[opts.currSlide];if(!isAfter&&isDuring!==true){opts.API.trigger('cycle-update-view-before',[opts,slideOpts,currSlide]);if(opts.updateView<0)
return;}
if(opts.slideActiveClass){opts.slides.removeClass(opts.slideActiveClass).eq(opts.currSlide).addClass(opts.slideActiveClass);}
if(isAfter&&opts.hideNonActive)
opts.slides.filter(':not(.'+opts.slideActiveClass+')').hide();opts.API.trigger('cycle-update-view',[opts,slideOpts,currSlide,isAfter]);if(isAfter)
opts.API.trigger('cycle-update-view-after',[opts,slideOpts,currSlide]);},getComponent:function(name){var opts=this.opts();var selector=opts[name];if(typeof selector==='string'){return(/^\s*[\>|\+|~]/).test(selector)?opts.container.find(selector):$(selector);}
if(selector.jquery)
return selector;return $(selector);},stackSlides:function(curr,next,fwd){var opts=this.opts();if(!curr){curr=opts.slides[opts.currSlide];next=opts.slides[opts.nextSlide];fwd=!opts.reverse;}
$(curr).css('zIndex',opts.maxZ);var i;var z=opts.maxZ-2;var len=opts.slideCount;if(fwd){for(i=opts.currSlide+1;i<len;i++)
$(opts.slides[i]).css('zIndex',z--);for(i=0;i<opts.currSlide;i++)
$(opts.slides[i]).css('zIndex',z--);}
else{for(i=opts.currSlide-1;i>=0;i--)
$(opts.slides[i]).css('zIndex',z--);for(i=len-1;i>opts.currSlide;i--)
$(opts.slides[i]).css('zIndex',z--);}
$(next).css('zIndex',opts.maxZ-1);},getSlideIndex:function(el){return this.opts().slides.index(el);}};$.fn.cycle.log=function log(){if(window.console&&console.log)
console.log('[cycle2] '+Array.prototype.join.call(arguments,' '));};$.fn.cycle.version=function(){return'Cycle2: '+version;};function lowerCase(s){return(s||'').toLowerCase();}
$.fn.cycle.transitions={custom:{},none:{before:function(opts,curr,next,fwd){opts.API.stackSlides(next,curr,fwd);opts.cssBefore={opacity:1,display:'block'};}},fade:{before:function(opts,curr,next,fwd){var css=opts.API.getSlideOpts(opts.nextSlide).slideCss||{};opts.API.stackSlides(curr,next,fwd);opts.cssBefore=$.extend(css,{opacity:0,display:'block'});opts.animIn={opacity:1};opts.animOut={opacity:0};}},fadeout:{before:function(opts,curr,next,fwd){var css=opts.API.getSlideOpts(opts.nextSlide).slideCss||{};opts.API.stackSlides(curr,next,fwd);opts.cssBefore=$.extend(css,{opacity:1,display:'block'});opts.animOut={opacity:0};}},scrollHorz:{before:function(opts,curr,next,fwd){opts.API.stackSlides(curr,next,fwd);var w=opts.container.css('overflow','hidden').width();opts.cssBefore={left:fwd?w:-w,top:0,opacity:1,display:'block'};opts.cssAfter={zIndex:opts._maxZ-2,left:0};opts.animIn={left:0};opts.animOut={left:fwd?-w:w};}}};$.fn.cycle.defaults={allowWrap:true,autoSelector:'.cycle-slideshow[data-cycle-auto-init!=false]',delay:0,easing:null,fx:'fade',hideNonActive:true,loop:0,manualFx:undefined,manualSpeed:undefined,manualTrump:true,maxZ:100,pauseOnHover:false,reverse:false,slideActiveClass:'cycle-slide-active',slideClass:'cycle-slide',slideCss:{position:'absolute',top:0,left:0},slides:'> img',speed:500,startingSlide:0,sync:true,timeout:4000,updateView:-1};$(document).ready(function(){$($.fn.cycle.defaults.autoSelector).cycle();});})(jQuery);
/*! Cycle2 autoheight plugin; Copyright (c) M.Alsup, 2012; version: 20130304 */
(function($){"use strict";$.extend($.fn.cycle.defaults,{autoHeight:0});$(document).on('cycle-initialized',function(e,opts){var autoHeight=opts.autoHeight;var t=$.type(autoHeight);var resizeThrottle=null;var ratio;if(t!=='string'&&t!=='number')
return;opts.container.on('cycle-slide-added cycle-slide-removed',initAutoHeight);opts.container.on('cycle-destroyed',onDestroy);if(autoHeight=='container'){opts.container.on('cycle-before',onBefore);}
else if(t==='string'&&/\d+\:\d+/.test(autoHeight)){ratio=autoHeight.match(/(\d+)\:(\d+)/);ratio=ratio[1]/ratio[2];opts._autoHeightRatio=ratio;}
if(t!=='number'){opts._autoHeightOnResize=function(){clearTimeout(resizeThrottle);resizeThrottle=setTimeout(onResize,50);};$(window).on('resize orientationchange',opts._autoHeightOnResize);}
setTimeout(onResize,30);function onResize(){initAutoHeight(e,opts);}});function initAutoHeight(e,opts){var clone,height,sentinelIndex;var autoHeight=opts.autoHeight;if(autoHeight=='container'){height=$(opts.slides[opts.currSlide]).outerHeight();opts.container.height(height);}
else if(opts._autoHeightRatio){opts.container.height(opts.container.width()/opts._autoHeightRatio);}
else if(autoHeight==='calc'||($.type(autoHeight)=='number'&&autoHeight>=0)){if(autoHeight==='calc')
sentinelIndex=calcSentinelIndex(e,opts);else if(autoHeight>=opts.slides.length)
sentinelIndex=0;else
sentinelIndex=autoHeight;if(sentinelIndex==opts._sentinelIndex)
return;opts._sentinelIndex=sentinelIndex;if(opts._sentinel)
opts._sentinel.remove();clone=$(opts.slides[sentinelIndex].cloneNode(true));clone.removeAttr('id name rel').find('[id],[name],[rel]').removeAttr('id name rel');clone.css({position:'static',visibility:'hidden',display:'block'}).prependTo(opts.container).addClass('cycle-sentinel cycle-slide').removeClass('cycle-slide-active');clone.find('*').css('visibility','hidden');opts._sentinel=clone;}}
function calcSentinelIndex(e,opts){var index=0,max=-1;opts.slides.each(function(i){var h=$(this).height();if(h>max){max=h;index=i;}});return index;}
function onBefore(e,opts,outgoing,incoming,forward){var h=$(incoming).outerHeight();var duration=opts.sync?opts.speed/2:opts.speed;opts.container.animate({height:h},duration);}
function onDestroy(e,opts){if(opts._autoHeightOnResize){$(window).off('resize orientationchange',opts._autoHeightOnResize);opts._autoHeightOnResize=null;}
opts.container.off('cycle-slide-added cycle-slide-removed',initAutoHeight);opts.container.off('cycle-destroyed',onDestroy);opts.container.off('cycle-before',onBefore);if(opts._sentinel){opts._sentinel.remove();opts._sentinel=null;}}})(jQuery);
/*! caption plugin for Cycle2;  version: 20130306 */
(function($){"use strict";$.extend($.fn.cycle.defaults,{caption:'> .cycle-caption',captionTemplate:'{{slideNum}} / {{slideCount}}',overlay:'> .cycle-overlay',overlayTemplate:'<div>{{title}}</div><div>{{desc}}</div>',captionModule:'caption'});$(document).on('cycle-update-view',function(e,opts,slideOpts,currSlide){if(opts.captionModule!=='caption')
return;var el;$.each(['caption','overlay'],function(){var name=this;var template=slideOpts[name+'Template'];var el=opts.API.getComponent(name);if(el.length&&template){el.html(opts.API.tmpl(template,slideOpts,opts,currSlide));el.show();}
else{el.hide();}});});$(document).on('cycle-destroyed',function(e,opts){var el;$.each(['caption','overlay'],function(){var name=this,template=opts[name+'Template'];if(opts[name]&&template){el=opts.API.getComponent('caption');el.empty();}});});})(jQuery);
/*! command plugin for Cycle2;  version: 20130707 */
(function($){"use strict";var c2=$.fn.cycle;$.fn.cycle=function(options){var cmd,cmdFn,opts;var args=$.makeArray(arguments);if($.type(options)=='number'){return this.cycle('goto',options);}
if($.type(options)=='string'){return this.each(function(){var cmdArgs;cmd=options;opts=$(this).data('cycle.opts');if(opts===undefined){c2.log('slideshow must be initialized before sending commands; "'+cmd+'" ignored');return;}
else{cmd=cmd=='goto'?'jump':cmd;cmdFn=opts.API[cmd];if($.isFunction(cmdFn)){cmdArgs=$.makeArray(args);cmdArgs.shift();return cmdFn.apply(opts.API,cmdArgs);}
else{c2.log('unknown command: ',cmd);}}});}
else{return c2.apply(this,arguments);}};$.extend($.fn.cycle,c2);$.extend(c2.API,{next:function(){var opts=this.opts();if(opts.busy&&!opts.manualTrump)
return;var count=opts.reverse?-1:1;if(opts.allowWrap===false&&(opts.currSlide+count)>=opts.slideCount)
return;opts.API.advanceSlide(count);opts.API.trigger('cycle-next',[opts]).log('cycle-next');},prev:function(){var opts=this.opts();if(opts.busy&&!opts.manualTrump)
return;var count=opts.reverse?1:-1;if(opts.allowWrap===false&&(opts.currSlide+count)<0)
return;opts.API.advanceSlide(count);opts.API.trigger('cycle-prev',[opts]).log('cycle-prev');},destroy:function(){this.stop();var opts=this.opts();var clean=$.isFunction($._data)?$._data:$.noop;clearTimeout(opts.timeoutId);opts.timeoutId=0;opts.API.stop();opts.API.trigger('cycle-destroyed',[opts]).log('cycle-destroyed');opts.container.removeData();clean(opts.container[0],'parsedAttrs',false);if(!opts.retainStylesOnDestroy){opts.container.removeAttr('style');opts.slides.removeAttr('style');opts.slides.removeClass(opts.slideActiveClass);}
opts.slides.each(function(){$(this).removeData();clean(this,'parsedAttrs',false);});},jump:function(index){var fwd;var opts=this.opts();if(opts.busy&&!opts.manualTrump)
return;var num=parseInt(index,10);if(isNaN(num)||num<0||num>=opts.slides.length){opts.API.log('goto: invalid slide index: '+num);return;}
if(num==opts.currSlide){opts.API.log('goto: skipping, already on slide',num);return;}
opts.nextSlide=num;clearTimeout(opts.timeoutId);opts.timeoutId=0;opts.API.log('goto: ',num,' (zero-index)');fwd=opts.currSlide<opts.nextSlide;opts.API.prepareTx(true,fwd);},stop:function(){var opts=this.opts();var pauseObj=opts.container;clearTimeout(opts.timeoutId);opts.timeoutId=0;opts.API.stopTransition();if(opts.pauseOnHover){if(opts.pauseOnHover!==true)
pauseObj=$(opts.pauseOnHover);pauseObj.off('mouseenter mouseleave');}
opts.API.trigger('cycle-stopped',[opts]).log('cycle-stopped');},reinit:function(){var opts=this.opts();opts.API.destroy();opts.container.cycle();},remove:function(index){var opts=this.opts();var slide,slideToRemove,slides=[],slideNum=1;for(var i=0;i<opts.slides.length;i++){slide=opts.slides[i];if(i==index){slideToRemove=slide;}
else{slides.push(slide);$(slide).data('cycle.opts').slideNum=slideNum;slideNum++;}}
if(slideToRemove){opts.slides=$(slides);opts.slideCount--;$(slideToRemove).remove();if(index==opts.currSlide)
opts.API.advanceSlide(1);else if(index<opts.currSlide)
opts.currSlide--;else
opts.currSlide++;opts.API.trigger('cycle-slide-removed',[opts,index,slideToRemove]).log('cycle-slide-removed');opts.API.updateView();}}});$(document).on('click.cycle','[data-cycle-cmd]',function(e){e.preventDefault();var el=$(this);var command=el.data('cycle-cmd');var context=el.data('cycle-context')||'.cycle-slideshow';$(context).cycle(command,el.data('cycle-arg'));});})(jQuery);
/*! hash plugin for Cycle2;  version: 20130905 */
(function($){"use strict";$(document).on('cycle-pre-initialize',function(e,opts){onHashChange(opts,true);opts._onHashChange=function(){onHashChange(opts,false);};$(window).on('hashchange',opts._onHashChange);});$(document).on('cycle-update-view',function(e,opts,slideOpts){if(slideOpts.hash&&('#'+slideOpts.hash)!=window.location.hash){opts._hashFence=true;window.location.hash=slideOpts.hash;}});$(document).on('cycle-destroyed',function(e,opts){if(opts._onHashChange){$(window).off('hashchange',opts._onHashChange);}});function onHashChange(opts,setStartingSlide){var hash;if(opts._hashFence){opts._hashFence=false;return;}
hash=window.location.hash.substring(1);opts.slides.each(function(i){if($(this).data('cycle-hash')==hash){if(setStartingSlide===true){opts.startingSlide=i;}
else{var fwd=opts.currSlide<i;opts.nextSlide=i;opts.API.prepareTx(true,fwd);}
return false;}});}})(jQuery);
/*! loader plugin for Cycle2;  version: 20130307 */
(function($){"use strict";$.extend($.fn.cycle.defaults,{loader:false});$(document).on('cycle-bootstrap',function(e,opts){var addFn;if(!opts.loader)
return;addFn=opts.API.add;opts.API.add=add;function add(slides,prepend){var slideArr=[];if($.type(slides)=='string')
slides=$.trim(slides);else if($.type(slides)==='array'){for(var i=0;i<slides.length;i++)
slides[i]=$(slides[i])[0];}
slides=$(slides);var slideCount=slides.length;if(!slideCount)
return;slides.hide().appendTo('body').each(function(i){var count=0;var slide=$(this);var images=slide.is('img')?slide:slide.find('img');slide.data('index',i);images=images.filter(':not(.cycle-loader-ignore)').filter(':not([src=""])');if(!images.length){--slideCount;slideArr.push(slide);return;}
count=images.length;images.each(function(){if(this.complete){imageLoaded();}
else{$(this).load(function(){imageLoaded();}).error(function(){if(--count===0){opts.API.log('slide skipped; img not loaded:',this.src);if(--slideCount===0&&opts.loader=='wait'){addFn.apply(opts.API,[slideArr,prepend]);}}});}});function imageLoaded(){if(--count===0){--slideCount;addSlide(slide);}}});if(slideCount)
opts.container.addClass('cycle-loading');function addSlide(slide){var curr;if(opts.loader=='wait'){slideArr.push(slide);if(slideCount===0){slideArr.sort(sorter);addFn.apply(opts.API,[slideArr,prepend]);opts.container.removeClass('cycle-loading');}}
else{curr=$(opts.slides[opts.currSlide]);addFn.apply(opts.API,[slide,prepend]);curr.show();opts.container.removeClass('cycle-loading');}}
function sorter(a,b){return a.data('index')-b.data('index');}}});})(jQuery);
/*! pager plugin for Cycle2;  version: 20130525 */
(function($){"use strict";$.extend($.fn.cycle.defaults,{pager:'> .cycle-pager',pagerActiveClass:'cycle-pager-active',pagerEvent:'click.cycle',pagerTemplate:'<span>&bull;</span>'});$(document).on('cycle-bootstrap',function(e,opts,API){API.buildPagerLink=buildPagerLink;});$(document).on('cycle-slide-added',function(e,opts,slideOpts,slideAdded){if(opts.pager){opts.API.buildPagerLink(opts,slideOpts,slideAdded);opts.API.page=page;}});$(document).on('cycle-slide-removed',function(e,opts,index,slideRemoved){if(opts.pager){var pagers=opts.API.getComponent('pager');pagers.each(function(){var pager=$(this);$(pager.children()[index]).remove();});}});$(document).on('cycle-update-view',function(e,opts,slideOpts){var pagers;if(opts.pager){pagers=opts.API.getComponent('pager');pagers.each(function(){$(this).children().removeClass(opts.pagerActiveClass).eq(opts.currSlide).addClass(opts.pagerActiveClass);});}});$(document).on('cycle-destroyed',function(e,opts){var pager=opts.API.getComponent('pager');if(pager){pager.children().off(opts.pagerEvent);if(opts.pagerTemplate)
pager.empty();}});function buildPagerLink(opts,slideOpts,slide){var pagerLink;var pagers=opts.API.getComponent('pager');pagers.each(function(){var pager=$(this);if(slideOpts.pagerTemplate){var markup=opts.API.tmpl(slideOpts.pagerTemplate,slideOpts,opts,slide[0]);pagerLink=$(markup).appendTo(pager);}
else{pagerLink=pager.children().eq(opts.slideCount-1);}
pagerLink.on(opts.pagerEvent,function(e){e.preventDefault();opts.API.page(pager,e.currentTarget);});});}
function page(pager,target){var opts=this.opts();if(opts.busy&&!opts.manualTrump)
return;var index=pager.children().index(target);var nextSlide=index;var fwd=opts.currSlide<nextSlide;if(opts.currSlide==nextSlide){return;}
opts.nextSlide=nextSlide;opts.API.prepareTx(true,fwd);opts.API.trigger('cycle-pager-activated',[opts,pager,target]);}})(jQuery);
/*! prevnext plugin for Cycle2;  version: 20130709 */
(function($){"use strict";$.extend($.fn.cycle.defaults,{next:'> .cycle-next',nextEvent:'click.cycle',disabledClass:'disabled',prev:'> .cycle-prev',prevEvent:'click.cycle',swipe:false});$(document).on('cycle-initialized',function(e,opts){opts.API.getComponent('next').on(opts.nextEvent,function(e){e.preventDefault();opts.API.next();});opts.API.getComponent('prev').on(opts.prevEvent,function(e){e.preventDefault();opts.API.prev();});if(opts.swipe){var nextEvent=opts.swipeVert?'swipeUp.cycle':'swipeLeft.cycle swipeleft.cycle';var prevEvent=opts.swipeVert?'swipeDown.cycle':'swipeRight.cycle swiperight.cycle';opts.container.on(nextEvent,function(e){opts.API.next();});opts.container.on(prevEvent,function(){opts.API.prev();});}});$(document).on('cycle-update-view',function(e,opts,slideOpts,currSlide){if(opts.allowWrap)
return;var cls=opts.disabledClass;var next=opts.API.getComponent('next');var prev=opts.API.getComponent('prev');var prevBoundry=opts._prevBoundry||0;var nextBoundry=(opts._nextBoundry!==undefined)?opts._nextBoundry:opts.slideCount-1;if(opts.currSlide==nextBoundry)
next.addClass(cls).prop('disabled',true);else
next.removeClass(cls).prop('disabled',false);if(opts.currSlide===prevBoundry)
prev.addClass(cls).prop('disabled',true);else
prev.removeClass(cls).prop('disabled',false);});$(document).on('cycle-destroyed',function(e,opts){opts.API.getComponent('prev').off(opts.nextEvent);opts.API.getComponent('next').off(opts.prevEvent);opts.container.off('swipeleft.cycle swiperight.cycle swipeLeft.cycle swipeRight.cycle swipeUp.cycle swipeDown.cycle');});})(jQuery);
/*! progressive loader plugin for Cycle2;  version: 20130315 */
(function($){"use strict";$.extend($.fn.cycle.defaults,{progressive:false});$(document).on('cycle-pre-initialize',function(e,opts){if(!opts.progressive)
return;var API=opts.API;var nextFn=API.next;var prevFn=API.prev;var prepareTxFn=API.prepareTx;var type=$.type(opts.progressive);var slides,scriptEl;if(type=='array'){slides=opts.progressive;}
else if($.isFunction(opts.progressive)){slides=opts.progressive(opts);}
else if(type=='string'){scriptEl=$(opts.progressive);slides=$.trim(scriptEl.html());if(!slides)
return;if(/^(\[)/.test(slides)){try{slides=$.parseJSON(slides);}
catch(err){API.log('error parsing progressive slides',err);return;}}
else{slides=slides.split(new RegExp(scriptEl.data('cycle-split')||'\n'));if(!slides[slides.length-1])
slides.pop();}}
if(prepareTxFn){API.prepareTx=function(manual,fwd){var index,slide;if(manual||slides.length===0){prepareTxFn.apply(opts.API,[manual,fwd]);return;}
if(fwd&&opts.currSlide==(opts.slideCount-1)){slide=slides[0];slides=slides.slice(1);opts.container.one('cycle-slide-added',function(e,opts){setTimeout(function(){opts.API.advanceSlide(1);},50);});opts.API.add(slide);}
else if(!fwd&&opts.currSlide===0){index=slides.length-1;slide=slides[index];slides=slides.slice(0,index);opts.container.one('cycle-slide-added',function(e,opts){setTimeout(function(){opts.currSlide=1;opts.API.advanceSlide(-1);},50);});opts.API.add(slide,true);}
else{prepareTxFn.apply(opts.API,[manual,fwd]);}};}
if(nextFn){API.next=function(){var opts=this.opts();if(slides.length&&opts.currSlide==(opts.slideCount-1)){var slide=slides[0];slides=slides.slice(1);opts.container.one('cycle-slide-added',function(e,opts){nextFn.apply(opts.API);opts.container.removeClass('cycle-loading');});opts.container.addClass('cycle-loading');opts.API.add(slide);}
else{nextFn.apply(opts.API);}};}
if(prevFn){API.prev=function(){var opts=this.opts();if(slides.length&&opts.currSlide===0){var index=slides.length-1;var slide=slides[index];slides=slides.slice(0,index);opts.container.one('cycle-slide-added',function(e,opts){opts.currSlide=1;opts.API.advanceSlide(-1);opts.container.removeClass('cycle-loading');});opts.container.addClass('cycle-loading');opts.API.add(slide,true);}
else{prevFn.apply(opts.API);}};}});})(jQuery);
/*! tmpl plugin for Cycle2;  version: 20121227 */
(function($){"use strict";$.extend($.fn.cycle.defaults,{tmplRegex:'{{((.)?.*?)}}'});$.extend($.fn.cycle.API,{tmpl:function(str,opts){var regex=new RegExp(opts.tmplRegex||$.fn.cycle.defaults.tmplRegex,'g');var args=$.makeArray(arguments);args.shift();return str.replace(regex,function(_,str){var i,j,obj,prop,names=str.split('.');for(i=0;i<args.length;i++){obj=args[i];if(!obj)
continue;if(names.length>1){prop=obj;for(j=0;j<names.length;j++){obj=prop;prop=prop[names[j]]||str;}}else{prop=obj[str];}
if($.isFunction(prop))
return prop.apply(obj,args);if(prop!==undefined&&prop!==null&&prop!=str)
return prop;}
return str;});}});})(jQuery);


/*===============================
http://iasitvlife.ro/modules/mod_bm_slider_for_k2/assets/js/jquery.cycle2.carousel.js
================================================================================*/;
/*! carousel transition plugin for Cycle2;  version: 20130528 */
(function($){"use strict";$(document).on('cycle-bootstrap',function(e,opts,API){if(opts.fx!=='carousel')
return;API.getSlideIndex=function(el){var slides=this.opts()._carouselWrap.children();var i=slides.index(el);return i%slides.length;};API.next=function(){var count=opts.reverse?-1:1;if(opts.allowWrap===false&&(opts.currSlide+count)>opts.slideCount-opts.carouselVisible)
return;opts.API.advanceSlide(count);opts.API.trigger('cycle-next',[opts]).log('cycle-next');};});$.fn.cycle.transitions.carousel={preInit:function(opts){opts.hideNonActive=false;opts.container.on('cycle-destroyed',$.proxy(this.onDestroy,opts.API));opts.API.stopTransition=this.stopTransition;for(var i=0;i<opts.startingSlide;i++){opts.container.append(opts.slides[0]);}},postInit:function(opts){var i,j,slide,pagerCutoffIndex,wrap;var vert=opts.carouselVertical;if(opts.carouselVisible&&opts.carouselVisible>opts.slideCount)
opts.carouselVisible=opts.slideCount-1;var visCount=opts.carouselVisible||opts.slides.length;var slideCSS={display:vert?'block':'inline-block',position:'relative'};opts.container.css({position:'relative',overflow:'hidden'});opts.slides.css(slideCSS);opts._currSlide=opts.currSlide;wrap=$('<div class="cycle-carousel-wrap"></div>').prependTo(opts.container).css({margin:0,padding:0,top:0,left:0,position:'absolute'}).append(opts.slides);opts._carouselWrap=wrap;if(!vert)
wrap.css('white-space','nowrap');if(opts.allowWrap!==false){for(j=0;j<(opts.carouselVisible===undefined?2:1);j++){for(i=0;i<opts.slideCount;i++){wrap.append(opts.slides[i].cloneNode(true));}
i=opts.slideCount;while(i--){wrap.prepend(opts.slides[i].cloneNode(true));}}
wrap.find('.cycle-slide-active').removeClass('cycle-slide-active');opts.slides.eq(opts.startingSlide).addClass('cycle-slide-active');}
if(opts.pager&&opts.allowWrap===false){pagerCutoffIndex=opts.slideCount-visCount;$(opts.pager).children().filter(':gt('+pagerCutoffIndex+')').hide();}
opts._nextBoundry=opts.slideCount-opts.carouselVisible;this.prepareDimensions(opts);},prepareDimensions:function(opts){var dim,offset,pagerCutoffIndex,tmp;var vert=opts.carouselVertical;var visCount=opts.carouselVisible||opts.slides.length;if(opts.carouselFluid&&opts.carouselVisible){if(!opts._carouselResizeThrottle){this.fluidSlides(opts);}}
else if(opts.carouselVisible&&opts.carouselSlideDimension){dim=visCount*opts.carouselSlideDimension;opts.container[vert?'height':'width'](dim);}
else if(opts.carouselVisible){dim=visCount*$(opts.slides[0])[vert?'outerHeight':'outerWidth'](true);opts.container[vert?'height':'width'](dim);}
offset=(opts.carouselOffset||0);if(opts.allowWrap!==false){if(opts.carouselSlideDimension){offset-=((opts.slideCount+opts.currSlide)*opts.carouselSlideDimension);}
else{tmp=opts._carouselWrap.children();for(var j=0;j<(opts.slideCount+opts.currSlide);j++){offset-=$(tmp[j])[vert?'outerHeight':'outerWidth'](true);}}}
opts._carouselWrap.css(vert?'top':'left',offset);},fluidSlides:function(opts){var timeout;var slide=opts.slides.eq(0);var adjustment=slide.outerWidth()-slide.width();var prepareDimensions=this.prepareDimensions;$(window).on('resize',resizeThrottle);opts._carouselResizeThrottle=resizeThrottle;onResize();function resizeThrottle(){clearTimeout(timeout);timeout=setTimeout(onResize,20);}
function onResize(){opts._carouselWrap.stop(false,true);var slideWidth=opts.container.width()/opts.carouselVisible;slideWidth=Math.ceil(slideWidth-adjustment);opts._carouselWrap.children().width(slideWidth);if(opts._sentinel)
opts._sentinel.width(slideWidth);prepareDimensions(opts);}},transition:function(opts,curr,next,fwd,callback){var moveBy,props={};var hops=opts.nextSlide-opts.currSlide;var vert=opts.carouselVertical;var speed=opts.speed;if(opts.allowWrap===false){fwd=hops>0;var currSlide=opts._currSlide;var maxCurr=opts.slideCount-opts.carouselVisible;if(hops>0&&opts.nextSlide>maxCurr&&currSlide==maxCurr){hops=0;}
else if(hops>0&&opts.nextSlide>maxCurr){hops=opts.nextSlide-currSlide-(opts.nextSlide-maxCurr);}
else if(hops<0&&opts.currSlide>maxCurr&&opts.nextSlide>maxCurr){hops=0;}
else if(hops<0&&opts.currSlide>maxCurr){hops+=opts.currSlide-maxCurr;}
else
currSlide=opts.currSlide;moveBy=this.getScroll(opts,vert,currSlide,hops);opts.API.opts()._currSlide=opts.nextSlide>maxCurr?maxCurr:opts.nextSlide;}
else{if(fwd&&opts.nextSlide===0){moveBy=this.getDim(opts,opts.currSlide,vert);callback=this.genCallback(opts,fwd,vert,callback);}
else if(!fwd&&opts.nextSlide==opts.slideCount-1){moveBy=this.getDim(opts,opts.currSlide,vert);callback=this.genCallback(opts,fwd,vert,callback);}
else{moveBy=this.getScroll(opts,vert,opts.currSlide,hops);}}
props[vert?'top':'left']=fwd?("-="+moveBy):("+="+moveBy);if(opts.throttleSpeed)
speed=(moveBy/$(opts.slides[0])[vert?'height':'width']())*opts.speed;opts._carouselWrap.animate(props,speed,opts.easing,callback);},getDim:function(opts,index,vert){var slide=$(opts.slides[index]);return slide[vert?'outerHeight':'outerWidth'](true);},getScroll:function(opts,vert,currSlide,hops){var i,moveBy=0;if(hops>0){for(i=currSlide;i<currSlide+hops;i++)
moveBy+=this.getDim(opts,i,vert);}
else{for(i=currSlide;i>currSlide+hops;i--)
moveBy+=this.getDim(opts,i,vert);}
return moveBy;},genCallback:function(opts,fwd,vert,callback){return function(){var pos=$(opts.slides[opts.nextSlide]).position();var offset=0-pos[vert?'top':'left']+(opts.carouselOffset||0);opts._carouselWrap.css(opts.carouselVertical?'top':'left',offset);callback();};},stopTransition:function(){var opts=this.opts();opts.slides.stop(false,true);opts._carouselWrap.stop(false,true);},onDestroy:function(e){var opts=this.opts();if(opts._carouselResizeThrottle)
$(window).off('resize',opts._carouselResizeThrottle);opts.slides.prependTo(opts.container);opts._carouselWrap.remove();}};})(jQuery);


/*===============================
http://iasitvlife.ro/modules/mod_highlighter_gk5/interface/scripts/engine.mootools.js
================================================================================*/;
window.addEvent("domready",function(){$$('.gkHighlighterGK5').each(function(el,i){var config=JSON.decode(el.get('data-config'));el.addClass('active');new GKNewsHighligher({wrapper:el,speed:config.animationSpeed,interval:config.animationInterval,fun:config.animationFun,type:config.animationType,mouseOver:config.mouseover});});});var GKNewsHighligher=new Class({options:{wrapper:null,speed:null,interval:null,fun:null,type:null,mouseOver:null},initialize:function(options){this.setOptions(options);var $this=this;this.prev=null;this.next=null;this.options.fun=this.options.fun.split(".");this.barCounter=0;this.item_anim=false;this.wrapper=this.options.wrapper.getElement('.gkHighlighterWrapper');var modInterface=this.options.wrapper.getElement('.gkHighlighterInterface');if(modInterface){if(modInterface.getElement('.next')){this.prev=this.options.wrapper.getElement('.prev');this.next=this.options.wrapper.getElement('.next');}
$this.wrapper.setStyle('margin-'+this.options.wrapper.getElement('.gkHighlighterInterface').getProperty('data-pos'),this.options.wrapper.getElement('.gkHighlighterInterface').getSize().x+25+"px");}
if($this.wrapper.getElement('.nowrap')){$this.wrapper.getElement('.nowrap').setStyle('position','static');}
switch(this.options.type){case'linear':this.linear();break;case'slides':this.slides();break;case'slidesBottom':this.slidesBottom();break;case'slidesLayerBottom':this.slidesLayerBottom();break;case'slidesLayer':this.slidesLayer();break;case'flipx':this.flipx();break;case'fadeLeft':this.fadeLeft();break;case'opacity':this.opacity();break;case'bar':if(this.isIE()){this.options.type='flipx';this.flipx();}else{this.bar();}break;}},isIE:function(){var myNav=navigator.userAgent.toLowerCase();return(myNav.indexOf('msie')!=-1)?parseInt(myNav.split('msie')[1]):false;},setRotation:function(rotation){this.options.wrapper.getElement('.gkHighlighterWrapperSub').setStyles({'-webkit-transform':rotation,'-moz-transform':rotation,'-o-transform':rotation,'transform':rotation});},linear:function(){var $this=this;var nowrap=$this.options.wrapper.getElement('.nowrap');$this.w=0;$this.options.wrapper.getElements('.nowrap > span').each(function(elmt,i){$this.w+=elmt.getSize().x;});$this.w+=80;$this.options.wrapper.getElement('.gkHighlighterWrapperSub').setStyle('overflow','hidden');var time=(($this.w+$this.options.wrapper.getSize().x)/$this.options.speed)*1000;var timeOriginal=time;var effect=new Fx.Tween(nowrap,{duration:time,transition:Fx.Transitions.linear,onComplete:function(){var w=$this.options.wrapper.getSize().x;effect.set('margin-left',w);effect.start('margin-left',w,-$this.w);}});if($this.options.mouseOver=='true'){nowrap.addEvent("mouseenter",function(){effect.pause();});nowrap.addEvent("mouseleave",function(){effect.resume();});}
effect.set('margin-left',$this.options.wrapper.getSize().x);effect.start('margin-left',$this.options.wrapper.getSize().x,-$this.w);},flipx:function(){var $this=this;$this.items=$this.options.wrapper.getElements('.gkHighlighterItem');$this.wrapper.setStyle('max-height','20px');$this.actual=0;$this.mouseIsOver=false;$this.animPlay=false;$this.timer=setInterval(function(){$this.timerFunc();},$this.options.interval);$this.items.each(function(elm,j){if(j!==0){elm.setStyle('opacity',0);}});if($this.options.mouseOver=='true'){$this.wrapper.addEvent("mouseenter",function(){$this.mouseIsOver=true;});$this.wrapper.addEvent("mouseleave",function(){$this.mouseIsOver=false;});}
if($this.next){$this.next.addEvent('click',function(e){new Event(e).stop();if(!$this.animPlay){$this.items[$this.actual].set('class','gkHighlighterItem').addClass('flipOutX').setStyle('opacity','0');$this.actual++;if($this.actual>$this.items.length-1){$this.actual=0;}
clearTimeout($this.timer);$this.items[$this.actual].set('class','gkHighlighterItem');setTimeout(function(){$this.items[$this.actual].addClass('flipInX').setStyle('opacity','1');},$this.options.speed);$this.timer=setInterval(function(){$this.timerFunc();},$this.options.interval);}});$this.prev.addEvent('click',function(e){new Event(e).stop();if(!$this.animPlay){$this.items[$this.actual].set('class','gkHighlighterItem').addClass('flipOutX').setStyle('opacity','0');($this.actual===0)?$this.actual=$this.items.length-1:$this.actual--;clearTimeout($this.timer);$this.items[$this.actual].set('class','gkHighlighterItem');setTimeout(function(){$this.items[$this.actual].addClass('flipInX').setStyle('opacity','1');},$this.options.speed);$this.timer=setInterval(function(){$this.timerFunc();},$this.options.interval);}});}},bar:function(){var $this=this;$this.items=$this.options.wrapper.getElements('.gkHighlighterItem');$this.wrapper.addClass('bar');$this.actual=0;$this.mouseIsOver=false;$this.animPlay=false;$this.timer=setInterval(function(){$this.timerFunc();},$this.options.interval);$this.items.each(function(elm,j){elm.setStyle('opacity',0);switch(j){case 0:elm.addClass('front');elm.setStyle('opacity',1);break;case 1:elm.addClass('bottom');break;case 2:elm.addClass('back');break;case 3:elm.addClass('top');break;default:elm.set('class','gkHighlighterItem gkHidden');break;}});if($this.options.mouseOver=='true'){$this.wrapper.addEvent("mouseenter",function(){$this.mouseIsOver=true;});$this.wrapper.addEvent("mouseleave",function(){$this.mouseIsOver=false;});}
if($this.next){$this.next.addEvent('click',function(e){new Event(e).stop();if(!$this.animPlay){$this.actual++;$this.barCounter++;$this.actual=($this.actual>$this.items.length-1)?0:$this.actual;var rotation='rotateX('+$this.barCounter*-90+'deg)';clearTimeout($this.timer);var active=$this.barCounter%$this.items.length;$this.items[active].setStyle('opacity',1);$this.setRotation(rotation);$this.timer=setInterval(function(){$this.timerFunc();},$this.options.interval);}});$this.prev.addEvent('click',function(e){new Event(e).stop();if(!$this.animPlay){$this.actual--;$this.barCounter--;if($this.actual>$this.items.length-1){$this.actual=0;}
var active=$this.barCounter%$this.items.length;$this.items[active].setStyle('opacity',1);var rotation='rotateX('+$this.barCounter*-90+'deg)';clearTimeout($this.timer);$this.setRotation(rotation);$this.timer=setInterval(function(){$this.timerFunc();},$this.options.interval);}});}},fadeLeft:function(){var $this=this;$this.items=$this.options.wrapper.getElements('.gkHighlighterItem');$this.wrapper.setStyle('max-height','20px');$this.actual=0;$this.mouseIsOver=false;$this.animPlay=false;$this.timer=setInterval(function(){$this.timerFunc();},$this.options.interval);$this.items.each(function(elm,j){elm.setStyle('z-index',$this.items.length-j);if(j!==0){elm.addClass('invisible');}});if($this.options.mouseOver=='true'){$this.wrapper.addEvent("mouseenter",function(){$this.mouseIsOver=true;});$this.wrapper.addEvent("mouseleave",function(){$this.mouseIsOver=false;});}
if($this.next){$this.next.addEvent('click',function(e){new Event(e).stop();if(!$this.animPlay){$this.items[$this.actual].set('class','gkHighlighterItem').addClass('fadeOutLeft');$this.actual++;$this.actual=($this.actual>$this.items.length-1)?0:$this.actual;clearTimeout($this.timer);$this.items[$this.actual].setStyle('opacity',0);$this.items[$this.actual].set('class','gkHighlighterItem');setTimeout(function(){$this.items[$this.actual].addClass('fadeInLeft');},$this.options.speed);$this.timer=setInterval(function(){$this.timerFunc();},$this.options.interval);}});$this.prev.addEvent('click',function(e){new Event(e).stop();if(!$this.animPlay){$this.items[$this.actual].set('class','gkHighlighterItem').addClass('fadeOutLeft');($this.actual===0)?$this.actual=$this.items.length-1:$this.actual--;clearTimeout($this.timer);$this.items[$this.actual].set('class','gkHighlighterItem');setTimeout(function(){$this.items[$this.actual].addClass('fadeInLeft');},$this.options.speed);$this.timer=setInterval(function(){$this.timerFunc();},$this.options.interval);}});}},opacity:function(){var $this=this;$this.items=$this.options.wrapper.getElements('.gkHighlighterItem');$this.wrapper.setStyle('max-height','20px');$this.actual=0;$this.mouseIsOver=false;$this.animPlay=false;$this.timer=setInterval(function(){$this.timerFunc();},$this.options.interval);$this.items.each(function(elm,j){elm.setStyle('z-index',$this.items.length-j);if(j!==0){elm.setStyle('opacity',0);}});if($this.options.mouseOver=='true'){$this.wrapper.addEvent("mouseenter",function(){$this.mouseIsOver=true;});$this.wrapper.addEvent("mouseleave",function(){$this.mouseIsOver=false;});}
if($this.next){$this.next.addEvent('click',function(e){new Event(e).stop();if(!$this.animPlay){$this.items[$this.actual].set('class','gkHighlighterItem').addClass('fadeOut');$this.actual++;$this.actual=($this.actual>$this.items.length-1)?0:$this.actual;clearTimeout($this.timer);$this.items[$this.actual].set('class','gkHighlighterItem');$this.items[$this.actual].setStyle('opacity',1);$this.timer=setInterval(function(){$this.timerFunc();},$this.options.interval);}});$this.prev.addEvent('click',function(e){new Event(e).stop();if(!$this.animPlay){$this.items[$this.actual].set('class','gkHighlighterItem').addClass('fadeOut');($this.actual===0)?$this.actual=$this.items.length-1:$this.actual--;clearTimeout($this.timer);$this.items[$this.actual].set('class','gkHighlighterItem');$this.items[$this.actual].setStyle('opacity',1);$this.timer=setInterval(function(){$this.timerFunc();},$this.options.interval);}});}},slides:function(){var $this=this;$this.items=$this.options.wrapper.getElements('.gkHighlighterItem');$this.items.setStyle('display','block');$this.effects1=[];$this.effects2=[];$this.actual=0;$this.mouseIsOver=false;$this.animPlay=false;$this.timer=setInterval(function(){$this.timerFunc();},$this.options.interval);$this.items.each(function(elm,j){elm.setStyle('z-index',$this.items.length-j);$this.effects1[j]=new Fx.Tween(elm,{duration:$this.options.speed,transition:Fx.Transitions[$this.options.fun[2]][$this.options.fun[3]],wait:true,onStart:function(){$this.animPlay=true;},onComplete:function(){$this.animPlay=false;}});$this.effects2[j]=new Fx.Tween(elm,{duration:$this.options.speed,transition:Fx.Transitions[$this.options.fun[2]][$this.options.fun[3]],wait:true});if(j!==0)$this.effects1[j].set('opacity',0);if($this.options.type!=='linear')$this.effects2[j].set('top',0);});if($this.options.mouseOver=='true'){$this.wrapper.addEvent("mouseenter",function(){$this.mouseIsOver=true;});$this.wrapper.addEvent("mouseleave",function(){$this.mouseIsOver=false;});}
if($this.next){$this.next.addEvent('click',function(e){new Event(e).stop();if(!$this.animPlay){$this.effects1[$this.actual].start('opacity',0);$this.effects2[$this.actual].start('top',0,-24);$this.actual++;$this.actual=($this.actual>$this.items.length-1)?0:$this.actual;$this.effects1[$this.actual].start('opacity',1);$this.effects2[$this.actual].start('top',24,0);clearTimeout($this.timer);$this.timer=setInterval(function(){$this.timerFunc();},$this.options.interval);}});$this.prev.addEvent('click',function(e){new Event(e).stop();if(!$this.animPlay){$this.effects1[$this.actual].start('opacity',0);$this.effects2[$this.actual].start('top',0,24);($this.actual===0)?$this.actual=$this.items.length-1:$this.actual--;$this.effects1[$this.actual].start('opacity',1);$this.effects2[$this.actual].start('top',-24,0);clearTimeout($this.timer);$this.timer=setInterval(function(){$this.timerFunc();},$this.options.interval);}});}},slidesBottom:function(){var $this=this;$this.items=$this.options.wrapper.getElements('.gkHighlighterItem');$this.items.setStyle('display','block');$this.effects1=[];$this.effects2=[];$this.actual=0;$this.mouseIsOver=false;$this.animPlay=false;$this.timer=setInterval(function(){$this.timerFunc();},$this.options.interval);$this.items.each(function(elm,j){$this.effects1[j]=new Fx.Tween(elm,{duration:$this.options.speed,transition:Fx.Transitions[$this.options.fun[2]][$this.options.fun[3]],wait:true,onStart:function(){$this.animPlay=true;},onComplete:function(){$this.animPlay=false;}});$this.effects2[j]=new Fx.Tween(elm,{duration:$this.options.speed,transition:Fx.Transitions[$this.options.fun[2]][$this.options.fun[3]],wait:true});if(j!==0)$this.effects1[j].set('opacity',0);if($this.options.type!=='linear')$this.effects2[j].set('top',0);});if($this.options.mouseOver=='true'){$this.wrapper.addEvent("mouseenter",function(){$this.mouseIsOver=true;});$this.wrapper.addEvent("mouseleave",function(){$this.mouseIsOver=false;});}
if($this.next){$this.next.addEvent('click',function(e){new Event(e).stop();if(!$this.animPlay){$this.effects1[$this.actual].start('opacity',0);$this.effects2[$this.actual].start('top',0,24);$this.actual++;$this.actual=($this.actual>$this.items.length-1)?0:$this.actual;$this.effects1[$this.actual].start('opacity',1);$this.effects2[$this.actual].start('top',-24,0);clearTimeout($this.timer);$this.timer=setInterval(function(){$this.timerFunc();},$this.options.interval);}});$this.prev.addEvent('click',function(e){new Event(e).stop();if(!$this.animPlay){$this.effects1[$this.actual].start('opacity',0);$this.effects2[$this.actual].start('top',0,-24);($this.actual===0)?$this.actual=$this.items.length-1:$this.actual--;$this.effects1[$this.actual].start('opacity',1);$this.effects2[$this.actual].start('top',24,0);clearTimeout($this.timer);$this.timer=setInterval(function(){$this.timerFunc();},$this.options.interval);}});}},slidesLayer:function(){var $this=this;$this.items=$this.options.wrapper.getElements('.gkHighlighterItem');$this.wrapper.setStyle('max-height','20px');$this.effects1=[];$this.effects2=[];$this.actual=0;$this.mouseIsOver=false;$this.animPlay=false;$this.timer=setInterval(function(){$this.timerFunc();},$this.options.interval);$this.items.each(function(elm,j){elm.setStyle('z-index',$this.items.length-j);$this.effects1[j]=new Fx.Tween(elm,{duration:$this.options.speed,transition:Fx.Transitions[$this.options.fun[2]][$this.options.fun[3]],wait:true,onStart:function(){$this.animPlay=true;},onComplete:function(){$this.animPlay=false;}});$this.effects2[j]=new Fx.Tween(elm,{onComplete:function(){elm.setStyle('top','0');elm.setStyle('z-index',1);},duration:$this.options.speed,transition:Fx.Transitions[$this.options.fun[2]][$this.options.fun[3]],wait:true});if(j!==0)$this.effects1[j].set('opacity',0);});if($this.options.mouseOver=='true'){$this.wrapper.addEvent("mouseenter",function(){$this.mouseIsOver=true;});$this.wrapper.addEvent("mouseleave",function(){$this.mouseIsOver=false;});}
if($this.next){$this.next.addEvent('click',function(e){new Event(e).stop();if(!$this.animPlay){$this.effects1[$this.actual].start('opacity',0);$this.effects2[$this.actual].start('top',0,-$this.items[$this.actual].getSize().y);var counter=0;for(var i=$this.actual;i<$this.items.length;i++){$this.items[i].setStyle('z-index',$this.items.length-counter);counter++;}
for(i=0;i<$this.actual;i++){$this.items[i].setStyle('z-index',$this.items[$this.items.length-1].getStyle('z-index')-1-i);}
$this.actual++;$this.actual=($this.actual>$this.items.length-1)?0:$this.actual;$this.effects1[$this.actual].start('opacity',1);clearTimeout($this.timer);$this.timer=setInterval(function(){$this.timerFunc();},$this.options.interval);}});$this.prev.addEvent('click',function(e){new Event(e).stop();if(!$this.animPlay){$this.effects1[$this.actual].start('opacity',0);$this.effects2[$this.actual].start('top',0,-$this.items[$this.actual].getSize().y);var counter=0;for(var i=$this.actual;i<$this.items.length;i++){$this.items[i].setStyle('z-index',$this.items.length-counter);counter++;}
for(i=0;i<$this.actual;i++){$this.items[i].setStyle('z-index',$this.items[$this.items.length-1].getStyle('z-index')-1-i);}
($this.actual===0)?$this.actual=$this.items.length-1:$this.actual--;$this.effects1[$this.actual].start('opacity',1);clearTimeout($this.timer);$this.timer=setInterval(function(){$this.timerFunc();},$this.options.interval);}});}},slidesLayerBottom:function(){var $this=this;$this.items=$this.options.wrapper.getElements('.gkHighlighterItem');$this.wrapper.setStyle('max-height','20px');$this.effects1=[];$this.effects2=[];$this.actual=0;$this.mouseIsOver=false;$this.animPlay=false;$this.timer=setInterval(function(){$this.timerFunc();},$this.options.interval);$this.items.each(function(elm,j){elm.setStyle('z-index',$this.items.length-j);if(j!==0){elm.setStyle('opacity',0);}
$this.effects1[j]=new Fx.Tween(elm,{duration:$this.options.speed,transition:Fx.Transitions[$this.options.fun[2]][$this.options.fun[3]],wait:true,onStart:function(){$this.animPlay=true;},onComplete:function(){$this.animPlay=false;}});$this.effects2[j]=new Fx.Tween(elm,{onComplete:function(){elm.setStyle('top','0');elm.setStyle('z-index',1);},duration:$this.options.speed,transition:Fx.Transitions[$this.options.fun[2]][$this.options.fun[3]],wait:true});});if($this.options.mouseOver=='true'){$this.wrapper.addEvent("mouseenter",function(){$this.mouseIsOver=true;});$this.wrapper.addEvent("mouseleave",function(){$this.mouseIsOver=false;});}
if($this.next){$this.next.addEvent('click',function(e){new Event(e).stop();if(!$this.animPlay){$this.effects1[$this.actual].start('opacity',0);$this.effects2[$this.actual].start('top',0,$this.items[$this.actual].getSize().y);var counter=0;for(var i=$this.actual;i<$this.items.length;i++){$this.items[i].setStyle('z-index',$this.items.length-counter);counter++;}
for(i=0;i<$this.actual;i++){$this.items[i].setStyle('z-index',$this.items[$this.items.length-1].getStyle('z-index')-1-i);}
$this.actual++;$this.actual=($this.actual>$this.items.length-1)?0:$this.actual;$this.effects1[$this.actual].start('opacity',1);clearTimeout($this.timer);$this.timer=setInterval(function(){$this.timerFunc();},$this.options.interval);}});$this.prev.addEvent('click',function(e){new Event(e).stop();if(!$this.animPlay){$this.effects1[$this.actual].start('opacity',0);$this.effects2[$this.actual].start('top',0,$this.items[$this.actual].getSize().y);var counter=0;for(var i=$this.actual;i<$this.items.length;i++){$this.items[i].setStyle('z-index',$this.items.length-counter);counter++;}
for(i=0;i<$this.actual;i++){$this.items[i].setStyle('z-index',$this.items[$this.items.length-1].getStyle('z-index')-1-i);}
($this.actual===0)?$this.actual=$this.items.length-1:$this.actual--;$this.effects1[$this.actual].start('opacity',1);clearTimeout($this.timer);$this.timer=setInterval(function(){$this.timerFunc();},$this.options.interval);clearTimeout($this.timer);$this.timer=setInterval(function(){$this.timerFunc();},$this.options.interval);}});}},timerFunc:function(){var $this=this;var height=this.items[$this.actual].getSize().y;if($this.mouseIsOver===false){if(this.options.type==='slides'){$this.effects1[$this.actual].start('opacity',0);$this.effects2[$this.actual].start('top',0,-height);$this.actual++;$this.actual=($this.actual>$this.items.length-1)?0:$this.actual;$this.effects1[$this.actual].start('opacity',1);$this.effects2[$this.actual].start('top',height,0);}else if(this.options.type==='flipx'){$this.items[$this.actual].set('class','gkHighlighterItem').addClass('flipOutX');$this.actual++;$this.actual=($this.actual>$this.items.length-1)?0:$this.actual;$this.items[$this.actual].set('class','gkHighlighterItem');setTimeout(function(){$this.items[$this.actual].addClass('flipInX');},$this.options.speed);}else if(this.options.type==='fadeLeft'){$this.items[$this.actual].set('class','gkHighlighterItem').addClass('fadeOutLeft');$this.actual++;$this.actual=($this.actual>$this.items.length-1)?0:$this.actual;$this.items[$this.actual].setStyle('opacity',0);$this.items[$this.actual].set('class','gkHighlighterItem');setTimeout(function(){$this.items[$this.actual].addClass('fadeInLeft');},$this.options.speed);}else if(this.options.type==='slidesBottom'){$this.effects1[$this.actual].start('opacity',0);$this.effects2[$this.actual].start('top',0,height);$this.actual++;$this.actual=($this.actual>$this.items.length-1)?0:$this.actual;$this.effects1[$this.actual].start('opacity',1);$this.effects2[$this.actual].start('top',-height,0);}else if(this.options.type==='slidesLayer'){$this.effects1[$this.actual].start('opacity',0);$this.effects2[$this.actual].start('top',0,-height);var counter=0;for(var i=$this.actual;i<$this.items.length;i++){$this.items[i].setStyle('z-index',$this.items.length-counter);counter++;}
for(i=0;i<$this.actual;i++){$this.items[i].setStyle('z-index',$this.items[$this.items.length-1].getStyle('z-index')-1-i);}
$this.actual++;$this.actual=($this.actual>$this.items.length-1)?0:$this.actual;$this.effects1[$this.actual].start('opacity',1);}else if(this.options.type=='slidesLayerBottom'){$this.effects1[$this.actual].start('opacity',0);$this.effects2[$this.actual].start('top',0,height);counter=0;for(var i=$this.actual;i<$this.items.length;i++){$this.items[i].setStyle('z-index',$this.items.length-counter);counter++;}
for(i=0;i<$this.actual;i++){$this.items[i].setStyle('z-index',$this.items[$this.items.length-1].getStyle('z-index')-1-i);}
$this.actual++;$this.actual=($this.actual>$this.items.length-1)?0:$this.actual;$this.effects1[$this.actual].start('opacity',1);}else if(this.options.type==='opacity'){$this.items[$this.actual].set('class','gkHighlighterItem').addClass('fadeOut');$this.actual++;$this.actual=($this.actual>$this.items.length-1)?0:$this.actual;$this.items[$this.actual].setStyle('opacity',1);$this.items[$this.actual].set('class','gkHighlighterItem');}else if(this.options.type==='bar'){$this.actual++;$this.barCounter++;$this.actual=($this.actual>$this.items.length-1)?0:$this.actual;if(!$this.items[$this.actual].hasClass('front')&&!$this.items[$this.actual].hasClass('bottom')&&!$this.items[$this.actual].hasClass('back')&&!$this.items[$this.actual].hasClass('top')){$this.items.each(function(el,i){el.set('class','gkHighlighterItem gkHidden');});var face=$this.actual;var start=$this.actual;$this.items[face].setStyle('opacity',1);$this.items[face].set('class','gkHighlighterItem');$this.items[face].addClass('front');for(var i=0;i<3;i++){face++;if(face>$this.items.length-1)face=0;$this.items[face].set('class','gkHighlighterItem');if(i===0){$this.items[face].addClass('bottom');}else if(i===1){$this.items[face].addClass('back');}else{$this.items[face].addClass('top');}}}
$this.items.each(function(el,i){if(!el.hasClass('front')&&!el.hasClass('bottom')&&!el.hasClass('back')&&!el.hasClass('top')){el.set('class','gkHighlighterItem');el.addClass('gkHidden');}});var active=$this.barCounter%$this.items.length;$this.items[active].setStyle('opacity',1);var rotation='rotateX('+$this.barCounter*-90+'deg)';$this.setRotation(rotation);}}}});GKNewsHighligher.implement(new Options);