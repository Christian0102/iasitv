<?php
/**
 * @package         JFBConnect
 * @copyright (c)   2009-2017 by SourceCoast - All Rights Reserved
 * @license         http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @version         Release v7.2.0
 * @build-date      2017/03/29
 */

if (!(defined('_JEXEC') || defined('ABSPATH'))) {     die('Restricted access'); };

class TableJFBConnectOpenGraphObject extends JTable
{
	public $id = null;
    public $plugin = null;
    public $system_name = null;
    public $display_name = null;
    public $type = null;
    public $published = 0;
    public $fb_built_in = false;
    public $params = "";
    public $created = null;
    public $modified = null;

	function __construct(&$db)
	{
		parent::__construct('#__opengraph_object', 'id', $db);
	}
}