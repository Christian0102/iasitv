<?php   

/*------------------------------------------------------------------------
# plg_ContentAds - K2 Content Ads Plugin
# ------------------------------------------------------------------------
# author    Erik Maier
# copyright Copyright (C) 2012 rkmaier.com
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.rkmaier.com
# Technical Support:rkmaier@rkmaier.com
-------------------------------------------------------------------------*/
defined( '_JEXEC' ) or die ( 'Restricted access' );

function checkcat($itemcat,$cats)
     {
     	if(in_array($itemcat,$cats)==TRUE)
     	{
			return 1;
		}
		else
		{
		return 0;	
		}
     }
     
function checkids($string,$itemid)
{
  $ids=explode(",",$string);
  if(in_array($itemid,$ids)==TRUE)
  {
  	return 1;
  }
  else
  {
  	return 0;
  }
}

