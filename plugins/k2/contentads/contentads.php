<?php
/**
 * @version  1.2.0
 * @package  K2 Plugin (K2 plugin)
 * @author   Erik Maier - www.rkmaier.com
 * @copyright Copyright (c) 2012
 * @license  GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

/*------------------------------------------------------------------------
# plg_ContentAds - K2 Content Ads Plugin
# ------------------------------------------------------------------------
# author    Erik Maier
# copyright Copyright (C) 2012 rkmaier.com
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.rkmaier.com
# Technical Support:rkmaier@rkmaier.com
-------------------------------------------------------------------------*/

require_once 'functions.php';
// no direct access
defined( '_JEXEC' ) or die ( 'Restricted access' );

// Load the K2 Plugin API
JLoader::register('K2Plugin', JPATH_ADMINISTRATOR.'/components/com_k2/lib/k2plugin.php');

// Initiate class to hold plugin events
class plgK2ContentAds extends K2Plugin {

	// Some params
	var $pluginName = 'contentads';
	var  $pluginNameHumanReadable = 'K2 Content Ads Plugin';
	var  $position;
	var  $view;
	var  $category = 0;
	var  $mode;
	var $code;
	var  $ids;
	var  $catfilter;
	var  $display;
	var  $cat;

	function plgK2ContentAds( &$subject, $params ) {
		
        parent::__construct($subject, $params );

}

function setParams(&$item, &$params)
{
		$view = JRequest::getVar( 'view' );
		$plugins= new K2Parameter($item->plugins, '', $this->pluginName);  //GET THE PLUGIN 
		//var_dump($this->params);


#GETTING PLUGIN PARAMETER VALUES 
		$this->view = $this->params->get('viewfilter');
		$this->position=$this->params->get('position');
		$this->category=$this->params->get('category_id' );
		$this->catfilter=$this->params->get('catfilter' );
		$this->ids = $this->params->get('itemids');
		$this->mode=$this->params->get( 'mode' );


        
//var_dump($this);
#SET THE CORRECT AD CODE		
			
	switch ($this->params->get('randcode')) {

    case 1:
        $this->code=$this->params->get( 'adcode' );
        break;
    case 2:
		$this->code=$this->params->get( 'adcode2' );
        break;
    case 0:
		if(rand(1,2)==1)
			{
				$this->code=$this->params->get( 'adcode' );
		    }
		    else
		    {
				$this->code=$this->params->get( 'adcode2' );
		    }
        break;
		}		
		
			
		
#SET THE CORRECT VIEWS FOR THE AD CODE

	$this->display=0;
	
	switch ($this->view) {
    
    case 0:
        $this->display=1;
        break;
    case 1:
        if ($view=="item")
        {
        $this->display=1;
        }
        break;
    case 2:
         if ($view=="itemlist" )
        {
        $this->display=1;
        }
        break;
		}
	
    $this->code="<div id='k2ad' style='padding:5px;text-align:center'>".$this->code."</div>";	
     return $this->code;

}

	

	public function onK2AfterDisplay( &$item, &$params, $limitstart ) {

		$mycode = (plgK2ContentAds::setParams($item, $params));

		if($this->catfilter==0)
		{
			$category=1;
		}
		else
		{
		$category=checkcat( $item->catid, $this->category );
	    }

		if ( $this->mode==0 &&  $this->display==1 && $this->position==0 && $category==1) {
						return $mycode;
							}
		else {
			$check=checkids( $this->ids, $item->id );
			if ( $check==1 && $this->display==1 && $this->position==0 ) {
				return $mycode;
			}

		}
		
	}



	function onK2AfterDisplayContent( &$item, &$params, $limitstart ) {

	 $mycode = (plgK2ContentAds::setParams($item, $params));

	 if($this->catfilter==0)
		{
			$category=1;
		}
		else
		{
		$category=checkcat( $item->catid, $this->category );
	    }

		if ( $this->mode==0 &&  $this->display==1 && $this->position==1 && $category==1) {
						return $mycode;
							}
		else {
			$check=checkids( $this->ids, $item->id );
			if ( $check==1 && $this->display==1 && $this->position==1 ) {
				return $mycode;
			}

		}

	}


	function onK2AfterDisplayTitle( &$item, &$params, $limitstart ) {


      $mycode = (plgK2ContentAds::setParams($item, $params));


  		if($this->catfilter==0)
		{
			$category=1;
		}
		else
		{
		$category=checkcat( $item->catid, $this->category );

	    }

		if ( $this->mode==0 &&  $this->display==1 && $this->position==2 && $category==1) {
						return $mycode;
							}
		else {
			$check=checkids( $this->ids, $item->id );
			if ( $check==1 && $this->display==1 && $this->position==2 ) {
				return $mycode;
			}

		}
	}


	function onK2BeforeDisplayContent( &$item, &$params, $limitstart ) {

		$mycode = (plgK2ContentAds::setParams($item, $params));

		if($this->catfilter==0)
		{
			$category=1;
		}
		else
		{
		$category=checkcat( $item->catid, $this->category );
	    }

		if ( $this->mode==0 &&  $this->display==1 && $this->position==3 && $category==1) {
						return $mycode;
							}
		else {
			$check=checkids( $this->ids, $item->id );
			if ( $check==1 && $this->display==1 && $this->position==3 ) {
				return $mycode;
			}

		}
	}

	function onK2BeforeDisplay( &$item, &$params, $limitstart ) {

		$mycode = (plgK2ContentAds::setParams($item, $params));
		
		if($this->catfilter==0)
		{
			$category=1;
		}
		else
		{
		$category=checkcat( $item->catid, $this->category );
	    }

		if ( $this->mode==0 &&  $this->display==1 && $this->position==4 && $category==1) {
						return $mycode;
							}
		else {
			$check=checkids( $this->ids, $item->id );
			if ( $check==1 && $this->display==1 && $this->position==4 ) {
				return $mycode;
			}

		}
	}
}
