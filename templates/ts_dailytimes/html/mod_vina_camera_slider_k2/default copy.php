<?php
/*
# ------------------------------------------------------------------------
# Vina Camera Slider for K2 for Joomla 3
# ------------------------------------------------------------------------
# Copyright(C) 2014 www.VinaGecko.com. All Rights Reserved.
# @license http://www.gnu.org/licenseses/gpl-3.0.html GNU/GPL
# Author: VinaGecko.com
# Websites: http://vinagecko.com
# Forum:    http://vinagecko.com/forum/
# ------------------------------------------------------------------------
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

$doc = JFactory::getDocument();
$doc->addScript('modules/mod_vina_camera_slider_k2/assets/jquery.easing.1.3.js', 'text/javascript');
$doc->addScript('modules/mod_vina_camera_slider_k2/assets/camera.js', 'text/javascript');
$doc->addStyleSheet('modules/mod_vina_camera_slider_k2/assets/camera.css');

$timthumb = JURI::base() . 'modules/mod_vina_camera_slider_k2/libs/timthumb.php?a=c&amp;q=99&amp;z=0';
?>
<style type="text/css">
#vina-camera-slider-k2-wrapper<?php echo $module->id; ?> {
	width: <?php echo $moduleWidth; ?>;
	max-width: <?php echo $maxWidth; ?>;
	clear: both;
}
#vina-copyright<?php echo $module->id; ?> {
	font-size: 12px;
	<?php if(!$params->get('copyRightText', 0)) : ?>
	height: 1px;
	overflow: hidden;
	<?php endif; ?>
	clear: both;
}
</style>
<div id="vina-camera-slider-k2-wrapper<?php echo $module->id; ?>" class="vina-camera-slider-k2">
	<div class="camera_wrap <?php echo $moduleStyle; ?>" id="vina-camera-slider-k2<?php echo $module->id; ?>">
		<?php 
			foreach($items as $key => $item) : 
				$image = $item->$itemImgSize;
				$title = $item->title;
				$link  = $item->link;
				$cname = $item->categoryname;
				$clink = $item->categoryLink;
				$hits  = $item->hits;
				$introtext = $item->introtext;
				$created   = $item->created;
				$bigImage  = $image;
			
				$bigImage   = ($resizeImage) ? $timthumb . '&w=' . $imageWidth . '&h=' . $imageHeight . '&src=' . $image : $image;
				$thumbImage = ($resizeImage) ? $timthumb . '&w=' . $thumbnailWidth . '&h=' . $thumbnailHeight . '&src=' . $image : $image;
		?>
		<div data-thumb="<?php echo $thumbImage; ?>" data-src="<?php echo $bigImage; ?>">
			<?php if($displayCaptions) : ?>
			<div class="camera_caption <?php echo $captionEffect; ?>" style="<?php echo $captionPosition; ?>">

				<?php if($itemCategory) : ?>

				<?php if($itemCategory) : ?>
					<span class="camera_cat"> <a href="<?php echo $clink; ?>"><?php echo $cname; ?></a></span>
				<?php endif; ?>

				<?php endif; ?>
				<!-- Title <B></B>lock -->

				<?php if($linkTitle && $itemTitle) : ?>
				<h3><a href="<?php echo $link; ?>" title="<?php echo $title; ?>"><?php echo $title; ?></a></h3>
				<?php else : ?>
				<h3><?php echo $title; ?></h3>
				<?php endif; ?>
				
				<!-- Info Block -->
				<?php if($itemAuthor || $itemDateCreated || $itemHits) : ?>
				<div class="info">
					<?php if($itemAuthor) : ?>
					<span> <a rel="author" title="<?php echo K2HelperUtilities::cleanHtml($item->author); ?>" href="<?php echo $item->authorLink; ?>"><?php echo $item->author; ?></a>.</span>
					<?php endif; ?>
					
					<?php if($itemDateCreated) : ?>
					<span> <?php echo JHTML::_('date', $created, 'F d, Y');?></span>
					<?php endif; ?>
				
					
					<?php if($itemHits) : ?>
					<span class="camera_hits"><i class="fa fa-heart-o"> </i> <?php echo $hits; ?></span>
					<?php endif; ?>
				</div>
				<?php endif; ?>
				
				<!-- Intro text Block -->
				<?php if($itemIntroText) : ?>
				<div class="introtext"><?php echo $introtext; ?></div>
				<?php endif; ?>
				
				<!-- Readmore Block -->
				<?php if($itemReadMore) : ?>
				<div class="readmore">
					<a class="morebutton" href="<?php echo $link; ?>" title="<?php echo $title; ?>">
						<?php echo JText::_('VINA_READ_MORE'); ?>
					</a>
				</div>
				<?php endif; ?>
			</div>
			<?php endif; ?>
		</div>
		<?php endforeach; ?>
	</div>
</div>
<script>
jQuery(document).ready(function ($) {
	jQuery('#vina-camera-slider-k2<?php echo $module->id; ?>').camera({
		loader				: '<?php echo $loaderStyle; ?>',
		barDirection		: '<?php echo $barDirection; ?>',
		barPosition			: '<?php echo $barPosition; ?>',
		fx					: '<?php echo $fx; ?>',
		piePosition			: '<?php echo $piePosition; ?>',
		height				: '<?php echo $moduleHeight; ?>',
		hover				: <?php echo ($pauseHover) ? 'true' : 'false'; ?>,			
		navigation			: <?php echo ($navigation) ? 'true' : 'false'; ?>,
		navigationHover		: <?php echo ($navigationHover) ? 'true' : 'false'; ?>,
		pagination			: <?php echo ($pagination) ? 'true' : 'false'; ?>,
		playPause			: <?php echo ($playPause) ? 'true' : 'false'; ?>,
		pauseOnClick		: <?php echo ($pauseOnClick) ? 'true' : 'false'; ?>,
		thumbnails			: <?php echo ($thumbnails) ? 'true' : 'false'; ?>,
		time				: <?php echo $duration; ?>,
		transPeriod			: <?php echo $transPeriod; ?>,
	});
});
</script>