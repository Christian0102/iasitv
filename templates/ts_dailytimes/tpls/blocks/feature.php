<?php
/**
 * @author Tripples http://www.themewinter.com
 * @copyright Copyright (c) 2013 - 2015
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or later
*/

defined('_JEXEC') or die;
?>

<!-- Feature -->

<?php if ($this->checkSpotlight('feature', 'feature')) : ?>
	<div class="ts-feature">
	<div class="container">
			<?php $this->spotlight('feature', 'feature') ?>
		</div>
	</div>
<?php endif ?>

<!-- Feature end -->