<?php
/**
 * @author Tripples http://www.themewinter.com
 * @copyright Copyright (c) 2013 - 2015
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or later
*/

defined('_JEXEC') or die;
?>

<!-- Breaking -->

<?php if ($this->checkSpotlight('hotnews', 'hotnews, topsocial')) : ?>
	<div class="container">
		<div class="ts-hotnews">
			<?php $this->spotlight('hotnews', 'hotnews, topsocial') ?>
		</div>
	</div>
<?php endif ?>

<!-- Breaking end -->