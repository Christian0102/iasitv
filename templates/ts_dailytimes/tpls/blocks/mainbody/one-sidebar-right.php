<?php
/**
 * @author Themewing http://www.themewing.com
 * @copyright Copyright (c) 2013 - 2014 royalSoft
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or later
*/

defined('_JEXEC') or die;

/**
 * Mainbody 2 columns: content - sidebar
 */
?>
<div id="t3-mainbody" class="container t3-mainbody one-sidebar-right">
	<div class="row">

		<!-- MAIN CONTENT -->
		<div id="t3-content" class="t3-content col-xs-12 col-sm-12  col-md-8">

			<!-- Content Top-->
			<?php if ($this->countModules('content-top')) : ?>
			<div class="ts-content-top <?php $this->_c('content-top') ?>">
				<jdoc:include type="modules" name="<?php $this->_p('content-top') ?>" style="T3Xhtml" />
			</div>
			<?php endif; ?>
			<!-- //Content top end-->


			<!-- Content mid -->
			<?php if ($this->checkSpotlight('contentmid', 'contentmid-1, contentmid-2')) : ?>
				<div class="ts-content-mid">
					<?php $this->spotlight('contentmid', 'contentmid-1, contentmid-2') ?>
				</div>
			<?php endif ?>
			<!-- Content mid end -->


			<!-- Content Bottom-->
			<?php if ($this->countModules('content-bottom')) : ?>
			<div class="ts-content-bottom <?php $this->_c('content-bottom') ?>">
				<jdoc:include type="modules" name="<?php $this->_p('content-bottom') ?>" style="T3Xhtml" />
			</div>
			<?php endif; ?>
			<!-- //Content top end-->


			<!-- //CONTENT MAST TOP -->
			<?php if($this->hasMessage()) : ?>
			<jdoc:include type="message" />
			<?php endif ?>
			<jdoc:include type="component" />
		</div>
		<!-- //MAIN CONTENT -->

		<!-- SIDEBAR RIGHT -->
		<div class="t3-sidebar t3-sidebar-right col-xs-12 col-sm-12  col-md-4 <?php $this->_c($vars['sidebar']) ?>">
			<jdoc:include type="modules" name="<?php $this->_p($vars['sidebar']) ?>" style="T3Xhtml" />
		</div>
		<!-- //SIDEBAR RIGHT -->

	</div>
</div> 
