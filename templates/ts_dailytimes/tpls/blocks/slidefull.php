<?php
/**
 * @author Tripples http://www.themewinter.com
 * @copyright Copyright (c) 2013 - 2015
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or later
*/

defined('_JEXEC') or die;
?>

<!-- Showcase -->

<?php if ($this->checkSpotlight('slidefull', 'slidefull')) : ?>
	<!-- SPOTLIGHT 1 -->
	<div class="container t3-sl t3-sl-1">
		<div class="ts-slidefull">
			<?php $this->spotlight('slidefull', 'slidefull') ?>
		</div>
	</div>
	<!-- //SPOTLIGHT 1 -->
<?php endif ?>

<!-- Showcase end -->