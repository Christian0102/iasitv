<?php
/**
 * @author Tripples http://www.themewinter.com
 * @copyright Copyright (c) 2013 - 2015
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or later
*/

defined('_JEXEC') or die;
?>

<!-- Showcase -->
<?php if ($this->checkSpotlight('showcase', 'showcase1, showcase2')) : ?>
	<div class="container">
		<div class="ts-showcase">
			<?php $this->spotlight('showcase', 'showcase1, showcase2') ?>
		</div>
	</div>
<?php endif ?>

<!-- Showcase end -->