<?php
/**
 * @author Tripples http://www.themewinter.com
 * @copyright Copyright (c) 2013 - 2015
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or later
*/

defined('_JEXEC') or die;
?>

<!-- Topbar -->

<?php if ($this->checkSpotlight('topbar', 'top1, top2')) : ?>
	<div class="ts-topbar">
		<div class="container">
			<?php $this->spotlight('topbar', 'top1, top2') ?>
		</div>
	</div>
<?php endif ?>
<!-- Topbar end -->