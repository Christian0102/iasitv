<?php
/**
 * @author Tripples http://www.themewinter.com
 * @copyright Copyright (c) 2013 - 2015
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or later
*/

defined('_JEXEC') or die;
?>

<!-- Showcase -->

<?php if ($this->checkSpotlight('main-bottom', 'main-bottom')) : ?>
	<!-- SPOTLIGHT 1 -->
	<div class="ts-main-bottom">
		<div class="container">
			<?php $this->spotlight('main-bottom', 'main-bottom') ?>
		</div>
	</div>
	<!-- //SPOTLIGHT 1 -->
<?php endif ?>

<!-- Showcase end -->