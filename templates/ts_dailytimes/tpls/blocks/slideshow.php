<?php
/**
 * @author Tripples http://www.themewinter.com
 * @copyright Copyright (c) 2013 - 2015
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or later
*/

defined('_JEXEC') or die;
?>

<!-- Slideshow -->

<?php if ($this->checkSpotlight('slideshow', 'slide1, slide2')) : ?>
	<div class="container">
		<div class="ts-slideshow">
			<?php $this->spotlight('slideshow', 'slide1, slide2') ?>
		</div>
	</div>
<?php endif ?>
<!-- Slideshow end -->