<?php
/**
 * @author Tripples http://www.themewinter.com
 * @copyright Copyright (c) 2013 - 2015
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or later
*/

defined('_JEXEC') or die;
?>

<!-- Footer Top -->
		<?php if ($this->countModules('footer-top')) { ?>	
	<div class="ts-footer-top">
			<jdoc:include type="modules" name="<?php $this->_p('footer-top') ?>" style="xhtml" />		
	</div>
		<?php } ?>

<!-- Footer Top end -->