<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// get params
$sitename  = $this->params->get('sitename');
$slogan    = $this->params->get('slogan', '');
$logotype  = $this->params->get('logotype', 'text');
$logoimage = $logotype == 'image' ? $this->params->get('logoimage', T3Path::getUrl('images/logo.png', '', true)) : '';
$logoimgsm = ($logotype == 'image' && $this->params->get('enable_logoimage_sm', 0)) ? $this->params->get('logoimage_sm', T3Path::getUrl('images/logo-sm.png', '', true)) : false;

if (!$sitename) {
	$sitename = JFactory::getConfig()->get('sitename');
}


?>

<!-- HEADER -->
<header id="t3-header" class="t3-header">
	<div class="container">
	<div class="row">

		<!-- Banner start -->
		<div class="col-md-3  col-xs-12 header-left clearfix">
	    	<div class="ts-subscribe">
		    	<?php if ($this->countModules('header-left')) { ?>	
		    	
		    	<div class="ts-date">
					<?php 
			        	echo JHtml::_('date', JFactory::getDate(), JText::_('DATE_FORMAT_LC'));
			       	?>
	    		</div>

					<jdoc:include type="modules" name="<?php $this->_p('header-left') ?>" style="xhtml" />		
				<?php } ?>
			</div>
		</div>
		<!-- Banner end -->

		<div class="col-md-5 col-xs-12">
			<!-- LOGO -->
			<div class="logo">
				<div class="logo-<?php echo $logotype, ($logoimgsm ? ' logo-control' : '') ?>">
					<a href="<?php echo JURI::base(true) ?>" title="<?php echo strip_tags($sitename) ?>">
						<?php if($logotype == 'image'): ?>
							<img class="logo-img" src="<?php echo JURI::base(true) . '/' . $logoimage ?>" alt="<?php echo strip_tags($sitename) ?>" />
						<?php endif ?>
						<?php if($logoimgsm) : ?>
							<img class="logo-img-sm" src="<?php echo JURI::base(true) . '/' . $logoimgsm ?>" alt="<?php echo strip_tags($sitename) ?>" />
						<?php endif ?>
						<span><?php echo $sitename ?></span>
					</a>
					<small class="site-slogan"><?php echo $slogan ?></small>
				</div>
			</div>
			<!-- //LOGO -->
		</div>

		<!-- Banner start -->
		<div class="col-md-4 col-xs-12 header-right clearfix">
			<?php if ($this->countModules('header-right')) { ?>	
				<jdoc:include type="modules" name="<?php $this->_p('header-right') ?>" style="xhtml" />		
			<?php } ?>
		</div>
		<!-- Banner end -->

	</div>
</div>
</header>
<!-- //HEADER -->
